package org.cen.persistance.user;

import java.io.IOException;

public interface IMemorySetting
{
	
	public Boolean load() throws IOException;
	
	public Boolean save() throws IOException;
	
	public String getMemorySetting(MemoryKey key);
	
	public void setSetting(MemoryKey key, String value);
	
}
