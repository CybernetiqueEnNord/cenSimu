package org.cen.persistance.user;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.cen.Main;
import org.cen.components.io.filepath.FileEmplacement;
import org.cen.components.io.map.IMapExportable;
import org.cen.components.io.map.IMapExporter;
import org.cen.components.io.map.IMapImporter;
import org.cen.components.io.map.MapExporter;
import org.cen.components.io.map.StringMapImporter;

public class MemorySetting implements IMemorySetting, IMapExportable
{
	private static final String MEMORY_FILE_NAME = "cenSimu.prefs";
	
	private Map<MemoryKey, String> memory = new HashMap<>();
	
	@Override
	public String getMemorySetting(MemoryKey key)
	{
		return memory.get(key);
	}
	
	@Override
	public Boolean load() throws IOException
	{
		File file = Main.app().getFilePathManager().getFile(FileEmplacement.USER_DIR, MEMORY_FILE_NAME);
		if(!file.exists()) return false;
		
		try(
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
		) {
			IMapImporter<String> importer = new StringMapImporter();
			Map<String, String> imported = importer.importFrom(br);
			for(Entry<String, String> entry:imported.entrySet())
			{
				MemoryKey key = null;
				if((key = MemoryKey.valueOf(entry.getKey())) != null)
				{
					memory.put(key, entry.getValue());
				}
			}
		}
		catch (ParseException e)
		{
			// should never happen with StringMapImporter
			e.printStackTrace();
		}
		return true;
	}
	
	@Override
	public Boolean save() throws IOException
	{
		File file = Main.app().getFilePathManager().getFile(FileEmplacement.USER_DIR, MEMORY_FILE_NAME);
		try(
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
		) {
			IMapExporter exporter = new MapExporter();
			exporter.export(this, bw);
		}
		return true;
	}
	
	@Override
	public Map<String, Serializable> getExportableValues()
	{
		Map<String, Serializable> map = new HashMap<>();
		for(Entry<MemoryKey, String> entry:memory.entrySet())
		{
			map.put(entry.getKey().name(), entry.getValue());
		}
		return map;
	}
	
	@Override
	public void setSetting(MemoryKey key, String value)
	{
		memory.put(key, value);
	}
	
}
