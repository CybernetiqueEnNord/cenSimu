package org.cen;

/**
 * Main class file. 
 * 
 * The purpose of this class is to be the unique referrer for any static
 * call, as every component of the application will be delegated to the 
 * application component. This class calls the initialization of the 
 * application, then runs it.
 * 
 * This class is intended to be the unique entry point of the runtime for
 * the whole package.
 * 
 * @author Emmanuel ZURMELY
 * @author Anastaszor <anastaszor@gmail.com>
 */
public class Main
{
	
	/**
	 * The singleton application.
	 */
	private static CenApplication simulator = null;
	
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		simulator = new CenApplication();
		simulator.init();
		simulator.getSimulatorMainFrame().setVisible(true);
	}
	
	/**
	 * Gets the singleton application component.
	 * 
	 * @return CenApplication the singleton application component.
	 */
	public static CenApplication app()
	{
		return simulator;
	}
	
}
