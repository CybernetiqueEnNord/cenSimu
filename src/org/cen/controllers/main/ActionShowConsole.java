package org.cen.controllers.main;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;

import org.cen.Main;

public class ActionShowConsole extends AbstractAction
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7332104324538600234L;
	
	private boolean visible = false;
	
	public ActionShowConsole()
	{
		super();
	}
	
	public ActionShowConsole(String name)
	{
		super(name);
	}
	
	public ActionShowConsole(String name, Icon icon)
	{
		super(name, icon);
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		visible = !visible;
		Main.app().getConsoleFrame().setVisible(visible);
		putValue(SELECTED_KEY, visible);
	}
	
}
