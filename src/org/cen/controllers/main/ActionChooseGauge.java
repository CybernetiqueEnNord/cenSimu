package org.cen.controllers.main;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.cen.Main;
import org.cen.components.gameboard.GameBoardMetadata;
import org.cen.components.io.file.InputFileType;
import org.cen.components.planification.RobotRole;

public class ActionChooseGauge extends AbstractAction
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private RobotRole role = null;
	
	public ActionChooseGauge(RobotRole role)
	{
		super("Choisir une Jauge");
		this.role = role;
	}
	
	@Override
	public void actionPerformed(ActionEvent evt)
	{
		GameBoardMetadata gbm = GameBoardMetadata.getMetadata(Main.app().getSimulatorMainFrame().getGameBoard());
		if(gbm == null)
		{
			try {
				throw new Exception("Impossible de trouver les metadonnées du gameboard "+Main.app().getSimulatorMainFrame().getGameBoard());
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e, "Une erreur a eu lieu", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
		JFileChooser fc = Main.app().getFilePathManager().getFileChooser(gbm, InputFileType.GAUGE);
		int returnVal = fc.showOpenDialog(null);
		File file = null;
		if(JFileChooser.APPROVE_OPTION == returnVal)
		{
			file = fc.getSelectedFile();
		}
		
		if(null == file) return;
		if(!file.exists()) return;
		if(!file.isFile()) return;
		
		Main.app().loadGaugeFromFile(file, this.role, true);
	}
	
}
