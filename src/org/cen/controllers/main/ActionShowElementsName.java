package org.cen.controllers.main;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;

import org.cen.Main;
import org.cen.views.frames.SimulatorMainFrame;

public class ActionShowElementsName extends AbstractAction
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1825470655335607432L;
	
	public ActionShowElementsName()
	{
		super();
	}
	
	public ActionShowElementsName(String name)
	{
		super(name);
	}
	
	public ActionShowElementsName(String name, Icon icon)
	{
		super(name, icon);
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		SimulatorMainFrame mainFrame = Main.app().getSimulatorMainFrame();
		boolean b = mainFrame.getGameBoardView().getDisplayLabels();
		mainFrame.getGameBoardView().setDisplayLabels(!b);
		mainFrame.updateGameBoard(false);
	}
	
}
