package org.cen.controllers.main;

import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.AbstractAction;

import org.cen.Main;

/**
 * ActionExit class file.
 * 
 * Main Frame action. This action terminates the program. This is similar than
 * clicking on the red cross on the top right corner of the window.
 * 
 * @author Anastaszor <anastaszor@gmail.com>
 */
public class ActionExit extends AbstractAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3702396653180915148L;
	
	public ActionExit(String string)
	{
		super(string);
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		try
		{
			Main.app().getMemorySettings().save();
		}
		catch (IOException e1)
		{
			// nothing to do, comfort feature
			e1.printStackTrace();
		}
		System.exit(0);
	}
	
}
