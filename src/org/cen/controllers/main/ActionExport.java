package org.cen.controllers.main;

import java.awt.event.ActionEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.cen.Main;
import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.io.export.order.OrderFunctionExporter;
import org.cen.components.planification.IPlanification;
import org.cen.components.planification.RobotRole;

public class ActionExport extends AbstractAction
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ActionExport()
	{
		super();
	}
	
	public ActionExport(String name)
	{
		super(name);
	}
	
	public ActionExport(String name, Icon icon)
	{
		super(name, icon);
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		IPlanification grandplan = Main.app().getPlanificationHolder().getValidPlanification(RobotRole.GRAND_ROBOT);
		if(null != grandplan)
		{
			JFileChooser fc = new JFileChooser();
			int returnVal = fc.showSaveDialog(null);
			File file = null;
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				file = fc.getSelectedFile();
			} else
				return;
			if (file == null)
				return;	
			
			if (file.exists()) {
				int result = JOptionPane.showConfirmDialog(null, 
					"The file " + file.getName() + " already exists. Would you like to overwrite it ?", 
					"Confirmation", JOptionPane.YES_NO_OPTION
				);
				if (result != JOptionPane.YES_OPTION) {
					return;
				}
			}
			try(
				FileWriter fw = new FileWriter(file);
				BufferedWriter bw = new BufferedWriter(fw);
			) {
				IOrderExporter exporter = new OrderFunctionExporter();
				String exported = exporter.export(grandplan.getTrajectory());
				bw.write(exported);
			}
			catch (Exception e1) 
			{
				e1.printStackTrace();
				JOptionPane.showMessageDialog(null, e1, "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		
		IPlanification petitplan = Main.app().getPlanificationHolder().getValidPlanification(RobotRole.PETIT_ROBOT);
		if(null != petitplan)
		{
			JFileChooser fc = new JFileChooser();
			int returnVal = fc.showSaveDialog(null);
			File file = null;
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				file = fc.getSelectedFile();
			} else
				return;
			if (file == null)
				return;	
			
			if (file.exists()) {
				int result = JOptionPane.showConfirmDialog(null, 
					"The file " + file.getName() + " already exists. Would you like to overwrite it ?", 
					"Confirmation", JOptionPane.YES_NO_OPTION
				);
				if (result != JOptionPane.YES_OPTION) {
					return;
				}
			}
			try(
				FileWriter fw = new FileWriter(file);
				BufferedWriter bw = new BufferedWriter(fw);
			) {
				IOrderExporter exporter = new OrderFunctionExporter();
				String exported = exporter.export(petitplan.getTrajectory());
				bw.write(exported);
			}
			catch (Exception e1) 
			{
				e1.printStackTrace();
				JOptionPane.showMessageDialog(null, e1, "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		
		if(null == grandplan && null == petitplan)
		{
			JOptionPane.showMessageDialog(null, "Aucune trajectoire n'est prête à être exportée", "Attention", JOptionPane.WARNING_MESSAGE);
		}
	}
	
}
