package org.cen.components.io.file;

public enum InputFileType
{
	TRAJECTORY("trajectories"), 
	GAUGE("gauges"),
	;
	
	private String dirname;
	
	private InputFileType(String directoryName)
	{
		this.dirname = directoryName;
	}
	
	public String getDirectoryName()
	{
		return dirname;
	}
	
}
