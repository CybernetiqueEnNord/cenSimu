package org.cen.components.io.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.cen.Main;
import org.cen.components.planification.RobotRole;

public class InputFileWatcher implements IInputFileWatcher
{
	
	private WatchService watcher = null;
	
	private Timer timer = null;
	
	private Map<RobotRole, File> gauges = new HashMap<>();
	private Map<RobotRole, File> trajectories = new HashMap<>();
	
	private Map<RobotRole, TimerTask> gaugesTasks = new HashMap<>();
	private Map<RobotRole, TimerTask> trajectoriesTasks = new HashMap<>();
	
	public InputFileWatcher()
	{
		timer = new Timer();
	}
	
	@Override
	public void watchGauge(final RobotRole role, final File file) throws IOException
	{
		File prec = gauges.put(role, file);
		if(null != prec)
		{
			gaugesTasks.remove(role);
		}
		final Path ppath = file.getParentFile().toPath();
		ppath.register(getWatcher(), StandardWatchEventKinds.ENTRY_MODIFY);
		
		TimerTask newtask = new TimerTask()
		{
			@Override
			public void run()
			{
				WatchKey key;
				try
				{
					key = getWatcher().poll();
					if(key == null) return;
					
					List<WatchEvent<?>> events = key.pollEvents();
					for(WatchEvent<?> event:events)
					{
						Kind<?> kind = event.kind();
						if(StandardWatchEventKinds.OVERFLOW == kind) continue;
						
						if(StandardWatchEventKinds.ENTRY_MODIFY == kind)
						{
							if(event.context().equals(ppath))
							{
								Main.app().loadGaugeFromFile(file, role, true);
							}
						}
					}
					
					key.reset();
				}
				catch (IOException e)
				{
					// nothing to do, abort watching
					e.printStackTrace();
				}
			}
		};
		
		TimerTask oldtask = gaugesTasks.put(role, newtask);
		if(null != oldtask)
			oldtask.cancel();
		timer.scheduleAtFixedRate(newtask, 1000, 1000);
	}
	
	@Override
	public void watchTrajectory(final RobotRole role, final File file) throws IOException
	{
		File prec = trajectories.put(role, file);
		if(null != prec)
		{
			trajectoriesTasks.remove(role);
		}
		final Path ppath = file.getParentFile().toPath();
		ppath.register(getWatcher(), StandardWatchEventKinds.ENTRY_MODIFY);
		
		TimerTask newtask = new TimerTask()
		{
			@Override
			public void run()
			{
				WatchKey key;
				try
				{
					key = getWatcher().poll();
					if(key == null) return;
					
					List<WatchEvent<?>> events = key.pollEvents();
					for(WatchEvent<?> event:events)
					{
						Kind<?> kind = event.kind();
						if(StandardWatchEventKinds.OVERFLOW == kind) continue;
						
						if(StandardWatchEventKinds.ENTRY_MODIFY == kind)
						{
							if(event.context().equals(ppath))
							{
								Main.app().loadTrajectoryFromFile(file, role, true);
							}
						}
					}
					
					key.reset();
				}
				catch (IOException e)
				{
					// nothing to do, abort watching
					e.printStackTrace();
				}
			}
		};
		
		TimerTask oldtask = trajectoriesTasks.put(role, newtask);
		if(null != oldtask)
			oldtask.cancel();
		timer.scheduleAtFixedRate(newtask, 1000, 1000);
	}
	
	protected WatchService getWatcher() throws IOException
	{
		if(null == watcher)
			watcher = FileSystems.getDefault().newWatchService();
		return watcher;
	}
	
}
