package org.cen.components.io.file;

import java.io.File;
import java.io.IOException;

import org.cen.components.planification.RobotRole;

public interface IInputFileWatcher
{
	
	public void watchGauge(RobotRole role, File file) throws IOException;
	
	public void watchTrajectory(RobotRole role, File file) throws IOException;
	
}
