package org.cen.components.io.export.order;

import org.cen.components.order.BezierQuadOrder;
import org.cen.components.order.ChangeSpeedModeOrder;
import org.cen.components.order.CommentOrder;
import org.cen.components.order.DirectCommandOrder;
import org.cen.components.order.DistanceOrder;
import org.cen.components.order.EnvoyerOrdreNxtOrder;
import org.cen.components.order.IOrderList;
import org.cen.components.order.InitialPositionOrder;
import org.cen.components.order.RecalageOrder;
import org.cen.components.order.RotationOrder;
import org.cen.components.order.TransformOrder;
import org.cen.components.order.WaitOrder;

public interface IOrderExporter
{
	public String export(IOrderList orderList) throws ExportException;
	
	public String exportBezierQuadOrder(BezierQuadOrder order) throws ExportException;
	
	public String exportCommentOrder(CommentOrder order) throws ExportException;
	
	public String exportDistanceOrder(DistanceOrder order) throws ExportException;
	
	public String exportInitialPositionOrder(InitialPositionOrder order) throws ExportException;
	
	public String exportRotationOrder(RotationOrder order) throws ExportException;
	
	public String exportTransformOrder(TransformOrder order) throws ExportException;
	
	public String exportWaitOrder(WaitOrder order) throws ExportException;
	
	public String exportChangeSpeedModeOrder(ChangeSpeedModeOrder changeSpeedModeOrder) throws ExportException;
	
	public String exportEnvoyerOrdreNxtOrder(EnvoyerOrdreNxtOrder engoyerOrdreNxtOrder) throws ExportException;
	
	public String exportDirectCommandOrder(DirectCommandOrder directCommandOrder) throws ExportException;
	
	public String exportRecalageOrder(RecalageOrder recalageOrder) throws ExportException;
	
}
