package org.cen.components.io.export.order;

import java.util.ArrayList;
import java.util.List;

import org.cen.components.cfunc.CArgumentMap;
import org.cen.components.cfunc.CArgumentValue;
import org.cen.components.cfunc.exceptions.CArgumentNotFoundException;
import org.cen.components.cfunc.exceptions.CInacceptableFormatException;
import org.cen.components.cfunc.exceptions.CLowerOverflowException;
import org.cen.components.cfunc.exceptions.CMissingArgumentException;
import org.cen.components.cfunc.exceptions.CUpperOverflowException;
import org.cen.components.cfunc.interfaces.ICArgumentMap;
import org.cen.components.cfunc.interfaces.ICFunction;
import org.cen.components.cfunc.specific.AttendreTirette;
import org.cen.components.cfunc.specific.AvancerMm;
import org.cen.components.cfunc.specific.ChangeSpeedMode;
import org.cen.components.cfunc.specific.DelayMs;
import org.cen.components.cfunc.specific.EnvoyerOrdreNxt;
import org.cen.components.cfunc.specific.RecalageMm;
import org.cen.components.cfunc.specific.TournerDeciDegree;
import org.cen.components.order.BezierQuadOrder;
import org.cen.components.order.ChangeSpeedModeOrder;
import org.cen.components.order.CommentOrder;
import org.cen.components.order.DirectCommandOrder;
import org.cen.components.order.DistanceOrder;
import org.cen.components.order.EnvoyerOrdreNxtOrder;
import org.cen.components.order.IOrder;
import org.cen.components.order.IOrderList;
import org.cen.components.order.InitialPositionOrder;
import org.cen.components.order.RecalageOrder;
import org.cen.components.order.RotationOrder;
import org.cen.components.order.TransformOrder;
import org.cen.components.order.WaitOrder;

public class OrderFunctionExporter implements IOrderExporter
{
	
	@Override
	public String export(IOrderList orderList) throws ExportException
	{
		StringBuilder sb = new StringBuilder();
		for(IOrder order:orderList)
		{
			sb.append(order.exportTo(this));
			sb.append("\n");
		}
		return sb.toString();
	}
	
	@Override
	public String exportBezierQuadOrder(BezierQuadOrder order)
	{
		return "// bezier quad export in "+getClass()+" not implemented";
	}
	
	@Override
	public String exportCommentOrder(CommentOrder order)
	{
		return "// " + order.getLine().replaceFirst("//\\s?", "");
	}
	
	@Override
	public String exportDistanceOrder(DistanceOrder order) throws ExportException
	{
		StringBuilder sb = new StringBuilder();
		sb.append("// Avancer de ");
		sb.append(order.getDistance().toCentimeters());
		sb.append("\n");
		
		boolean cannotpass = false;
		ICFunction function = new AvancerMm();
		List<ICArgumentMap> argmaplist = new ArrayList<>();
		double currentLengthToTravel = order.getDistance().toMillimeters().getValue();
		do
		{
			cannotpass = false;
			ICArgumentMap argmap = new CArgumentMap();
			argmap.add(new CArgumentValue(
				AvancerMm.ARG1NAME, 
				Math.round(currentLengthToTravel)
			));
			try
			{
				function.validate(argmap);
				argmaplist.add(argmap);
			}
			catch(CUpperOverflowException e)
			{
				argmap = new CArgumentMap();
				argmap.add(new CArgumentValue(AvancerMm.ARG1NAME, e.getMaxValue()));
				argmaplist.add(argmap);
				currentLengthToTravel -= e.getMaxValue();
				cannotpass = true;
			}
			catch(CLowerOverflowException e)
			{
				argmap = new CArgumentMap();
				argmap.add(new CArgumentValue(AvancerMm.ARG1NAME, e.getMinValue()));
				argmaplist.add(argmap);
				currentLengthToTravel += e.getMinValue();
				cannotpass = true;
			}
			catch(CInacceptableFormatException
				| CArgumentNotFoundException | CMissingArgumentException e
			) {
				throw new ExportException(order, e);
			}
		}
		while(cannotpass);
		
		for(ICArgumentMap argmap:argmaplist)
		{
			sb.append(function.render(argmap));
		}
		
		return sb.toString();
	}
	
	@Override
	public String exportInitialPositionOrder(InitialPositionOrder order)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("// Partir de ");
		sb.append(order.getPosition().toMillimeters());
		sb.append("\n");
		
		ICFunction function = new AttendreTirette();
		sb.append(function.render(new CArgumentMap()));
		
		return sb.toString();
	}
	
	@Override
	public String exportRotationOrder(RotationOrder order) throws ExportException
	{
		StringBuilder sb = new StringBuilder();
		sb.append("// Tourner de ");
		sb.append(order.getRotationAngle().toDegrees());
		sb.append("\n");
		
		boolean cannotpass = false;
		ICFunction function = new TournerDeciDegree();
		List<ICArgumentMap> argmaplist = new ArrayList<>();
		double currentAngleToRotate = order.getRotationAngle().toDecidegrees().getValue();
		do
		{
			cannotpass = false;
			ICArgumentMap argmap = new CArgumentMap();
			argmap.add(new CArgumentValue(
				TournerDeciDegree.ARG1NAME, 
				Math.round(currentAngleToRotate)
			));
			try
			{
				function.validate(argmap);
				argmaplist.add(argmap);
			}
			catch (CUpperOverflowException e)
			{
				argmap = new CArgumentMap();
				argmap.add(new CArgumentValue(TournerDeciDegree.ARG1NAME, e.getMaxValue()));
				argmaplist.add(argmap);
				currentAngleToRotate -= e.getMaxValue();
				cannotpass = true;
			}
			catch (CLowerOverflowException e)
			{
				argmap = new CArgumentMap();
				argmap.add(new CArgumentValue(TournerDeciDegree.ARG1NAME, e.getMinValue()));
				argmaplist.add(argmap);
				currentAngleToRotate += e.getMinValue();
				cannotpass = true;
			}
			catch (CInacceptableFormatException | CArgumentNotFoundException
				| CMissingArgumentException e
			) {
				throw new ExportException(order, e);
			}
		}
		while(cannotpass);
		
		for(ICArgumentMap argmap:argmaplist)
		{
			sb.append(function.render(argmap));
		}
		
		return sb.toString();
	}
	
	@Override
	public String exportTransformOrder(TransformOrder order)
	{
		return "// transform export in "+getClass()+" not implemented";
	}
	
	@Override
	public String exportWaitOrder(WaitOrder order) throws ExportException
	{
		StringBuilder sb = new StringBuilder();
		sb.append("// Attendre ");
		sb.append(order.getDelay().toMilliseconds());
		sb.append("\n");
		
		boolean cannotpass = false;
		ICFunction function = new DelayMs();
		List<ICArgumentMap> argmaplist = new ArrayList<>();
		double currentTimeToRest = order.getDelay().toMilliseconds().getValue();
		do
		{
			cannotpass = false;
			ICArgumentMap argmap = new CArgumentMap();
			argmap.add(new CArgumentValue(
				DelayMs.ARG1NAME, 
				Math.round(currentTimeToRest)
			));
			try
			{
				function.validate(argmap);
				argmaplist.add(argmap);
			}
			catch(CUpperOverflowException e)
			{
				argmap = new CArgumentMap();
				argmap.add(new CArgumentValue(DelayMs.ARG1NAME, e.getMaxValue()));
				argmaplist.add(argmap);
				currentTimeToRest -= e.getMaxValue();
				cannotpass = true;
			}
			catch(CLowerOverflowException e)
			{
				argmap = new CArgumentMap();
				argmap.add(new CArgumentValue(DelayMs.ARG1NAME, e.getMinValue()));
				argmaplist.add(argmap);
				currentTimeToRest += e.getMinValue();
				cannotpass = true;
			}
			catch(CInacceptableFormatException
				| CArgumentNotFoundException | CMissingArgumentException e)
			{
				e.printStackTrace();
				throw new ExportException(order, e);
			}
		}
		while(cannotpass);
		
		for(ICArgumentMap argmap:argmaplist)
		{
			sb.append(function.render(argmap));
		}
		
		return sb.toString();
	}
	
	@Override
	public String exportChangeSpeedModeOrder(ChangeSpeedModeOrder order)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("// change speed mode to ");
		sb.append(order.getMode());
		sb.append("\n");
		
		ICFunction function = new ChangeSpeedMode();
		ICArgumentMap argmap = new CArgumentMap();
		argmap.add(new CArgumentValue(
			ChangeSpeedMode.ARG1NAME, Long.valueOf(order.getMode().getValue())
		));
		
		sb.append(function.render(argmap));
		
		return sb.toString();
	}

	@Override
	public String exportEnvoyerOrdreNxtOrder(EnvoyerOrdreNxtOrder order)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("// envoyer ordre nxt ");
		sb.append(order.getOrdre());
		sb.append("\n");
		
		ICFunction function = new EnvoyerOrdreNxt();
		ICArgumentMap argmap = new CArgumentMap();
		argmap.add(new CArgumentValue(
			EnvoyerOrdreNxt.ARG1NAME, Long.valueOf(order.getOrdre().getOrdreNumber())
		));
		
		sb.append(function.render(argmap));
		
		return sb.toString();
	}
	
	@Override
	public String exportDirectCommandOrder(DirectCommandOrder directCommandOrder)
	{
		return directCommandOrder.getCommandCline();
	}
	
	@Override
	public String exportRecalageOrder(RecalageOrder order) throws ExportException
	{
		StringBuilder sb = new StringBuilder();
		sb.append("// Recalage\n");
		
		ICFunction function = new RecalageMm();
		ICArgumentMap argmap = new CArgumentMap();
		argmap.add(new CArgumentValue(
			RecalageMm.ARG1NAME, Math.round(order.getDistance().toMillimeters().getValue())
		));
		
		sb.append(function.render(argmap));
		
		return sb.toString();
	}
	
}
