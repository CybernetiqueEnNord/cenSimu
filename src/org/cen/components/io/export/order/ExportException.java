package org.cen.components.io.export.order;

import org.cen.components.cfunc.exceptions.CValidationException;
import org.cen.components.order.DistanceOrder;
import org.cen.components.order.RotationOrder;
import org.cen.components.order.WaitOrder;

public class ExportException extends Exception
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ExportException(RotationOrder order, CValidationException previous)
	{
		super(previous);
	}
	
	public ExportException(DistanceOrder order, CValidationException e)
	{
		// TODO Auto-generated constructor stub
	}
	
	public ExportException(WaitOrder order, CValidationException e)
	{
		// TODO Auto-generated constructor stub
	}
	
}
