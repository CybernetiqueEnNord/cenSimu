package org.cen.components.io;

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Scanner;

import org.cen.components.io.trajectory.ITrajectoryParser;
import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.distance.Distance2DMillimeter;
import org.cen.components.math.distance.IDistance2D;
import org.cen.components.math.position.Position2DMillimeter;
import org.cen.components.math.time.DelayMillisecond;
import org.cen.components.nxt.IOrdreNxt;
import org.cen.components.nxt.OrdreNxt2016;
import org.cen.components.order.BezierQuadOrder;
import org.cen.components.order.ChangeSpeedModeOrder;
import org.cen.components.order.CommentOrder;
import org.cen.components.order.DirectCommandOrder;
import org.cen.components.order.DistanceOrder;
import org.cen.components.order.EnvoyerOrdreNxtOrder;
import org.cen.components.order.IOrderList;
import org.cen.components.order.InitialPositionOrder;
import org.cen.components.order.OrderList;
import org.cen.components.order.RecalageOrder;
import org.cen.components.order.RotationOrder;
import org.cen.components.order.TransformOrder;
import org.cen.components.order.WaitOrder;
import org.cen.components.speed.SpeedMode;

/**
 * 
 * @deprecated
 */
public class XYParser implements ITrajectoryParser 
{
	private class CommonData {
		double additionalTime = 0;
//		double linearSpeed = defaultLinearSpeed;
//		double rotationSpeed = defaultRotationSpeed;
	}

	private static final int X_MAX_TABLE = 2000;
	private static final int Y_MAX_TABLE = 3000;
	
	private double angleScale = 1;
	private double angleTranslation = 0;
	private double defaultLinearSpeed = 0.1;
	private double defaultRotationSpeed = 0.1;
	private String delimiter;
	
	private IAngle initialAngle = new AngleRadian(0d);
	
	private double lastx = 0;
	private double lasty = 0;
	private int sourceLine = 0;
	private double xScale = 1;
	private double xTranslation = 0;
	private double yScale = 1;
	private double yTranslation = 0;

	public XYParser() {
		this(";");
	}

	public XYParser(String delimiter) {
		super();
		this.delimiter = delimiter;
	}
	
	@Override
	public IOrderList parseStream(BufferedReader source) throws IOException, ParseException
	{
		IOrderList list = new OrderList();
		String line;
		while((line = source.readLine()) != null)
		{
			IOrderList inner = this.parseLine(line);
			if(inner != null)
			{
				list.append(inner);
			}
		}
		return list;
	}
	
//	public ITrajectoryPath getPath(String name) {
//		ITrajectoryPath path = new StraightLine(name, initialAngle, finalAngle, frames);
//		return path;
//	}

//	private double getRotationDuration(double angle, double speed) {
//		double duration = angle / (2.0 * Math.PI) / speed;
//		return duration;
//	}

	private IOrderList handleXYCoordinates(Point2D p, CommonData data) throws ParseException {
		IOrderList ol = new OrderList();
//		double dx = p.getX() - lastx;
//		double dy = p.getY() - lasty;
//		double angle = Math.atan2(dy, dx);

		// if linear speed is negative, move backward
//		if (data.linearSpeed < 0) {
//			angle += Math.PI;
//		}

		// initial position
//		if (frames.isEmpty()) {
//			throw new ParseException("initial position has not been defined", 0);
//		}

		double distance = p.distance(lastx, lasty);
		if (distance == 0) {
			return ol;
		}
//		double theta = Math.abs(lastAngle.getRotationAngle(new AngleRadian(angle)).getValue());
//		double theta = Math.abs(Angle.getRotationAngle(lastAngle, angle));

		// rotation
//		if (theta > MIN_ANGLE && distance > MIN_DISTANCE) {
////			timestamp += getRotationDuration(theta, data.rotationSpeed);
////			KeyFrame frame = new KeyFrame(new TrajectoryForcedRotation(null), 0, new AngleRadian(angle), 0, data.rotationSpeed, new Position2DMillimeter(lastx, lasty), timestamp, sourceLine);
////			frames.add(frame);
//			ol.add(new RotationOrder(new AngleRadian(angle), RotationWay.eval(angle), sourceLine));
//		}

		// straight line
//		timestamp += distance / Math.abs(data.linearSpeed);
//		KeyFrame frame = new KeyFrame(new TrajectorySegment(null), data.linearSpeed, new AngleRadian(angle), 0, 0, new Position2DMillimeter(p), timestamp, sourceLine);
//		frames.add(frame);
		ol.add(new DistanceOrder(new Distance2DMillimeter(distance), sourceLine));

		// pause
		ol.add(new WaitOrder(new DelayMillisecond(data.additionalTime), sourceLine));

		lastx = p.getX();
		lasty = p.getY();
//		lastAngle = new AngleRadian(angle);
		return ol;
	}

	private IOrderList parseBezier(Scanner s)
	{
		IOrderList ol = new OrderList();
		// final position
		Point2D p = readPoint(s);
		// control point 1
		Point2D cp1 = readPoint(s);
		// control point 2
		Point2D cp2 = readPoint(s);
//		CommonData data = parseCommon(s);
		
		// start angle
		double dx = cp1.getX() - lastx;
		double dy = cp1.getY() - lasty;
//		double startAngle = Math.atan2(dy, dx);
		
		// end angle
		dx = p.getX() - cp2.getX();
		dy = p.getY() - cp2.getY();
		double endAngle = Math.atan2(dy, dx);
		
		// initial rotation
//		double theta = Math.abs(lastAngle.getRotationAngle(new AngleRadian(startAngle)).getValue());
//		double theta = Math.abs(Angle.getRotationAngle(lastAngle, startAngle));
//		if (theta > MIN_ANGLE) {
////			timestamp += getRotationDuration(theta, data.rotationSpeed);
////			Point2D last = new Point2D.Double(lastx, lasty);
////			KeyFrame frame = new KeyFrame(new TrajectoryForcedRotation(null), 0, new AngleRadian(startAngle), 0, data.rotationSpeed, new Position2DMillimeter(last), timestamp, sourceLine);
////			frames.add(frame);
//			ol.add(new RotationOrder(new AngleRadian(startAngle), RotationWay.eval(startAngle), sourceLine));
//		}
		
//		double distante = p.distance(lastx, lasty);
//		timestamp += distante / data.linearSpeed;
//		KeyFrame frame = new KeyFrame(new TrajectoryBezierQuad(null), data.linearSpeed, new AngleRadian(endAngle), 0, data.rotationSpeed, new Position2DMillimeter(p), timestamp, sourceLine, cp1, cp2);
//		frames.add(frame);
		ol.add(new BezierQuadOrder(
			new AngleRadian(endAngle), 
			new Position2DMillimeter(cp1), 
			new Position2DMillimeter(cp2)
		));
		
		return ol;
	}

	private IOrderList parseComment(String line) {
		IOrderList ol = new OrderList();
		ol.add(new CommentOrder(line));
		return ol;
//		int index = frames.size() - 1;
//		if (index < 0) {
//			return;
//		}
//		KeyFrame frame = frames.get(index);
//		line = line.substring(2);
//		frame.addComment(line);
//		parseCommentContent(line, frame);
	}

	private CommonData parseCommon(Scanner s) {
		CommonData data = new CommonData();
		if (s.hasNextDouble()) {
//			data.linearSpeed = s.nextDouble();
			s.nextDouble();
		}
		if (s.hasNextDouble()) {
//			data.rotationSpeed = s.nextDouble();
			s.nextDouble();
		}
		if (s.hasNextDouble()) {
			data.additionalTime = s.nextDouble();
		}
		return data;
	}

	private IOrderList parseInitialPosition(Scanner s) throws ParseException {
//		if (!frames.isEmpty()) {
//			throw new ParseException("the initial position must be the first entry", 0);
//		}

		Point2D p = readPoint(s);
		initialAngle = readAngle(s);
		defaultLinearSpeed = s.nextDouble();
		defaultRotationSpeed = s.nextDouble();
//		timestamp = s.nextDouble();
		double additionalTime = 0;
		if (s.hasNextDouble()) {
			additionalTime = s.nextDouble();
		}
		
//		KeyFrame frame = new KeyFrame(new TrajectoryStart(), 0, initialAngle, 0, 0, new Position2DMillimeter(p), timestamp, sourceLine);
//		frames.add(frame);
//		
//		lastx = x;
//		lasty = y;
//		lastAngle = initialAngle;
		
		IOrderList ol = new OrderList();
		ol.add(new InitialPositionOrder(new Position2DMillimeter(p), initialAngle, defaultLinearSpeed, defaultRotationSpeed, 0, sourceLine));
		if(additionalTime > 0)
		{
			ol.add(new WaitOrder(new DelayMillisecond(additionalTime), sourceLine));
		}
		return ol;
	}
	
	public IOrderList parseLine(String line) throws ParseException
	{
		System.out.println(line);
		sourceLine++;
		try(Scanner s = new Scanner(line)) {
			s.useDelimiter(delimiter);
			
			if (!s.hasNext()) {
				return null;
			}
			
			String type = s.next();
			char c = type.charAt(0);
			switch (c) {
			case '/':
			case '#':
			case 'c':
				return parseComment(line);
			case '$':
				return parseDirectComment(line);
			case 'b':
				return parseBezier(s);
			case 'd':
				return parseStraightDistance(s);
			case 'l':
				return parseRecalge(s);
			case 'i':
				return parseInitialPosition(s);
			case 'o':
				return parseOrientation(s);
			case 'p':
				return parsePause(s);
			case 'r':
				return parseRotation(s);
			case 's':
				return parseSegment(s);
			case 't':
				return parseTransform(s);
			case 'v':
				return parseChangeSpeedMode(s);
			case 'x':
				return parseOrdreNxt(s);
			case 'z':
				return parseOrientedPositionSegment(s);
			default:
				throw new ParseException("unexpected value: " + type, 0);
			}
		}
	}
	
	private IOrderList parseOrientedPositionSegment(Scanner s)
	{
		int x = s.nextInt();
		int y = s.nextInt();
//		double th = s.nextDouble();
		OrderList ol = new OrderList();
		if(x < 0 && x > X_MAX_TABLE)
		{
			throw new IllegalArgumentException("Le point n'est pas dans la table en x");
		}
		if(y < 0 && y > Y_MAX_TABLE)
		{
			throw new IllegalArgumentException("Le point n'est pas dans la table en y");
		}
		
		// TODO
		
		return ol;
	}
	
	private IOrderList parseRecalge(Scanner s)
	{
		int d = s.nextInt();
		if(d > 0)
		{
			OrderList ol = new OrderList();
			ol.add(new RecalageOrder(new Distance2DMillimeter(Double.valueOf(d))));
			return ol;
		}
		return null;
	}

	private IOrderList parseDirectComment(String line)
	{
		OrderList ol = new OrderList();
		ol.add(new DirectCommandOrder(line.substring(2)));
		return ol;
	}
	
	private IOrderList parseOrdreNxt(Scanner s) throws ParseException
	{
		int ordre = s.nextInt();
		IOrdreNxt finalOrder = null;
		for(IOrdreNxt ordre1:OrdreNxt2016.values())
		{
			if(ordre1.getOrdreNumber().equals(ordre))
			{
				finalOrder = ordre1;
			}
		}
		
		if(finalOrder == null)
		{
			throw new ParseException("Impossible de trouver l'ordre "+ordre+" dans la liste des valeurs.", sourceLine);
		}
		
		OrderList ol = new OrderList();
		ol.add(new EnvoyerOrdreNxtOrder(finalOrder));
		return ol;
	}

	private IOrderList parseChangeSpeedMode(Scanner s) throws ParseException
	{
		int mode = s.nextInt();
		if(mode > 0)
		{
			for(SpeedMode sp:SpeedMode.values())
			{
				if(sp.getValue().equals(mode))
				{
					OrderList ol = new OrderList();
					ol.add(new ChangeSpeedModeOrder(sp));
					return ol;
				}
			}
		}
		throw new ParseException("Impossible to detect speed mode "+mode, sourceLine);
	}
	
	private IOrderList parseOrientation(Scanner s) {
//		IAngle finalAngle = readAngle(s);
		double minAngle = Math.toRadians(s.nextDouble());
//		double signum = Math.signum(minAngle);
//		double theta = lastAngle.getRotationAngle(finalAngle).getValue();
//		double theta = Angle.getRotationAngle(lastAngle, finalAngle);
//		if (Math.abs(theta) < Math.abs(minAngle)) {
//			theta += AngleRadian._2PI * signum;
//		}
//		if (Math.signum(theta) != signum) {
//			theta += 2 * AngleRadian._2PI * signum;
//		}

//		double opposite = new AngleRadian(theta).opposite().getValue();
//		double opposite = Angle.getRotationAngle(-lastAngle, -finalAngle);
//		if (Math.abs(opposite) < Math.abs(minAngle)) {
//			opposite += AngleRadian._2PI * signum;
//		}
//		if (Math.signum(opposite) != signum) {
//			opposite += 2 * AngleRadian._2PI * signum;
//		}
		
		CommonData data = parseCommon(s);
		
//		double angle = lastAngle.getValue() + theta;
		
//		Point2D p = new Point2D.Double(lastx, lasty);
//		timestamp += getRotationDuration(Math.abs(theta), data.rotationSpeed);
//		KeyFrame frame = new KeyFrame(new TrajectoryForcedRotation(null), 0, new AngleRadian(angle), theta, data.rotationSpeed, new Position2DMillimeter(p), timestamp, sourceLine);
//		NumberFormat format = NumberFormat.getNumberInstance(Locale.ROOT);
//		frame.addComment(String.format("%s=%s", KEY_ORIENTATION, format.format(opposite)));
//		frames.add(frame);
		IOrderList ol = new OrderList();
		ol.add(new RotationOrder(new AngleRadian(minAngle), sourceLine));
		ol.add(new WaitOrder(new DelayMillisecond(data.additionalTime), sourceLine));
		
//		lastAngle = new AngleRadian(angle);// % Angle.PIx2;
		return ol;
	}

	private IOrderList parsePause(Scanner s) {
		double duration = s.nextDouble();
		if(duration > 0)
		{
			OrderList ol = new OrderList();
			ol.add(new WaitOrder(new DelayMillisecond(duration), sourceLine));
			return ol;
		}
		
		return null;
//		Point2D p = new Double(lastx, lasty);
//		timestamp += duration;
//		KeyFrame frame = new KeyFrame(new TrajectoryDelay(null, 0d), 0, lastAngle, 0, 0, new Position2DMillimeter(p), timestamp, sourceLine);
//		frame.addComment(String.format("%s=%.0f", KEY_PAUSE, duration * 1000));
//		frames.add(frame);
	}

	private IOrderList parseRotation(Scanner s) {
		double initial = s.nextDouble();
		double theta = Math.toRadians(initial) * angleScale;
		CommonData data = parseCommon(s);
		
//		double angle = lastAngle.getValue() + theta;
		
//		KeyFrame frame = new KeyFrame(new TrajectoryForcedRotation(null), 0, new AngleRadian(angle), theta, data.rotationSpeed, new Position2DMillimeter(p), timestamp, sourceLine);
//		frames.add(frame);
		OrderList ol = new OrderList();
		ol.add(new RotationOrder(new AngleRadian(theta), sourceLine));
		if(data.additionalTime > 0)
		{
			ol.add(new WaitOrder(new DelayMillisecond(data.additionalTime), sourceLine));
		}
		
//		lastAngle = new AngleRadian(angle);// % Angle.PIx2;
		return ol;
	}

	private IOrderList parseSegment(Scanner s) throws ParseException {
		Point2D p = readPoint(s);
		CommonData data = parseCommon(s);
		
		return handleXYCoordinates(p, data);
	}

	private IOrderList parseStraightDistance(Scanner s) throws ParseException {
		double distance = s.nextDouble();
		IDistance2D d2d = new Distance2DMillimeter(distance);
//		CommonData data = parseCommon(s);
		
		OrderList ol = new OrderList();
		ol.add(new DistanceOrder(d2d, sourceLine));
		return ol;
//		
//		double angle = lastAngle.getValue();
//		double x = lastx + distance * Math.cos(angle);
//		double y = lasty + distance * Math.sin(angle);
//		
//		data.linearSpeed = Math.copySign(data.linearSpeed, Math.signum(distance));
//		
//		return handleXYCoordinates(new Point2D.Double(x, y), data);
	}

	private IOrderList parseTransform(Scanner s) {
		xScale = s.nextDouble();
		xTranslation = s.nextDouble();
		yScale = s.nextDouble();
		yTranslation = s.nextDouble();
		angleScale = s.nextDouble();
		angleTranslation = Math.toRadians(s.nextDouble());
		OrderList ol = new OrderList();
		ol.add(new TransformOrder(xScale, xTranslation, yScale, yTranslation, angleScale, angleTranslation));
		return ol;
	}
	
	private IAngle readAngle(Scanner s) {
		double angle = Math.toRadians(s.nextDouble());
		angle = transformCoordinate(angle, angleScale, angleTranslation);
		return new AngleRadian(angle);
	}
	
	private Point2D readPoint(Scanner s)
	{
		return new Point2D.Double(readX(s), readY(s));
	}
	
	private double readX(Scanner s) {
		return readCoordinate(s, xScale, xTranslation);
	}
	
	private double readY(Scanner s) {
		return readCoordinate(s, yScale, yTranslation);
	}
	
	private double readCoordinate(Scanner s, double scale, double translation) {
		double c = s.nextDouble();
		c = transformCoordinate(c, scale, translation);
		return c;
	}
	
	private double transformCoordinate(double value, double scale, double translation) {
		value *= scale;
		value += translation;
		return value;
	}
	
//	public String toString()
//	{
//		StringBuilder str = new StringBuilder("{");
//		str.append("angleScale=\"").append(angleScale).append("\"");
//		str.append(" angleTranslation=\"").append(angleTranslation).append("\"");
//		str.append(" defaultLinearSpeed=\"").append(defaultLinearSpeed).append("\"");
//		str.append(" defaultRotationSpeed=\"").append(defaultRotationSpeed).append("\"");
//		str.append(" delimiter=\"").append(delimiter).append("\"");
//		str.append(" finalAngle=\"").append(finalAngle).append("\"");
//		str.append(" initialAngle=\"").append(initialAngle).append("\"");
//		str.append(" lastAngle=\"").append(lastAngle).append("\"");
//		str.append(" lastx=\"").append(lastx).append("\"");
//		str.append(" lasty=\"").append(lasty).append("\"");
//		str.append(" sourceLine=\"").append(sourceLine).append("\"");
//		str.append(" timestamp=\"").append(timestamp).append("\"");
//		str.append(" xScale=\"").append(xScale).append("\"");
//		str.append(" xTranslation=\"").append(xTranslation).append("\"");
//		str.append(" yScale=\"").append(yScale).append("\"");
//		str.append(" yTranslation=\"").append(yTranslation).append("\"");
//		str.append(" frames=(").append(frames.size()).append(")[");
//		for(KeyFrame kf:frames)
//		{
//			str.append(kf.toString()).append(",");
//		}
//		str.append("]");
//		str.append("}");
//		return str.toString();
//	}
	
}
