package org.cen.components.io.trajectory;

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cen.components.math.angle.AngleDegree;
import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.distance.Distance2DMillimeter;
import org.cen.components.math.distance.IDistance2D;
import org.cen.components.math.position.Position2DMillimeter;
import org.cen.components.math.time.DelayMillisecond;
import org.cen.components.nxt.IOrdreNxt;
import org.cen.components.nxt.OrdreNxt_2017;
import org.cen.components.order.BezierQuadOrder;
import org.cen.components.order.ChangeSpeedModeOrder;
import org.cen.components.order.CommentOrder;
import org.cen.components.order.DirectCommandOrder;
import org.cen.components.order.DistanceOrder;
import org.cen.components.order.EnvoyerOrdreNxtOrder;
import org.cen.components.order.IOrderList;
import org.cen.components.order.InitialPositionOrder;
import org.cen.components.order.OrderList;
import org.cen.components.order.OrderMetadata;
import org.cen.components.order.RecalageOrder;
import org.cen.components.order.RotationOrder;
import org.cen.components.order.TargetOrientationOrder;
import org.cen.components.order.TargetPositionOrder;
import org.cen.components.order.WaitOrder;
import org.cen.components.speed.SpeedMode;

public class TrajectoryParser implements ITrajectoryParser
{
	
	private Integer currentLine = -1;
	
	private String integerPattern = "(-?\\d+)";
	private String doublePattern = "(-?\\d+\\.?\\d+?)";
	private String pointPattern = doublePattern+";"+doublePattern;
	
	@Override
	public IOrderList parseStream(BufferedReader source) throws IOException, ParseException
	{
		IOrderList list = new OrderList();
		String line;
		while((line = source.readLine()) != null)
		{
			IOrderList inner = this.parseLine(line);
			if(inner != null)
			{
				list.append(inner);
			}
		}
		return list;
	}
	
	public IOrderList parseLine(String line) throws ParseException
	{
		currentLine++;
		String newLine = line.trim();
		if(newLine.length() == 0) return null;
		
		OrderMetadata om = getMetadata(newLine);
		
		switch (om.getCommandOrder())
		{
			case "/":
			case "//":
			case "#":
			case "c":
				return parseComment(om);
			case "$":
				return parseDirectComment(om);
			case "b":
				return parseBezier(om);
			case "d":
				return parseStraightDistance(om);
			case "l":
				return parseRecalage(om);
			case "i":
				return parseInitialPosition(om);
			case "o":
				return parseOrientation(om);
			case "p":
				return parsePause(om);
			case "r":
			case "rot":
				return parseRotation(om);
			case "s":
				return parseSegment(om);
			case "v":
			case "spd":
				return parseChangeSpeedMode(om);
			case "x":
			case "nxt":
				return parseOrdreNxt(om);
			case "":
				return new OrderList();
			default:
				// nothing to do
		}
		
		throw new ParseException("Unrecognized command line: " + line, currentLine);
	}
	
	private OrderMetadata getMetadata(String line) throws ParseException
	{
		OrderMetadata om = new OrderMetadata();
		om.setLine(currentLine);
		
		if(line.length() > 2 && line.substring(0, 2).equals("//"))
		{
			om.setCommandOrder("//");
			om.setCommandValue(line.substring(2).trim());
			return om;
		}
		
		if(line.contains(":"))
		{
			String[] parts = line.split(":", 2);
			om.setCommandOrder(parts[0]);
			om.setCommandValue(parts[1]);
			return om;
		}
		
		if(line.contains(";"))
		{
			String[] parts = line.split(";", 2);
			om.setCommandOrder(parts[0]);
			om.setCommandValue(parts[1]);
			return om;
		}
		
		return om;
	}
	
	private IOrderList parseOrdreNxt(OrderMetadata om) throws ParseException
	{
		Pattern ip = Pattern.compile(integerPattern);
		Matcher m = ip.matcher(om.getCommandValue());
		if(!m.find())
			throw new ParseException("Impossible parse speed mode", currentLine);
		
		int ordre = Integer.valueOf(m.group(1));
		IOrdreNxt finalOrder = null;
		for(IOrdreNxt ordre1:OrdreNxt_2017.values())
		{
			if(ordre1.getOrdreNumber().equals(ordre))
			{
				finalOrder = ordre1;
			}
		}
		
		if(finalOrder == null)
		{
			throw new ParseException("Impossible de trouver l'ordre "+ordre+" dans la liste des valeurs.", currentLine);
		}
		
		OrderList ol = new OrderList();
		ol.add(new EnvoyerOrdreNxtOrder(finalOrder));
		return ol;
	}
	
	private IOrderList parseChangeSpeedMode(OrderMetadata om) throws ParseException
	{
		Pattern ip = Pattern.compile(integerPattern);
		Matcher m = ip.matcher(om.getCommandValue());
		if(!m.find())
			throw new ParseException("Impossible parse speed mode", currentLine);
		
		int mode = Integer.valueOf(m.group(1));
		if(mode > 0)
		{
			for(SpeedMode sp:SpeedMode.values())
			{
				if(sp.getValue().equals(mode))
				{
					OrderList ol = new OrderList();
					ol.add(new ChangeSpeedModeOrder(sp));
					return ol;
				}
			}
		}
		
		throw new ParseException("Impossible to detect speed mode "+om, currentLine);
	}
	
	private IOrderList parseSegment(OrderMetadata om) throws ParseException
	{
		Pattern pp = Pattern.compile(pointPattern);
		Matcher m = pp.matcher(om.getCommandValue());
		if(!m.find())
			throw new ParseException("Impossible to parse segment order", currentLine);
		
		OrderList ol = new OrderList();
		ol.add(new TargetPositionOrder(new Position2DMillimeter(
			new Point2D.Double(Double.valueOf(m.group(1)), Double.valueOf(m.group(2)))
		)));
		return ol;
	}
	
	private IOrderList parseRotation(OrderMetadata om) throws ParseException
	{
		Pattern pp = Pattern.compile(doublePattern);
		Matcher m = pp.matcher(om.getCommandValue());
		if(!m.find())
			throw new ParseException("Impossible to parse rotation order", currentLine);
		
		OrderList ol = new OrderList();
		ol.add(new RotationOrder(new AngleRadian(Double.valueOf(m.group(1))), currentLine));
		return ol;
	}
	
	private IOrderList parsePause(OrderMetadata om) throws ParseException
	{
		Pattern pp = Pattern.compile(integerPattern);
		Matcher m = pp.matcher(om.getCommandValue());
		if(!m.find())
			throw new ParseException("Impossible to parse pause order", currentLine);
		
		OrderList ol = new OrderList();
		ol.add(new WaitOrder(new DelayMillisecond(Double.valueOf(m.group())), currentLine));
		return ol;
	}
	
	private IOrderList parseOrientation(OrderMetadata om) throws ParseException
	{
		Pattern pp = Pattern.compile(doublePattern);
		Matcher m = pp.matcher(om.getCommandValue());
		if(!m.find())
			throw new ParseException("Impossible to parse orientation order", currentLine);
		
		OrderList ol = new OrderList();
		ol.add(new TargetOrientationOrder(new AngleDegree(Double.valueOf(m.group(1)))));
		return ol;
	}
	
	private IOrderList parseInitialPosition(OrderMetadata om) throws ParseException
	{
		Pattern pp = Pattern.compile(pointPattern+";"+doublePattern);
		Matcher m = pp.matcher(om.getCommandValue());
		if(!m.find())
			throw new ParseException("Impossible to parse initial position order", currentLine);
		
		OrderList ol = new OrderList();
		ol.add(new InitialPositionOrder(
			new Position2DMillimeter(new Point2D.Double(Double.valueOf(m.group(1)), Double.valueOf(m.group(2)))), 
			new AngleDegree(Double.valueOf(m.group(3))), 
			0d, 0d, 0d, currentLine
		));
		return ol;
	}
	
	private IOrderList parseRecalage(OrderMetadata om) throws ParseException
	{
		Pattern ip = Pattern.compile(integerPattern);
		Matcher m = ip.matcher(om.getCommandValue());
		if(!m.find())
			throw new ParseException("Impossible to parse recalage order", currentLine);
		
		OrderList ol = new OrderList();
		ol.add(new RecalageOrder(new Distance2DMillimeter(Double.valueOf(m.group(1)))));
		return ol;
	}
	
	private IOrderList parseStraightDistance(OrderMetadata om) throws ParseException
	{
		Pattern ip = Pattern.compile(integerPattern);
		Matcher m = ip.matcher(om.getCommandValue());
		if(!m.find()) 
			throw new ParseException("Impossible to parse straight distance order ", currentLine);
		
		
		IDistance2D d2d = new Distance2DMillimeter(Double.valueOf(m.group()));
		
		OrderList ol = new OrderList();
		ol.add(new DistanceOrder(d2d, this.currentLine));
		return ol;
	}
	
	private IOrderList parseBezier(OrderMetadata om) throws ParseException
	{
		Pattern bp = Pattern.compile(pointPattern+";"+pointPattern);
		Matcher m = bp.matcher(om.getCommandValue());
		if(!m.find()) 
			throw new ParseException("Impossible to parse bezier order ", currentLine);
		
		Point2D cp1 = new Point2D.Double(Double.valueOf(m.group(1)), Double.valueOf(m.group(2)));
		Point2D cp2 = new Point2D.Double(Double.valueOf(m.group(3)), Double.valueOf(m.group(4)));
		Double endAngle = Math.atan2(cp2.getX() - cp1.getX(), cp2.getY() - cp2.getY());
		
		IOrderList ol = new OrderList();
		ol.add(new BezierQuadOrder(
			new AngleRadian(endAngle), 
			new Position2DMillimeter(cp1), 
			new Position2DMillimeter(cp2)
		));
		
		return ol;
	}
	
	private IOrderList parseDirectComment(OrderMetadata om)
	{
		OrderList ol = new OrderList();
		ol.add(new DirectCommandOrder(om.getCommandValue().substring(2)));
		return ol;
	}
	
	private IOrderList parseComment(OrderMetadata om)
	{
		IOrderList ol = new OrderList();
		ol.add(new CommentOrder(om.getCommandValue()));
		return ol;
	}
	
}
