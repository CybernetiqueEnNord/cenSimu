package org.cen.components.io.trajectory;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;

import org.cen.components.order.IOrderList;

/**
 * Trajectory Parser interface. Such parser must be able to parse a whole
 * file from any source of characters. The structure of the source is up to
 * the parser to decode.
 * 
 * @author Anastaszor
 */
public interface ITrajectoryParser
{
	
	public IOrderList parseStream(BufferedReader source) throws IOException, ParseException;
	
}
