package org.cen.components.io.filepath;

import java.io.File;

public enum FileEmplacement
{
	USER_DIR(System.getProperty("user.home") + File.separatorChar + ".cenSimu"),
	WORKING_DIR(System.getProperty("user.dir") + File.separatorChar + "data"),
	;
	
	private File file = null;
	
	private FileEmplacement(String path)
	{
		this.file = new File(path);
		this.file.mkdirs();
	}
	
	public File getFile()
	{
		return this.file;
	}
	
}
