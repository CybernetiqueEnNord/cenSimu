package org.cen.components.io.filepath;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;

import org.cen.components.gameboard.GameBoardMetadata;
import org.cen.components.io.file.InputFileType;

public interface IFilePathManager
{
	
	public File getFile(FileEmplacement emplacement, String filename) throws IOException;
	
	public JFileChooser getFileChooser(GameBoardMetadata meta, InputFileType type);
	
}
