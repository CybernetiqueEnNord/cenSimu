package org.cen.components.io.filepath;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;

import org.cen.components.gameboard.GameBoardMetadata;
import org.cen.components.io.file.InputFileType;

public class FilePathManager implements IFilePathManager
{
	
	@Override
	public File getFile(FileEmplacement emplacement, String filename) throws IOException
	{
		return new File(emplacement.getFile().getCanonicalPath() + File.separatorChar + filename);
	}
	
	@Override
	public JFileChooser getFileChooser(GameBoardMetadata meta, InputFileType type)
	{
		File working_dir = FileEmplacement.WORKING_DIR.getFile();
		if(!working_dir.exists())
			working_dir.mkdir();
		
		File year_dir = new File(working_dir.getAbsolutePath() + File.separatorChar + meta.getYear());
		if(!year_dir.exists())
			year_dir.mkdir();
		
		File type_dir = new File(year_dir.getAbsolutePath() + File.separatorChar + type.getDirectoryName());
		if(!type_dir.exists())
			type_dir.mkdir();
		
		return new JFileChooser(type_dir.getAbsolutePath());
	}
	
}
