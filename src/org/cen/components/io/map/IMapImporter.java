package org.cen.components.io.map;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Map;

public interface IMapImporter<T>
{
	public Map<String, T> importFrom(BufferedReader reader) throws IOException, ParseException;
}
