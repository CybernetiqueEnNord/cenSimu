package org.cen.components.io.map;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map.Entry;

public class MapExporter implements IMapExporter
{
	
	@Override
	public void export(IMapExportable exportable, BufferedWriter bw) throws IOException
	{
		for(Entry<String, Serializable> entry:exportable.getExportableValues().entrySet())
		{
			bw.write(entry.getKey());
			bw.write(": ");
			bw.write(entry.getValue().toString());
			bw.write("\n");
		}
		bw.flush();
	}
	
}
