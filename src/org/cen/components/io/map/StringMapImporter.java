package org.cen.components.io.map;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringMapImporter implements IMapImporter<String>
{
	
	private Pattern keyVal = Pattern.compile("^([\\d\\w_]+?):\\s(.*?)$");
	
	@Override
	public Map<String, String> importFrom(BufferedReader reader) throws IOException, ParseException
	{
		Map<String, String> lines = new HashMap<>();
		String currentLine = null;
		while((currentLine = reader.readLine()) != null)
		{
			Matcher m = keyVal.matcher(currentLine);
			if(m.find())
			{
				lines.put(m.group(1), m.group(2));
			}
		}
		return lines;
	}
	
}
