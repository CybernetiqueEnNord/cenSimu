package org.cen.components.io.map;

import java.io.Serializable;
import java.util.Map;

public interface IMapExportable
{
	
	public Map<String, Serializable> getExportableValues();
	
}
