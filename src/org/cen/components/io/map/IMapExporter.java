package org.cen.components.io.map;

import java.io.BufferedWriter;
import java.io.IOException;

public interface IMapExporter
{
	public void export(IMapExportable memorySetting, BufferedWriter bw) throws IOException;
}
