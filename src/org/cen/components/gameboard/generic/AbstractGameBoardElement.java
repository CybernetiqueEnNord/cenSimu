package org.cen.components.gameboard.generic;

import java.awt.Graphics2D;
import java.awt.Shape;

import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.position.IPosition2D;

/**
 * Abstract base class of a game board element.
 * 
 * @author Emmanuel ZURMELY
 */
public abstract class AbstractGameBoardElement implements IGameBoardElement
{
	/**
	 * The bounds of the element.
	 */
	protected Shape bounds;

	/**
	 * The name of the element.
	 */
	protected String name;

	/**
	 * The drawing order of the element. The lower value are drawn first
	 */
	protected int order;

	/**
	 * The orientation of the element in radians.
	 */
	protected IAngle orientation;

	/**
	 * The position of the element on the game board.
	 */
	protected IPosition2D position;

	/**
	 * Constructor
	 * 
	 * @param name
	 *            the name of the element
	 * @param position
	 *            the position of the element on the game board
	 */
	public AbstractGameBoardElement(String name, IPosition2D position) {
		this(name, position, new AngleRadian(0.0), 0);
	}

	/**
	 * Constructor
	 * 
	 * @param name
	 *            the name of the element
	 * @param position
	 *            the position of the element on the game board
	 * @param orientation
	 *            the orientation of the element in radians
	 */
	public AbstractGameBoardElement(String name, IPosition2D position, IAngle orientation) {
		this(name, position, orientation, 0);
	}

	/**
	 * Constructor
	 * 
	 * @param name
	 *            the name of the element
	 * @param position
	 *            the position of the element on the game board
	 * @param orientation
	 *            the orientation of the element in radians
	 * @param order
	 *            the display order of the element
	 */
	public AbstractGameBoardElement(String name, IPosition2D position, IAngle orientation, int order) {
		super();
		this.name = name;
		this.position = position;
		this.orientation = orientation;
		this.order = order;
	}

	@Override
	public Shape getBounds() {
		return bounds;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getOrder() {
		return order;
	}

	@Override
	public IAngle getOrientation() {
		return orientation;
	}

	@Override
	public IPosition2D getPosition() {
		return position;
	}

	@Override
	public Movable getMovableStatus()
	{
		return Movable.INNAMOVIBLE;
	}

	@Override
	public boolean isObstacle() {
		return false;
	}

	@Override
	public void paintUnscaled(Graphics2D g) {
		// default implementation does nothing
	}
}
