package org.cen.components.gameboard.generic;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.cen.components.gameboard.cup2018.TourCentrale;
import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.position.Position2DMillimeter;

abstract public class AbstractGameBoard2018 implements IGameBoardService
{
	
	public static final double BOARD_WIDTH = 2000d;
	public static final double BOARD_HEIGHT = 3000d;
	public static final double BORDER_WIDTH = 22d;
	
	protected List<IGameBoardElement> elements;
	protected Rectangle2D gameplayBounds;
	protected Rectangle2D visibleBounds;
	
	public AbstractGameBoard2018(Color playerA, Color playerB)
	{
		super();
		gameplayBounds = new Rectangle2D.Double(0, 0, BOARD_WIDTH, BOARD_HEIGHT);
		visibleBounds = new Rectangle2D.Double(-150, -200, BOARD_WIDTH + 400, BOARD_HEIGHT + 400);
		elements = new ArrayList<>();
		
		elements.add(new Board(getDefaultBoardColor(), BOARD_WIDTH, BOARD_HEIGHT));
		
		elements.add(new Border("border", BOARD_HEIGHT + BORDER_WIDTH * 2, BORDER_WIDTH, RALColor.RAL_9005, new Position2DMillimeter(0d, -BORDER_WIDTH), new AngleRadian(Math.PI / 2), 0));
		elements.add(new Border("border", BOARD_WIDTH, BORDER_WIDTH, RALColor.RAL_9005, new Position2DMillimeter(0d, -BORDER_WIDTH), new AngleRadian(0.0), 0));
		elements.add(new Border("border", BOARD_HEIGHT + BORDER_WIDTH * 2, BORDER_WIDTH, RALColor.RAL_9005, new Position2DMillimeter(BOARD_WIDTH + BORDER_WIDTH, -BORDER_WIDTH), new AngleRadian(Math.PI / 2), 0));
		elements.add(new Border("border", BOARD_WIDTH, BORDER_WIDTH, RALColor.RAL_9005, new Position2DMillimeter(0d, BOARD_HEIGHT), new AngleRadian(0.0), 0));
		
		elements.add(new BaliseSupport("balise-up-left", new Point2D.Double(50, 3094), playerA, false));
		elements.add(new BaliseSupport("balise-up-middle", new Point2D.Double(1000, 3094), playerB, false));
		elements.add(new BaliseSupport("balise-up-right", new Point2D.Double(1950, 3094), playerA, false));
		elements.add(new BaliseSupport("balise-bt-left", new Point2D.Double(50, -94), playerB, true));
		elements.add(new BaliseSupport("balise-bt-middle", new Point2D.Double(1000, -94), playerA, true));
		elements.add(new BaliseSupport("balise-bt-right", new Point2D.Double(1950, -94), playerB, true));
		
		getElements().add(new TourCentrale("tour-centrale", new Point2D.Double(-122, 1500), playerA, playerB));
		
		elements.add(new TableSeparation("table_separation_1", new Position2DMillimeter(new Point2D.Double(1000, 1000))));
		elements.add(new TableSeparation("table_separation_1", new Position2DMillimeter(new Point2D.Double(1000, 2000))));
	}

	protected abstract Color getDefaultBoardColor();
	
	@Override
	public List<IGameBoardElement> findElements(String elementName) {
		List<IGameBoardElement> found = new ArrayList<IGameBoardElement>();
		List<IGameBoardElement> elements = getElements();
		Iterator<IGameBoardElement> i = elements.iterator();
		while (i.hasNext()) {
			IGameBoardElement e = i.next();
			if (e.getName().equals(elementName)) {
				found.add(e);
			}
		}
		return found;
	}
	
	@Override
	public void removeElements(String elementName) {
		List<IGameBoardElement> elements = getElements();
		List<IGameBoardElement> found = findElements(elementName);
		for (IGameBoardElement e : found) {
			elements.remove(e);
		}
	}
	
	@Override
	public List<IGameBoardElement> getElements()
	{
		return elements;
	}
	
	@Override
	public Rectangle2D getGameplayBounds()
	{
		return gameplayBounds;
	}
	
	@Override
	public Rectangle2D getVisibleBounds()
	{
		return visibleBounds;
	}
	
}
