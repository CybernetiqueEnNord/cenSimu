package org.cen.components.gameboard.generic;

import java.awt.Color;

/**
 * 
 * @see http://ralcolor.com/
 * @see http://rgb.to/ral
 * @see http://paletton.com/
 */
public class RALColor {
	
	// Daffodil Yellow
	public static final Color RAL_1007 = new Color(220, 156, 0);
	
	// Traffic Yellow
	public static final Color RAL_1023 = new Color(250, 210, 1); //new Color(252, 189, 31);
	
	// Signal Orange
	public static final Color RAL_2010 = new Color(208, 93, 40);
	
	// Signal Red
	public static final Color RAL_3001 = new Color(165, 32, 25); //new Color(163, 23, 26);
	
	// Traffic Red
	public static final Color RAL_3020 = new Color(204, 6, 5);
	
	// Purple
	public static final Color RAL_4008 = new Color(146, 78, 125); //new Color(125, 31, 122);
	
	// Clear Blue
	public static final Color RAL_5012 = new Color(59, 131, 189); //new Color(41, 115, 184);
	
	// Sky Blue
	public static final Color RAL_5015 = new Color(34, 113, 179); //new Color(23, 97, 171);
	
	// Emerald Green
	public static final Color RAL_6001 = new Color(40, 114, 51);
	
	// Yellow-Green
	public static final Color RAL_6018 = new Color(87, 166, 57); //new Color(79, 168, 51);
	
	// squirrel grey
	public static final Color RAL_7000 = new Color(120, 133, 139);
	
	// Pebble grey
	public static final Color RAL_7032 = new Color(184, 183, 153);
	
	// Signal Brown
	public static final Color RAL_8002 = new Color(108, 59, 42); //new Color(110, 59, 58);
	
	// Jet Black
	public static final Color RAL_9005 = new Color(10, 10, 10); //Color.BLACK;
	
	// Pure White
	public static final Color RAL_9010 = new Color(241, 236, 225);
	
	// Traffic White
	public static final Color RAL_9016 = new Color(246, 246, 246);
	
	// pure yellow
	public static final Color YELLOW = new Color(255, 255, 0);
	
	// pure red
	public static final Color RED = new Color(255, 0, 0);
	
	// pure green
	public static final Color GREEN = new Color(0, 255, 0);
	
	// pure blue
	public static final Color BLUE = new Color(0, 0, 255);
	
}
