package org.cen.components.gameboard.generic;

import org.cen.components.math.angle.IAngle;
import org.cen.components.math.position.IPosition2D;

public interface IMovable
{
	/**
	 * Sets the position of the object
	 * 
	 * @param position
	 *            the position of the object
	 */
	void setPosition(IPosition2D position);
	
	/**
	 * Sets the orientation of the object
	 * 
	 * @param angle
	 *            the orientation angle
	 */
	void setOrientation(IAngle angle);
	
}
