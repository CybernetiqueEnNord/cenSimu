package org.cen.components.gameboard.generic;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.distance.IDistance2D;
import org.cen.components.math.position.IPosition2D;
import org.cen.components.math.position.Position2DMillimeter;
import org.cen.components.trajectories.ITrajectoryMovement;
import org.cen.components.trajectories.KeyFrame;
import org.cen.components.trajectories.KeyFrameCollection;
import org.cen.components.trajectories.KeyFrameLinearInterpolator;
import org.cen.components.trajectories.TrajectoryBezierCubic;
import org.cen.components.trajectories.TrajectoryClothoid;
import org.cen.components.trajectories.TrajectoryDelay;
import org.cen.components.trajectories.TrajectoryRotation;
import org.cen.components.trajectories.TrajectorySegment;
import org.cen.components.trajectories.TrajectoryStart;

public class StraightLine extends AbstractTrajectoryPath {

	public static final String KEY_POSITION_X = "positionX";
	public static final String KEY_POSITION_Y = "positionY";
	public static final String KEY_XY = "xy";
	public static final String KEY_PRECOMMENTS = "preComment";
	public static final String KEY_POSTCOMMENTS = "postComment";
	public static final String KEY_ANGLE = "angle";
	public static final String KEY_ORIENTATION = "orientation";
	public static final String KEY_PAUSE = "pause";
	public static final String KEY_NXT = null;
	public static final String KEY_DELAY = null;
	public static String KEY_DISTANCE;
	private KeyFrame keyFrame;
	
	private Path2D path;
	private boolean absolutePosition = false;
	private KeyFrameCollection frames;
	private IAngle lastOrientation;
	private IPosition2D last;

	public StraightLine(String name, IAngle initialAngle, IAngle finalAngle, KeyFrameCollection frames)
	{
		super(name, new Position2DMillimeter(new Point2D.Double()), null, null, initialAngle, finalAngle);
		this.frames = frames;
		order = 1000;
		
		int n = frames.size();
		start = frames.first().getPosition();
		end = frames.last().getPosition();

		boolean forward = true;

		path = new Path2D.Double();
		path.moveTo(start.getValue().getX(), start.getValue().getY());
		for (int i = 1; i < n; i++) {
			KeyFrame frame = frames.get(i);
			ITrajectoryMovement movement = frame.getMovement();
			movement.move(this, forward, frame);
		}
		
	}

	public void addComments(Map<String, String> params, String values, boolean preComments) {
		String c;
		if (preComments) {
			c = params.get(KEY_PRECOMMENTS);
		} else {
			c = params.get(KEY_POSTCOMMENTS);
		}

		if (c != null) {
			c += values;
		} else {
			c = values;
		}
		c += "\r\n";

		if (preComments) {
			params.put(KEY_PRECOMMENTS, c);
		} else {
			params.put(KEY_POSTCOMMENTS, c);
		}
	}

	public void addParameters(Map<String, String> params, String values) {
		if (values.startsWith("//")) {
			addComments(params, values, false);
			return;
		}

		Pattern p = Pattern.compile("((\\w+)=([\\w0-9\\-,\\.]+))*");
		Matcher m = p.matcher(values);
		boolean found = false;
		while (m.find()) {
			String key = m.group(2);
			if (key == null) {
				continue;
			}
			String value = m.group(3);
			params.put(key, value);
			found = true;
		}

		if (!found) {
			addComments(params, values, false);
		}
	}

	public void addValue(Map<String, String> params, String key, double d) {
		params.put(key, String.format("%.0f", d));
	}

	private void clear(Map<String, String> params, String key) {
		params.remove(key);
	}

	public double getDoubleValue(Map<String, String> params, String key) {
		String value = params.get(key);
		double d = Double.parseDouble(value);
		return d;
	}

	public KeyFrame getKeyFrame(double timestamp) {
		if (keyFrame == null || keyFrame.getTimestamp() != timestamp) {
			keyFrame = frames.interpolate(new KeyFrameLinearInterpolator(), timestamp);
		}
		return keyFrame;
	}
	
	public KeyFrame getNextKeyFrame(Double timestamp)
	{
		return frames.nextFrame(timestamp);
	}

	@Override
	public IAngle getOrientation() {
		return new AngleRadian(0.0);
	}

	@Override
	public IAngle getOrientation(double timestamp) {
		KeyFrame frame = getKeyFrame(timestamp);
		return frame.getOrientation();
	}

	@Override
	public Shape getPath() {
		return path;
	}

	@Override
	public IPosition2D getPosition(double timestamp) {
		KeyFrame frame = getKeyFrame(timestamp);
		return frame.getPosition();
	}

	@Override
	public double getSourceLine(double timestamp) {
		KeyFrame frame = getKeyFrame(timestamp);
		return frame.getSourceLine();
	}

	@Override
	public Point2D[] getTrajectoryControlPoints() {
		return null;
	}

	@Override
	public String getTrajectoryDescription() {
		StringBuilder sb = new StringBuilder();
		Map<String, String> params = new HashMap<String, String>();
		for (int i = 0; i < frames.size(); i++) {
			KeyFrame frame = frames.get(i);
			writeLine(sb, frame);
			if (frame.hasComments()) {
				ArrayList<String> comments = frame.getComments();
				for (String s : comments) {
					addParameters(params, s);
				}
			}

			ITrajectoryMovement movement = frame.getMovement();
			if(movement instanceof TrajectoryStart)
			{
				last = frame.getPosition();
				lastOrientation = frame.getOrientation();
				sb.append(String.format("// start at %s\r\n", last.toString()));
				break;
			}
			else if(movement instanceof TrajectoryBezierCubic)
			{
			}
			else if(movement instanceof TrajectoryClothoid)
			{
			}
			else if(movement instanceof TrajectorySegment)
			{
				IPosition2D p = frame.getPosition();
				IDistance2D distance = p.distance(last);
				double speed = frame.getMovementSpeed();
				String direction = speed > 0 ? "forward" : "backward";
				if (absolutePosition) {
					addComments(params, String.format("// move to x=%.0f mm, y=%.0f mm", p.getValue().getX(), p.getValue().getY()), true);
					addValue(params, KEY_POSITION_X, p.getValue().getX());
					addValue(params, KEY_POSITION_Y, p.getValue().getY());
				} else {
					addComments(params, String.format("// move %s of %.0f mm (%.0f)", direction, distance, 9.557 * distance.getValue() * Math.signum(speed)), true);
					addValue(params, KEY_DISTANCE, 9.557 * distance.getValue() * Math.signum(speed));
				}
				last = p;
				break;
			}
			else if(movement instanceof TrajectoryDelay)
			{
				// double delay = frame.getTimestamp() - lastTimestamp;
				// addComments(params, String.format("delay_ms(%.0f);", delay *
				// 1000), true);
			}
			else if(movement instanceof TrajectoryRotation)
			{
				IAngle o = frame.getOrientation();
				IAngle angle = lastOrientation.getRotationAngle(o);
				if (frame.useRelativeAngle()) {
					// the angle is a relative angle that can be > 180°
					angle = new AngleRadian(frame.getRelativeAngle());
				}
				if (!absolutePosition || !isLine(i + 1)) {
					addComments(params, String.format("// rotation of %.0f° (%.0f)", angle, 22527.5d / 360d * angle.toDegrees().getValue()), true);
					addValue(params, KEY_ANGLE, 22527.5d / 360d * angle.toDegrees().getValue());
					if (params.containsKey(KEY_ORIENTATION)) {
						double opposite = getDoubleValue(params, KEY_ORIENTATION);
						opposite = Math.toDegrees(opposite);
						addValue(params, KEY_ORIENTATION, 22527.5d / 360d * opposite);
					}
				}
				lastOrientation = o;
			}
			writeCommands(sb, params);
		}
		return sb.toString();
	}

	private boolean isLine(int i) {
		if (i >= frames.size()) {
			return false;
		}
		KeyFrame frame = frames.get(i);
		boolean line = frame.getMovement() instanceof TrajectorySegment;
		return line;
	}

	@Override
	public void paint(Graphics2D g, double timestamp) {
		super.paint(g);

		IPosition2D p = getPosition(timestamp);
		double x = p.getValue().getX() - position.getValue().getX();
		double y = p.getValue().getY() - position.getValue().getY();
		g.translate(x, y);

		double angle = getOrientation(timestamp).getValue() - orientation.getValue();
		g.rotate(angle);

		paintGauge(g);
	}

	@Override
	public void paintUnscaled(Graphics2D g) {
		paintUnscaled(g, 0d);
	}

	@Override
	public void paintUnscaled(Graphics2D g, double timestamp) {
		if (timestamp == 0d) {
			super.paintUnscaled(g);
		}
	}

	private void writeCommands(StringBuilder sb, Map<String, String> params) {
		String comments = params.get(KEY_PRECOMMENTS);
		if (comments != null) {
			sb.append(comments);
			clear(params, KEY_PRECOMMENTS);
		}

		if (params.containsKey(KEY_ORIENTATION)) {
			String v = params.get(KEY_ANGLE);
			String o = params.get(KEY_ORIENTATION);
			addComments(params, String.format("FCM_tourner_sens_couleur(%s, %s);", v, o), true);
			clear(params, KEY_ANGLE);
			clear(params, KEY_ORIENTATION);
		} else if (params.containsKey(KEY_ANGLE)) {
			String v = params.get(KEY_ANGLE);
			sb.append(String.format("FCM_tourner(%s);\r\n", v));
			clear(params, KEY_ANGLE);
		} else if (params.containsKey(KEY_DISTANCE)) {
			String distance = params.get(KEY_DISTANCE);
			if (params.containsKey(KEY_NXT)) {
				String cmd = params.get(KEY_NXT);
				String delay = params.get(KEY_DELAY);
				if (delay == null) {
					delay = "0";
				}
				sb.append(String.format("FCM_avancer_et_envoyer_ordre(%s, %s, %s);\r\n", distance, cmd, delay));
			} else {
				sb.append(String.format("FCM_avancer(%s);\r\n", distance));
			}
			clear(params, KEY_DISTANCE);
			clear(params, KEY_NXT);
			clear(params, KEY_DELAY);
		} else if (params.containsKey(KEY_POSITION_X) && params.containsKey(KEY_POSITION_Y)) {
			String x = params.get(KEY_POSITION_X);
			String y = params.get(KEY_POSITION_Y);
			sb.append(String.format("FCM_atteindreXI_mm(%s, %s);\r\n", x, y));
			clear(params, KEY_POSITION_X);
			clear(params, KEY_POSITION_Y);
		}
		if (params.containsKey(KEY_XY)) {
			String s = params.get(KEY_XY);
			absolutePosition = s.equals("true");
			clear(params, KEY_XY);
		}
		if (params.containsKey(KEY_PAUSE)) {
			String v = params.get(KEY_PAUSE);
			int d = Integer.parseInt(v);
			while (d > 255) {
				sb.append("delay_ms(255);\r\n");
				d -= 255;
			}
			sb.append(String.format("delay_ms(%d);\r\n", d));
			clear(params, KEY_PAUSE);
		}

		comments = params.get(KEY_POSTCOMMENTS);
		if (comments != null) {
			sb.append(comments);
			clear(params, KEY_POSTCOMMENTS);
		}
	}

	private void writeLine(StringBuilder sb, KeyFrame frame) {
		int line = frame.getSourceLine();
		sb.append(String.format("// line %d\r\n", line));
	}

	public KeyFrameCollection getFrames()
	{
		return frames;
	}

	public double getDoubleValue(Map<String, String> params, Object key) {
		String value = params.get(key);
		double d = Double.parseDouble(value);
		return d;
	}

}
