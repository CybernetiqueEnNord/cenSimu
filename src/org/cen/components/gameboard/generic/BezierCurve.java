package org.cen.components.gameboard.generic;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.cen.components.math.angle.IAngle;
import org.cen.components.math.position.IPosition2D;
import org.cen.components.math.position.Position2DMillimeter;

/**
 * Graphical representation of a bezier curve.
 * 
 * @author Emmanuel ZURMELY
 */
public class BezierCurve extends AbstractTrajectoryPath
{
	private IPosition2D controlPoint1;
	private IPosition2D controlPoint2;
	private Path2D path;
	private Path2D trajectory;

	public BezierCurve(String name, IPosition2D start, IPosition2D end, IPosition2D controlPoint1, IPosition2D controlPoint2, IAngle initialAngle, IAngle finalAngle) {
		super(name, start);
		this.start = start;
		this.end = end;
		this.controlPoint1 = controlPoint1;
		this.controlPoint2 = controlPoint2;
		this.initialAngle = initialAngle;
		this.finalAngle = finalAngle;

		double x = start.getValue().getX();
		double y = start.getValue().getY();
		end = new Position2DMillimeter(end.getValue().getX() - x, end.getValue().getY() - y);
		controlPoint1 = new Position2DMillimeter(controlPoint1.getValue().getX() - x, controlPoint1.getValue().getY() - y);
		controlPoint2 = new Position2DMillimeter(controlPoint2.getValue().getX() - x, controlPoint2.getValue().getY() - y);

		Dimension dimension = new Dimension(20, 20);
		path = new Path2D.Double();
		trajectory = new Path2D.Double();

		// Points
		addPoint(trajectory, start.getValue(), dimension);
		addPoint(trajectory, end.getValue(), dimension);
		addPoint(trajectory, controlPoint1.getValue(), dimension);
		addPoint(trajectory, controlPoint2.getValue(), dimension);

		// Curve
		path.moveTo(0, 0);
		path.curveTo(
			controlPoint1.getValue().getX(), 
			controlPoint1.getValue().getY(), 
			controlPoint2.getValue().getX(), 
			controlPoint2.getValue().getY(), 
			end.getValue().getX(), 
			end.getValue().getY()
		);
		
		trajectory.append(path, false);
	}

	private void addPoint(Path2D path, Point2D point, Dimension dimension) {
		double x = point.getX();
		double y = point.getY();
		path.append(new Rectangle2D.Double(x - dimension.width / 2, y - dimension.height / 2, dimension.width, dimension.height), false);
	}

	@Override
	public IAngle getOrientation(double timestamp) {
		return getOrientation();
	}

	public Shape getPath() {
		return path;
	}

	@Override
	public IPosition2D getPosition(double timestamp) {
		return getPosition();
	}

	@Override
	public Point2D[] getTrajectoryControlPoints()
	{
		return new Point2D[] { controlPoint1.getValue(), controlPoint2.getValue() };
	}

	@Override
	public void paint(Graphics2D g, double timestamp) {
		paint(g);
	}

	@Override
	public void paintUnscaled(Graphics2D g, double timestamp) {
		paintUnscaled(g);
	}
}
