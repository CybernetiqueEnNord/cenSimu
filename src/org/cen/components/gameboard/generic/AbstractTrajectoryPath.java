package org.cen.components.gameboard.generic;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Point2D;

import org.cen.components.gauge.IGauge;
import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.position.IPosition2D;
import org.cen.components.math.position.Position2DMillimeter;
import org.cen.components.trajectories.ITrajectoryPath;
import org.cen.components.trajectories.TrajectoryStroke;

public abstract class AbstractTrajectoryPath extends AbstractGameBoardElement implements ITrajectoryPath, IGauge, IGameBoardTimedElement {
	protected static final Color GAUGE_COLOR = new Color(0x200000ff, true);
	protected static final Stroke OUTLINE_STROKE = new BasicStroke(2, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER, 10, new float[] { 15, 15 }, 0);
	protected IPosition2D end;
	protected IAngle finalAngle;
	private IGauge gauge;
	protected IAngle initialAngle;
	protected IPosition2D start;
	private Stroke stroke = new BasicStroke(3);
	private Shape trajectoryGauge;

	public AbstractTrajectoryPath(String name, IPosition2D position) {
		this(name, position, new Position2DMillimeter(new Point2D.Double()), new Position2DMillimeter(new Point2D.Double()), new AngleRadian(0d), new AngleRadian(0d));
	}

	public AbstractTrajectoryPath(String name, IPosition2D position, IPosition2D start, IPosition2D end, IAngle initialAngle, IAngle finalAngle) {
		super(name, position);
		this.start = start;
		this.end = end;
		this.initialAngle = initialAngle;
		this.finalAngle = finalAngle;
	}

	private Color getColor() {
		return Color.BLUE;
	}
	
	@Override
	public Movable getMovableStatus()
	{
		return Movable.TRANSPARENT;
	}

	@Override
	public IGauge getGauge() {
		return gauge;
	}

	public Color getGaugeColor() {
		return GAUGE_COLOR;
	}

	@Override
	public Shape getGaugeShape() {
		if (trajectoryGauge == null) {
			if (gauge == null) {
				return null;
			}
			Shape p = getPath();
			Shape g = gauge.getGaugeShape();
			TrajectoryStroke ts = new TrajectoryStroke(g, initialAngle);
			trajectoryGauge = ts.createStrokedShape(p);
		}
		return trajectoryGauge;
	}

	@Override
	public IAngle getRobotFinalAngle() {
		return finalAngle;
	}

	@Override
	public IAngle getRobotInitialAngle() {
		return initialAngle;
	}

	@Override
	public double getSourceLine(double timestamp) {
		return 0;
	}

	private Stroke getStroke() {
		return stroke;
	}

	@Override
	public String getTrajectoryDescription() {
		return null;
	}

	@Override
	public IPosition2D getTrajectoryEnd() {
		return end;
	}

	@Override
	public IPosition2D getTrajectoryStart() {
		return start;
	}

	@Override
	public void paint(Graphics2D g) {
		Shape gauge = getGaugeShape();
		if (gauge != null) {
			g.setColor(GAUGE_COLOR);
			g.fill(gauge);
			g.setColor(Color.WHITE);
			g.setStroke(OUTLINE_STROKE);
			g.draw(gauge);
		}

		Stroke stroke = getStroke();
		Shape path = getPath();
		Color color = getColor();
		g.setStroke(stroke);
		g.setColor(color);
		g.draw(path);
	}

	protected void paintGauge(Graphics2D g) {
		IGauge gauge = getGauge();
		if (gauge == null) {
			return;
		}
		Shape gaugeShape = gauge.getGaugeShape();
		if (gaugeShape == null) {
			return;
		}
		Color gaugeColor = gauge.getGaugeColor();
		g.setColor(gaugeColor);
		g.fill(gaugeShape);
		Stroke stroke = getStroke();
		g.setStroke(stroke);
		g.setColor(Color.BLACK);
		g.draw(gaugeShape);
	}

	@Override
	public void setGauge(IGauge gauge) {
		if (gauge == this.gauge) {
			return;
		}
		trajectoryGauge = null;
		this.gauge = gauge;
	}
	
}
