package org.cen.components.gameboard.generic;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.math.position.Position2DMillimeter;

/**
 * BaliseSupport class file.
 * 
 * This class represents a new (2018+) balise support element.
 * 
 * The element has this shape : (scale about ~ 1 vertical unit = 10mm and 1 horizontal unit = 5mm) 
 * 
 * The cotations are in mm.
 * 
 * |-----------122------------|
 *      |---------100---------|
 * 
 *                |-|----10
 * 
 * +----+---------+ +---------+  --- --- ---
 * |    |         | |         |   |   |   |
 * |    |         | |         |   50  |   |
 * |    |         | |         |   |   |   |
 * |    |         | |         |   |  100  |
 * |    |         \_/         |  ---  |   |
 * |    |                     |       |  122
 * |    |                     |       |   |
 * |    |                     |       |   |
 * |    |                     |       |   |
 * |    +---------------------+      ---  |
 * |                          |           |
 * +--------------------------+          ---
 * 
 * The outer path is the square minus the rainure.
 * The inner path is the corner.
 * The position of the figure is placed at the center of the circle in the rainure.
 * The piece is contained in a square [left:-72, right:50] x [bottom:-72, top:50]. 
 * 
 * @author Anastaszor
 */
public class BaliseSupport extends AbstractGameBoardElement implements IGameBoardElement
{
	
	private Color color = null;
	private Path2D outer = null;
	private Path2D inner = null;
	private boolean invertOnYAxis = false;
	
	public BaliseSupport(String name, Point2D position, Color color, boolean invertOnYAxis)
	{
		super(name, new Position2DMillimeter(position));
		this.color = color;
		this.invertOnYAxis = invertOnYAxis;
		
		outer = new Path2D.Double();
		// begin the path at the bottom left position
		outer.moveTo(position.getX() - 72, position.getY() - 72);
		// go to bottom right
		outer.lineTo(position.getX() + 50, position.getY() - 72);
		// go to top right
		outer.lineTo(position.getX() + 50, position.getY() + 50);
		// go to top middle right
		outer.lineTo(position.getX() + 5, position.getY() + 50);
		// go to middle middle right
		outer.lineTo(position.getX() + 5, position.getY());
		// circle around
		outer.curveTo(
			position.getX() + 5, position.getY() - 5,	// point at the bottom right of the square containing the semi circle
			position.getX() - 5, position.getY() - 5,	// point at the bottom left of the square containing the semi circle
			position.getX() - 5, position.getY()	// point at the middle left of the square containing the semi circle
		);
		// go to middle middle left
		outer.lineTo(position.getX() - 5, position.getY() + 50);
		// go to top left
		outer.lineTo(position.getX() - 72, position.getY() + 50);
		// go to bottom left
		outer.closePath();
		
		inner = new Path2D.Double();
		// begin the path at the bottom left position
		inner.moveTo(position.getX() - 72, position.getY() - 72);
		// go to bottom right
		inner.lineTo(position.getX() + 50, position.getY() - 72);
		// go to middle right
		inner.lineTo(position.getX() + 50, position.getY() - 50);
		// go to middle middle
		inner.lineTo(position.getX() - 50, position.getY() - 50);
		// go to top middle
		inner.lineTo(position.getX() - 50, position.getY() + 50);
		// go to top left
		inner.lineTo(position.getX() -72 , position.getY() + 50);
		// go to bottom left
		inner.closePath();
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(this.color);
		g.fill(outer);	// fill in player color
		g.setColor(RALColor.RAL_9005);
		g.draw(outer);	// draw borders in black
		g.setColor(this.color);
		g.fill(inner);	// fill in player color
		g.setColor(RALColor.RAL_9005);
		g.draw(inner);	// draw borders in black
		g.translate(position.getValue().getX(), position.getValue().getY());
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
	}
	
}
