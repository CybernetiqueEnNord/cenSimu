package org.cen.components.gameboard.generic;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.cup2015.Strokes;
import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.orientedPosition.OrientedPosition2D;
import org.cen.components.math.position.Position2DMillimeter;
import org.cen.components.trajectories.TrajectoryPlanner;

public class PlannedTrajectory extends AbstractGameBoardElement {
	private Path2D line;

	public PlannedTrajectory(String name) {
		super(name, new Position2DMillimeter(0d, 0d), new AngleRadian(0.0), 2000);
		line = new Path2D.Double();
	}

	public void planTrajectory(OrientedPosition2D start, OrientedPosition2D end) {
		TrajectoryPlanner planner = new TrajectoryPlanner(10, 100);
		OrientedPosition2D[] points = planner.getTrajectory(start, end);
		line.reset();
		Point2D p = start.getPosition().getValue();
		line.moveTo(p.getX(), p.getY());
		for (OrientedPosition2D point : points) {
			line.lineTo(point.getPosition().getValue().getX(), point.getPosition().getValue().getY());
		}
	}

	@Override
	public void paint(Graphics2D g) {
		g.setStroke(Strokes.thickStroke);
		g.setColor(Color.MAGENTA);
		g.draw(line);
	}
	
	@Override
	public Movable getMovableStatus()
	{
		return Movable.TRANSPARENT;
	}
	
}
