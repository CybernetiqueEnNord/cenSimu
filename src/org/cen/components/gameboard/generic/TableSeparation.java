package org.cen.components.gameboard.generic;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import org.cen.components.math.position.IPosition2D;

public class TableSeparation extends AbstractGameBoardElement
{
	
	private Color color = null;
	private Rectangle2D outer = null;
	
	public TableSeparation(String name, IPosition2D positionCentrale)
	{
		super(name, positionCentrale);
		color = RALColor.RAL_9005;
		outer = new Rectangle2D.Double(
			positionCentrale.getValue().getX() - 1000,
			positionCentrale.getValue().getY(),
			2000,
			1
		);
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(color);
		g.fill(outer);
		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
	@Override
	public Movable getMovableStatus()
	{
		return Movable.TRANSPARENT;
	}
	
}
