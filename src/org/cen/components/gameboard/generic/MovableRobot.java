package org.cen.components.gameboard.generic;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Arc2D;

import org.cen.components.gameboard.cup2015.Strokes;
import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.position.IPosition2D;
import org.cen.components.math.position.Position2DMillimeter;

public class MovableRobot extends AbstractGameBoardElement implements IMovable
{
	private Shape area;
	
	public MovableRobot(String name) {
		super(name, new Position2DMillimeter(0d, 0d), new AngleRadian(0.0), 1000);
		area = new Arc2D.Double(-105, -105, 210, 210, 0, 360, Arc2D.PIE);
	}
	
	@Override
	public void paint(Graphics2D g) {
		g.setColor(Color.LIGHT_GRAY);
		g.fill(area);
		g.setColor(Color.BLACK);
		g.setStroke(Strokes.thinStroke);
		g.draw(area);
	}
	
	@Override
	public void setPosition(IPosition2D position) {
		this.position = position;
	}
	
	@Override
	public void setOrientation(IAngle angle) {
		this.orientation = angle;
	}
	
	@Override
	public Movable getMovableStatus()
	{
		return Movable.MOVABLE;
	}
	
}
