package org.cen.components.gameboard.generic;

/**
 * Base interface of events related to then game board.
 * 
 * @author Emmanuel ZURMELY
 */
public interface IGameBoardEvent {
}
