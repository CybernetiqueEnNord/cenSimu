package org.cen.components.gameboard.generic;

public enum Movable
{
	TRANSPARENT,	// on passe à travers, pas de mouvement à prévoir
	MOVABLE,		// on pousse, mouvement à prévoir de l'élément
	INNAMOVIBLE;	// impossible de le bouger, le robot ne peut pas franchir
}
