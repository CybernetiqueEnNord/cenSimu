package org.cen.components.gameboard.generic;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import org.cen.components.math.angle.IAngle;
import org.cen.components.math.position.IPosition2D;

public class Border extends AbstractGameBoardElement
{
	public static String BORDER_WIDTH = "borderWidth";
	
	private final Color color;
	
	private final double length;
	
	private final double width;
	
	public Border(String name, double length, double width, Color color, IPosition2D position, IAngle orientation, int order) {
		super(name, position, orientation, order);
		this.length = length;
		this.color = color;
		this.width = width;
	}
	
	@Override
	public void paint(Graphics2D g) {
		g.setColor(color);
		g.fillRect(0, 0, (int) length, (int) width);
		g.setColor(RALColor.RAL_9005);
		g.draw(new Rectangle2D.Double(0, 0, length, width));
	}
	
}
