package org.cen.components.gameboard.cup2016;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

public class Closet extends AbstractGameBoardElement
{
	private Path2D top = null;
	private Path2D door = null;
	private boolean symetry = false;
	
	public Closet(String name, Point2D position, boolean symetry)
	{
		super(name, new Position2DMillimeter(position));
		this.symetry = symetry;
		
		top = new Path2D.Double();
		top.moveTo(position.getX() - 50, position.getY());
		top.lineTo(position.getX() - 50, position.getY() + 100);
		top.lineTo(position.getX() + 50, position.getY() + 100);
		top.lineTo(position.getX() + 50, position.getY());
		top.lineTo(position.getX() + 25, position.getY());
		top.lineTo(position.getX() + 25, position.getY() + 100);
		top.lineTo(position.getX() + 25, position.getY());
		top.lineTo(position.getX(), position.getY());
		top.lineTo(position.getX(), position.getY() + 100);
		top.lineTo(position.getX(), position.getY());
		top.lineTo(position.getX() - 25, position.getY());
		top.lineTo(position.getX() - 25, position.getY() + 100);
		top.lineTo(position.getX() - 25, position.getY());
		top.lineTo(position.getX() - 50, position.getY());
		top.closePath();
		
		door = new Path2D.Double();
		double th = 0;
		if(symetry)
		{
			door.moveTo(0, 0);
			door.lineTo(100, 0);
			door.lineTo(100, -10);
			door.lineTo(0, -10);
			door.closePath();
			th = - Math.PI / 3;
		}
		else
		{
			door.moveTo(0, 0);
			door.lineTo(100, 0);
			door.lineTo(100, 10);
			door.lineTo(0, 10);
			door.closePath();
			th = Math.PI / 3;
		}
		// scaleX, shearY, shearX, scaleY, trX, trY
//		door.transform(AffineTransform.getRotateInstance(th));
		door.transform(new AffineTransform(Math.cos(th), - Math.sin(th), Math.sin(th), Math.cos(th), 0, 0));
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.translate(- position.getValue().getX(), - position.getValue().getY());
		g.setColor(RALColor.RAL_7032);
		g.fill(top);
		g.setColor(RALColor.RAL_9005);
		g.draw(top);
		if(symetry)
			g.translate(position.getValue().getX() + 50, position.getValue().getY());
		else
			g.translate(position.getValue().getX() + 50, position.getValue().getY() + 100);
		g.setColor(RALColor.RAL_7032);
		g.fill(door);
		g.setColor(RALColor.RAL_9005);
		g.draw(door);
		if(symetry)
			g.translate(- 50, 0);
		else
			g.translate(- 50, -100);
	}
	
}
