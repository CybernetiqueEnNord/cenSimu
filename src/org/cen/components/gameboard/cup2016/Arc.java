package org.cen.components.gameboard.cup2016;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.cup2015.Strokes;
import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.Movable;
import org.cen.components.math.Droite2D;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.position.Position2DMillimeter;

public class Arc extends AbstractGameBoardElement
{
	
	private Path2D path = new Path2D.Double();
	private Color color = null;
	
	public Arc(String name, double startAngle, double endAngle, double radius, 
		Color color, Point2D centerPosition, IAngle orientation, int order
	) {
		super(name, new Position2DMillimeter(centerPosition));
		this.orientation = orientation;
		this.order = order;
		this.color = color;
		
		Point2D start = new Point2D.Double(
			centerPosition.getX() + radius * Math.cos(startAngle),
			centerPosition.getY() + radius * Math.sin(startAngle)
		);
		Point2D end = new Point2D.Double(
			centerPosition.getX() + radius * Math.cos(endAngle),
			centerPosition.getY() + radius * Math.sin(endAngle)
		);
		
		// le point de controle d'un arc de cercle est à l'intersection des
		// tangentes à cet arc aux points de départ et d'arrivée
		Droite2D d1 = new Droite2D(centerPosition, start);
		Droite2D p1 = d1.getPerpendicular(start);
		Droite2D d2 = new Droite2D(centerPosition, end);
		Droite2D p2 = d2.getPerpendicular(end);
		Point2D control = p1.intersect(p2);
		
		path.moveTo(start.getX(), start.getY());
		
		path.curveTo(
			start.getX(), start.getY(), 
			control.getX(), control.getY(), 
			end.getX(), end.getY()
		);
		
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setStroke(Strokes.thickStroke);
		g.setColor(color);
		g.draw(path);
		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
	@Override
	public Movable getMovableStatus()
	{
		return Movable.TRANSPARENT;
	}
	
}
