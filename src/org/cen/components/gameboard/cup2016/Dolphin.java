package org.cen.components.gameboard.cup2016;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.cup2015.Strokes;
import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.Movable;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

public class Dolphin extends AbstractGameBoardElement
{
	
	private Path2D drawing = null;
	
	public Dolphin(String name, Point2D position, boolean symetry)
	{
		super(name, new Position2DMillimeter(position));
		drawing = new Path2D.Double();
		
		drawing.moveTo(130, -50);
		drawing.curveTo(130, -50, 220, -80, 270, -125);
		drawing.curveTo(270, -125, 500, 0, 550, -250);
		drawing.curveTo(550, -250, 700, -250, 700, -200);
		drawing.curveTo(700, -200, 650, -330, 550, -340);
		drawing.curveTo(550, -340, 500, -500, 360, -540);
		drawing.curveTo(360, -540, 340, -570, 300, -600);
		drawing.curveTo(300, -600, 240, -600, 250, -500);
		drawing.curveTo(250, -500, 170, -400, 260, -300);
		drawing.moveTo(510, -460);
		drawing.curveTo(510, -460, 530, -480, 500, -545);
		drawing.curveTo(500, -545, 490, -540, 460, -510);
		drawing.moveTo(220, -400);
		drawing.curveTo(220, -400, 180, -400, 140, -440);
		drawing.curveTo(140, -440, 140, -540, 50, -580);
		drawing.curveTo(50, -580, 0, -600, 20, -550);
		drawing.curveTo(20, -550, 80, -450, 20, -350);
		drawing.curveTo(20, -350, 0, -300, 50, -300);
		drawing.curveTo(50, -300, 80, -300, 125, -330);
		drawing.curveTo(125, -330, 200, -250, 225, -160);
		drawing.curveTo(225, -160, 170, -125, 120, -60);
		drawing.curveTo(120, -60, 100, -40, 130, -50);
		
		if(symetry)
		{
			drawing.transform(AffineTransform.getScaleInstance(1, -1));
		}
	}
	
	@Override
	public void paint(Graphics2D g)
	{
//		g.translate(- position.getX(), - position.getY());
		g.setStroke(Strokes.thickStroke);
		g.setColor(RALColor.RAL_9005);
		g.draw(drawing);
//		g.translate(position.getX(), position.getY());
	}
	
	@Override
	public Movable getMovableStatus()
	{
		return Movable.TRANSPARENT;
	}
	
}
