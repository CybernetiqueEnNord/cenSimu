package org.cen.components.gameboard.cup2016;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.math.position.Position2DMillimeter;

public class Pool extends AbstractGameBoardElement
{
	
	private Color borderColor = null;
	private Color innerColor = null;
	private Rectangle2D outer = null;
	private Rectangle2D inner = null;
	
	public Pool(String name, Point2D position, int height, int width, int borderwidth, Color borderColor, Color innerColor)
	{
		super(name, new Position2DMillimeter(position));
		this.borderColor = borderColor;
		this.innerColor = innerColor;
		outer = new Rectangle2D.Double(
				position.getX(), position.getY(), height, width
		);
		inner = new Rectangle2D.Double(
			position.getX()+borderwidth, position.getY()+borderwidth, 
			height - 2 * borderwidth, width - 2 * borderwidth
		);
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.translate(- position.getValue().getX(), - position.getValue().getY());
		g.setColor(borderColor);
		g.fill(outer);
		g.setColor(innerColor);
		g.fill(inner);
		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
}
