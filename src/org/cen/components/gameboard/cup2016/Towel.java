package org.cen.components.gameboard.cup2016;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.Movable;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

public class Towel extends AbstractGameBoardElement
{
	private Color color = null;
	private Path2D towelArea = null;
	private Path2D leftLine = null;
	private Path2D rightLine = null;
	
	public Towel(String string, Point2D position, Color color)
	{
		super(string, new Position2DMillimeter(position));
		this.color = color;
		towelArea = new Path2D.Double();
		towelArea.moveTo(position.getX(), position.getY());
		towelArea.lineTo(position.getX()+500, position.getY());
		towelArea.lineTo(position.getX()+500, position.getY()+300);
		towelArea.lineTo(position.getX(), position.getY()+300);
		towelArea.closePath();
		leftLine = new Path2D.Double();
		leftLine.moveTo(position.getX()+50, position.getY());
		leftLine.lineTo(position.getX()+100, position.getY());
		leftLine.lineTo(position.getX()+100, position.getY()+300);
		leftLine.lineTo(position.getX()+50, position.getY()+300);
		leftLine.closePath();
		rightLine = new Path2D.Double();
		leftLine.moveTo(position.getX()+400, position.getY());
		leftLine.lineTo(position.getX()+450, position.getY());
		leftLine.lineTo(position.getX()+450, position.getY()+300);
		leftLine.lineTo(position.getX()+400, position.getY()+300);
		leftLine.closePath();
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(color);
		g.fill(towelArea);
		g.setColor(RALColor.RAL_9016);
		g.fill(leftLine);
		g.fill(rightLine);
		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
	@Override
	public Movable getMovableStatus()
	{
		return Movable.TRANSPARENT;
	}
	
}
