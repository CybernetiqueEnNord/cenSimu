package org.cen.components.gameboard.cup2016;

import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.Movable;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

public class Rock extends AbstractGameBoardElement
{
	
	private Path2D outer = null;
	private Path2D inner = null;
	
	public Rock(String name, Point2D position, boolean b)
	{
		super(name, new Position2DMillimeter(position));
		int bigRadius = b ? 250 : -250;
		int litRadius = b ? 150 : -150;
		outer = new Path2D.Double();
		outer.moveTo(position.getX(), position.getY());
		outer.lineTo(position.getX() - 250, position.getY());
		outer.curveTo(
			position.getX() - 250, position.getY(), 
			position.getX() - 250, position.getY() + bigRadius, 
			position.getX(), position.getY() + bigRadius
		);
		outer.closePath();
		inner = new Path2D.Double();
		inner.moveTo(position.getX(), position.getY());
		inner.lineTo(position.getX() - 150, position.getY());
		inner.curveTo(
			position.getX() - 150, position.getY(), 
			position.getX() - 150, position.getY() + litRadius, 
			position.getX(), position.getY() + litRadius
		);
		inner.closePath();
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(RALColor.RAL_7032);
		g.fill(outer);
		g.setColor(RALColor.RAL_9005);
		g.draw(outer);
		g.setColor(RALColor.RAL_7032);
		g.fill(inner);
		g.setColor(RALColor.RAL_9005);
		g.draw(inner);
		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
	@Override
	public Movable getMovableStatus()
	{
		return Movable.INNAMOVIBLE;
	}
	
}
