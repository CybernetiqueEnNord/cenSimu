package org.cen.components.gameboard.cup2016;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D.Double;
import java.awt.geom.Rectangle2D;

import org.cen.components.gameboard.cup2015.Strokes;
import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.Movable;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

public class Shell extends AbstractGameBoardElement
{
	
	private Color color = null;
	private Ellipse2D outer = null;
	private Rectangle2D inner = null;
	
	public Shell(String name, Double position, Color color)
	{
		super(name, new Position2DMillimeter(position));
		this.color = color;
		outer = new Ellipse2D.Double(position.getX() - 29, position.getY() - 29, 58, 58);
		inner = new Rectangle2D.Double(position.getX() - 3, position.getY() - 25, 6, 50);
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.translate(- position.getValue().getX(), - position.getValue().getY());
		g.setStroke(Strokes.thinStroke);
		g.setColor(color);
		g.fill(outer);
		g.setColor(RALColor.RAL_9005);
		g.draw(outer);
		g.draw(inner);
		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
	@Override
	public Movable getMovableStatus()
	{
		return Movable.MOVABLE;
	}
	
}
