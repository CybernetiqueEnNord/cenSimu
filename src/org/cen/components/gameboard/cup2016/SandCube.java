package org.cen.components.gameboard.cup2016;

import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.cen.components.gameboard.cup2015.Strokes;
import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.Movable;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

public class SandCube extends AbstractGameBoardElement
{
	
	public SandCube(String name, Point2D position)
	{
		super(name, new Position2DMillimeter(position));
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		Rectangle2D d = new Rectangle2D.Double(position.getValue().getX() - 29, position.getValue().getY() - 29, 58, 58);
		g.translate(- position.getValue().getX(), - position.getValue().getY());
		g.setColor(RALColor.RAL_1007);
		g.fill(d);
		g.setColor(RALColor.RAL_9005);
		g.setStroke(Strokes.thinStroke);
		g.draw(d);
		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
	@Override
	public Movable getMovableStatus()
	{
		return Movable.MOVABLE;
	}
	
}
