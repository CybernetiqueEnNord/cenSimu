package org.cen.components.gameboard.cup2016;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.Movable;
import org.cen.components.math.position.Position2DMillimeter;

public class Sea extends AbstractGameBoardElement
{
	
	private Color color = null;
	private Path2D bounds = null;
	
	public Sea(String string, Point2D position, Color color)
	{
		super(string, new Position2DMillimeter(position));
		this.color = color;
		bounds = new Path2D.Double();
		bounds.moveTo(position.getX(), position.getY());
		bounds.lineTo(position.getX()+800, position.getY()); // bottom
		bounds.lineTo(position.getX()+800, position.getY()+3000); // right
		bounds.lineTo(position.getX(), position.getY()+3000); // top
		bounds.lineTo(position.getX(), position.getY()+2400); // top left
		bounds.curveTo(
			position.getX(), position.getY()+2400, 
			position.getX(), position.getY()+2300, 
			position.getX()+50, position.getY()+2200
		); // top angle
		bounds.curveTo(
			position.getX()+50, position.getY()+2200,
			position.getX()+500, position.getY()+1500,
			position.getX()+50, position.getY()+800
		); // large curve
		bounds.curveTo(
			position.getX()+50, position.getY()+800,
			position.getX(), position.getY()+700,
			position.getX(), position.getY()+600
		); // bottom angle
		bounds.closePath();
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(color);
		g.fill(bounds);
		// on ne peint pas les bords s'ils sont à la même hauteur que le reste
//		g.setColor(RALColor.RAL_9005);
//		g.draw(bounds);
		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
	@Override
	public Movable getMovableStatus()
	{
		return Movable.TRANSPARENT;
	}
	
}
