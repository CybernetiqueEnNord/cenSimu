package org.cen.components.gameboard.cup2016;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.List;

import org.cen.components.gameboard.generic.AbstractGameBoard;
import org.cen.components.gameboard.generic.Border;
import org.cen.components.gameboard.generic.IGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.position.Position2DMillimeter;

public class GameBoard2016 extends AbstractGameBoard {
	
	// purple
	public static final Color COLOR_A = RALColor.RAL_4008;
	
	// emerald green
	public static final Color COLOR_B = RALColor.RAL_6001;
	
	public static class Configuration1 extends GameBoard2016
	{
		public Configuration1()
		{
			super();
			getElements().add(new Shell("sh-alpha-1", new Point2D.Double(1825, 2925), COLOR_B));
			getElements().add(new Shell("sh-alpha-2", new Point2D.Double(1925, 2925), COLOR_B));
			getElements().add(new Shell("sh-alpha-3", new Point2D.Double(1925, 2825), COLOR_B));
			
			getElements().add(new Shell("sh-beta-1", new Point2D.Double(1250, 2800), RALColor.RAL_9016));
			getElements().add(new Shell("sh-beta-2", new Point2D.Double(1550, 2800), RALColor.RAL_9016));
			
			getElements().add(new Shell("sh-gamma-1", new Point2D.Double(1450, 2100), COLOR_B));
			getElements().add(new Shell("sh-gamma-2", new Point2D.Double(1650, 1800), COLOR_A));
			getElements().add(new Shell("sh-gamma-3", new Point2D.Double(1500, 1500), RALColor.RAL_9016));
			getElements().add(new Shell("sh-gamma-4", new Point2D.Double(1800, 1500), RALColor.RAL_9016));
			getElements().add(new Shell("sh-gamma-5", new Point2D.Double(1650, 1200), COLOR_B));
			getElements().add(new Shell("sh-gamma-6", new Point2D.Double(1450, 900), COLOR_A));
			
			getElements().add(new Shell("sh-delta-1", new Point2D.Double(1250, 200), RALColor.RAL_9016));
			getElements().add(new Shell("sh-delta-1", new Point2D.Double(1550, 200), RALColor.RAL_9016));
			
			getElements().add(new Shell("sh-epsilon-1", new Point2D.Double(1925, 175), COLOR_A));
			getElements().add(new Shell("sh-epsilon-2", new Point2D.Double(1925, 75), COLOR_A));
			getElements().add(new Shell("sh-epsilon-3", new Point2D.Double(1825, 75), COLOR_A));
		}
	}
	
	public static class Configuration2 extends GameBoard2016
	{
		public Configuration2()
		{
			super();
			getElements().add(new Shell("sh-alpha-1", new Point2D.Double(1825, 2925), COLOR_B));
			getElements().add(new Shell("sh-alpha-2", new Point2D.Double(1925, 2925), RALColor.RAL_9016));
			getElements().add(new Shell("sh-alpha-3", new Point2D.Double(1925, 2825), COLOR_B));
			
			getElements().add(new Shell("sh-beta-1", new Point2D.Double(1250, 2800), COLOR_B));
			getElements().add(new Shell("sh-beta-2", new Point2D.Double(1550, 2800), RALColor.RAL_9016));
			
			getElements().add(new Shell("sh-gamma-1", new Point2D.Double(1450, 2100), COLOR_B));
			getElements().add(new Shell("sh-gamma-2", new Point2D.Double(1650, 1800), COLOR_B));
			getElements().add(new Shell("sh-gamma-3", new Point2D.Double(1500, 1500), RALColor.RAL_9016));
			getElements().add(new Shell("sh-gamma-4", new Point2D.Double(1800, 1500), RALColor.RAL_9016));
			getElements().add(new Shell("sh-gamma-5", new Point2D.Double(1650, 1200), COLOR_A));
			getElements().add(new Shell("sh-gamma-6", new Point2D.Double(1450, 900), COLOR_A));
			
			getElements().add(new Shell("sh-delta-1", new Point2D.Double(1250, 200), COLOR_A));
			getElements().add(new Shell("sh-delta-2", new Point2D.Double(1550, 200), RALColor.RAL_9016));
			
			getElements().add(new Shell("sh-epsilon-1", new Point2D.Double(1925, 175), COLOR_A));
			getElements().add(new Shell("sh-epsilon-2", new Point2D.Double(1925, 75), RALColor.RAL_9016));
			getElements().add(new Shell("sh-epsilon-3", new Point2D.Double(1825, 75), COLOR_A));
		}
	}
	
	public static class Configuration3 extends GameBoard2016
	{
		public Configuration3()
		{
			super();
			getElements().add(new Shell("sh-alpha-1", new Point2D.Double(1825, 2925), COLOR_B));
			getElements().add(new Shell("sh-alpha-2", new Point2D.Double(1925, 2925), RALColor.RAL_9016));
			getElements().add(new Shell("sh-alpha-3", new Point2D.Double(1925, 2825), COLOR_B));
			
			getElements().add(new Shell("sh-beta-1", new Point2D.Double(1250, 2800), COLOR_B));
			getElements().add(new Shell("sh-beta-2", new Point2D.Double(1550, 2800), RALColor.RAL_9016));
			getElements().add(new Shell("sh-beta-3", new Point2D.Double(1250, 2300), COLOR_B));
			getElements().add(new Shell("sh-beta-4", new Point2D.Double(1550, 2300), RALColor.RAL_9016));
			
			getElements().add(new Shell("sh-gamma-1", new Point2D.Double(1650, 1800), COLOR_B));
			getElements().add(new Shell("sh-gamma-2", new Point2D.Double(1650, 1200), COLOR_A));
			
			getElements().add(new Shell("sh-delta-3", new Point2D.Double(1250, 700), COLOR_A));
			getElements().add(new Shell("sh-delta-4", new Point2D.Double(1550, 700), RALColor.RAL_9016));
			getElements().add(new Shell("sh-delta-1", new Point2D.Double(1250, 200), COLOR_A));
			getElements().add(new Shell("sh-delta-2", new Point2D.Double(1550, 200), RALColor.RAL_9016));
			
			getElements().add(new Shell("sh-epsilon-1", new Point2D.Double(1925, 175), COLOR_A));
			getElements().add(new Shell("sh-epsilon-2", new Point2D.Double(1925, 75), RALColor.RAL_9016));
			getElements().add(new Shell("sh-epsilon-3", new Point2D.Double(1825, 75), COLOR_A));
		}
	}
	
	public static class Configuration4 extends GameBoard2016
	{
		public Configuration4()
		{
			super();
			getElements().add(new Shell("sh-alpha-1", new Point2D.Double(1825, 2925), COLOR_A));
			getElements().add(new Shell("sh-alpha-2", new Point2D.Double(1925, 2925), RALColor.RAL_9016));
			getElements().add(new Shell("sh-alpha-3", new Point2D.Double(1925, 2825), COLOR_A));
			
			getElements().add(new Shell("sh-beta-1", new Point2D.Double(1250, 2800), COLOR_B));
			getElements().add(new Shell("sh-beta-2", new Point2D.Double(1550, 2800), COLOR_B));
			getElements().add(new Shell("sh-beta-3", new Point2D.Double(1250, 2300), COLOR_B));
			getElements().add(new Shell("sh-beta-4", new Point2D.Double(1550, 2300), RALColor.RAL_9016));
			
			getElements().add(new Shell("sh-gamma-1", new Point2D.Double(1650, 1800), RALColor.RAL_9016));
			getElements().add(new Shell("sh-gamma-2", new Point2D.Double(1650, 1200), RALColor.RAL_9016));
			
			getElements().add(new Shell("sh-delta-3", new Point2D.Double(1250, 700), COLOR_A));
			getElements().add(new Shell("sh-delta-4", new Point2D.Double(1550, 700), RALColor.RAL_9016));
			getElements().add(new Shell("sh-delta-1", new Point2D.Double(1250, 200), COLOR_A));
			getElements().add(new Shell("sh-delta-2", new Point2D.Double(1550, 200), COLOR_A));
			
			getElements().add(new Shell("sh-epsilon-1", new Point2D.Double(1925, 175), COLOR_B));
			getElements().add(new Shell("sh-epsilon-2", new Point2D.Double(1925, 75), RALColor.RAL_9016));
			getElements().add(new Shell("sh-epsilon-3", new Point2D.Double(1825, 75), COLOR_B));
		}
	}
	
	public static class Configuration5 extends GameBoard2016
	{
		public Configuration5()
		{
			super();
			getElements().add(new Shell("sh-alpha-1", new Point2D.Double(1825, 2925), COLOR_B));
			getElements().add(new Shell("sh-alpha-2", new Point2D.Double(1925, 2925), RALColor.RAL_9016));
			getElements().add(new Shell("sh-alpha-3", new Point2D.Double(1925, 2825), RALColor.RAL_9016));
			
			getElements().add(new Shell("sh-beta-1", new Point2D.Double(1250, 2800), COLOR_B));
			getElements().add(new Shell("sh-beta-2", new Point2D.Double(1550, 2800), COLOR_B));
			getElements().add(new Shell("sh-beta-3", new Point2D.Double(1250, 2300), COLOR_B));
			getElements().add(new Shell("sh-beta-4", new Point2D.Double(1550, 2300), COLOR_A));
			getElements().add(new Shell("sh-beta++", new Point2D.Double(1850, 2350), RALColor.RAL_9016));
			
			getElements().add(new Shell("sh-gamma-1", new Point2D.Double(1650, 1800), RALColor.RAL_9016));
			getElements().add(new Shell("sh-gamma-2", new Point2D.Double(1650, 1200), RALColor.RAL_9016));
			
			getElements().add(new Shell("sh-delta++", new Point2D.Double(1850, 650), RALColor.RAL_9016));
			getElements().add(new Shell("sh-delta-3", new Point2D.Double(1250, 700), COLOR_A));
			getElements().add(new Shell("sh-delta-4", new Point2D.Double(1550, 700), COLOR_B));
			getElements().add(new Shell("sh-delta-1", new Point2D.Double(1250, 200), COLOR_A));
			getElements().add(new Shell("sh-delta-2", new Point2D.Double(1550, 200), COLOR_A));
			
			getElements().add(new Shell("sh-epsilon-1", new Point2D.Double(1925, 175), RALColor.RAL_9016));
			getElements().add(new Shell("sh-epsilon-2", new Point2D.Double(1925, 75), RALColor.RAL_9016));
			getElements().add(new Shell("sh-epsilon-3", new Point2D.Double(1825, 75), COLOR_B));
		}
	}
	
	private GameBoard2016()
	{
		super();
		
		elements.add(new Border("top-dune", 200, 22, RALColor.RAL_9005, new Position2DMillimeter(0d, 2178d), new AngleRadian(0.0), 0));
		elements.add(new Border("bottom-dune", 200, 20, RALColor.RAL_9005, new Position2DMillimeter(0d, 800d), new AngleRadian(0.0), 0));
		
		elements.add(new Arc("top-arc", 0, AngleRadian.PI_2, 589, COLOR_B, new Point2D.Double(750, 1500), new AngleRadian(0.0), 0));
		elements.add(new Arc("bottom-arc", 0, - AngleRadian.PI_2, 589, COLOR_A, new Point2D.Double(750, 1500), new AngleRadian(0.0), 0));
		
		elements.add(new Border("vertical-fort", 22, 1200, RALColor.RAL_1007, new Position2DMillimeter(750d, 900d), new AngleRadian(0.0), 0));
		elements.add(new Border("horizontal-fort", 600, 44, RALColor.RAL_1007, new Position2DMillimeter(750d, 1476d), new AngleRadian(0.0), 0));
		
		elements.add(new Dolphin("dolphin-top", new Point2D.Double(0, 3000), false));
		elements.add(new Dolphin("dolphin-bottom", new Point2D.Double(0, 0), true));
		
		elements.add(new Towel("towel-bottom", new Point2D.Double(600, 0), COLOR_A));
		elements.add(new Towel("towel-top", new Point2D.Double(600, 2700), COLOR_B));
		
		elements.add(new Sea("sea", new Point2D.Double(1200, 0), RALColor.RAL_5012));
		
		elements.add(new Rock("rock-top", new Point2D.Double(2000, 3000), false));
		elements.add(new Rock("rock-bottom", new Point2D.Double(2000, 0), true));
		
		elements.add(new Border("top-aquarium", 254, 22, RALColor.RAL_9005, new Position2DMillimeter(1978d, 2050d), new AngleRadian(0.0), 0));
		elements.add(new Border("bottom-aquarium", 254, 22, RALColor.RAL_9005, new Position2DMillimeter(1978d, 928d), new AngleRadian(0.0), 0));
		elements.add(new Net("net-for-fishs", new Point2D.Double(2022, 950), 210, 1100, RALColor.RAL_8002));
		elements.add(new Pool("bottom-pool", new Point2D.Double(2022, 498), 210, 420, 5, RALColor.RAL_9005, RALColor.RAL_5015));
		elements.add(new Pool("top-pool", new Point2D.Double(2022, 2082), 210, 420, 5, RALColor.RAL_9005, RALColor.RAL_5015));
		
		elements.add(new Closet("closet-top-top", new Point2D.Double(-50, 2650), false));
		elements.add(new Closet("closet-top-bottom", new Point2D.Double(-50, 2350), false));
		elements.add(new Closet("closet-bottom-top", new Point2D.Double(-50, 550), true));
		elements.add(new Closet("closet-bottom-bottom", new Point2D.Double(-50, 250), true));
		
		elements.add(new SandCube("sc-alpha-1-1", new Point2D.Double(879, 2391)));
		elements.add(new SandCube("sc-alpha-1-2", new Point2D.Double(937, 2391)));
		elements.add(new SandCube("sc-alpha-2-1", new Point2D.Double(879, 2333)));
		elements.add(new SandCube("sc-alpha-2-2", new Point2D.Double(937, 2333)));
		elements.add(new SandCylr("sl-alpha", new Point2D.Double(908, 2362)));
		
		elements.add(new SandCube("sc-beta-1-1", new Point2D.Double(29, 2149)));
		elements.add(new SandCube("sc-beta-1-2", new Point2D.Double(87, 2149)));
		elements.add(new SandCube("sc-beta-2-1", new Point2D.Double(29, 2091)));
		elements.add(new SandCube("sc-beta-2-2", new Point2D.Double(87, 2091)));
		elements.add(new SandCylr("sl-beta", new Point2D.Double(58, 2120)));
		
		elements.add(new SandCube("sc-gamma-1-1", new Point2D.Double(29, 909)));
		elements.add(new SandCube("sc-gamma-1-2", new Point2D.Double(87, 909)));
		elements.add(new SandCube("sc-gamma-2-2", new Point2D.Double(29, 851)));
		elements.add(new SandCube("sc-gamma-2-2", new Point2D.Double(87, 851)));
		elements.add(new SandCylr("sl-gamma", new Point2D.Double(58, 880)));
		
		elements.add(new SandCube("sc-delta-1-1", new Point2D.Double(879, 667)));
		elements.add(new SandCube("sc-delta-1-2", new Point2D.Double(937, 667)));
		elements.add(new SandCube("sc-delta-2-1", new Point2D.Double(879, 609)));
		elements.add(new SandCube("sc-delta-2-2", new Point2D.Double(937, 609)));
		elements.add(new SandCylr("sl-delta", new Point2D.Double(908, 638)));
		
		elements.add(new SandCube("sc-epsilon-1-1", new Point2D.Double(29, 1732)));
		elements.add(new SandCube("sc-epsilon-2-1", new Point2D.Double(29, 1674)));
		elements.add(new SandCylr("sl-epsilon-2-1", new Point2D.Double(29, 1674)));
		elements.add(new SandCube("sc-epsilon-3-1", new Point2D.Double(29, 1616)));
		elements.add(new SandCylr("sl-epsilon-3-1", new Point2D.Double(29, 1616)));
		elements.add(new SandCube("sc-epsilon-4-1", new Point2D.Double(29, 1558)));
		elements.add(new SandCylr("sl-epsilon-4-1", new Point2D.Double(29, 1558)));
		elements.add(new SandCube("sc-epsilon-5-1", new Point2D.Double(29, 1500)));
		elements.add(new SandCylr("sl-epsilon-5-1", new Point2D.Double(29, 1500)));
		elements.add(new SandCube("sc-epsilon-6-1", new Point2D.Double(29, 1442)));
		elements.add(new SandCylr("sl-epsilon-6-1", new Point2D.Double(29, 1442)));
		elements.add(new SandCube("sc-epsilon-7-1", new Point2D.Double(29, 1384)));
		elements.add(new SandCylr("sl-epsilon-7-1", new Point2D.Double(29, 1384)));
		elements.add(new SandCube("sc-epsilon-8-1", new Point2D.Double(29, 1326)));
		elements.add(new SandCylr("sl-epsilon-8-1", new Point2D.Double(29, 1326)));
		elements.add(new SandCube("sc-epsilon-9-1", new Point2D.Double(29, 1268)));
		
		elements.add(new SandCube("sc-epsilon-2-4", new Point2D.Double(87, 1558)));
		elements.add(new SandCylr("sl-epsilon-2-4", new Point2D.Double(87, 1558)));
		elements.add(new SandCube("sc-epsilon-2-5", new Point2D.Double(87, 1500)));
		elements.add(new SandCylr("sl-epsilon-2-5", new Point2D.Double(87, 1500)));
		elements.add(new SandCube("sc-epsilon-2-6", new Point2D.Double(87, 1442)));
		elements.add(new SandCylr("sl-epsilon-2-7", new Point2D.Double(87, 1442)));
		
		elements.add(new SandCube("sc-epsilon-3-5", new Point2D.Double(145, 1500)));
		elements.add(new SandCylr("sl-epsilon-3-5", new Point2D.Double(145, 1500)));
		
	}
	
	@Override
	public List<IGameBoardElement> getElements()
	{
		return elements;
	}
	
	@Override
	public Rectangle2D getGameplayBounds()
	{
		return gameplayBounds;
	}
	
	@Override
	public Rectangle2D getVisibleBounds()
	{
		return visibleBounds;
	}
	
	@Override
	protected Color getDefaultBoardColor()
	{
		return RALColor.RAL_1023;
	}

}
