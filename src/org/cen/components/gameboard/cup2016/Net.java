package org.cen.components.gameboard.cup2016;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.math.position.Position2DMillimeter;

public class Net extends AbstractGameBoardElement
{
	
	private Color color = null;
	private Rectangle2D outer = null;
	
	public Net(String name, Point2D position, int length, int height, Color color)
	{
		super(name, new Position2DMillimeter(position));
		this.color = color;
		outer = new Rectangle2D.Double();
		outer.setRect(position.getX(), position.getY(), length, height);
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(color);
		g.fill(outer);
		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
}
