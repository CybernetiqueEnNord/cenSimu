package org.cen.components.gameboard.cup2018;

import java.awt.Color;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoard2018;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.gameboard.generic.StartArea;
import org.cen.components.math.position.Position2DMillimeter;

/**
 * GameBoard2018 class file.
 * 
 * This class represents all the graphic elements that represents the game board for year 2018.
 * 
 * @author Anastaszor
 */
public class GameBoard2018 extends AbstractGameBoard2018
{
	
	public static final Color COLOR_A = RALColor.RAL_2010;
	public static final Color COLOR_B = RALColor.RAL_6018;
	
	public GameBoard2018()
	{
		super(COLOR_A, COLOR_B);
		
		getElements().add(new SupportFleur("support-fleur-top", new Point2D.Double(2137, 1385), COLOR_B));
		getElements().add(new SupportFleur("support-fleur-bottom", new Point2D.Double(2137, 1615), COLOR_A));
		getElements().add(new RampeAbeille("rampe-abeille-top", new Point2D.Double(2150, 3044), COLOR_A, false));
		getElements().add(new RampeAbeille("rampe-abeille-bottom", new Point2D.Double(2150, -44), COLOR_B, true));
		getElements().add(new StationEpuration("station-top", new Point2D.Double(1875, 1500), COLOR_B, COLOR_A));
		getElements().add(new ChateaudEau("chateau-top", new Point2D.Double(-207, 2825), COLOR_A, false));
		getElements().add(new ChateaudEau("chateau-bottom", new Point2D.Double(-207, 175), COLOR_B, true));
		getElements().add(new Robinet("robinet-top", new Point2D.Double(840, 2928), COLOR_A, 0d));
		getElements().add(new Robinet("robinet-topright", new Point2D.Double(1928, 2390), COLOR_B, -Math.PI/2));
		getElements().add(new Robinet("robinet-btmright", new Point2D.Double(1928, 610), COLOR_A, -Math.PI/2));
		getElements().add(new Robinet("robinet-btm", new Point2D.Double(840, 72), COLOR_B, Math.PI));
		
		getElements().add(new StartArea(COLOR_A, new Position2DMillimeter(new Point2D.Double(0, 2600)), 650, 400));
		getElements().add(new StartArea(COLOR_B, new Position2DMillimeter(new Point2D.Double(0, 0)), 650, 400));
		
		getElements().add(new ZoneConstruction("buildzone-top", new Point2D.Double(200, 2000), COLOR_A, false));
		getElements().add(new ZoneConstruction("buildzone-btm", new Point2D.Double(200, 1000), COLOR_B, true));
		
		getElements().add(new FollowLine2018("follow-top", new Point2D.Double(0, 1500), false));
		getElements().add(new FollowLine2018("follow-btm", new Point2D.Double(0, 1500), true));
		
		
		getElements().add(new BuildingBlock("bblock-t1t", new Point2D.Double(540, 2150 + 58), RALColor.RAL_6018));
		getElements().add(new BuildingBlock("bblock-t1l", new Point2D.Double(540 - 58, 2150), RALColor.RAL_9005));
		getElements().add(new BuildingBlock("bblock-t1c", new Point2D.Double(540, 2150), RALColor.RAL_1023));
		getElements().add(new BuildingBlock("bblock-t1b", new Point2D.Double(540, 2150 - 58), RALColor.RAL_2010));
		getElements().add(new BuildingBlock("bblock-t1r", new Point2D.Double(540 + 58, 2150), RALColor.RAL_5015));
		
		getElements().add(new BuildingBlock("bblock-t2t", new Point2D.Double(1190, 2700 + 58), RALColor.RAL_6018));
		getElements().add(new BuildingBlock("bblock-t2l", new Point2D.Double(1190 - 58, 2700), RALColor.RAL_9005));
		getElements().add(new BuildingBlock("bblock-t2c", new Point2D.Double(1190, 2700), RALColor.RAL_1023));
		getElements().add(new BuildingBlock("bblock-t2b", new Point2D.Double(1190, 2700 - 58), RALColor.RAL_2010));
		getElements().add(new BuildingBlock("bblock-t2r", new Point2D.Double(1190 + 58, 2700), RALColor.RAL_5015));
		
		getElements().add(new BuildingBlock("bblock-b3t", new Point2D.Double(1500, 1900 + 58), RALColor.RAL_6018));
		getElements().add(new BuildingBlock("bblock-b3l", new Point2D.Double(1500 - 58, 1900), RALColor.RAL_9005));
		getElements().add(new BuildingBlock("bblock-b3c", new Point2D.Double(1500, 1900), RALColor.RAL_1023));
		getElements().add(new BuildingBlock("bblock-b3b", new Point2D.Double(1500, 1900 - 58), RALColor.RAL_2010));
		getElements().add(new BuildingBlock("bblock-b3r", new Point2D.Double(1500 + 58, 1900), RALColor.RAL_5015));
		
		
		getElements().add(new BuildingBlock("bblock-b1t", new Point2D.Double(540, 850 + 58), RALColor.RAL_2010));
		getElements().add(new BuildingBlock("bblock-b1l", new Point2D.Double(540 - 58, 850), RALColor.RAL_9005));
		getElements().add(new BuildingBlock("bblock-b1c", new Point2D.Double(540, 850), RALColor.RAL_1023));
		getElements().add(new BuildingBlock("bblock-b1b", new Point2D.Double(540, 850 - 58), RALColor.RAL_6018));
		getElements().add(new BuildingBlock("bblock-b1r", new Point2D.Double(540 + 58, 850), RALColor.RAL_5015));
		
		getElements().add(new BuildingBlock("bblock-b2t", new Point2D.Double(1190, 300 + 58), RALColor.RAL_2010));
		getElements().add(new BuildingBlock("bblock-b2l", new Point2D.Double(1190 - 58, 300), RALColor.RAL_9005));
		getElements().add(new BuildingBlock("bblock-b2c", new Point2D.Double(1190, 300), RALColor.RAL_1023));
		getElements().add(new BuildingBlock("bblock-b2b", new Point2D.Double(1190, 300 - 58), RALColor.RAL_6018));
		getElements().add(new BuildingBlock("bblock-b2r", new Point2D.Double(1190 + 58, 300), RALColor.RAL_5015));
		
		getElements().add(new BuildingBlock("bblock-b3t", new Point2D.Double(1500, 1100 + 58), RALColor.RAL_2010));
		getElements().add(new BuildingBlock("bblock-b3l", new Point2D.Double(1500 - 58, 1100), RALColor.RAL_9005));
		getElements().add(new BuildingBlock("bblock-b3c", new Point2D.Double(1500, 1100), RALColor.RAL_1023));
		getElements().add(new BuildingBlock("bblock-b3b", new Point2D.Double(1500, 1100 - 58), RALColor.RAL_6018));
		getElements().add(new BuildingBlock("bblock-b3r", new Point2D.Double(1500 + 58, 1100), RALColor.RAL_5015));
		
	}
	
	@Override
	protected Color getDefaultBoardColor()
	{
		return RALColor.RAL_7032;
	}
	
}
