package org.cen.components.gameboard.cup2018;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D.Double;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.IGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

/**
 * TourCentrale class file.
 * 
 * This class represents a new (2018+) tour centrale element.
 * 
 * The element has this shape : (scale about ~ 1 vertical unit = 10mm and 1 horizontal unit = 5mm) 
 * 
 * The cotations are in mm.
 * 
 * 
 * 
 *     |------------------200-------------------|
 * 
 * |22-|                                    |22-|
 * 
 *                                          +---+   ---
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |   260
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                   10---|-|               |   |    |
 *                                          |   |    |
 *     +------------------+ +---------------+---+   ---
 *     |                  | |                   |    |
 *     |                  | |                   |    |
 *     |                  | |                   |    |
 *     |                  | |                   |    |
 *     |                  | |                   |    |
 *     |                  | |                   |    |
 *     |                  | |                   |    |
 *     |                  | |                   |    |
 *     |                  | |                   |    |
 * +---+                  \_/                   |   200   ---
 * |   |                                        |    |     |
 * |   |                                        |    |     |
 * |   |                                        |    |     |
 * |   |                                        |    |     |
 * |   |                                        |    |     |
 * |   |                                        |    |     |
 * |   |                                        |    |     |
 * |   |                                        |    |     |
 * |   +-------------------x--------------------+   ---   200
 * |   |                                        |    |     |
 * |   |                                        |    |     |
 * |   |                                        |    |     |
 * |   |                                        |    |     |
 * |   |                                        |    |     |
 * |   |                                        |    |     |
 * |   |                                        |    |     |
 * |   |                                        |    |     |
 * +---+                  /-\                   |   200   ---
 *     |                  | |                   |    |
 *     |                  | |                   |    |
 *     |                  | |                   |    |
 *     |                  | |                   |    |
 *     |                  | |                   |    |
 *     |                  | |                   |    |
 *     |                  | |                   |    |
 *     |                  | |                   |    |
 *     |                  | |                   |    |
 *     +------------------+ +---------------+---+   ---
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |   260
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          |   |    |
 *                                          +---+   ---
 * 
 * The outer path is the path for both supports.
 * The top path is the platform for the color A
 * The btm path is the platform for the color B
 * The position of the figure is placed at the middle between the two platforms.
 * The piece is contained in a square [left:-122, right:100] x [bottom:-460, top:460]
 * 
 * @author Anastaszor
 */
public class TourCentrale extends AbstractGameBoardElement implements IGameBoardElement
{
	
	private Color colorA = null;
	private Color colorB = null;
	private Path2D outer = null;
	private Path2D top = null;
	private Path2D btm = null;
	
	public TourCentrale(String name, Double position, Color colorA, Color colorB)
	{
		super(name, new Position2DMillimeter(position));
		this.colorA = colorA;
		this.colorB = colorB;
		
		outer = new Path2D.Double();
		// begin the path at the inner bottom right position
		outer.moveTo(position.getX() + 78, position.getY() - 460);
		// go to outer bottom right position
		outer.lineTo(position.getX() + 100, position.getY() - 460);
		// go to outer top right position
		outer.lineTo(position.getX() + 100, position.getY() + 460);
		// go to inner top right position
		outer.lineTo(position.getX() + 78, position.getY() + 460);
		// go to inner top middle right position
		outer.lineTo(position.getX() + 78, position.getY() + 75);
		// go to inner top middle left position
		outer.lineTo(position.getX() - 122, position.getY() + 75);
		// go to inner bottom middle left position
		outer.lineTo(position.getX() - 122, position.getY() - 75);
		// go to inner bottom middle right position
		outer.lineTo(position.getX() + 78, position.getY() - 75);
		// go to inner bottom right position
		outer.closePath();
		
		top = new Path2D.Double();
		// begin the path at the bottom left position
		top.moveTo(position.getX() - 100, position.getY());
		// go to bottom right position
		top.lineTo(position.getX() + 100, position.getY());
		// go to top right position
		top.lineTo(position.getX() + 100, position.getY() + 200);
		// go to middle top right position
		top.lineTo(position.getX() + 5, position.getY() + 200);
		// go to inner right position
		top.lineTo(position.getX() + 5, position.getY() + 100);
		// circle around
		top.curveTo(
			position.getX() + 5, position.getY() + 95,	// position of the bottom right corner of the square containing the semi circle
			position.getX() - 5, position.getY() + 95,	// position of the bottom left corner of the square containing the semi circle
			position.getX() - 5, position.getY() + 100	// position at the middle left of the square containing the semi circle
		);
		// go to middle top left position
		top.lineTo(position.getX() - 5, position.getY() + 200);
		// go to top left position
		top.lineTo(position.getX() -100, position.getY() + 200);
		// go to bottom left position
		top.closePath();
		
		// this path is made clockwise
		btm = new Path2D.Double();
		// begin the path at the top left position
		btm.moveTo(position.getX() - 100, position.getY());
		// go to top right position
		btm.lineTo(position.getX() + 100, position.getY());
		// go to bottom right position
		btm.lineTo(position.getX() + 100, position.getY() - 200);
		// go to bottom middle right position
		btm.lineTo(position.getX() + 5, position.getY() - 200);
		// go to middle middle right position
		btm.lineTo(position.getX() + 5, position.getY() - 100);
		// circle around
		btm.curveTo(
			position.getX() + 5, position.getY() - 95,	// position at the top right corner of the square containing the semi circle
			position.getX() - 5, position.getY() - 95,	// position at the top left corner of the square containing the semi circle
			position.getX() - 5, position.getY() - 100	// position at the middle left of the square containing the semi circle
		);
		// go to middle bottom left position
		btm.lineTo(position.getX() - 5, position.getY() - 200);
		// go to bottom left position
		btm.lineTo(position.getX() - 100, position.getY() - 200);
		// go to top left position
		btm.closePath();
		
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		
		g.setColor(RALColor.RAL_7032);
		g.fill(outer);	// fill in ground color
		g.setColor(RALColor.RAL_9005);
		g.draw(outer);	// draw borders in black
		
		g.setColor(this.colorB);
		g.fill(top);	// fill in player color
		g.setColor(RALColor.RAL_9005);
		g.draw(top);	// draw borders in black
		
		g.setColor(this.colorA);
		g.fill(btm);
		g.setColor(RALColor.RAL_9005);
		g.draw(btm);
		
		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
}
