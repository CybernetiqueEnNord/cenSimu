package org.cen.components.gameboard.cup2018;

import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.IGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

/**
 * FollowLine2018 class file.
 * 
 * This class represents the line for the follower robots 2018.
 * 
 * The path looks roughly like this :
 * 
 *      |
 *      |             _.
 *      |    |      .¯
 *      +----+------+-
 *      |    |
 *      |
 * -----+
 *      |
 *      |
 *     _|_
 * 
 * The outer path is the whole path of the line. It contains multiple segments.
 * The position of the figure is at the bottom left of the figure.
 * The piece is contained in a square [left:0, right:2000] x [bottom:0, top:1500]
 * 
 * @author Anastaszor
 */
public class FollowLine2018 extends AbstractGameBoardElement implements IGameBoardElement
{
	
	private Path2D outer = null;
	
	private boolean invertOnYAxis = false;
	
	public FollowLine2018(String name, Point2D position, boolean revertOnYAxis)
	{
		super(name, new Position2DMillimeter(position));
		this.invertOnYAxis = revertOnYAxis;
		
		outer = new Path2D.Double();
		// bottom left path
		/*
		 * -----+
		 *      |
		 *      |
		 *     _|
		 */
		// begin at inner top left position
		outer.moveTo(0, 340);
		// go to inner top right position
		outer.lineTo(320, 340);
		// go to inner bottom right position
		outer.lineTo(320, 30);
		// go to inner bottom left position
		outer.lineTo(250, 30);
		// go to outer bottom left position
		outer.lineTo(250, 0);
		// go to outer bottom inner left position
		outer.lineTo(270, 0);
		// go to outer bottom inner left position
		outer.lineTo(270, 10);
		// go to outer bottom right position
		outer.lineTo(340, 10);
		// go to outer top right position
		outer.lineTo(340, 360);
		// go to outer top left position
		outer.lineTo(0, 360);
		// go to inner top left position
		outer.closePath();
		
		// top left path
		/*
		 *      |
		 *      |
		 *      |
		 *      |
		 *      |
		 * -----+
		 */
		// begin at outer bottom left position
		outer.moveTo(0, 380);
		// go to outer bottom right position
		outer.lineTo(340, 380);
		// go to outer top right position
		outer.lineTo(340, 1500);
		// go to inner top right position
		outer.lineTo(320, 1500);
		// go to inner bottom right position
		outer.lineTo(320, 400);
		// go to inner bottom left position
		outer.lineTo(0, 400);
		// go to outer bottom left position
		outer.closePath();
		
		
		// bottom right path
		/*
		 *               _.
		 *             .¯
		 * +----+------+-
		 * |    |
		 * |
		 * |
		 * |_
		 */
		// begin at outer bottom left position
		outer.moveTo(360, 10);
		// go to outer bottom left position
		outer.lineTo(430, 10);
		// go to outer bottom left position
		outer.lineTo(430, 0);
		// go to inner bottom left position
		outer.lineTo(450, 0);
		// go to inner bottom left position
		outer.lineTo(450, 30);
		// go to inner bottom left position
		outer.lineTo(380, 30);
		// go to inner top left position
		outer.lineTo(380, 860);
		// go to inner top middle position
		outer.lineTo(810, 860);
		// go to inner bottom middle position
		outer.lineTo(810, 630);
		// go to inner bottom middle position
		outer.lineTo(870, 630);
		// go to inner top middle position
		outer.lineTo(870, 860);
		// go to inner top right position
		outer.lineTo(1700, 860);
		// go to inner sup right position
		outer.lineTo(1700, 920);
		// go to inner sup left position
		outer.lineTo(1570, 920);
		// circle around 1940,920
		outer.quadTo(1570, 1260, 1940, 1260);
		// go to sup right position
		outer.lineTo(2000, 1260);
		// go to outer sup right position
		outer.lineTo(2000, 1280);
		// go to the outer sup right position
		outer.lineTo(1940, 1280);
		// circle around 1940, 920
		outer.quadTo(1550, 1280, 1550, 920);
		// go to outer right position
		outer.lineTo(1550, 900);
		// go to inner sup position
		outer.lineTo(1680, 900);
		// go to outer sub position
		outer.lineTo(1680, 880);
		// go to outer middle position
		outer.lineTo(850, 880);
		// go to outer middle position
		outer.lineTo(850, 650);
		// go to outer middle position
		outer.lineTo(830, 650);
		// go to outer middle position
		outer.lineTo(830, 880);
		// go to outer right position
		outer.lineTo(360, 880);
		// go to bottom right position
		outer.closePath();
		
		// top right path
		/*
		 *  |
		 *  |            _.
		 *  |   |      .¯
		 *  +---+------+
		 */
		// begin at the bottom left position
		outer.moveTo(360, 900);
		// go to the bottom middle position
		outer.lineTo(830, 900);
		// go to the bottom middle position
		outer.lineTo(830, 1200);
		// go to the bottom middle position
		outer.lineTo(850, 1200);
		// go to the bottom middle position
		outer.lineTo(850, 900);
		// go to the bottom right position
		outer.lineTo(1530, 900);
		// go to the bottom right position
		outer.lineTo(1530, 920);
		// circle around 1530, 1940
		outer.quadTo(1530, 1300, 1940, 1300);
		// go to the top right position
		outer.lineTo(2000, 1300);
		// go to the sup right position
		outer.lineTo(2000, 1320);
		// go to the sup right position
		outer.lineTo(1940, 1320);
		// circle around 1530, 1940
		outer.quadTo(1510, 1320, 1510, 920);
		// go to the top middle position
		outer.lineTo(870, 920);
		// go to the sup middle position
		outer.lineTo(870, 1220);
		// go to the sup middle position
		outer.lineTo(810, 1220);
		// go to the top middle position
		outer.lineTo(810, 920);
		// go to the top left position
		outer.lineTo(380, 920);
		// go to the sup left position
		outer.lineTo(380, 1500);
		// go to the sup left position
		outer.lineTo(360, 1500);
		// go to the bottom left position
		outer.closePath();
		
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
//		g.translate(-position.getValue().getX(), -position.getValue().getY());
		
		g.setColor(RALColor.RAL_9005);
		g.fill(outer);	// fill in black
		
//		g.translate(position.getValue().getX(), position.getValue().getY());
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
	}
	
}
