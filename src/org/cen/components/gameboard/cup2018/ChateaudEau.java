package org.cen.components.gameboard.cup2018;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D.Double;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.IGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

/**
 * ChateaudEau class file.
 * 
 * This class represents a new chateau d'eau element.
 * 
 * The element has this shape : (scale about ~ 1 vertical unit = 10mm and 1 horizontal unit = 5mm) 
 * 
 * The cotations are in mm.
 * 
 * +------------------------------------------------------------------------+  ---  ---
 * |                                                                        |   10   |
 * +  +-------------------------------------------------------------------+ +  ---   |
 * |  |                                                                   | |   20   |
 * |  |   +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+ +  ---   |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |       370
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  |   ~                                                               | |        |
 * |  +---+---------------------------------------------------------------+ |        |
 * |                                                                        |        |
 * +--+---+-----------------------------------------------------------------|       ---
 * 
 * The outer path is the outer square of the castle.
 * The inner path is the inner square of the castle.
 * The net path if the path that represents the net. 
 * The position of the castle is at the center of the outer square.
 * The piece is contained in a square [left:-185, right:185] x [bottom:-185, top:185]
 * 
 * @author Anastaszor
 */
public class ChateaudEau extends AbstractGameBoardElement implements IGameBoardElement
{
	
	private Color color = null;
	private Path2D outer = null;
	private Path2D inner = null;
	private Path2D net = null;
	private boolean invertOnYAxis = false;
	
	public ChateaudEau(String name, Double position, Color color, boolean invertOnYAxis)
	{
		super(name, new Position2DMillimeter(position));
		this.color = color;
		this.invertOnYAxis = invertOnYAxis;
		
		outer = new Path2D.Double();
		// begin the path at the bottom left position
		outer.moveTo(position.getX() - 185, position.getY() - 185);
		// go to bottom right
		outer.lineTo(position.getX() + 185, position.getY() - 185);
		// go to top right
		outer.lineTo(position.getX() + 185, position.getY() + 185);
		// go to top left
		outer.lineTo(position.getX() - 185, position.getY() + 185);
		// go to bottom left
		outer.closePath();
		
		inner = new Path2D.Double();
		// begin the path at the bottom left position
		inner.moveTo(position.getX() - 175, position.getY() - 175);
		// go to bottom right
		inner.lineTo(position.getX() + 175, position.getY() - 175);
		// go to top right
		inner.lineTo(position.getX() + 175, position.getY() + 175);
		// go to top left
		inner.lineTo(position.getX() - 175, position.getY() + 175);
		// go to bottom lft
		inner.closePath();
		
		net = new Path2D.Double();
		// begin the path at the outer bottom left position
		net.moveTo(position.getX() - 155, position.getY() - 175);
		// go to inner bottom left
		net.lineTo(position.getX() - 150, position.getY() - 175);
		// go to inner top left
		net.lineTo(position.getX() - 150, position.getY() + 150);
		// go to inner top right
		net.lineTo(position.getX() + 175, position.getY() + 150);
		// go to outer top right
		net.lineTo(position.getX() + 175, position.getY() + 155);
		// go to outer top left
		net.lineTo(position.getX() - 155, position.getY() + 155);
		// go to outer bottom left
		net.closePath();
		
		
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		
		g.setColor(this.color);
		g.fill(outer);	// fill in player color
		g.setColor(RALColor.RAL_9005);
		g.draw(outer);	// draw borders in black
		
		g.setColor(this.color);
		g.fill(inner);	// fill in player color
		g.setColor(RALColor.RAL_9005);
		g.draw(inner);	// draw borders in black
		
		g.setColor(RALColor.RAL_7032);
		g.fill(net);
		g.setColor(RALColor.RAL_9005);
		g.draw(net);
		
		g.translate(position.getValue().getX(), position.getValue().getY());
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
	}
	
}
