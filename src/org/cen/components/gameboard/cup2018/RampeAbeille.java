package org.cen.components.gameboard.cup2018;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.IGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

/**
 * RampeAbeille class file.
 * 
 * This class represents a rampe for the bee.
 * 
 * The element has this shape : (scale about ~ 1 vertical unit = 10 mm and 1 horizontal unit = 5 mm)
 * 
 * The cotations are in mm.
 * 
 * |-----------150------------|
 * 
 * +--------------------------+ <- origin ---   ---
 * |                          |            22    |
 * +---------------------+    |           ---    |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                1292
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       |    |                  |
 *                       +----+                 ---
 * 
 * The outer path is the contour of the element.
 * The inner path is the placement zone of the abeille
 * The position of the figure is placed at the top left of the reversed L
 * The piece is contained in a square [left:-150, right:0] x [bottom:-1792, top:0].
 * 
 * @author Anastaszor
 */
public class RampeAbeille extends AbstractGameBoardElement implements IGameBoardElement
{
	
	private Path2D outer;
	private Path2D inner;
	private Color color;
	private boolean invertOnYAxis = false;
	
	public RampeAbeille(String name, Point2D position, Color color, boolean invertOnYAxis)
	{
		super(name, new Position2DMillimeter(position));
		this.color = color;
		this.invertOnYAxis = invertOnYAxis;
		
		outer = new Path2D.Double();
		// begin the path at the outer top right position
		outer.moveTo(position.getX(), position.getY());
		// go to outer top left position
		outer.lineTo(position.getX() - 150, position.getY());
		// go to inner top left position
		outer.lineTo(position.getX() - 150, position.getY() - 22);
		// go to inner top right position
		outer.lineTo(position.getX() - 22, position.getY() - 22);
		// go to inner bottom right position
		outer.lineTo(position.getX() - 22, position.getY() - 1314);
		// go to outer bottom right position
		outer.lineTo(position.getX(), position.getY() - 1314);
		// go to outer top right position
		outer.closePath();
		
		inner = new Path2D.Double();
		// begin the path at the top right position
		inner.moveTo(position.getX(), position.getY());
		// go to top left position
		inner.lineTo(position.getX() - 22, position.getY());
		// go to bottom left position
		inner.lineTo(position.getX() - 22, position.getY() - 200);
		// go to bottom right position
		inner.lineTo(position.getX(), position.getY() - 200);
		// go to top right position
		inner.closePath();
		
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(RALColor.RAL_7032);
		g.fill(outer);	// fill in player color
		g.setColor(RALColor.RAL_9005);
		g.draw(outer);	// draw borders in black
		g.setColor(this.color);
		g.fill(inner);	// fill in player color
		g.setColor(RALColor.RAL_9005);
		g.draw(inner);	// draw borders in black
		g.translate(position.getValue().getX(), position.getValue().getY());
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
	}
	
}
