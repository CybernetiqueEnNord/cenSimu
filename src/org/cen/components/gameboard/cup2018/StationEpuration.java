package org.cen.components.gameboard.cup2018;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.IGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

/**
 * StationEpuration class file.
 * 
 * This class represents a station epuration element.
 * 
 * The element has this shape : (scale about ~ 1 vertical unit = 10mm and 1 horizontal unit = 5mm) 
 * 
 * The cotations are in mm.
 * 
 * 
 * |-------------------250--------------------------|
 * 
 * |22|                                          |22|
 * 
 * +--+------------------------------------------+--+  \ 6-12mm   ---
 * +--+------------------------------------------+--+  /           |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |             600
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * |  |                                          |  |              |
 * +--+------------------------------------------+--+   ----       |
 * |  |                                          |  |    22       ---
 * +--+------------------------------------------+--+   ----
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * |  |                                          |  |
 * +--+------------------------------------------+--+
 * +--+------------------------------------------+--+
 * 
 * The outer path will follow the whole structure.
 * The inner path will materialize the separation.
 * The top path will materialize the colorA.
 * The bottom path will materialize the colorB.
 * The position of the figure is placed at the center of mass.
 * The piece is contained in a square [left:-125, right:125] x [bottom:-600, top:600]
 * 
 * @author Anastaszor
 */
public class StationEpuration extends AbstractGameBoardElement implements IGameBoardElement
{
	
	private Path2D outer;
	private Path2D inner;
	private Path2D top;
	private Path2D btm;
	
	private Color colorA;
	private Color colorB;
	
	public StationEpuration(String name, Point2D position, Color colorA, Color colorB)
	{
		super(name, new Position2DMillimeter(position));
		this.colorA = colorA;
		this.colorB = colorB;
		
		outer = new Path2D.Double();
		// begin the path at the bottom left position
		outer.moveTo(position.getX() -125, position.getY() - 600);
		// go to bottom right
		outer.lineTo(position.getX() + 125, position.getY() - 600);
		// go to top right
		outer.lineTo(position.getX() + 125, position.getY() + 600);
		// go to top left
		outer.lineTo(position.getX() - 125, position.getY() + 600);
		// go to bottom left
		outer.closePath();
		
		inner = new Path2D.Double();
		// begin the path at the bottom left position
		inner.moveTo(position.getX() - 125, position.getY() - 11);
		// go to bottom right
		inner.lineTo(position.getX() + 125, position.getY() - 11);
		// go to top right
		inner.lineTo(position.getX() + 125, position.getY() + 11);
		// go to top left
		inner.lineTo(position.getX() - 125, position.getY() + 11);
		// go to bottom left
		inner.closePath();
		
		top = new Path2D.Double();
		// begin the path at the outer bottom left position
		top.moveTo(position.getX() - 125, position.getY());
		// go to inner bottom left position
		top.lineTo(position.getX() - 103, position.getY());
		// go to inner top left position
		top.lineTo(position.getX() - 103, position.getY() + 590);
		// go to inner top right position
		top.lineTo(position.getX() + 103, position.getY() + 590);
		// go to inner bottom right position
		top.lineTo(position.getX() + 103, position.getY());
		// go to outer bottom right position
		top.lineTo(position.getX() + 125, position.getY());
		// go to outer top right position
		top.lineTo(position.getX() + 125, position.getY() + 600);
		// go to outer top left position
		top.lineTo(position.getX() - 125, position.getY() + 600);
		// go to outer bottom left position
		top.closePath();
		
		// this path is made clockwise
		btm = new Path2D.Double();
		// begin path at the outer top left position
		btm.moveTo(position.getX() - 125, position.getY());
		// go to inner top left position
		btm.lineTo(position.getX() - 103, position.getY());
		// go to inner bottom left position
		btm.lineTo(position.getX() - 103, position.getY() - 590);
		// go to inner bottom right position
		btm.lineTo(position.getX() + 103, position.getY() - 590);
		// go to inner top right position
		btm.lineTo(position.getX() + 103, position.getY());
		// go to outer top right position
		btm.lineTo(position.getX() + 125, position.getY());
		// go to outer bottom right position
		btm.lineTo(position.getX() + 125, position.getY() - 600);
		// go to outer bottom left position
		btm.lineTo(position.getX() - 125, position.getY() - 600);
		// go to outer top left position
		btm.closePath();
		
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		
		g.setColor(RALColor.RAL_7032);
		g.fill(outer);	// fill
		g.setColor(RALColor.RAL_9005);
		g.draw(outer);	// draw borders in black
		
		g.setColor(RALColor.RAL_7032);
		g.fill(inner);	// fill
		g.setColor(RALColor.RAL_9005);
		g.draw(inner);	// draw borders in black
		
		g.setColor(colorA);
		g.fill(top);	// fill in player a color
		g.setColor(RALColor.RAL_9005);
		g.draw(top);	// draw borders in black
		
		g.setColor(colorB);
		g.fill(btm);	// fill in player b color
		g.setColor(RALColor.RAL_9005);
		g.draw(btm);	// draw borders in black
		
		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
}
