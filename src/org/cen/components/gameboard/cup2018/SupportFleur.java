package org.cen.components.gameboard.cup2018;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.IGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

/**
 * SupportFleur class file.
 * 
 * This class represents a support for the fleur (balloon) element.
 * 
 * The element has this shape : (scale about ~ 1 vertical unit = 10mm and 1 horizontal unit = 5mm)
 * 
 * The cotations are in mm.
 * 
 * 
 * |--------------------230---------------------|
 * 
 *                                         |-22-|
 * 
 * +--------------------------------------------+  ---   ---
 * |                                            |   22    |
 * +---------------------------------------+    |  ---    |
 *                                         |    |         |
 *                                         |    |         |
 *                                         |    |         |
 *                                         |    |         |
 *                                         |    |         |
 *                                         |    |         |
 *                                         |    |         |
 *                                         |    |         |
 *                   x                     |    |        230
 *                                         |    |         |
 *                                         |    |         |
 *                                         |    |         |
 *                                         |    |         |
 *                                         |    |         |
 *                                         |    |         |
 *                                         |    |         |
 *                                         |    |         |
 * |---------------------------------------+    |  ---    |
 * |                                            |   22    |
 * +--------------------------------------------+  ---   ---
 * 
 * The outer path is the description of the figure.
 * The position of the figure is at the center of the outer square (not the inner square, there is one border that is
 * 		not positioned)
 * The piece is contained in a square [left:-115, right:115] x [bottom:-115, top:115]
 * 
 * @author Anastaszor
 */
public class SupportFleur extends AbstractGameBoardElement implements IGameBoardElement
{
	
	private Path2D outer = null;
	private Color color;
	
	public SupportFleur(String name, Point2D position, Color color)
	{
		super(name, new Position2DMillimeter(position));
		this.color = color;
		
		outer = new Path2D.Double();
		// begin the path at the outer bottom left position
		outer.moveTo(position.getX() - 115, position.getY() - 115);
		// go to outer bottom right
		outer.lineTo(position.getX() + 115, position.getY() - 115);
		// go to outer top right
		outer.lineTo(position.getX() + 115, position.getY() + 115);
		// go to outer top left
		outer.lineTo(position.getX() - 115, position.getY() + 115);
		// go to inner top left
		outer.lineTo(position.getX() - 115, position.getY() + 83);
		// go to inner top right
		outer.lineTo(position.getX() + 83, position.getY() + 83);
		// go to inner bottom right
		outer.lineTo(position.getX() + 83, position.getY() - 83);
		// go to inner bottom left
		outer.lineTo(position.getX() - 115, position.getY() - 83);
		// go to outer bottom left
		outer.closePath();
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(this.color);
		g.fill(outer);	// fill in player color
		g.setColor(RALColor.RAL_9005);
		g.draw(outer);	// draw borders in black
		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
}
