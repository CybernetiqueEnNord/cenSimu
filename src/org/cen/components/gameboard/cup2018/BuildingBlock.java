package org.cen.components.gameboard.cup2018;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.IGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

/**
 * BuildingBlock class file.
 * 
 * This class represents a new (2018+) building block element.
 * 
 * The element has this shape : (scale about ~ 1 vertical unit = 10mm and 1 horizontal unit = 5mm) 
 * 
 * The cotations are in mm.
 * 
 * This is a cube with 58mm each side. :)
 * 
 * +--------+
 * |        |
 * |   x    |
 * |        |
 * +--------+
 * 
 * The outer path is the form of the cube.
 * The position of the cube is placed at the center of the cube.
 * The piece is contained in a square [left:-29, right:29] x [bottom:-29, top:29]
 * 
 * @author Anastaszor
 */
public class BuildingBlock extends AbstractGameBoardElement implements IGameBoardElement
{
	
	private Color color;
	private Rectangle2D outer;
	
	public BuildingBlock(String name, Point2D position, Color color)
	{
		super(name, new Position2DMillimeter(position));
		this.color = color;
		outer = new Rectangle2D.Double(-29, -29, 58, 58);
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.setColor(this.color);
		g.fill(outer);	// fill in player color
		g.setColor(RALColor.RAL_9005);
		g.draw(outer);
	}
	
}
