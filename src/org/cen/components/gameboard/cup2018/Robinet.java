package org.cen.components.gameboard.cup2018;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.IGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.position.Position2DMillimeter;

/**
 * Robinet class file.
 * 
 * This class represents a water distributor element.
 * 
 * The element has this shape : (scale about ~ 1 vertical unit = 10mm and 1 horizontal unit = 5mm) 
 * 
 * The cotations are in mm.
 * 
 *    |----|<- 30mm
 * 
 *    +----+                               ---
 *    |    |                                |
 *    |.--.|                               72
 *   /¯    ¯\                               |
 * //        \\                             |
 * ||        ||  <<-- diam ext 60, int 54  ---
 * \\        //
 *   \_    _/
 *     ¨--¨
 * 
 * The support path is the path for the support.
 * The tupe path is the path for the rainure
 * The position of the figure is placed at the center of the
 * 
 * @author Anastaszor
 */
public class Robinet extends AbstractGameBoardElement implements IGameBoardElement
{
	
	private Color color;
	private Path2D support;
	private Ellipse2D tube;
	
	public Robinet(String name, Point2D position, Color color, double rotation)
	{
		super(name, new Position2DMillimeter(position), new AngleRadian(rotation));
		this.color = color;
		
		support = new Path2D.Double();
		// begin at the bottom left position
		support.moveTo(position.getX() - 15, position.getY());
		// go to bottom right
		support.lineTo(position.getX() + 15, position.getY());
		// go to top right
		support.lineTo(position.getX() + 15, position.getY() + 72);
		// go to top left
		support.lineTo(position.getX() - 15, position.getY() + 72);
		// go to bottom right
		support.closePath();
		
		tube = new Ellipse2D.Double(position.getX() - 36, position.getY() - 36, 72, 72);
		
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		
		g.setColor(this.color);
		g.fill(support);	// fill in player color
		g.setColor(RALColor.RAL_9005);
		g.draw(support);	// draw borders in black
		
		g.setColor(RALColor.RAL_9010);
		g.fill(tube);	// fill in white
		g.setColor(RALColor.RAL_9005);
		g.draw(tube);	// draw borders in black
		
		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
}
