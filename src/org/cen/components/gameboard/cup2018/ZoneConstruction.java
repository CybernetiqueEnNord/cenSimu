package org.cen.components.gameboard.cup2018;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.IGameBoardElement;
import org.cen.components.math.position.Position2DMillimeter;

/**
 * ZoneContruction class file.
 * 
 * This class represents a new (2018+) balise support element.
 * 
 * The element has this shape : (scale about ~ 1 vertical unit = 10mm and 1 horizontal unit = 5mm) 
 * 
 * The cotations are in mm.
 * 
 * 
 * |-----------------200------------------|
 *                                   |-22-|
 * 
 *                                   +----+       ---
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |       600
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 *                                   |    |        |
 * +---------------------------------+    |  ---   |
 * |                                      |   22   |
 * +--------------------------------------+  ---  ---
 *                                        
 *                                        A
 *                                        |
 *                                      origin
 *                                      
 * The path represents the whole form.
 * The position of the figure is placed on the outer corner of the figure.
 * The piece is contained in a square [left:-200, right:0] x [bottom:0, top:600]
 * 
 * @author Anastaszor
 */
public class ZoneConstruction extends AbstractGameBoardElement implements IGameBoardElement
{
	
	private Color color = null;
	private Path2D outer = null;
	private boolean invertOnYAxis = false;
	
	public ZoneConstruction(String name, Point2D position, Color color, boolean invertOnYAxis)
	{
		super(name, new Position2DMillimeter(position));
		this.color = color;
		this.invertOnYAxis = invertOnYAxis;
		
		outer = new Path2D.Double();
		// begin the path at the outer bottom left position
		outer.moveTo(position.getX() - 200, position.getY());
		// go to outer bottom right 
		outer.lineTo(position.getX(), position.getY());
		// go to outer top right
		outer.lineTo(position.getX(), position.getY() + 600);
		// go to inner top right
		outer.lineTo(position.getX() - 22, position.getY() + 600);
		// go to inner bottom right
		outer.lineTo(position.getX() - 22, position.getY() + 22);
		// go to inner bottom left
		outer.lineTo(position.getX() - 200, position.getY() + 22);
		// go to outer bottom left
		outer.closePath();
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(this.color);
		g.fill(outer);	// fill in player color
		g.translate(position.getValue().getX(), position.getValue().getY());
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
	}
	
}
