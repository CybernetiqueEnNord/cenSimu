package org.cen.components.gameboard;

import java.util.Arrays;
import java.util.List;

import org.cen.components.gameboard.generic.IGameBoardService;

public enum GameBoardMetadata
{
	GB_2020("2020 — Sail the World", 2020,
		GameBoardConfigurationMetadata.GB_2020_1
	),
	GB_2019("2019 — Atom Factory", 2019,
		GameBoardConfigurationMetadata.GB_2019_1
	),
	GB_2018("2018 — Robot Cities", 2018,
		GameBoardConfigurationMetadata.GB_2018_1
	),
	GB_2017("2017 — Moon Village", 2017,
		GameBoardConfigurationMetadata.GB_2017_1
	),
	GB_2016("2016 — The Beach Bots", 2016,
		GameBoardConfigurationMetadata.GB_2016_1,
		GameBoardConfigurationMetadata.GB_2016_2,
		GameBoardConfigurationMetadata.GB_2016_3,
		GameBoardConfigurationMetadata.GB_2016_4,
		GameBoardConfigurationMetadata.GB_2016_5
	),
	GB_2015("2015 — Robot Movies", 2015,
		GameBoardConfigurationMetadata.GB_2015_1
	),
	GB_2014("2014 — Préhistobots", 2014,
		GameBoardConfigurationMetadata.GB_2014_1
	),
	;
	
	private List<GameBoardConfigurationMetadata> configurations = null;
	private String buttonName = null;
	private Integer year = null;
	
	private GameBoardMetadata(String buttonName, Integer year, GameBoardConfigurationMetadata ... configurations)
	{
		this.configurations = Arrays.asList(configurations);
		this.buttonName = buttonName;
		this.year = year;
	}
	
	public List<GameBoardConfigurationMetadata> getConfigurations()
	{
		return configurations;
	}
	
	public String getName()
	{
		return this.buttonName;
	}
	
	public Integer getYear()
	{
		return year;
	}
	
	public static GameBoardMetadata getMetadata(IGameBoardService gameBoard)
	{
		for(GameBoardMetadata gbm:GameBoardMetadata.values())
		{
			for(GameBoardConfigurationMetadata gbcm:gbm.getConfigurations())
			{
				if(gameBoard.getClass().isAssignableFrom(gbcm.getGameBoardClass()))
					return gbm;
			}
		}
		return null;
	}
	
}
