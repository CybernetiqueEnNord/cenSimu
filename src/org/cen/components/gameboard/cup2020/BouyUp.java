package org.cen.components.gameboard.cup2020;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

public class BouyUp extends AbstractGameBoardElement
{
	
	private Color color = null;
	private Ellipse2D outer = null;
	private Path2D inner = null;
	
	public BouyUp(String name, Point2D position, Color color)
	{
		super(name, new Position2DMillimeter(position));
		this.color = color;
		
		outer = new Ellipse2D.Double(-36, -36, 72, 72);
		inner = new Path2D.Double();
		// fleche d'en haut vers le bas
		inner.moveTo(36, 0);
		inner.lineTo(18, 0);
		inner.lineTo(24, -6);
		inner.moveTo(18, 0);
		inner.lineTo(24, 6);
		// fleche de la droite vers la gauche
		inner.moveTo(0, -36);
		inner.lineTo(0, -18);
		inner.lineTo(-6, -24);
		inner.moveTo(0, -18);
		inner.lineTo(6, -24);
		// fleche de la gauche vers la droite
		inner.moveTo(0, 36);
		inner.lineTo(0, 18);
		inner.lineTo(-6, 24);
		inner.moveTo(0, 18);
		inner.lineTo(6, 24);
		// fleche du bas vers le haut
		inner.moveTo(-36, 0);
		inner.lineTo(-18, 0);
		inner.lineTo(-24, -6);
		inner.moveTo(-18, 0);
		inner.lineTo(-24, 6);
	}
	
	@Override
	public void paint(Graphics2D g)
	{
//		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(this.color);
		g.fill(outer); // fill in player color
		g.setColor(RALColor.RAL_9005);
		g.draw(outer);
		g.draw(inner);
//		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
}
