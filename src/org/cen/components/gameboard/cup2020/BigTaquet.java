package org.cen.components.gameboard.cup2020;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

/**
 * BigTaquet class file.
 * 
 * THis class represents a specific piece of wood that is placed on the floor 
 * for it to be impassable by any robot. 
 * 
 * +-+
 * +-+
 * 
 * The cotations are in mm.
 * 
 * The center of the paint is at the center of the rectangle.
 * 
 * @author Anastaszor
 */
public class BigTaquet extends AbstractGameBoardElement
{
	private Color color = null;
	private Path2D outer = null;
	
	public BigTaquet(String name, Point2D position, Color color)
	{
		super(name, new Position2DMillimeter(position));
		this.color = color;
		
		outer = new Path2D.Double();
		// begin the path at the bottom left position
		outer.moveTo(position.getX() - 150, position.getY() - 11);
		// go to bottom right
		outer.lineTo(position.getX() + 150, position.getY() - 11);
		// go to top right
		outer.lineTo(position.getX() + 150, position.getY() + 11);
		// go to top left
		outer.lineTo(position.getX() - 150, position.getY() + 11);
		// go to bottom left
		outer.closePath();
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(this.color);
		g.fill(outer);	// fill in player color
		g.setColor(RALColor.RAL_9005);
		g.draw(outer);
		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
}
