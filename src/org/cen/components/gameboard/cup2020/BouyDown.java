package org.cen.components.gameboard.cup2020;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

public class BouyDown extends AbstractGameBoardElement
{
	
	private Color color = null;
	private Ellipse2D outer = null;
	private Ellipse2D inner = null;
	
	public BouyDown(String name, Point2D position, Color color)
	{
		super(name, new Position2DMillimeter(position));
		this.color = color;
		
		outer = new Ellipse2D.Double(-36, -36, 72, 72);
		inner = new Ellipse2D.Double(-30, -30, 60, 60);
	}
	
	@Override
	public void paint(Graphics2D g)
	{
//		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(this.color);
		g.fill(outer); // fill in player color
		g.setColor(RALColor.RAL_9005);
		g.draw(outer);
		g.draw(inner);
//		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
}
