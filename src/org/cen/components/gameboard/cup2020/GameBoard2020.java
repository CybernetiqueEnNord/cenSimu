package org.cen.components.gameboard.cup2020;

import java.awt.Color;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.cup2019.ZoneExperience;
import org.cen.components.gameboard.generic.AbstractGameBoard2018;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.angle.AngleRadian;

public class GameBoard2020 extends AbstractGameBoard2018
{
	
	public static final Color COLOR_A = RALColor.BLUE;
	public static final Color COLOR_B = RALColor.YELLOW;
	public static final Color PLATEAU = new Color(0x09, 0xA0, 0xD2); // some kind of blue
	public static final Color GREEN   = RALColor.GREEN;
	public static final Color RED     = RALColor.RED;
	public static final Color NORTH   = new Color(0x03, 0x61, 0x80); // some kind of dark blue
	public static final Color SOUTH   = new Color(0x5a, 0xbc, 0xdd); // some kind of lime blue
	
	public GameBoard2020()
	{
		super(COLOR_A, COLOR_B);
		
		getElements().add(new LittleTaquet("taquet1", new Point2D.Double(1925, 900), RALColor.RAL_7032));
		getElements().add(new BigTaquet("taquetM", new Point2D.Double(1850, 1500), RALColor.RAL_7032));
		getElements().add(new LittleTaquet("taquet2", new Point2D.Double(1925, 2100), RALColor.RAL_7032));
		
		getElements().add(new ZoneExperience("phare-a", new Point2D.Double(-122, 225), COLOR_A, false));
		getElements().add(new ZoneExperience("phare-b", new Point2D.Double(-122, 2775d), COLOR_B, false));
		
		getElements().add(new DockingArea("dkn-a", new Point2D.Double(500, 0), NORTH, new AngleRadian(Math.PI/4)));
		getElements().add(new DockingArea("dks-a", new Point2D.Double(1100, 0), SOUTH, new AngleRadian(-Math.PI)));
		getElements().add(new DockingArea("dkn-b", new Point2D.Double(500, 3000), NORTH, new AngleRadian(Math.PI/2)));
		getElements().add(new DockingArea("dks-b", new Point2D.Double(1100, 3000), SOUTH, new AngleRadian(-Math.PI/4)));
		
		getElements().add(new InnerCurve("ic1", new Point2D.Double(1000, 2000), false));
		getElements().add(new InnerCurve("ic2", new Point2D.Double(1000, 1000), true));
		getElements().add(new OuterCurve("oc1", new Point2D.Double(1000, 2400), false));
		getElements().add(new OuterCurve("oc2", new Point2D.Double(1000, 600), true));
		
		getElements().add(new ZonePortStart("port-a", new Point2D.Double(800, 200), COLOR_A));
		getElements().add(new ZonePortStart("port-b", new Point2D.Double(800, 2800), COLOR_B));
		getElements().add(new LongChenal("ec-bb-a", new Point2D.Double(515, 200), GREEN));
		getElements().add(new LongChenal("ec-tr-a", new Point2D.Double(1085, 200), RED));
		getElements().add(new LongChenal("ec-bb-b", new Point2D.Double(515, 2800), RED));
		getElements().add(new LongChenal("ec-tr-b", new Point2D.Double(1085, 2800), GREEN));
		getElements().add(new SmallChenal("ec-bb-b2", new Point2D.Double(1850, 1100), GREEN));
		getElements().add(new SmallChenal("ec-st-b2", new Point2D.Double(1850, 1200), COLOR_B));
		getElements().add(new SmallChenal("ec-tr-b2", new Point2D.Double(1850, 1300), RED));
		getElements().add(new SmallChenal("ec-bb-a2", new Point2D.Double(1850, 1700), GREEN));
		getElements().add(new SmallChenal("ec-st-a2", new Point2D.Double(1850, 1800), COLOR_A));
		getElements().add(new SmallChenal("ec-tr-a2", new Point2D.Double(1850, 1900), RED));
		
		getElements().add(new TeamEcueil("ecueil-a", new Point2D.Double(1600, -67), COLOR_A));
		getElements().add(new TeamEcueil("ecueil-b", new Point2D.Double(1600, 3067), COLOR_B));
		getElements().add(new NeutralEcueil("neutral-a", new Point2D.Double(-67, 850), RALColor.RAL_7032));
		getElements().add(new NeutralEcueil("neutral-b", new Point2D.Double(-67, 2150), RALColor.RAL_7032));
		getElements().add(new Girouette("girouette", new Point2D.Double(13, 1500)));
		
		getElements().add(new BouyUp("b-u01", new Point2D.Double(400, 300), RED));
		getElements().add(new BouyUp("b-u02", new Point2D.Double(510, 450), GREEN));
		getElements().add(new BouyUp("b-u03", new Point2D.Double(1080, 450), RED));
		getElements().add(new BouyUp("b-u04", new Point2D.Double(1200, 300), GREEN));
		getElements().add(new BouyUp("b-u05", new Point2D.Double(100, 670), RED));
		getElements().add(new BouyUp("b-u06", new Point2D.Double(400, 950), GREEN));
		getElements().add(new BouyUp("b-u07", new Point2D.Double(800, 1100), RED));
		getElements().add(new BouyUp("b-u08", new Point2D.Double(1200, 1270), GREEN));
		getElements().add(new BouyUp("b-u09", new Point2D.Double(1650, 1065), GREEN));
		getElements().add(new BouyUp("b-u10", new Point2D.Double(1955, 1005), RED));
		getElements().add(new BouyUp("b-u11", new Point2D.Double(1955, 1395), GREEN));
		getElements().add(new BouyUp("b-u12", new Point2D.Double(1650, 1335), RED));
		
		getElements().add(new BouyUp("b-u13", new Point2D.Double(1650, 1665), GREEN));
		getElements().add(new BouyUp("b-u14", new Point2D.Double(1955, 1605), RED));
		getElements().add(new BouyUp("b-u15", new Point2D.Double(1955, 1995), GREEN));
		getElements().add(new BouyUp("b-u16", new Point2D.Double(1650, 1935), RED));
		getElements().add(new BouyUp("b-u17", new Point2D.Double(1200, 1730), RED));
		getElements().add(new BouyUp("b-u18", new Point2D.Double(800, 1900), GREEN));
		getElements().add(new BouyUp("b-u19", new Point2D.Double(400, 2050), RED));
		getElements().add(new BouyUp("b-u20", new Point2D.Double(100, 2330), GREEN));
		getElements().add(new BouyUp("b-u21", new Point2D.Double(1200, 2700), RED));
		getElements().add(new BouyUp("b-u22", new Point2D.Double(1080, 2550), GREEN));
		getElements().add(new BouyUp("b-u23", new Point2D.Double(510, 2550), RED));
		getElements().add(new BouyUp("b-u24", new Point2D.Double(400, 2700), GREEN));
		
		getElements().add(new BouyDown("b-d01", new Point2D.Double(1450, -67), RED));
		getElements().add(new BouyDown("b-d02", new Point2D.Double(1525, -67), GREEN));
		getElements().add(new BouyDown("b-d03", new Point2D.Double(1600, -67), RED));
		getElements().add(new BouyDown("b-d04", new Point2D.Double(1675, -67), GREEN));
		getElements().add(new BouyDown("b-d05", new Point2D.Double(1750, -67), RED));
		
		getElements().add(new BouyDown("b-d06", new Point2D.Double(1450, 3067), GREEN));
		getElements().add(new BouyDown("b-d07", new Point2D.Double(1525, 3067), RED));
		getElements().add(new BouyDown("b-d08", new Point2D.Double(1600, 3067), GREEN));
		getElements().add(new BouyDown("b-d09", new Point2D.Double(1675, 3067), RED));
		getElements().add(new BouyDown("b-d10", new Point2D.Double(1750, 3067), GREEN));
		
		getElements().add(new BouyDown("b-d11", new Point2D.Double(-67, 700), GREEN));
		getElements().add(new BouyDown("b-d12", new Point2D.Double(-67, 775), GREEN));
		getElements().add(new BouyDown("b-d13", new Point2D.Double(-67, 850), GREEN));
		getElements().add(new BouyDown("b-d14", new Point2D.Double(-67, 925), RED));
		getElements().add(new BouyDown("b-d15", new Point2D.Double(-67, 1000), RED));
		
		getElements().add(new BouyDown("b-d11", new Point2D.Double(-67, 2000), GREEN));
		getElements().add(new BouyDown("b-d12", new Point2D.Double(-67, 2075), GREEN));
		getElements().add(new BouyDown("b-d13", new Point2D.Double(-67, 2150), RED));
		getElements().add(new BouyDown("b-d14", new Point2D.Double(-67, 2225), RED));
		getElements().add(new BouyDown("b-d15", new Point2D.Double(-67, 2300), RED));
	}
	
	@Override
	protected Color getDefaultBoardColor()
	{
		return PLATEAU;
	}
	
}
