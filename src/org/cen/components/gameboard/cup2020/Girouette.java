package org.cen.components.gameboard.cup2020;

import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

public class Girouette extends AbstractGameBoardElement
{
	protected Path2D outer = null;
	
	public Girouette(String name, Point2D position)
	{
		super(name, new Position2DMillimeter(position));
		
		outer = new Path2D.Double();
		// begin the path at the bottom left position
		outer.moveTo(position.getX() - 13, position.getY() - 140);
		// go to bottom right
		outer.lineTo(position.getX() + 13, position.getY() - 140);
		// go to top right
		outer.lineTo(position.getX() + 13, position.getY() + 140);
		// go to top left
		outer.lineTo(position.getX() - 13, position.getY() + 140);
		// go to bottom left
		outer.closePath();
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(RALColor.RAL_7032);
		g.fill(outer);	// fill in player color
		g.setColor(RALColor.RAL_9005);
		g.draw(outer);
		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
}
