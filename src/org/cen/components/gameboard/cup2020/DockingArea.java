package org.cen.components.gameboard.cup2020;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.position.Position2DMillimeter;

/**
 * DockingArea class file.
 * 
 * This class represents where the robots should be at the end of the match.
 * 
 * The element has this shape : (scale about ~ 1 vertical unit = 100mm and 1 horizontal unit = 100mm)
 * 
 * ++
 * | +
 * |  +
 * |   +
 * +---+
 * 
 * The cotations are in mm.
 * 
 * The center of the paint is in the corner of the quarter circle
 * 
 * 
 * @author Anastaszor
 */
public class DockingArea extends AbstractGameBoardElement
{
	
	protected Color color = null;
	protected Path2D outer = null;
	
	public DockingArea(String name, Point2D position, Color color, IAngle orientation)
	{
		super(name, new Position2DMillimeter(position), orientation);
		this.color = color;
		
		outer = new Path2D.Double();
		outer.moveTo(0, 0);
		outer.lineTo(0, 400);
		outer.curveTo(0, 400, 400, 400, 400, 0);
		outer.closePath();
	}
	
	@Override
	public void paint(Graphics2D g)
	{
//		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.rotate(this.orientation.toRadians().getValue());
		g.setColor(this.color);
		g.fill(outer);	// fill in player color
		g.rotate(-this.orientation.toRadians().getValue());
//		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
}
