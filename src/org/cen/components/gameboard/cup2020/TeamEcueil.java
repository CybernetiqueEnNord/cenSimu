package org.cen.components.gameboard.cup2020;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

/**
 * TeamEcueil class file.
 * 
 * This class represents where the buoys should be at the start of the match for
 * one specific team.
 * 
 * The element has this shape : (scale about ~ 1 vertical unit = 100mm and 1 horizontal unit = 100mm)
 * 
 * +----+
 * +----+
 * 
 * The cotations are in mm.
 * 
 * The center of the paint is at the center of the rectangle.
 * 
 * @author Anastaszor
 */
public class TeamEcueil extends AbstractGameBoardElement
{
	
	private Color color = null;
	private Path2D outer = null;
	private Path2D inner = null;
	
	public TeamEcueil(String name, Point2D position, Color color)
	{
		super(name, new Position2DMillimeter(position));
		this.color = color;
		
		outer = new Path2D.Double();
		// begin the path at the bottom left position
		outer.moveTo(position.getX() - 210, position.getY() - 67);
		// go to bottom right
		outer.lineTo(position.getX() + 210, position.getY() - 67);
		// go to top right
		outer.lineTo(position.getX() + 210, position.getY() + 67);
		// go to top left
		outer.lineTo(position.getX() - 210, position.getY() + 67);
		// go to bottom left
		outer.closePath();
		
		inner = new Path2D.Double();
		// begin the path at the bottom left position
		inner.moveTo(position.getX() - 188, position.getY() - 45);
		// go to bottom right
		inner.lineTo(position.getX() + 188, position.getY() - 45);
		// go to top right
		inner.lineTo(position.getX() + 188, position.getY() + 45);
		// go to top left
		inner.lineTo(position.getX() - 188, position.getY() + 45);
		// go to bottom left
		inner.closePath();
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(this.color);
		g.fill(outer); // fill in player color
		g.setColor(RALColor.RAL_9005);
		g.draw(outer);
		g.setColor(RALColor.RAL_7032);
		g.fill(inner);
		g.setColor(RALColor.RAL_9005);
		g.draw(inner);
		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
}
