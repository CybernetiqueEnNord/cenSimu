package org.cen.components.gameboard.cup2020;

import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

public class OuterCurve extends AbstractGameBoardElement
{
	
	protected Path2D outer = null;
	protected boolean invert = false;
	
	public OuterCurve(String name, Point2D position, boolean invertOnYAxis)
	{
		super(name, new Position2DMillimeter(position));
		this.invert = invertOnYAxis;
		
		outer = new Path2D.Double();
		outer.moveTo(0, -10);
		curveTo(outer, 0, -10, 200, -10, 500, 360, 600, 360);
		outer.lineTo(1000, 360);
		outer.lineTo(1000, 380);
		outer.lineTo(600, 380);
		curveTo(outer, 600, 380, 500, 380, 200, 10, 0, 10);
		outer.lineTo(-450, 10);
		curveTo(outer, -450, 10, -600, 10, -800, 380, -1000, 380);
		outer.lineTo(-1000, 380);
		outer.lineTo(-1000, 360);
		curveTo(outer, -1000, 360, -800, 360, -600, -10, -450, -10);
		outer.closePath();
	}
	
	public void curveTo(Path2D path, int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4)
	{
		// draws an S curve
		outer.curveTo(x1, y1, x2, y2, (x2 + x3) / 2, (y2 + y3) / 2);
		outer.curveTo((x2 + x3) / 2, (y2 + y3) / 2, x3, y3, x4, y4);
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		if(invert)
		{
			g.scale(1, -1);
		}
//		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(RALColor.RAL_9010);
		g.fill(outer);	// fill in player color
		g.setColor(RALColor.RAL_9005);
		g.draw(outer);
//		g.translate(position.getValue().getX(), position.getValue().getY());
		if(invert)
		{
			g.scale(1, -1);
		}
	}
	
}
