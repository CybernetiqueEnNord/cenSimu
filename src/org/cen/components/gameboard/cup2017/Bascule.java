package org.cen.components.gameboard.cup2017;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

public class Bascule extends AbstractGameBoardElement
{
	
	private Rectangle area = null;
	
	// (0,0) au centre du rectangle
	public Bascule(String name, Point2D position)
	{
		super(name, new Position2DMillimeter(position));
		area = new Rectangle(-180, -175, 360, 350);
	}
	
	@Override
	public void paint(Graphics2D g)
	{
//		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(RALColor.RAL_7032);
		g.fill(area);
//		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
}
