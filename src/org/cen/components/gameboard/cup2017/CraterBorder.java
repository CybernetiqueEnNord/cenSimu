package org.cen.components.gameboard.cup2017;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

public class CraterBorder extends AbstractGameBoardElement
{
	
	private Rectangle area = null;
	private double theta;
	
	// (0, 0) at the center of the rectangle
	public CraterBorder(String name, Point2D position, double theta)
	{
		super(name, new Position2DMillimeter(position));
		this.theta = theta;
		area = new Rectangle(-15, -50, 30, 100);
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.rotate(-theta);
		g.setColor(RALColor.RAL_8002);
		g.fill(area);
		g.rotate(theta);
		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
}
