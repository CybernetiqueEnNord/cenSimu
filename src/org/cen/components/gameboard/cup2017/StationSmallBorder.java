package org.cen.components.gameboard.cup2017;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

public class StationSmallBorder extends AbstractGameBoardElement
{
	
	private Rectangle area = null;
	
	// (0,0) au centre du rectangle
	public StationSmallBorder(String name, Point2D position)
	{
		super(name, new Position2DMillimeter(position));
		area = new Rectangle(-11, -40, 22, 80);
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(RALColor.RAL_9016);
		g.fill(area);
		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
}
