package org.cen.components.gameboard.cup2017;

import java.awt.Graphics2D;
import java.awt.geom.Arc2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

public class Rocket extends AbstractGameBoardElement
{
	
	private Arc2D circle = null;
	
	public Rocket(String name, Point2D position)
	{
		super(name, new Position2DMillimeter(position));
		circle = new Arc2D.Double(-40, -40, 80, 80, 0, 360, Arc2D.PIE);
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.setColor(RALColor.RAL_8002);
		g.fill(circle);
	}
	
}
