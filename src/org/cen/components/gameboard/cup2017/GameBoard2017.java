package org.cen.components.gameboard.cup2017;

import java.awt.Color;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoard;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.angle.AngleRadian;

public class GameBoard2017 extends AbstractGameBoard
{
	
	public static final Color COLOR_A = RALColor.RAL_1023;
	public static final Color COLOR_B = RALColor.RAL_5015;
	
	public GameBoard2017()
	{
		super();
		getElements().add(new StartZone("sz-top", new Point2D.Double(0, 3000), COLOR_A));
		getElements().add(new StartZone("sz-bottom", new Point2D.Double(0, 1070), COLOR_B));
		
		getElements().add(new StartSeparation("ssp-top", new Point2D.Double(360, 3000)));
		getElements().add(new StartSeparation("ssp-bottom", new Point2D.Double(360, 710)));
		
		getElements().add(new Crater("top-crater", new Point2D.Double(540, 2350)));
		getElements().add(new Crater("bottom-crater", new Point2D.Double(540, 650)));
		getElements().add(new Crater("top-station", new Point2D.Double(1870, 1930)));
		getElements().add(new Crater("bottom-station", new Point2D.Double(1870, 1070)));
		
		getElements().add(new CraterQuarter("big-crater-q1", new Point2D.Double(2000, 0), 0));
		getElements().add(new CraterQuarter("big-crater-q2", new Point2D.Double(2000, 0), Math.PI/4));
		getElements().add(new CraterQuarter("big-crater-q3", new Point2D.Double(2000, 3000), -Math.PI/4));
		getElements().add(new CraterQuarter("big-crater-q4", new Point2D.Double(2000, 3000), -Math.PI/2));
		
		getElements().add(new CentralBase("central-base", new Point2D.Double(2000, 1500)));
		
		getElements().add(new Rocket("rkt-south", new Point2D.Double(1350, 40)));
		getElements().add(new Rocket("rkt-msouth", new Point2D.Double(40, 1150)));
		getElements().add(new Rocket("rkt-mnorth", new Point2D.Double(40, 1850)));
		getElements().add(new Rocket("rkt-north", new Point2D.Double(1350, 2960)));
		
		getElements().add(new Bascule("bascule-south", new Point2D.Double(180, 535)));
		getElements().add(new Bascule("bascule-north", new Point2D.Double(180, 2465)));
		
		getElements().add(new Soute("soute-south", new Point2D.Double(-111, 250)));
		getElements().add(new Soute("soute-north", new Point2D.Double(-111, 2750)));
		
		getElements().add(new StationBorder("station-north", new Point2D.Double(925, 3000), new AngleRadian(0d)));
		getElements().add(new StationBorder("station-south", new Point2D.Double(925, 0), new AngleRadian(Math.PI)));
		
		getElements().add(new LunarModule("lm-a1", new Point2D.Double(600, 2800), COLOR_A, null));
		getElements().add(new LunarModule("lm-n2", new Point2D.Double(1100, 2500), COLOR_A, COLOR_B));
		getElements().add(new LunarModule("lm-a3", new Point2D.Double(1850, 2200), COLOR_A, null));
		getElements().add(new LunarModule("lm-n3", new Point2D.Double(1400, 2100), COLOR_A, COLOR_B));
		getElements().add(new LunarModule("lm-n5", new Point2D.Double(600, 2000), COLOR_A, COLOR_B));
		getElements().add(new LunarModule("lm-n6", new Point2D.Double(600, 1000), COLOR_B, COLOR_A));
		getElements().add(new LunarModule("lm-n6", new Point2D.Double(1400, 900), COLOR_B, COLOR_A));
		getElements().add(new LunarModule("lm-b8", new Point2D.Double(1850, 800), COLOR_B, null));
		getElements().add(new LunarModule("lm-n9", new Point2D.Double(1100, 500), COLOR_B, COLOR_A));
		getElements().add(new LunarModule("lm-b0", new Point2D.Double(600, 200), COLOR_B, null));
	}
	
	@Override
	protected Color getDefaultBoardColor()
	{
		return RALColor.RAL_7032;
	}
	
}
