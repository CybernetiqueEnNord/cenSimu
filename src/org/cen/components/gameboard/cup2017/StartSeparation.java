package org.cen.components.gameboard.cup2017;

import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

public class StartSeparation extends AbstractGameBoardElement
{
	
	private Path2D area = null;
	
	// point (0,0) en haut à gauche
	public StartSeparation(String name, Point2D position)
	{
		super(name, new Position2DMillimeter(position));
		area = new Path2D.Double();
		area.moveTo(position.getX(), position.getY());
		area.lineTo(position.getX()+22, position.getY());
		area.lineTo(position.getX()+22, position.getY()-710);
		area.lineTo(position.getX(), position.getY()-710);
		area.closePath();
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(RALColor.RAL_8002);
		g.fill(area);
		g.translate(position.getValue().getX(), position.getValue().getY());
	}
	
}
