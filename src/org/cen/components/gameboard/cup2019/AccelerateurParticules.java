package org.cen.components.gameboard.cup2019;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

public class AccelerateurParticules extends AbstractGameBoardElement
{
	
	private Color color = null;
	private Path2D outer = null;
	private Path2D inner = null;
	private boolean invertOnYAxis = false;
	
	public AccelerateurParticules(String name, Point2D position, Color color, boolean invertOnYAxis)
	{
		super(name, new Position2DMillimeter(position));
		this.color = color;
		this.invertOnYAxis = invertOnYAxis;
		
		outer = new Path2D.Double();
		inner = new Path2D.Double();
		// begin the path at the bottom left position
		outer.moveTo(position.getX() - 15, position.getY() - 500);
		inner.moveTo(position.getX() - 13, position.getY() - 498);
		outer.lineTo(position.getX() + 15, position.getY() - 500);
		inner.lineTo(position.getX() + 13, position.getY() - 498);
		outer.lineTo(position.getX() + 15, position.getY() - 250);
		
		outer.lineTo(position.getX() + 20, position.getY() - 250);
		
		outer.lineTo(position.getX() + 20, position.getY() + 185);
		
		outer.lineTo(position.getX() + 35, position.getY() + 185);
		
		outer.lineTo(position.getX() + 35, position.getY() + 265);
		
		outer.lineTo(position.getX() + 42, position.getY() + 265);
		
		outer.lineTo(position.getX() + 42, position.getY() + 287);
		
		outer.lineTo(position.getX() + 20, position.getY() + 287);
		
		outer.lineTo(position.getX() + 20, position.getY() + 500);
		inner.lineTo(position.getX() + 13, position.getY() + 478);
		outer.lineTo(position.getX() - 15, position.getY() + 500);
		inner.lineTo(position.getX() - 13, position.getY() + 478);
		outer.closePath();
		inner.closePath();
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(RALColor.RAL_9005);
		g.fill(outer);
		g.setColor(this.color);
		g.fill(inner);	// fill in player color
		g.translate(position.getValue().getX(), position.getValue().getY());
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
	}
	
}
