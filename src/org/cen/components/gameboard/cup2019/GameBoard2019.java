package org.cen.components.gameboard.cup2019;

import java.awt.Color;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoard2018;
import org.cen.components.gameboard.generic.RALColor;

public class GameBoard2019 extends AbstractGameBoard2018
{
	
	public static final Color COLOR_A = RALColor.RAL_4008;
	public static final Color COLOR_B = RALColor.RAL_1023;
	public static final Color REDIUM  = RALColor.RAL_3020;
	public static final Color GREENIUM = RALColor.RAL_6018;
	public static final Color BLUEIUM = RALColor.RAL_5015;
	public static final Color GOLDENIUM = RALColor.RAL_9010;
	
	public GameBoard2019()
	{
		super(COLOR_A, COLOR_B);
		
		getElements().add(new ZoneTableau("zone-redium-a", new Point2D.Double(450d, 225d), REDIUM, false));
		getElements().add(new ZoneTableau("zone-redium-b", new Point2D.Double(450d, 2775d), REDIUM, true));
		getElements().add(new ZoneTableau("zone-greenium-a", new Point2D.Double(750d, 225d), GREENIUM, false));
		getElements().add(new ZoneTableau("zone-greenium-b", new Point2D.Double(750d, 2775d), GREENIUM, true));
		getElements().add(new ZoneTableau("zone-blueium-a", new Point2D.Double(1050d, 225d), BLUEIUM, false));
		getElements().add(new ZoneTableau("zone-blueium-b", new Point2D.Double(1050d, 2775d), BLUEIUM, true));
//		getElements().add(new ZoneGoldenium("zone-goldenium-a", new Point2D.Double(900d, 500d), GOLDENIUM, false));
//		getElements().add(new ZoneGoldenium("zone-goldenium-b", new Point2D.Double(900d, 2500d), GOLDENIUM, true));
		getElements().add(new ZoneChaos("zone-chaos-a", new Point2D.Double(1050d, 1000d), RALColor.RAL_7000, false));
		getElements().add(new ZoneChaos("zone-chaos-b", new Point2D.Double(1050d, 2000d), RALColor.RAL_7000, false));
		
		getElements().add(new ZoneExperience("exp-a", new Point2D.Double(-122, 225), COLOR_B, false));
		getElements().add(new ZoneExperience("exp-b", new Point2D.Double(-122, 2775d), COLOR_A, false));
		
		getElements().add(new PetitDistributeur("pdist-a", new Point2D.Double(2015d, 225d), COLOR_A, false));
		getElements().add(new PetitDistributeur("pdest-b", new Point2D.Double(2015d, 2775d), COLOR_B, false));
		
		getElements().add(new RampeAcces("rampe-a", new Point2D.Double(1578d, 1250d), RALColor.RAL_9005, false));
		getElements().add(new RampeAcces("rampe-b", new Point2D.Double(1578d, 1750d), RALColor.RAL_9005, true));
		getElements().add(new SeparateurBalance("separateur", new Point2D.Double(1689d, 1500d), RALColor.RAL_9005, false));
		getElements().add(new PlateauBalance("plateau-b", new Point2D.Double(1789d, 1365d), COLOR_B, false));
		getElements().add(new PlateauBalance("plateau-a", new Point2D.Double(1789d, 1635d), COLOR_A, true));
		
		getElements().add(new GrandDistributeur("gdist-a", new Point2D.Double(1558d, 750d), RALColor.RAL_7000, false));
		getElements().add(new GrandDistributeur("gdist-b", new Point2D.Double(1558d, 2250d), RALColor.RAL_7000, false));
		
		getElements().add(new AccelerateurParticules("accel-b", new Point2D.Double(15d, 2000d), COLOR_B, false));
		getElements().add(new AccelerateurParticules("accel-a", new Point2D.Double(15d, 1000d), COLOR_A, true));
		
		getElements().add(new AtomePlat("atom-r-a1", new Point2D.Double(450d, 500d), REDIUM, false));
		getElements().add(new AtomePlat("atom-r-b1", new Point2D.Double(450d, 2500d), REDIUM, false));
		getElements().add(new AtomePlat("atop-r-a2", new Point2D.Double(750d, 500d), REDIUM, false));
		getElements().add(new AtomePlat("atop-r-b2", new Point2D.Double(750d, 2500d), REDIUM, false));
		getElements().add(new AtomePlat("atom-g-a1", new Point2D.Double(1050d, 500d), GREENIUM, false));
		getElements().add(new AtomePlat("atom-g-b1", new Point2D.Double(1050d, 2500d), GREENIUM, false));
		
		getElements().add(new AtomePlat("atom-g-a6", new Point2D.Double(1800d, 900d), GREENIUM, false));
		getElements().add(new AtomePlat("atom-g-b6", new Point2D.Double(1800d, 2100d), GREENIUM, false));
		
		getElements().add(new AtomePlat("atom-r-a4", new Point2D.Double(1000d, 950d), REDIUM, false));
		getElements().add(new AtomePlat("atom-r-b4", new Point2D.Double(1000d, 2050d), REDIUM, false));
		getElements().add(new AtomePlat("atom-g-a2", new Point2D.Double(1100d, 950d), GREENIUM, false));
		getElements().add(new AtomePlat("atom-g-b2", new Point2D.Double(1100d, 2050d), GREENIUM, false));
		getElements().add(new AtomePlat("atom-r-a5", new Point2D.Double(1100d, 1050d), REDIUM, false));
		getElements().add(new AtomePlat("atom-r-b5", new Point2D.Double(1100d, 1950d), REDIUM, false));
		getElements().add(new AtomePlat("atom-b-a1", new Point2D.Double(1000d, 1050d), BLUEIUM, false));
		getElements().add(new AtomePlat("atom-b-b1", new Point2D.Double(1000d, 1950d), BLUEIUM, false));
		
		getElements().add(new AtomeDebout("atom-b-a3", new Point2D.Double(2013d, 125d), BLUEIUM, false));
		getElements().add(new AtomeDebout("atom-b-b3", new Point2D.Double(2013d, 2875d), BLUEIUM, false));
		getElements().add(new AtomeDebout("atom-g-a3", new Point2D.Double(2013d, 225d), GREENIUM, false));
		getElements().add(new AtomeDebout("atom-g-b3", new Point2D.Double(2013d, 2775d), GREENIUM, false));
		getElements().add(new AtomeDebout("atom-r-a7", new Point2D.Double(2013d, 325d), REDIUM, false));
		getElements().add(new AtomeDebout("atom-r-b7", new Point2D.Double(2013d, 2675d), REDIUM, false));
		
		getElements().add(new AtomeDebout("atom-r-a8", new Point2D.Double(1556d, 500d), REDIUM, false));
		getElements().add(new AtomeDebout("atom-r-b8", new Point2D.Double(1556d, 2500d), REDIUM, false));
		getElements().add(new AtomeDebout("atom-g-a4", new Point2D.Double(1556d, 600d), GREENIUM, false));
		getElements().add(new AtomeDebout("atom-g-b4", new Point2D.Double(1556d, 2400d), GREENIUM, false));
		getElements().add(new AtomeDebout("atom-r-a9", new Point2D.Double(1556d, 700d), REDIUM, false));
		getElements().add(new AtomeDebout("atom-r-b9", new Point2D.Double(1556d, 2300d), REDIUM, false));
		getElements().add(new AtomeDebout("atom-b-a2", new Point2D.Double(1556d, 800d), BLUEIUM, false));
		getElements().add(new AtomeDebout("atom-b-b2", new Point2D.Double(1556d, 2200d), BLUEIUM, false));
		getElements().add(new AtomeDebout("atom-r-a10", new Point2D.Double(1556d, 900d), REDIUM, false));
		getElements().add(new AtomeDebout("atom-r-b10", new Point2D.Double(1556d, 2100d), REDIUM, false));
		getElements().add(new AtomeDebout("atom-g-a5", new Point2D.Double(1556d, 1000d), GREENIUM, false));
		getElements().add(new AtomeDebout("atom-g-b5", new Point2D.Double(1556d, 2000d), GREENIUM, false));
		
		getElements().add(new AtomeDebout("atom-b-a3", new Point2D.Double(15d, 1300d), BLUEIUM, false));
		getElements().add(new AtomeDebout("atom-b-b3", new Point2D.Double(15d, 1700d), BLUEIUM, false));
		
		getElements().add(new AtomeDebout("atom-g-a1", new Point2D.Double(13d, 775d), GOLDENIUM, false));
		getElements().add(new AtomeDebout("atom-g-a2", new Point2D.Double(37d, 775d), GOLDENIUM, false));
		getElements().add(new AtomeDebout("atom-g-b1", new Point2D.Double(13d, 2225d), GOLDENIUM, false));
		getElements().add(new AtomeDebout("atom-g-b2", new Point2D.Double(37d, 2225d), GOLDENIUM, false));
	}
	
	@Override
	protected Color getDefaultBoardColor()
	{
		return RALColor.RAL_7032;
	}

}
