package org.cen.components.gameboard.cup2019;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.math.position.Position2DMillimeter;

public class RampeAcces extends AbstractGameBoardElement
{
	
	private Color color = null;
	private Path2D outer = null;
	private boolean invertOnYAxis = false;
	
	public RampeAcces(String name, Point2D position, Color color, boolean invertOnYAxis)
	{
		super(name, new Position2DMillimeter(position));
		this.color = color;
		this.invertOnYAxis = invertOnYAxis;
		
		outer = new Path2D.Double();
		// begin the path at the bottom left position
		outer.moveTo(position.getX() + 0, position.getY() - 800);
		// go to inner bottom left
		outer.lineTo(position.getX() + 22, position.getY() - 800);
		// go to inner top left
		outer.lineTo(position.getX() + 22, position.getY() - 22);
		// go to inner top right
		outer.lineTo(position.getX() + 422, position.getY() - 22);
		// go to outer top right
		outer.lineTo(position.getX() + 422, position.getY() + 0);
		// go to outer top left
		outer.lineTo(position.getX() + 0, position.getY() + 0);
		// go to outer bottom left
		outer.closePath();
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(this.color);
		g.fill(outer);	// fill in player color
		g.translate(position.getValue().getX(), position.getValue().getY());
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
	}
	
}
