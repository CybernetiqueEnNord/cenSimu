package org.cen.components.gameboard.cup2019;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.math.position.Position2DMillimeter;

/**
 * ZoneGoldenium class file.
 * 
 * This class represents where the goldenium has to be put.
 * 
 * The element has this shape : (scale about ~ 1 vertical unit = 100mm and 1 horizontal unit = 100mm) 
 * 
 * ++ 
 * ++
 * 
 * The cotations are in mm.
 * 
 * The center of the paint is at the center of the rectangle.
 * 
 * @author Anastaszor
 */
public class ZoneGoldenium extends AbstractGameBoardElement
{
	
	private Color color = null;
	private Path2D outer = null;
	private boolean invertOnYAxis = false;
	
	public ZoneGoldenium(String name, Point2D position, Color color, boolean invertOnYAxis)
	{
		super(name, new Position2DMillimeter(position));
		this.color = color;
		this.invertOnYAxis = invertOnYAxis;
		
		outer = new Path2D.Double();
		// begin the path at the bottom left position
		outer.moveTo(position.getX() - 50, position.getY() - 50);
		// go to bottom right 
		outer.lineTo(position.getX() + 50 , position.getY() - 50);
		// go to top right
		outer.lineTo(position.getX() + 50, position.getY() + 50);
		// go to top left
		outer.lineTo(position.getX() - 50, position.getY() + 50);
		// go to bottom left
		outer.closePath();
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(this.color);
		g.fill(outer);	// fill in player color
		g.translate(position.getValue().getX(), position.getValue().getY());
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
	}

}
