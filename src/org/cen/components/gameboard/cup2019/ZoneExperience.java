package org.cen.components.gameboard.cup2019;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

public class ZoneExperience extends AbstractGameBoardElement
{
	
	private Color color = null;
	private Path2D outer = null;
	private boolean invertOnYAxis = false;
	
	public ZoneExperience(String name, Point2D position, Color color, boolean invertOnYAxis)
	{
		super(name, new Position2DMillimeter(position));
		this.color = color;
		this.invertOnYAxis = invertOnYAxis;
		
		outer = new Path2D.Double();
		// begin the path at the bottom left position
		outer.moveTo(position.getX() - 100, position.getY() - 225);
		// go to bottom right 
		outer.lineTo(position.getX() + 100, position.getY() - 225);
		// go to top right
		outer.lineTo(position.getX() + 100, position.getY() + 225);
		// go to top left
		outer.lineTo(position.getX() - 100, position.getY() + 225);
		
		outer.lineTo(position.getX() - 100, position.getY() + 5);
		
		outer.lineTo(position.getX() - 0  , position.getY() + 5);
		
		outer.curveTo(
			position.getX() + 5, position.getY() + 5,
			position.getX() + 5, position.getY() - 5,
			position.getX() + 0, position.getY() - 5
		);
		
		outer.lineTo(position.getX() - 100, position.getY() - 5);
		
		// go to bottom left
		outer.closePath();
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(this.color);
		g.fill(outer);	// fill in player color
		g.setColor(RALColor.RAL_9005);
		g.draw(outer);
		g.translate(position.getValue().getX(), position.getValue().getY());
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
	}
	
}
