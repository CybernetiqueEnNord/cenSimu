package org.cen.components.gameboard.cup2019;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

public class PlateauBalance extends AbstractGameBoardElement
{
	
	private Color color = null;
	private Path2D outer = null;
	private Path2D inner = null;
	private Path2D cross = null;
	private boolean invertOnYAxis = false;
	
	public PlateauBalance(String name, Point2D position, Color color, boolean invertOnYAxis)
	{
		super(name, new Position2DMillimeter(position));
		this.color = color;
		this.invertOnYAxis = invertOnYAxis;
		
		outer = new Path2D.Double();
		inner = new Path2D.Double();
		// begin the path at the bottom left position
		outer.moveTo(position.getX() - 211, position.getY() - 110);
		inner.moveTo(position.getX() - 209, position.getY() - 108);
		// go to bottom right 
		outer.lineTo(position.getX() + 211, position.getY() - 110);
		inner.lineTo(position.getX() + 209, position.getY() - 108);
		// go to top right
		outer.lineTo(position.getX() + 211, position.getY() + 110);
		inner.lineTo(position.getX() + 209, position.getY() + 108);
		// go to top left
		outer.lineTo(position.getX() - 211, position.getY() + 110);
		inner.lineTo(position.getX() - 209, position.getY() + 108);
		// go to bottom left
		outer.closePath();
		inner.closePath();
		
		cross = new Path2D.Double();
		cross.moveTo(position.getX() - 209, position.getY() + 108);
		cross.lineTo(position.getX() - 2  , position.getY() + 2);
		cross.lineTo(position.getX() - 2  , position.getY() + 120);
		cross.lineTo(position.getX() + 2  , position.getY() + 120);
		cross.lineTo(position.getX() + 2  , position.getY() + 2);
		cross.lineTo(position.getX() + 209, position.getY() + 108);
		cross.lineTo(position.getX() + 211, position.getY() + 106);
		cross.lineTo(position.getX() + 2  , position.getY() + 0);
		cross.lineTo(position.getX() + 211, position.getY() - 106);
		cross.lineTo(position.getX() + 209, position.getY() - 108);
		cross.lineTo(position.getX() + 2  , position.getY() - 2);
		cross.lineTo(position.getX() + 2  , position.getY() - 60);
		cross.lineTo(position.getX() - 2  , position.getY() - 60);
		cross.lineTo(position.getX() - 2  , position.getY() - 2);
		cross.lineTo(position.getX() - 209, position.getY() - 108);
		cross.lineTo(position.getX() - 211, position.getY() - 106);
		cross.lineTo(position.getX() - 2  , position.getY() - 0);
		cross.lineTo(position.getX() - 211, position.getY() + 106);
		cross.closePath();
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(RALColor.RAL_9005);
		g.fill(outer);
		g.setColor(this.color);
		g.fill(inner);	// fill in player color
		g.setColor(RALColor.RAL_9005);
		g.fill(cross);
		g.translate(position.getValue().getX(), position.getValue().getY());
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
	}
	
}
