package org.cen.components.gameboard.cup2019;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.math.position.Position2DMillimeter;

public class ZoneChaos extends AbstractGameBoardElement
{
	
	private Color color = null;
	private Ellipse2D outer = null;
	private boolean invertOnYAxis = false;
	
	public ZoneChaos(String name, Point2D position, Color color, boolean invertOnYAxis)
	{
		super(name, new Position2DMillimeter(position));
		this.color = color;
		this.invertOnYAxis = invertOnYAxis;
		
		outer = new Ellipse2D.Double(position.getX() - 150, position.getY() - 150, 300, 300);
	}
	
	@Override
	public void paint(Graphics2D g)
	{
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
		g.translate(-position.getValue().getX(), -position.getValue().getY());
		g.setColor(this.color);
		g.fill(outer);	// fill in player color
		g.translate(position.getValue().getX(), position.getValue().getY());
		if(this.invertOnYAxis)
		{
			g.scale(1, -1);
		}
	}
	
}
