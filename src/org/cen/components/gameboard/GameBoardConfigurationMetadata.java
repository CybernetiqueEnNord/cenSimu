package org.cen.components.gameboard;

import org.cen.components.gameboard.cup2014.GameBoard2014;
import org.cen.components.gameboard.cup2015.GameBoard2015;
import org.cen.components.gameboard.cup2016.GameBoard2016;
import org.cen.components.gameboard.cup2017.GameBoard2017;
import org.cen.components.gameboard.cup2018.GameBoard2018;
import org.cen.components.gameboard.cup2019.GameBoard2019;
import org.cen.components.gameboard.cup2020.GameBoard2020;
import org.cen.components.gameboard.generic.IGameBoardService;

public enum GameBoardConfigurationMetadata
{
	GB_2020_1(GameBoard2020.class, "Gameboard 2020"),
	GB_2019_1(GameBoard2019.class, "GameBoard 2019"),
	GB_2018_1(GameBoard2018.class, "GameBoard 2018"),
	GB_2017_1(GameBoard2017.class, "GameBoard 2017"),
	GB_2016_1(GameBoard2016.Configuration1.class, "Configuration 1"),
	GB_2016_2(GameBoard2016.Configuration2.class, "Configuration 2"),
	GB_2016_3(GameBoard2016.Configuration3.class, "Configuration 3"),
	GB_2016_4(GameBoard2016.Configuration4.class, "Configuration 4"),
	GB_2016_5(GameBoard2016.Configuration5.class, "Configuration 5"),
	GB_2015_1(GameBoard2015.class, "GameBoard 2015"),
	GB_2014_1(GameBoard2014.class, "GameBoard 2014"),
	;
	
	private Class<? extends IGameBoardService> clazz;
	
	private String buttonName;
	
	private GameBoardConfigurationMetadata(Class<? extends IGameBoardService> gameboardclass, String buttonName)
	{
		this.clazz = gameboardclass;
		this.buttonName = buttonName;
	}
	
	public Class<? extends IGameBoardService> getGameBoardClass()
	{
		return this.clazz;
	}
	
	public IGameBoardService getGameBoard()
	{
		try
		{
			return this.clazz.newInstance();
		}
		catch (InstantiationException | IllegalAccessException e)
		{
			e.printStackTrace();
			return new GameBoard2019();
		}
	}
	
	public String getName()
	{
		return this.buttonName;
	}
	
}
