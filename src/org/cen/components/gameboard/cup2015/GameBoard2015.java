package org.cen.components.gameboard.cup2015;

import java.awt.Color;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.Collections;

import org.cen.components.gameboard.generic.AbstractGameBoard;
import org.cen.components.gameboard.generic.GameBoardElementsComparator;
import org.cen.components.gameboard.generic.RALColor;

/**
 * Gameboard for the cup 2015.
 */
public class GameBoard2015 extends AbstractGameBoard
{
	public static final double BOARD_MIDDLE_HEIGHT = BOARD_HEIGHT / 2.0d;

	public static final double BOARD_MIDDLE_WIDTH = BOARD_WIDTH / 2.0d;

	public static final Color COLOR_A = RALColor.RAL_1023;
	public static final Color COLOR_B = RALColor.RAL_6018;

	public static double symetrize(boolean symetric, double value)
	{
		if (symetric) {
			value = BOARD_HEIGHT - value;
		}
		return value;
	}

	private static AffineTransform symetricTransform;

	public static AffineTransform getSymetricTransform() {
		if (symetricTransform == null) {
			AffineTransform t = new AffineTransform();
			t.translate(0, BOARD_HEIGHT);
			t.scale(1, -1);
			symetricTransform = t;
		}
		return symetricTransform;
	}

	public GameBoard2015()
	{
		super();
		elements.add(new StartArea("start-a", new Point2D.Double(BOARD_MIDDLE_WIDTH, 0), COLOR_A, RALColor.RAL_6018, false));
		elements.add(new StartArea("start-b", new Point2D.Double(BOARD_MIDDLE_WIDTH, 0), COLOR_B, RALColor.RAL_1023, true));

		elements.add(new FilmEditingArea("editing", new Point2D.Double(BOARD_WIDTH, BOARD_MIDDLE_HEIGHT)));
		elements.add(new CentralArea("central", new Point2D.Double(1070, BOARD_MIDDLE_HEIGHT)));

		elements.add(new PathLine("path-a", new Point2D.Double(0, 0), false));
		elements.add(new PathLine("path-b", new Point2D.Double(0, 0), true));

		elements.add(new StepsArea("steps-a", new Point2D.Double(0, BOARD_MIDDLE_HEIGHT - BORDER_WIDTH / 2 - StepsArea.HALF_HEIGHT), COLOR_A));
		elements.add(new StepsArea("steps-b", new Point2D.Double(0, BOARD_MIDDLE_HEIGHT + BORDER_WIDTH / 2 + StepsArea.HALF_HEIGHT), COLOR_B));

		elements.add(new PopCornBasket("popcorn-a-left", new Point2D.Double(1750, 250)));
		elements.add(new PopCornBasket("popcorn-a-right", new Point2D.Double(830, 910)));
		elements.add(new PopCornBasket("popcorn-b-left", new Point2D.Double(1750, 2750)));
		elements.add(new PopCornBasket("popcorn-b-right", new Point2D.Double(830, 2090)));
		elements.add(new PopCornBasket("popcorn-center", new Point2D.Double(1650, 1500)));

		elements.add(new Stand("stand-a-1", new Point2D.Double(200, 90), COLOR_A));
		elements.add(new Stand("stand-a-2", new Point2D.Double(1750, 90), COLOR_A));
		elements.add(new Stand("stand-a-3", new Point2D.Double(1850, 90), COLOR_A));
		elements.add(new Stand("stand-a-4", new Point2D.Double(100, 850), COLOR_A));
		elements.add(new Stand("stand-a-5", new Point2D.Double(200, 850), COLOR_A));
		elements.add(new Stand("stand-a-6", new Point2D.Double(1355, 870), COLOR_A));
		elements.add(new Stand("stand-a-7", new Point2D.Double(1770, 1100), COLOR_A));
		elements.add(new Stand("stand-a-8", new Point2D.Double(1400, 1300), COLOR_A));
		elements.add(new Stand("stand-a-1", new Point2D.Double(200, 2910), COLOR_B));
		elements.add(new Stand("stand-a-2", new Point2D.Double(1750, 2910), COLOR_B));
		elements.add(new Stand("stand-a-3", new Point2D.Double(1850, 2910), COLOR_B));
		elements.add(new Stand("stand-a-4", new Point2D.Double(100, 2150), COLOR_B));
		elements.add(new Stand("stand-a-5", new Point2D.Double(200, 2150), COLOR_B));
		elements.add(new Stand("stand-a-6", new Point2D.Double(1355, 2130), COLOR_B));
		elements.add(new Stand("stand-a-7", new Point2D.Double(1770, 1900), COLOR_B));
		elements.add(new Stand("stand-a-8", new Point2D.Double(1400, 1700), COLOR_B));
		
		elements.add(new PopCornDispenser("dispenser-a-1", new Point2D.Double(0, 300)));
		elements.add(new PopCornDispenser("dispenser-a-2", new Point2D.Double(0, 600)));
		elements.add(new PopCornDispenser("dispenser-b-2", new Point2D.Double(0, 2400)));
		elements.add(new PopCornDispenser("dispenser-b-1", new Point2D.Double(0, 2700)));
		
		Collections.sort(elements, new GameBoardElementsComparator());
	}

	@Override
	protected Color getDefaultBoardColor()
	{
		return RALColor.RAL_7032;
	}
	
}
