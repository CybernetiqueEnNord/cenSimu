package org.cen.components.gameboard.cup2015;

import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.Movable;
import org.cen.components.gameboard.generic.PathUtils;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.position.Position2DMillimeter;

public class FilmEditingArea extends AbstractGameBoardElement {
	private Path2D path = new Path2D.Double();

	public FilmEditingArea(String name, Point2D position) {
		super(name, new Position2DMillimeter(position), new AngleRadian(AngleRadian.PI_2), 1);

		path.moveTo(-400, 0);
		path.lineTo(-400, 100);
		PathUtils.arcTo(path, -300, 200, -400, 200);
		path.lineTo(300, 200);
		PathUtils.arcTo(path, 400, 100, 400, 200);
		path.lineTo(400, 0);
		path.closePath();
	}

	@Override
	public void paint(Graphics2D g) {
		g.setStroke(Strokes.thinStroke);
		g.setColor(RALColor.RAL_3020);
		g.fill(path);
		g.setColor(RALColor.RAL_9005);
		g.draw(path);
	}
	
	@Override
	public Movable getMovableStatus()
	{
		return Movable.MOVABLE;
	}
	
}
