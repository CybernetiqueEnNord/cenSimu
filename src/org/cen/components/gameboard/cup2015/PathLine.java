package org.cen.components.gameboard.cup2015;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.Movable;
import org.cen.components.gameboard.generic.PathUtils;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.position.Position2DMillimeter;

public class PathLine extends AbstractGameBoardElement {
	private Path2D path = new Path2D.Double();

	public PathLine(String name, Point2D position, boolean symetric) {
		super(name, new Position2DMillimeter(position));
		order = 3;
		path.moveTo(820, 550);
		path.lineTo(1700, 550);
		PathUtils.arcTo(path, 1850, 400, 1850, 550);
		PathUtils.arcTo(path, 2000, 250, 1850, 250);
		path.moveTo(1850, 400);
		path.lineTo(1850, 860);
		PathUtils.arcTo(path, 1700, 1010, 1850, 1010);
		PathUtils.arcTo(path, 1550, 1160, 1550, 1010);
		path.lineTo(1550, 1500);
		path.moveTo(1850, 550);
		path.lineTo(2000, 550);
		path.moveTo(1850, 850);
		path.lineTo(2000, 850);
		if (symetric) {
			AffineTransform t = GameBoard2015.getSymetricTransform();
			path.transform(t);
		}
	}

	@Override
	public void paint(Graphics2D g) {
		g.setStroke(Strokes.thickStroke);
		g.setColor(RALColor.RAL_9005);
		g.draw(path);
	}
	
	@Override
	public Movable getMovableStatus()
	{
		return Movable.TRANSPARENT;
	}
	
}
