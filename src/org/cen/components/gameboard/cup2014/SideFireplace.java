package org.cen.components.gameboard.cup2014;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Arc2D;
import java.awt.geom.Point2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.position.Position2DMillimeter;

public class SideFireplace extends AbstractGameBoardElement {
	private Shape shape = new Arc2D.Double(-250, -250, 500, 500, -90, -90, Arc2D.PIE);

	public SideFireplace(String name, Point2D position, IAngle orientation) {
		super(name, new Position2DMillimeter(position), orientation, 3);
	}

	@Override
	public void paint(Graphics2D g) {
		g.setColor(RALColor.RAL_8002);
		g.fill(shape);
	}
}
