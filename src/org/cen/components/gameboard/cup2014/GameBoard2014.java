package org.cen.components.gameboard.cup2014;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Collections;
import java.util.List;

import org.cen.components.gameboard.generic.AbstractGameBoard;
import org.cen.components.gameboard.generic.Border;
import org.cen.components.gameboard.generic.GameBoardElementsComparator;
import org.cen.components.gameboard.generic.IGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.position.Position2DMillimeter;

/**
 * Gameboard for the cup 2012.
 */
public class GameBoard2014 extends AbstractGameBoard
{

	public static final double BOARD_MIDDLE_HEIGHT = BOARD_HEIGHT / 2.0d;

	public static final double BOARD_MIDDLE_WIDTH = BOARD_WIDTH / 2.0d;

	public static double symetrize(boolean symetric, double value)
	{
		if (symetric) {
			value = BOARD_HEIGHT - value;
		}
		return value;
	}
	
	public GameBoard2014()
	{
		super();
		elements.add(new Tree("tree-red1", new Point2D.Double(1300, 0), new AngleRadian(0.0)));
		elements.add(new Tree("tree-red2", new Point2D.Double(BOARD_WIDTH, 700), new AngleRadian(AngleRadian.PI_2)));
		elements.add(new Tree("tree-yellow1", new Point2D.Double(1300, BOARD_HEIGHT), new AngleRadian(0.0)));
		elements.add(new Tree("tree-yellow2", new Point2D.Double(BOARD_WIDTH, BOARD_HEIGHT - 700), new AngleRadian(AngleRadian.PI_2)));

		elements.add(new CentralFireplace("fireplace-centre", new Point2D.Double(1050, BOARD_HEIGHT / 2)));
		elements.add(new SideFireplace("fireplace-red", new Point2D.Double(BOARD_WIDTH, 0), new AngleRadian(0.0)));
		elements.add(new SideFireplace("fireplace-yellow", new Point2D.Double(BOARD_WIDTH, BOARD_HEIGHT), new AngleRadian(AngleRadian.PI_2)));

		elements.add(new Basket("basket-red", new Point2D.Double(0, BOARD_HEIGHT - 400 - 700), RALColor.RAL_3020));
		elements.add(new Basket("basket-yellow", new Point2D.Double(0, 400), RALColor.RAL_1023));

		elements.add(new MobileTorch("torch-mobile-red", new Point2D.Double(1100, 900)));
		elements.add(new MobileTorch("torch-mobile-yellow", new Point2D.Double(1100, BOARD_HEIGHT - 900)));
		elements.add(new FixedTorch("torch-fixed-red1", new Point2D.Double(800, 0), new AngleRadian(0.0)));
		elements.add(new FixedTorch("torch-fixed-red2", new Point2D.Double(BOARD_WIDTH, 1300), new AngleRadian(AngleRadian.PI_2)));
		elements.add(new FixedTorch("torch-fixed-yellow1", new Point2D.Double(800, BOARD_HEIGHT), new AngleRadian(Math.PI)));
		elements.add(new FixedTorch("torch-fixed-yellow2", new Point2D.Double(BOARD_WIDTH, BOARD_HEIGHT - 1300), new AngleRadian(AngleRadian.PI_2)));

		elements.add(new Fire("fire-torch-fixed-red1", new Point2D.Double(800, Fire.HEIGHT / 2), new AngleRadian(0.0)));
		elements.add(new Fire("fire-torch-fixed-red2", new Point2D.Double(BOARD_WIDTH - Fire.HEIGHT / 2, 1300), new AngleRadian(AngleRadian.PI_2)));
		elements.add(new Fire("fire-torch-fixed-yellow1", new Point2D.Double(800, BOARD_HEIGHT - Fire.HEIGHT / 2), new AngleRadian(Math.PI)));
		elements.add(new Fire("fire-torch-fixed-yellow2", new Point2D.Double(BOARD_WIDTH - Fire.HEIGHT / 2, BOARD_HEIGHT - 1300), new AngleRadian(AngleRadian.PI_2)));

		elements.add(new Fire("fire-free-red1", new Point2D.Double(1100, 400), new AngleRadian(AngleRadian.PI_2)));
		elements.add(new Fire("fire-free-red2", new Point2D.Double(1600, 900), new AngleRadian(0.0)));
		elements.add(new Fire("fire-free-red3", new Point2D.Double(600, 900), new AngleRadian(0.0)));
		elements.add(new Fire("fire-free-yellow1", new Point2D.Double(1100, BOARD_HEIGHT - 400), new AngleRadian(AngleRadian.PI_2)));
		elements.add(new Fire("fire-free-yellow2", new Point2D.Double(1600, BOARD_HEIGHT - 900), new AngleRadian(0.0)));
		elements.add(new Fire("fire-free-yellow3", new Point2D.Double(600, BOARD_HEIGHT - 900), new AngleRadian(0.0)));

		elements.add(new FrescoPath("fresco-red", new Point2D.Double(0, 0), false));
		elements.add(new FrescoPath("fresco-yellow", new Point2D.Double(0, 0), true));

		elements.add(new StartArea("start-red", new Point2D.Double(0, 0), RALColor.RAL_3020, false));
		elements.add(new StartArea("start-yellow", new Point2D.Double(0, 0), RALColor.RAL_1023, true));

		elements.add(new Border("border", 700, BORDER_WIDTH, RALColor.RAL_3020, new Position2DMillimeter(0d, -BORDER_WIDTH), new AngleRadian(0.0), 3));
		elements.add(new Border("border", 700, BORDER_WIDTH, RALColor.RAL_1023, new Position2DMillimeter(0d, BOARD_HEIGHT), new AngleRadian(0.0), 3));
		elements.add(new Border("border", BOARD_WIDTH - 700 + BORDER_WIDTH, BORDER_WIDTH, Color.WHITE, new Position2DMillimeter(700d, -BORDER_WIDTH), new AngleRadian(0.0), 3));
		elements.add(new Border("border", BOARD_WIDTH - 700 + BORDER_WIDTH, BORDER_WIDTH, Color.WHITE, new Position2DMillimeter(700d, BOARD_HEIGHT), new AngleRadian(0.0), 3));
		elements.add(new Border("border", BORDER_WIDTH, 400 + BORDER_WIDTH, RALColor.RAL_3020, new Position2DMillimeter(-BORDER_WIDTH, -BORDER_WIDTH), new AngleRadian(0.0), 3));
		elements.add(new Border("border", BORDER_WIDTH, 400 + BORDER_WIDTH, RALColor.RAL_1023, new Position2DMillimeter(-BORDER_WIDTH, BOARD_HEIGHT - 400), new AngleRadian(0.0), 3));
		elements.add(new Border("border", BORDER_WIDTH, 800, Color.WHITE, new Position2DMillimeter(-BORDER_WIDTH, 1100d), new AngleRadian(0.0), 3));
		elements.add(new Border("border", BORDER_WIDTH, 700, RALColor.RAL_8002, new Position2DMillimeter(-BORDER_WIDTH, 400d), new AngleRadian(0.0), 3));
		elements.add(new Border("border", BORDER_WIDTH, 700, RALColor.RAL_8002, new Position2DMillimeter(-BORDER_WIDTH, 1900d), new AngleRadian(0.0), 3));

		elements.add(new Beacon("beacon-red-1", new Point2D.Double(-62, -62), RALColor.RAL_3020));
		elements.add(new Beacon("beacon-red-2", new Point2D.Double(BOARD_WIDTH + 62, -62), RALColor.RAL_3020));
		elements.add(new Beacon("beacon-red-3", new Point2D.Double(BOARD_WIDTH / 2, BOARD_HEIGHT + 62), RALColor.RAL_3020));

		elements.add(new Beacon("beacon-yellow-1", new Point2D.Double(-62, BOARD_HEIGHT + 62), RALColor.RAL_1023));
		elements.add(new Beacon("beacon-yellow-2", new Point2D.Double(BOARD_WIDTH + 62, BOARD_HEIGHT + 62), RALColor.RAL_1023));
		elements.add(new Beacon("beacon-yellow-3", new Point2D.Double(BOARD_WIDTH / 2, -62), RALColor.RAL_1023));

		Collections.sort(elements, new GameBoardElementsComparator());
	}

	@Override
	public List<IGameBoardElement> getElements() {
		return elements;
	}

	@Override
	public Rectangle2D getGameplayBounds() {
		return gameplayBounds;
	}

	@Override
	public Rectangle2D getVisibleBounds() {
		return visibleBounds;
	}
	
	@Override
	protected Color getDefaultBoardColor()
	{
		return RALColor.RAL_6001;
	}
	
}
