package org.cen.components.gameboard.cup2014;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.RALColor;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.position.Position2DMillimeter;

public class FixedTorch extends AbstractGameBoardElement {
	private Shape rect1 = new Rectangle2D.Double(-47 - 22, 0, 22, 22);
	private Shape rect2 = new Rectangle2D.Double(47, 0, 22, 22);

	public FixedTorch(String name, Point2D position, IAngle orientation) {
		super(name, new Position2DMillimeter(position), orientation, 3);
	}

	@Override
	public void paint(Graphics2D g) {
		// torch
		g.setColor(RALColor.RAL_7032);
		g.fill(rect1);
		g.fill(rect2);
	}
}
