package org.cen.components.gameboard.cup2014;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.cen.components.gameboard.generic.AbstractGameBoardElement;
import org.cen.components.gameboard.generic.Movable;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.position.Position2DMillimeter;

public class Fire extends AbstractGameBoardElement {
	public static final double WIDTH = 140 - (30 / Math.cos(Math.PI / 6) / 2);
	public static final double HEIGHT = 30;
	private Shape rect = new Rectangle2D.Double(-WIDTH / 2, -HEIGHT / 2, WIDTH, HEIGHT);

	public Fire(String name, Point2D position, IAngle orientation) {
		super(name, new Position2DMillimeter(position), orientation, 5);
	}

	@Override
	public void paint(Graphics2D g) {
		// torch
		g.setColor(Color.BLACK);
		g.fill(rect);
	}
	
	@Override
	public Movable getMovableStatus()
	{
		return Movable.MOVABLE;
	}
	
}
