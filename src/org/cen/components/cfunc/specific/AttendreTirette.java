package org.cen.components.cfunc.specific;

import org.cen.components.cfunc.interfaces.CType;

/**
 * Demande au robot d'attendre que la tirette soit levée avant de continuer
 * son plan d'actions.
 * 
 * @return void
 */
public class AttendreTirette extends RobinFunction
{
	
	public AttendreTirette()
	{
		super(CType.VOID, "attendre_tirette", null);
	}
	
}
