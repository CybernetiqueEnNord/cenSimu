package org.cen.components.cfunc.specific;

import org.cen.components.cfunc.CArgument;
import org.cen.components.cfunc.CArgumentList;
import org.cen.components.cfunc.interfaces.CType;

/**
 * Demande au robot d'exécuter l'ordre donné.
 * 
 * @param int8 ordre le numéro de l'ordre souhaité. valeurs entre [-128, +127]
 * @return void
 */
public class EnvoyerOrdreNxt extends RobinFunction
{
	
	public static final String ARG1NAME = "ordre";
	
	public EnvoyerOrdreNxt()
	{
		super(CType.VOID, "envoyer_ordre_NXT", new CArgumentList()
			.addArgument(new CArgument(CType.OCTET, ARG1NAME))
		);
	}
	
}
