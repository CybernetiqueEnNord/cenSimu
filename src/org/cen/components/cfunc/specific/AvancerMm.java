package org.cen.components.cfunc.specific;

import org.cen.components.cfunc.CArgument;
import org.cen.components.cfunc.CArgumentList;
import org.cen.components.cfunc.interfaces.CType;

/**
 * Demande au robot d'avancer d'une certaine quantité de déplacement.
 * Calcule le nombre de pas et délègue à FCM_XCall_avancer_pas().
 * 
 * @param int16 distance_mm la quantité de distance à combler jusqu'à la cible, en mm
 * 						valeurs entre [-32768, +32767]
 * 						// TODO a recalculer avec le taux de conversion en pas
 * @return int8 = 0
 * @todo renvoyer une valeur pour savoir de combien on a manqué la cible
 */
public class AvancerMm extends RobinFunction
{
	
	public static final String ARG1NAME = "distance_mm";
	
	public AvancerMm()
	{
		super(CType.OCTET, "avancer_mm", new CArgumentList()
			.addArgument(new CArgument(CType.INT, ARG1NAME))
		);
	}
	
}
