package org.cen.components.cfunc.specific;

import org.cen.components.cfunc.CArgument;
import org.cen.components.cfunc.CArgumentList;
import org.cen.components.cfunc.interfaces.CType;

/**
 * Demande au robot de se recaler. Le robot effectuera un déplacement linéaire
 * jusqu'à ce que la quantité de mouvement soit épuisée, ou que l'erreur de
 * déplacement soit trop grande, suivant ce qui vient en premier. Le robot 
 * déverrouille l'asservissement angulaire dès que l'erreur devient trop grande,
 * ce qui lui permet de devenir perpendiculaire à la bordure qui le bloque
 * (pour autant que la base mécanique soit bien perpendiculaire).
 * 
 * @param int16 distance la quantité de distance maximale que le robot doit
 * 					parcourir avant de s'arrêter obligatoirement. Une quantité
 * 					positive fera avancer le robot jusqu'à la butée, une quantité
 * 					négative fera reculer le robot jusqu'à la butée. valeurs
 * 					entre [-32768, +32767]
 * @return void
 * @todo renvoyer la valeur de la distance effective parcourue
 */
public class RecalagePas extends RobinFunction
{
	
	public static final String ARG1NAME = "distance";
	
	public RecalagePas()
	{
		super(CType.VOID, "recalage_pas", new CArgumentList()
			.addArgument(new CArgument(CType.INT, ARG1NAME))
		);
	}
	
}
