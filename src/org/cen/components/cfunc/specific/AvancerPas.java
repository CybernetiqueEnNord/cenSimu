package org.cen.components.cfunc.specific;

import org.cen.components.cfunc.CArgument;
import org.cen.components.cfunc.CArgumentList;
import org.cen.components.cfunc.interfaces.CType;

/**
 * Demande au robot d'avancer d'une certaine quantité de déplacement.
 * 
 * @param int16 distance_pas la quantité de distance à combler jusqu'à la 
 * 						cible, en pas, valeurs entre [-32768, +32767]
 * @return int16 = 0
 * @todo renvoyer une valeur pour savoir de combien on a manqué la cible
 */
public class AvancerPas extends RobinFunction
{
	
	public static final String ARG1NAME = "distance_pas";
	
	public AvancerPas()
	{
		super(CType.VOID, "avancer_pas", new CArgumentList()
			.addArgument(new CArgument(CType.INT, ARG1NAME))
		);
	}
	
}
