package org.cen.components.cfunc.specific;

import org.cen.components.cfunc.CArgument;
import org.cen.components.cfunc.CArgumentList;
import org.cen.components.cfunc.interfaces.CType;

/**
 * Demande au robot de tourner sur lui même d'une certaine quantité d'angle.
 * Calcule la valeur en pas angulaire, puis délègue à {@link TournerPas}.
 * 
 * @param int16 angle_deci_degree la quantité d'angle dont il faut tourner, en
 * 					décidegrés. valeurs entre [-32768, 32767]
 * @return int8 = 0
 * @todo renvoyer une valeur pour savoir de combien on a manqué la cible
 */
public class TournerDeciDegree extends RobinFunction
{
	
	public static final String ARG1NAME = "angle_deci_degree";
	
	public TournerDeciDegree()
	{
		super(CType.OCTET, "tourner_deci_degree", new CArgumentList()
			.addArgument(new CArgument(CType.INT, ARG1NAME))
		);
	}
	
}
