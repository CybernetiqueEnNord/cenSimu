package org.cen.components.cfunc.specific;

import org.cen.components.cfunc.CArgument;
import org.cen.components.cfunc.CArgumentList;
import org.cen.components.cfunc.interfaces.CType;

/**
 * Demande au robot de tourner sur lui même d'une certaine quantité d'angle.
 * 
 * @param int32 angle_pas la quantité d'angle dont il faut tourner, en pas.
 * 					valeurs entre [-2147483648, +2147483647]
 * @return void
 * @todo renvoyer une valeur pour savoir de combien on a manqué la cible
 */
public class TournerPas extends RobinFunction
{
	
	public static final String ARG1NAME = "angle_pas";
	
	public TournerPas()
	{
		super(CType.VOID, "tourner_pas", new CArgumentList()
			.addArgument(new CArgument(CType.LONG, ARG1NAME))
		);
	}
	
}
