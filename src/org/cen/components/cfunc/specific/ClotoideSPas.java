package org.cen.components.cfunc.specific;

import org.cen.components.cfunc.CArgument;
import org.cen.components.cfunc.CArgumentList;
import org.cen.components.cfunc.interfaces.CType;

public class ClotoideSPas extends RobinFunction
{
	
	public static final String ARG1NAME = "longueur_curviligne_pas";
	public static final String ARG2NAME = "vitesse";
	public static final String ARG3NAME = "longueur_declenchement_1_pas";
	public static final String ARG4NAME = "angle_1_pas";
	public static final String ARG5NAME = "longueur_declenchement_2_pas";
	public static final String ARG6NAME = "angle_2_pas";
	
	public ClotoideSPas()
	{
		super(CType.OCTET, "clotoide_s_pas", new CArgumentList()
			.addArgument(new CArgument(CType.INT, ARG1NAME))
			.addArgument(new CArgument(CType.OCTET, ARG2NAME))
			.addArgument(new CArgument(CType.INT, ARG3NAME))
			.addArgument(new CArgument(CType.INT, ARG4NAME))
			.addArgument(new CArgument(CType.INT, ARG5NAME))
			.addArgument(new CArgument(CType.INT, ARG6NAME))
		);
	}
	
}
