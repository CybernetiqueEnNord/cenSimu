package org.cen.components.cfunc.specific;

import org.cen.components.cfunc.CArgument;
import org.cen.components.cfunc.CArgumentList;
import org.cen.components.cfunc.interfaces.CType;

/**
 * Demande au robot d'obtenir l'angle voulu. Aligne le robot dans la direction
 * donnée par l'angle en fonction de l'angle odométrique actuel.
 * 
 * @param int32 angleAAtteindre_pas l'angle à atteindre en valeur absolue 
 * 						valeurs entre [-2147483648, +2147483647]
 * @return void
 * @todo renvoyer une valeur pour savoir de combien on a manqué la cible
 */
public class AtteindreAnglePas extends RobinFunction
{
	
	public static final String ARG1NAME = "angleAAtteindre_pas";
	
	public AtteindreAnglePas()
	{
		super(CType.VOID, "atteindre_angle_pas", new CArgumentList()
			.addArgument(new CArgument(CType.LONG, ARG1NAME))
		);
	}
	
}
