package org.cen.components.cfunc.specific;

import org.cen.components.cfunc.CArgument;
import org.cen.components.cfunc.CArgumentList;
import org.cen.components.cfunc.interfaces.CType;

/**
 * Demande au robot d'atteindre un point (X,Y) donnée à partir de la position
 * et de l'angle odométriques actuels.
 * Procède en 2 temps : d'abord aligne le robot dans la direction X/Y, puis
 * 		avance de la quantité calculée jusqu'à atteindre (X,Y) en ligne droite.
 * 
 * @param int16 X_pas la coordonnée X du point à atteindre, en pas.
 * 						valeurs entre [-32768, +32767]
 * @param int16 Y_pas la coordonnée Y du point à atteindre, en pas.
 * 						valeurs entre [-32768, +32767]
 * @return int16 = 0
 * @todo renvoyer une valeur pour savoir de combien on a manqué la cible
 */
public class AtteindreXYpas extends RobinFunction
{
	
	public static final String ARG1NAME = "X_pas";
	public static final String ARG2NAME = "Y_pas";
	
	public AtteindreXYpas()
	{
		super(CType.INT, "atteindreXY_pas", new CArgumentList()
			.addArgument(new CArgument(CType.INT, ARG1NAME))
			.addArgument(new CArgument(CType.INT, ARG2NAME))
		);
	}
	
}
