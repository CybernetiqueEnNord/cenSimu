package org.cen.components.cfunc.specific;

import org.cen.components.cfunc.CArgument;
import org.cen.components.cfunc.CArgumentList;
import org.cen.components.cfunc.interfaces.CType;

/**
 * Demande au robot d'atteindre un point (X,Y) donné à partir de la position
 * et de l'angle odométriques actuels.
 * Calcule le nombre de pas et délégue à {@link AtteindreXYpas}.
 * 
 * @param int16 x_mm la coordonnée X du point à atteindre, en mm.
 * 						valeurs entre [-32768, +32767] 
 * 						// TODO a recalculer avec le taux de conversion en pas
 * @param int16 y_mm la coordonnée Y du point à atteindre, en mm.
 * 						valeurs entre [-32768, +32767]
 * 						// TODO a recalculer avec le taux de conversion en pas
 * @return void
 * @todo renvoyer une valeur pour savoir de combien on a manqué la cible
 */
public class AtteindreXYmm extends RobinFunction
{
	public static final String ARG1NAME = "x_mm";
	public static final String ARG2NAME = "y_mm";
	
	public AtteindreXYmm()
	{
		super(CType.VOID, "atteindreXY_mm", new CArgumentList()
			.addArgument(new CArgument(CType.INT, ARG1NAME))
			.addArgument(new CArgument(CType.INT, ARG2NAME))
		);
	}
	
}
