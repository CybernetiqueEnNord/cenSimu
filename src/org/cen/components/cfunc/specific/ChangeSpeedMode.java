package org.cen.components.cfunc.specific;

import org.cen.components.cfunc.CArgument;
import org.cen.components.cfunc.CArgumentList;
import org.cen.components.cfunc.interfaces.CType;

public class ChangeSpeedMode extends RobinFunction
{
	
	public static final String ARG1NAME = "speed_mode";
	
	public ChangeSpeedMode()
	{
		super(CType.VOID, "change_speed_mode", new CArgumentList()
			.addArgument(new CArgument(CType.OCTET, ARG1NAME))
		);
	}
	
}
