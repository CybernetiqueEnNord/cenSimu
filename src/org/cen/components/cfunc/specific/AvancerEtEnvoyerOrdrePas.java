package org.cen.components.cfunc.specific;

import org.cen.components.cfunc.CArgument;
import org.cen.components.cfunc.CArgumentList;
import org.cen.components.cfunc.interfaces.CType;

/**
 * Demande au robot d'effectuer simultanément un déplacement, et d'exécuter
 * l'ordre demandé aprés qu'un certain délai soit passé. Si le robot a 
 * effectué le déplacement mais que le délai n'est pas terminé, alors l'ordre 
 * ne sera pas exécuté.
 * 
 * @param int16 distance_pas la quantité de distance à combler jusqu'à la cible
 * 						valeurs entre [-32768, +32767]
 * @param int8 ordre_nxt l'ordre qu'il est demandé d'exécuter
 * 						valeurs entre [-128, +127]
 * @param int16 delay_ms le délai depuis le début du déplacement avant d'envoyer
 * 						l'ordre au NXT. valeurs entre [0, 32767]
 * @return void
 * @todo renvoyer une valeur pour savoir de combien on a manqué la cible (distance)
 */
public class AvancerEtEnvoyerOrdrePas extends RobinFunction
{
	
	public static final String ARG1NAME = "distance_pas";
	public static final String ARG2NAME = "ordre_nxt";
	public static final String ARG3NAME = "delay_ms";
	
	public AvancerEtEnvoyerOrdrePas()
	{
		super(CType.VOID, "avancer_et_envoyer_ordre_pas", new CArgumentList()
			.addArgument(new CArgument(CType.INT, ARG1NAME))
			.addArgument(new CArgument(CType.OCTET, ARG2NAME))
			.addArgument(new CArgument(CType.INT, ARG3NAME))
		);
	}
	
}
