package org.cen.components.cfunc.specific;

import org.cen.components.cfunc.CArgument;
import org.cen.components.cfunc.CArgumentList;
import org.cen.components.cfunc.interfaces.CType;

public class RecalageMm extends RobinFunction
{
	
	public static final String ARG1NAME = "distance_mm";
	
	public RecalageMm()
	{
		super(CType.OCTET, "recalage_mm", new CArgumentList()
			.addArgument(new CArgument(CType.INT, ARG1NAME))
		);
	}
	
}
