package org.cen.components.cfunc.specific;

import org.cen.components.cfunc.CArgument;
import org.cen.components.cfunc.CArgumentList;
import org.cen.components.cfunc.interfaces.CType;

/**
 * Demande au robot de se recaler. Le robot effectuera un déplacement linéaire
 * jusqu'à ce que la quantité de mouvement soit épuisée, ou que l'erreur de 
 * déplacement soit trop grande, suivant ce qui vient en premier. Une fois à
 * l'arrêt, le robot reset sa position linéaire et angulaire avec les paramètres
 * qui ont été transmis.
 *
 * @param int16 distance_pas la distance maximale que le robot doit parcourir
 * 					avant de s'arr�ter obligatoirement. Une quantité positive
 * 					fera avancer le robot jusqu'à la butée, une quantité 
 * 					négative fera reculer le robot jusqu'à la butée. Valeurs
 * 					entre [-32768, +32767]
 * @param int16 x_pas la nouvelle position x que le robot doit prendre en
 * 					compte comme le départ du prochain mouvement. Valeurs entre
 * 					[-32768, +32767]
 * @param int16 y_pas la nouvelle position y que le robot doit prendre en
 * 					compte comme le départ du prochain mouvement. Valeurs entre
 * 					[-32768, +32767]
 * @param int16 angle_pas le nouvel angle que le robot doit prendre en compte
 * 					pour le départ du prochain mouvement. Valeurs entre
 * 					[-32768, +32767]
 * @return int16 0
 * @todo renvoyer la valeur de la distance effective parcourue
 */
public class RecalagePasSetPosition extends RobinFunction
{
	
	public static final String ARG1NAME = "distance_pas";
	public static final String ARG2NAME = "x_pas";
	public static final String ARG3NAME = "y_pas";
	public static final String ARG4NAME = "angle_pas";
	
	public RecalagePasSetPosition()
	{
		super(CType.LONG, "recalage_pas_set_position", new CArgumentList()
			.addArgument(new CArgument(CType.INT, ARG1NAME))
			.addArgument(new CArgument(CType.INT, ARG2NAME))
			.addArgument(new CArgument(CType.INT, ARG3NAME))
			.addArgument(new CArgument(CType.INT, ARG4NAME))
		);
	}
	
}
