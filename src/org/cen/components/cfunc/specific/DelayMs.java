package org.cen.components.cfunc.specific;

import org.cen.components.cfunc.CArgument;
import org.cen.components.cfunc.CArgumentList;
import org.cen.components.cfunc.CFunction;
import org.cen.components.cfunc.interfaces.CType;


public class DelayMs extends CFunction
{
	
	public static final String ARG1NAME = "delay";
	
	public DelayMs()
	{
		super(CType.VOID, "delay_ms", new CArgumentList()
			.addArgument(new CArgument(CType.OCTET, ARG1NAME))
		);
	}
	
}
