package org.cen.components.cfunc.specific;

import org.cen.components.cfunc.CArgumentList;
import org.cen.components.cfunc.CFunction;
import org.cen.components.cfunc.interfaces.ICType;


public abstract class RobinFunction extends CFunction
{
	
	public RobinFunction(ICType returnType, String name, CArgumentList arglist)
	{
		super(returnType, "FCM_XCall_" + name, arglist);
	}
	
}
