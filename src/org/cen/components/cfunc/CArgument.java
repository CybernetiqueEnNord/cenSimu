package org.cen.components.cfunc;

import org.cen.components.cfunc.exceptions.CInacceptableFormatException;
import org.cen.components.cfunc.exceptions.CLowerOverflowException;
import org.cen.components.cfunc.exceptions.CUpperOverflowException;
import org.cen.components.cfunc.interfaces.ICArgument;
import org.cen.components.cfunc.interfaces.ICArgumentValue;
import org.cen.components.cfunc.interfaces.ICType;

public class CArgument implements ICArgument
{
	
	private ICType type = null;
	private String name = null;
	
	public CArgument(ICType type, String name)
	{
		this.type = type;
		this.name = name;
	}
	
	@Override
	public ICType getType()
	{
		return type;
	}
	
	@Override
	public String getName()
	{
		return name;
	}
	
	@Override
	public void checkAccepts(ICArgumentValue argval) throws 
		CInacceptableFormatException, CUpperOverflowException, CLowerOverflowException
	{
		Object value = argval.getValue();
		if(value == null)
		{
			if(!type.acceptsNull())
				throw new CInacceptableFormatException();
			return;
		}
		if(!Long.class.isAssignableFrom(value.getClass()))
			throw new CInacceptableFormatException();
		
		Long valuel = (Long) value;
		if(valuel > type.getBorneSup())
			throw new CUpperOverflowException(type.getBorneSup(), valuel);
		if(valuel < type.getBorneInf())
			throw new CLowerOverflowException(type.getBorneInf(), valuel);
	}
	
	@Override
	public String toString()
	{
		return getType().toString() + " " + getName();
	}
	
}
