package org.cen.components.cfunc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.cen.components.cfunc.interfaces.ICArgument;
import org.cen.components.cfunc.interfaces.ICArgumentList;


public class CArgumentList implements ICArgumentList
{
	
	private List<ICArgument> arguments = null;
	
	public CArgumentList()
	{
		arguments = new ArrayList<>();
	}
	
	public CArgumentList addArgument(ICArgument argument)
	{
		arguments.add(argument);
		return this;
	}
	
	@Override
	public Iterator<ICArgument> iterator()
	{
		return arguments.iterator();
	}
	
	@Override
	public ICArgument get(String name)
	{
		for(ICArgument arg:arguments)
		{
			if(arg.getName().equals(name))
				return arg;
		}
		return null;
	}
	
	@Override
	public int getCount()
	{
		return arguments.size();
	}
	
	@Override
	public String toString()
	{
		return getClass() + arguments.toString();
	}
	
}
