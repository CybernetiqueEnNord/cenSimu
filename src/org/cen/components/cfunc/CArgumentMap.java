package org.cen.components.cfunc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.cen.components.cfunc.interfaces.ICArgumentMap;
import org.cen.components.cfunc.interfaces.ICArgumentValue;


public class CArgumentMap implements ICArgumentMap
{
	
	private List<ICArgumentValue> values = new ArrayList<>();
	
	@Override
	public Iterator<ICArgumentValue> iterator()
	{
		return values.iterator();
	}
	
	@Override
	public ICArgumentValue get(String name)
	{
		for(ICArgumentValue value:values)
		{
			if(value.getName().equals(name))
				return value;
		}
		return null;
	}
	
	@Override
	public void add(ICArgumentValue value)
	{
		values.add(value);
	}
	
	@Override
	public String toString()
	{
		return getClass() + values.toString();
	}
	
}
