package org.cen.components.cfunc;

import org.cen.components.cfunc.interfaces.ICArgumentValue;


public class CArgumentValue implements ICArgumentValue
{
	
	private String argname = null;
	private Long value = null;
	
	public CArgumentValue(String arg1name, Long value)
	{
		this.argname = arg1name;
		this.value = value;
	}
	
	@Override
	public String getName()
	{
		return argname;
	}
	
	@Override
	public Object getValue()
	{
		return value;
	}
	
	@Override
	public String toString()
	{
		return argname+":"+value;
	}
	
}
