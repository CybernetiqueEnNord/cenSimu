package org.cen.components.cfunc.exceptions;


public class CInacceptableFormatException extends CValidationException
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CInacceptableFormatException()
	{
		super("Inacceptable format");
	}
	
}
