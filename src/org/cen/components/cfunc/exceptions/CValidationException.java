package org.cen.components.cfunc.exceptions;


public class CValidationException extends Exception
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CValidationException(String str)
	{
		super(str);
	}
	
}
