package org.cen.components.cfunc.exceptions;

import org.cen.components.cfunc.interfaces.ICArgument;


public class CMissingArgumentException extends CValidationException
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ICArgument argument = null;
	
	public CMissingArgumentException(ICArgument arg)
	{
		super("L'argument "+arg.getName()+" est manquant.");
		argument = arg;
	}
	
	public ICArgument getArgument()
	{
		return argument;
	}
	
}
