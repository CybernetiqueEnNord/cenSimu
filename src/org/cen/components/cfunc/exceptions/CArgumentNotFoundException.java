package org.cen.components.cfunc.exceptions;

import org.cen.components.cfunc.interfaces.ICArgumentValue;

public class CArgumentNotFoundException extends CValidationException
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ICArgumentValue argval = null;
	
	public CArgumentNotFoundException(ICArgumentValue argval)
	{
		super("L'argument "+argval.getName()+" non trouvé.");
		this.argval = argval;
	}
	
	public ICArgumentValue getArgument()
	{
		return argval;
	}
	
}
