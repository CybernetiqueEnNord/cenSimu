package org.cen.components.cfunc.exceptions;


public class CLowerOverflowException extends CValidationException
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long borneInf = null;
	private Long actualValue = null;
	
	public CLowerOverflowException(Long borneInf, Long actualValue)
	{
		super("lower overflow");
		this.borneInf = borneInf;
		this.actualValue = actualValue;
	}
	
	public Long getMinValue()
	{
		return borneInf;
	}
	
	public Long getActualValue()
	{
		return actualValue;
	}
	
}
