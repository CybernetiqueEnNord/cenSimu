package org.cen.components.cfunc.exceptions;


public class CUpperOverflowException extends CValidationException
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long borneSup = null;
	private Long actualValue = null;
	
	public CUpperOverflowException(Long borneSup, Long actualValue)
	{
		super("upper overflow");
		this.borneSup = borneSup;
		this.actualValue = actualValue;
	}
	
	public Long getMaxValue()
	{
		return borneSup;
	}
	
	public Long getActualValue()
	{
		return actualValue;
	}
	
}
