package org.cen.components.cfunc;

import org.cen.components.cfunc.exceptions.CArgumentNotFoundException;
import org.cen.components.cfunc.exceptions.CInacceptableFormatException;
import org.cen.components.cfunc.exceptions.CLowerOverflowException;
import org.cen.components.cfunc.exceptions.CMissingArgumentException;
import org.cen.components.cfunc.exceptions.CUpperOverflowException;
import org.cen.components.cfunc.interfaces.ICArgument;
import org.cen.components.cfunc.interfaces.ICArgumentList;
import org.cen.components.cfunc.interfaces.ICArgumentMap;
import org.cen.components.cfunc.interfaces.ICArgumentValue;
import org.cen.components.cfunc.interfaces.ICFunction;
import org.cen.components.cfunc.interfaces.ICType;

public class CFunction implements ICFunction
{
	
	private ICType returnType = null;
	private String name = null;
	private ICArgumentList argumentList = null;
	
	public CFunction(ICType returnType, String name, ICArgumentList argumentList)
	{
		this.returnType = returnType;
		this.name = name;
		this.argumentList = argumentList;
		if(this.argumentList == null)
			this.argumentList = new CArgumentList();
	}
	
	@Override
	public ICType getReturnType()
	{
		return returnType;
	}
	
	@Override
	public String getName()
	{
		return name;
	}
	
	@Override
	public ICArgumentList getArgumentList()
	{
		return argumentList;
	}
	
	@Override
	public void validate(ICArgumentMap argmap) throws
		CArgumentNotFoundException, CInacceptableFormatException, 
		CUpperOverflowException, CLowerOverflowException, CMissingArgumentException
	{
		for(ICArgumentValue argval:argmap)
		{
			ICArgument argmeta = argumentList.get(argval.getName());
			if(argmeta == null)
				throw new CArgumentNotFoundException(argval);
			argmeta.checkAccepts(argval);
		}
		for(ICArgument arg:argumentList)
		{
			ICArgumentValue argval = argmap.get(arg.getName());
			if(argval == null)
				throw new CMissingArgumentException(arg);
		}
	}
	
	@Override
	public String render(ICArgumentMap argmap)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(name);
		sb.append("(");
		int argcount = argumentList.getCount();
		int currentarg = 0;
		for(ICArgument arg:argumentList)
		{
			currentarg++;
			ICArgumentValue argval = argmap.get(arg.getName());
			if(argval == null)
				sb.append("null");
			Object converted = argval.getValue();
			if(converted == null)
				sb.append("null");
			else
				sb.append(converted);
			if(currentarg < argcount)
				sb.append(", ");
		}
		sb.append(");\n");
		return sb.toString();
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(name);
		sb.append("(");
		int argcount = argumentList.getCount();
		int currentarg = 0;
		for(ICArgument arg:argumentList)
		{
			currentarg++;
			sb.append(arg.toString());
			if(currentarg < argcount)
				sb.append(", ");
		}
		sb.append(");");
		return sb.toString();
	}
	
}
