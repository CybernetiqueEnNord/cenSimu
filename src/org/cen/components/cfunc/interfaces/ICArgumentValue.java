package org.cen.components.cfunc.interfaces;


public interface ICArgumentValue
{
	
	public String getName();
	
	public Object getValue();
	
}
