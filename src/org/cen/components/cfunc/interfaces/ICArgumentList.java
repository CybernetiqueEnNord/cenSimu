package org.cen.components.cfunc.interfaces;

public interface ICArgumentList extends Iterable<ICArgument>
{
	
	public ICArgument get(String name);
	
	public int getCount();
	
}
