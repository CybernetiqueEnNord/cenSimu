package org.cen.components.cfunc.interfaces;


public interface ICType
{
	
	public Long getBorneInf();
	
	public Long getBorneSup();
	
	public boolean acceptsNull();
	
}
