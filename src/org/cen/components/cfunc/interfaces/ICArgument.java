package org.cen.components.cfunc.interfaces;

import org.cen.components.cfunc.exceptions.CInacceptableFormatException;
import org.cen.components.cfunc.exceptions.CLowerOverflowException;
import org.cen.components.cfunc.exceptions.CUpperOverflowException;

public interface ICArgument
{
	
	public ICType getType();
	
	public String getName();
	
	public void checkAccepts(ICArgumentValue argval) throws 
		CInacceptableFormatException,
		CUpperOverflowException,
		CLowerOverflowException;
	
}
