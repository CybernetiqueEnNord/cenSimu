package org.cen.components.cfunc.interfaces;


public enum CType implements ICType
{
	VOID(0l, 0l, true),
	BOOLEAN(0l, 1l, false),
	OCTET(0l, 255l, false),
	INT(-32768l, 32767l, false),
	UINT(0l, 65536l, false),
	LONG(-2147483648l, 2147483647l, false),
	ULONG(0l, 4294967296l, false),
	;
	
	private Long borneinf = null;
	
	private Long bornesup = null;
	
	private boolean acceptsNull = false;
	
	private CType(Long borneinf, Long bornesup, boolean acceptsNull)
	{
		this.borneinf = borneinf;
		this.bornesup = bornesup;
		this.acceptsNull = acceptsNull;
	}
	
	@Override
	public Long getBorneInf()
	{
		return borneinf;
	}
	
	@Override
	public Long getBorneSup()
	{
		return bornesup;
	}
	
	@Override
	public boolean acceptsNull()
	{
		return acceptsNull;
	}
	
}
