package org.cen.components.cfunc.interfaces;

import org.cen.components.cfunc.exceptions.CArgumentNotFoundException;
import org.cen.components.cfunc.exceptions.CInacceptableFormatException;
import org.cen.components.cfunc.exceptions.CLowerOverflowException;
import org.cen.components.cfunc.exceptions.CMissingArgumentException;
import org.cen.components.cfunc.exceptions.CUpperOverflowException;

public interface ICFunction
{
	
	public ICType getReturnType();
	
	public String getName();
	
	public ICArgumentList getArgumentList();
	
	public void validate(ICArgumentMap argmap) throws 
		CInacceptableFormatException,
		CUpperOverflowException,
		CLowerOverflowException, 
		CArgumentNotFoundException,
		CMissingArgumentException;
	
	public String render(ICArgumentMap argmap);
	
}
