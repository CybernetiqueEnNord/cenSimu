package org.cen.components.cfunc.interfaces;


public interface ICArgumentMap extends Iterable<ICArgumentValue>
{
	
	public ICArgumentValue get(String name);
	
	public void add(ICArgumentValue value);
	
}
