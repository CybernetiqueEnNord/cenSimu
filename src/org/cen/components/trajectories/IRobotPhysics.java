package org.cen.components.trajectories;


public interface IRobotPhysics
{
	
	/**
	 * The name of the gauge.
	 * @return
	 */
	public String getName();
	/**
	 * 
	 * @return pas.mm-1
	 */
	public Double get_pas_per_mm();
	/**
	 * 
	 * @return pas.deg-1
	 */
	public Double get_pas_per_tr();
	/**
	 * 
	 * @return mm.s-1
	 */
	public Double getMaxSpeed();
	/**
	 * 
	 * @return deg.s-1
	 */
	public Double getMaxAngularSpeed();
	/**
	 * 
	 * @return mm.s-2
	 */
	public Double getMaxAcceleration();
	/**
	 * 
	 * @return deg.s-2
	 */
	public Double getMaxAngularAcceleration();
	/**
	 * 
	 * @return mm.s-3
	 */
	public Double getMaxJerk();
	/**
	 * 
	 * @return deg.s-3
	 */
	public Double getMaxAngularJerk();
	
}
