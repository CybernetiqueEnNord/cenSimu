package org.cen.components.trajectories;

import org.cen.components.math.angle.IAngle;
import org.cen.components.math.orientedPosition.IOrientedPosition2D;
import org.cen.components.math.position.IPosition2D;

public abstract class AChainedTrajectory implements ITrajectoryPart
{
	
	protected ITrajectoryPart previous;
	
	protected boolean backward;
	
	public AChainedTrajectory(ITrajectoryPart previous)
	{
		if(previous == null)
			return;
		if(previous == this)
			throw new IllegalArgumentException("Previous part can't be self.");
		setPrevious(previous);
	}
	
	public void setPrevious(ITrajectoryPart previous)
	{
		this.previous = previous;
	}
	
	public boolean getBackward()
	{
		return backward;
	}
	
	@Override
	public IOrientedPosition2D getStartPosition()
	{
		return previous.getEndPosition();
	}
	
	@Override
	public OrientedSpeed getStartSpeed()
	{
		return previous.getEndSpeed();
	}
	
	@Override
	public OrientedAcceleration getStartAcceleration()
	{
		return previous.getEndAcceleration();
	}
	
	@Override
	public boolean setBackward(boolean direction)
	{
		backward = direction;
		return true;
	}
	
	@Override
	public boolean setStartPosition(IPosition2D p2d)
	{
		return previous.setEndPosition(p2d);
	}
	
	@Override
	public boolean setStartXPosition(Double x)
	{
		return previous.setEndXPosition(x);
	}
	
	@Override
	public boolean setStartYPosition(Double y)
	{
		return previous.setEndYPosition(y);
	}
	
	@Override
	public boolean setStartAngle(IAngle th)
	{
		return previous.setEndAngle(th);
	}
	
	@Override
	public boolean setStartLinearSpeed(Double v)
	{
		return previous.setEndLinearSpeed(v);
	}
	
	@Override
	public boolean setStartAngularSpeed(Double thp)
	{
		return previous.setEndAngularSpeed(thp);
	}
	
	@Override
	public boolean setStartLinearAcceleration(Double a)
	{
		return previous.setEndLinearAcceleration(a);
	}
	
	@Override
	public boolean setStartAngularAcceleration(Double ths)
	{
		return previous.setEndAngularAcceleration(ths);
	}
	
}
