package org.cen.components.trajectories;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.position.IPosition2D;
import org.cen.components.math.time.IDelay;
import org.cen.components.order.BezierQuadOrder;
import org.cen.components.order.CommentOrder;
import org.cen.components.order.DistanceOrder;
import org.cen.components.order.IOrder;
import org.cen.components.order.InitialPositionOrder;
import org.cen.components.order.RotationOrder;
import org.cen.components.order.TransformOrder;
import org.cen.components.order.WaitOrder;


public class KeyFrameCollection implements Iterable<KeyFrame>
{
	// scrap
	IPosition2D lastPosition = null;
	IAngle lastAngle = null;
	Double defaultLinearSpeed = null;
	Double defaultRotationSpeed = null;
	Double timestamp = null;
	// end scrap
	
	private List<KeyFrame> frames;
	
	public KeyFrameCollection()
	{
		frames = new ArrayList<KeyFrame>();
	}
	
	public int size()
	{
		return frames.size();
	}
	
	public KeyFrame first()
	{
		if(this.size() > 0)
			return frames.get(0);
		return null;
	}
	
	public KeyFrame last()
	{
		if(this.size() > 0)
			return frames.get(this.size() - 1);
		return null;
	}
	
	public KeyFrame get(int i)
	{
		return frames.get(i);
	}
	
	public class KeyFrameSearch extends KeyFrame {
		public KeyFrameSearch(double timestamp) {
			super(null, 0, new AngleRadian(0.0), 0, 0, null, timestamp, 0);
		}
	}
	
	public KeyFrame interpolate(KeyFrameLinearInterpolator interpolator, double timestamp)
	{
		KeyFrameSearch searchKey = new KeyFrameSearch(timestamp);
		int index = Collections.binarySearch(frames, searchKey);
		if (index >= 0) {
			return frames.get(index);
		}
		
		int n = frames.size();
		index = -index - 2;
		if (index < 0) {
			KeyFrame frame = frames.get(0);
			return frame;
		}
		KeyFrame start = frames.get(index);
		if (index == n - 1) {
			return start;
		}
		KeyFrame end = frames.get(index + 1);
		return new KeyFrameLinearInterpolator().interpolate(start, end, end.getMovement(), timestamp);
	}
	
	public KeyFrame nextFrame(Double timestamp2)
	{
		if(frames.isEmpty())
			return null;
		
		KeyFrame f = null;
		Iterator<KeyFrame> ikf = iterator();
		while(ikf.hasNext())
		{
			f = ikf.next();
			if(f.getTimestamp() > timestamp2)
				break;
		}
		return f;
	}
	
	@Override
	public Iterator<KeyFrame> iterator()
	{
		return frames.iterator();
	}
	
	public void add(KeyFrame frame)
	{
		frames.add(frame);
	}
	
	public boolean isEmpty()
	{
		return frames.isEmpty();
	}
	
	public void populateWith(IOrder order)
	{
		order.populateKeyFrameCollection(this);
	}
	
	public void populateBezierQuad(BezierQuadOrder bezierQuadOrder)
	{
		IAngle endAngle = bezierQuadOrder.getEndAngle();
		Point2D p = bezierQuadOrder.getFinalPoint();
		Point2D cp1 = bezierQuadOrder.getFirstControlPoint();
		Point2D cp2 = bezierQuadOrder.getSecondControlPoint();
		double distante = p.distance(lastPosition.getValue());
		timestamp += distante / defaultLinearSpeed;
		int sourceLine = bezierQuadOrder.getSourceLine();
		frames.add(new KeyFrame(new TrajectoryBezierQuad(null), defaultLinearSpeed, endAngle, 0, defaultRotationSpeed, lastPosition, timestamp, sourceLine, cp1, cp2));
	}
	
	public void populateComment(CommentOrder commentOrder)
	{
		// nothing to do
	}
	
	public void populateDistance(DistanceOrder distanceOrder)
	{
		lastPosition = lastPosition.move(distanceOrder.getDistance(), lastAngle);
		Double distance = distanceOrder.getDistance().getValue();
		int sourceLine = distanceOrder.getSourceLine();
		
		timestamp += Math.abs(distance / defaultLinearSpeed);
		double linearspeed = Math.copySign(defaultLinearSpeed, Math.signum(distance));
		
		frames.add(new KeyFrame(new TrajectorySegment(null), linearspeed, lastAngle, 0, 0, lastPosition, timestamp, sourceLine));
	}
	
	public void populateInitial(InitialPositionOrder initialPositionOrder)
	{
		lastPosition = initialPositionOrder.getPosition();
		lastAngle = initialPositionOrder.getInitialAngle();
		defaultLinearSpeed = initialPositionOrder.getDefaultLinearSpeed();
		defaultRotationSpeed = initialPositionOrder.getDefaultRotationSpeed();
		timestamp = initialPositionOrder.getTimestamp();
		Integer sourceLine = initialPositionOrder.getSourceLine();
		
		frames.add(new KeyFrame(new TrajectoryStart(), 0, lastAngle, 0, 0, lastPosition, timestamp, sourceLine));
	}
	
	public void populateRotation(RotationOrder rotationOrder)
	{
		int sourceLine = rotationOrder.getSourceLine();
		lastAngle = lastAngle.add(rotationOrder.getRotationAngle());
		timestamp += (Math.abs(rotationOrder.getRotationAngle().getValue()) / (2.0 * Math.PI) / defaultRotationSpeed);
		
		frames.add(new KeyFrame(new TrajectoryForcedRotation(null), 0, lastAngle, rotationOrder.getRotationAngle().getValue(), defaultRotationSpeed, lastPosition, timestamp, sourceLine));
	}
	
	public void populateTransform(TransformOrder transformOrder)
	{
		// TODO Auto-generated method stub
		
	}
	
	public void populateWait(WaitOrder waitOrder)
	{
		IDelay delay = waitOrder.getDelay();
		timestamp += delay.getValue();
		Integer sourceLine = waitOrder.getSourceLine();
		
		frames.add(new KeyFrame(new TrajectoryDelay(null, delay), 0, lastAngle, 0, defaultRotationSpeed, lastPosition, timestamp, sourceLine));
	}
	
}
