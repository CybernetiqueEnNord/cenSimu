package org.cen.components.trajectories;

import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.angle.IAngle;

public class TravelReport implements ITravelReport
{
	
	private Double travelTime = 0d;
	
	private Double travelDistance = 0d;
	
	private IAngle travelAngle = new AngleRadian(0.0);
	
	public void addTime(Double seconds)
	{
		travelTime += Math.abs(seconds);
	}
	
	@Override
	public Double getTravelTime()
	{
		return travelTime;
	}
	
	public void addDistance(Double mm)
	{
		travelDistance += Math.abs(mm);
	}
	
	@Override
	public Double getTravelDistance()
	{
		return travelDistance;
	}
	
	public void addAngle(IAngle rad)
	{
		travelAngle = travelAngle.add(rad);
	}
	
	@Override
	public IAngle getTravelAngle()
	{
		return travelAngle;
	}
	
}
