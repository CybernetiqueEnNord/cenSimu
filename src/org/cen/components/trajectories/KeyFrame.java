package org.cen.components.trajectories;

import java.awt.geom.Point2D;
import java.util.ArrayList;

import org.cen.components.math.angle.IAngle;
import org.cen.components.math.position.IPosition2D;

public class KeyFrame implements Comparable<KeyFrame> {
	private ArrayList<String> comments;
	private Point2D[] controlPoints;
	private ITrajectoryMovement movement;
	private double movementSpeed;

	private IAngle orientation;
	private IPosition2D position;

	private double relativeAngle;
	private double rotationSpeed;
	private int sourceLine;
	private double timestamp;

	public KeyFrame(ITrajectoryMovement movement, 
			double movementSpeed, 
			IAngle orientation, 
			double relativeAngle, 
			double rotationSpeed,
			IPosition2D position, 
			double timestamp, 
			int sourceLine, 
			Point2D... controlPoints
	) {
		super();
		this.movement = movement;
		this.movementSpeed = movementSpeed;
		this.orientation = orientation;
		this.relativeAngle = relativeAngle;
		this.rotationSpeed = rotationSpeed;
		this.position = position;
		this.timestamp = timestamp;
		this.sourceLine = sourceLine;
		this.controlPoints = controlPoints;
		this.comments = new ArrayList<String>();
	}

	public void addComment(String comment) {
		if (comments == null) {
			comments = new ArrayList<String>();
		}
		comments.add(comment);
	}

	@Override
	public int compareTo(KeyFrame k) {
		return Double.compare(timestamp, k.timestamp);
	}

	public ArrayList<String> getComments() {
		return comments;
	}

	public Point2D[] getControlPoints() {
		return controlPoints;
	}

	public ITrajectoryMovement getMovement() {
		return movement;
	}

	public double getMovementSpeed() {
		return movementSpeed;
	}

	public IAngle getOrientation() {
		return orientation;
	}

	public IPosition2D getPosition() {
		return position;
	}

	public double getRelativeAngle() {
		return relativeAngle;
	}

	public double getRotationSpeed() {
		return rotationSpeed;
	}

	public int getSourceLine() {
		return sourceLine;
	}

	public double getTimestamp() {
		return timestamp;
	}

	public boolean hasComments() {
		return (comments != null) && (!comments.isEmpty());
	}

	public boolean useRelativeAngle() {
		return relativeAngle != 0;
	}
	
	public String toString()
	{
		StringBuilder str = new StringBuilder("{");
		str.append("movement=\"").append(movement.toString()).append("\"");
		str.append(" movementSpeed=\"").append(movementSpeed).append("\"");
		str.append(" orientation=\"").append(orientation).append("\"");
		str.append(" position=\"").append(position).append("\"");
		str.append(" relativeAngle=\"").append(relativeAngle).append("\"");
		str.append(" rotationSpeed=\"").append(rotationSpeed).append("\"");
		str.append(" sourceLine=\"").append(sourceLine).append("\"");
		str.append(" timestamp=\"").append(timestamp).append("\"");
		str.append(" controlPoints=(").append(controlPoints.length).append(")[");
		for(Point2D pt:controlPoints)
		{
			str.append(pt).append(",");
		}
		str.append("]");
		str.append(" comments=(").append(comments.size()).append(")[");
		for(String ct:comments)
		{
			str.append(ct).append(",");
		}
		str.append("]");
		str.append("}");
		return str.toString();
	}
	
}
