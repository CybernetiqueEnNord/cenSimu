package org.cen.components.trajectories;

import java.awt.geom.Point2D;

import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.orientedPosition.OrientedPosition2D;
import org.cen.components.math.position.IPosition2D;

public class TrajectoryRotation extends AChainedTrajectory
{
	
	private IAngle endOrientation = new AngleRadian(0.0);
	
	public TrajectoryRotation(ITrajectoryPart previous)
	{
		super(previous);
	}
	
	@Override
	public String getPartName()
	{
		return "Rotation";
	}
	
	@Override
	public OrientedPosition2D getEndPosition()
	{
		return new OrientedPosition2D(getStartPosition().getPosition(), endOrientation);
	}
	
	public OrientedSpeed getEndSpeed()
	{
		return new OrientedSpeed(new Point2D.Double(), 0d);
	}
	
	@Override
	public OrientedAcceleration getEndAcceleration()
	{
		return new OrientedAcceleration(new Point2D.Double(), 0d);
	}
	
	@Override
	public boolean setEndPosition(IPosition2D p2d)
	{
		return setStartPosition(p2d);
	}
	
	@Override
	public boolean setEndXPosition(Double x)
	{
		return setStartXPosition(x);
	}
	
	@Override
	public boolean setEndYPosition(Double y)
	{
		return setStartYPosition(y);
	}
	
	@Override
	public boolean setEndAngle(IAngle th)
	{
		endOrientation = th;
		// TODO CALCULUS TO BE STRAIGHT LINE FROM PREVIOUS LINE
		return true;
	}
	
	@Override
	public boolean setEndLinearSpeed(Double v)
	{
		return false;
	}
	
	@Override
	public boolean setEndAngularSpeed(Double thp)
	{
		return false;
	}
	
	@Override
	public boolean setEndLinearAcceleration(Double a)
	{
		return false;
	}
	
	@Override
	public boolean setEndAngularAcceleration(Double ths)
	{
		return false;
	}
	
	public boolean setDirection(IPosition2D p2d)
	{
		endOrientation = new AngleRadian(Math.atan2(
			p2d.getValue().getY() - previous.getEndPosition().getPosition().getValue().getY(), 
			p2d.getValue().getX() - previous.getEndPosition().getPosition().getValue().getX()
		));
		return true;
	}
	
	public Double length()
	{
		return 0d;
	}
	
	public IAngle angle()
	{
		return endOrientation.getRotationAngle(previous.getEndPosition().getOrientation());
	}
	
	@Override
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection)
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public String exportTo(IOrderExporter exporter)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
}
