package org.cen.components.trajectories;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.cen.components.math.Bezier;
import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.orientedPosition.OrientedPosition2D;
import org.cen.components.math.position.Position2DMillimeter;

public class TrajectoryPlanner {
	public static void main(String[] args) {
		TrajectoryPlanner p = new TrajectoryPlanner(10, 100);
		OrientedPosition2D[] points = p.getTrajectory(
			new OrientedPosition2D(new Position2DMillimeter(0d, 0d), new AngleRadian(Math.PI / 2)), 
			new OrientedPosition2D(new Position2DMillimeter(1000d, 1000d), new AngleRadian(Math.PI / 2))
		);
		for (OrientedPosition2D point : points) {
			System.out.println(point);
		}
	}

	private double interpolationDistance;

	private double mmPerRadian;

	public TrajectoryPlanner(double mmPerDegree, double interpolationDistance) {
		super();
		this.mmPerRadian = Math.toDegrees(mmPerDegree);
		this.interpolationDistance = interpolationDistance;
	}

	private Point2D getControlPoint(OrientedPosition2D p, double angleIncrement, boolean forward) {
		Point2D l = p.getPosition().getValue();
		double angle = p.getOrientation().getValue();
		double signum = forward ? 1.0 : -1.0;
		double x = l.getX() + Math.cos(angle) * mmPerRadian * angleIncrement * signum;
		double y = l.getY() + Math.sin(angle) * mmPerRadian * angleIncrement * signum;
		l = new Point2D.Double(x, y);
		return l;
	}

	public OrientedPosition2D[] getTrajectory(OrientedPosition2D start, OrientedPosition2D end) {
		List<OrientedPosition2D> points = new ArrayList<OrientedPosition2D>();

		Point2D s = start.getPosition().getValue();
		Point2D e = end.getPosition().getValue();
		double d = s.distance(e);
		double dx = e.getX() - s.getX();
		double dy = e.getY() - s.getY();

		double angle = Math.atan2(dy, dx);
		double da = Math.abs(start.getOrientation().getValue() - angle);
		Point2D cp1 = getControlPoint(start, da, true);
		da = Math.abs(end.getOrientation().getValue() - angle);
		Point2D cp2 = getControlPoint(end, da, false);

		int n = (int) (d / interpolationDistance);
		for (int i = 0; i < n; i++) {
			double t = (double) i / n;
			Point2D p = Bezier.getPoint(t, s, cp1, cp2, e);
			double o = Bezier.getAngle(t, s, cp1, cp2, e);
			points.add(new OrientedPosition2D(new Position2DMillimeter(p), new AngleRadian(o)));
		}
		points.add(end);

		OrientedPosition2D[] array = new OrientedPosition2D[points.size()];
		return points.toArray(array);
	}
}
