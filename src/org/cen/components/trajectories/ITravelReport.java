package org.cen.components.trajectories;

import org.cen.components.math.angle.IAngle;

public interface ITravelReport
{
	
	public Double getTravelTime();
	
	public Double getTravelDistance();
	
	public IAngle getTravelAngle();
	
}
