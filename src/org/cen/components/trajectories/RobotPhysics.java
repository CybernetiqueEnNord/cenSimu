package org.cen.components.trajectories;


public class RobotPhysics implements IRobotPhysics
{
	
	/**
	 * 
	 * @var String
	 */
	private String name = null;
	/**
	 * 
	 * @var pas.mm-1
	 */
	private Double pas_per_mm = 1d;
	/**
	 * 
	 * @var pas.tr
	 */
	private Double pas_per_tr = 1d;
	/**
	 * 
	 * @var mm.s-1
	 */
	private Double maxSpeed = 1d;
	/**
	 * 
	 * @var deg.s-1
	 */
	private Double maxAngularSpeed = 1d;
	/**
	 * 
	 * @var mm.s-2
	 */
	private Double maxAcceleration = 1d;
	/**
	 * 
	 * @var deg.s-2
	 */
	private Double maxAngularAcceleration = 1d;
	/**
	 * 
	 * @return mm.s-3
	 */
	private Double maxJerk = 1d;
	/**
	 * 
	 * @return deg.s-3
	 */
	private Double maxAngularJerk = 1d;
	
	
	@Override
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	@Override
	public Double get_pas_per_mm()
	{
		return pas_per_mm;
	}
	
	public void set_pas_per_mm(Double pas_per_mm)
	{
		this.pas_per_mm = pas_per_mm;
	}
	
	@Override
	public Double get_pas_per_tr()
	{
		return pas_per_tr;
	}
	
	public void set_pas_per_tr(Double pas_per_tr)
	{
		this.pas_per_tr = pas_per_tr;
	}
	
	@Override
	public Double getMaxSpeed()
	{
		return maxSpeed;
	}
	
	public void setMaxSpeed(Double maxSpeed)
	{
		this.maxSpeed = maxSpeed;
	}
	
	@Override
	public Double getMaxAngularSpeed()
	{
		return maxAngularSpeed;
	}
	
	public void setMaxAngularSpeed(Double maxAngularSpeed)
	{
		this.maxAngularSpeed = maxAngularSpeed;
	}
	
	@Override
	public Double getMaxAcceleration()
	{
		return maxAcceleration;
	}
	
	public void setMaxAcceleration(Double maxAcceleration)
	{
		this.maxAcceleration = maxAcceleration;
	}
	
	@Override
	public Double getMaxAngularAcceleration()
	{
		return maxAngularAcceleration;
	}
	
	public void setMaxAngularAcceleration(Double maxAngularAcceleration)
	{
		this.maxAngularAcceleration = maxAngularAcceleration;
	}
	
	public Double getMaxJerk()
	{
		return maxJerk;
	}
	
	public void setMaxJerk(Double maxJerk)
	{
		this.maxJerk = maxJerk;
	}
	
	public Double getMaxAngularJerk()
	{
		return maxAngularJerk;
	}
	
	public void setMaxAngularJerk(Double maxAngularJerk)
	{
		this.maxAngularJerk = maxAngularJerk;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder("{RobotGauge");
		sb.append(" name=").append(this.getName());
		sb.append(" pas_per_mm=").append(this.get_pas_per_mm());
		sb.append(" pas_per_tr=").append(this.get_pas_per_tr());
		sb.append(" maxSpeed=").append(this.getMaxSpeed());
		sb.append(" maxAngularSpeed=").append(this.getMaxAngularSpeed());
		sb.append(" maxAcceleration=").append(this.getMaxAcceleration());
		sb.append(" maxAngularAcceleration=").append(this.getMaxAngularAcceleration());
		sb.append(" maxJerk=").append(this.getMaxJerk());
		sb.append(" maxAngularJerk=").append(this.getMaxAngularJerk());
		return sb.append("}").toString();
	}
	
}
