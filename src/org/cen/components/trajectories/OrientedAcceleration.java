package org.cen.components.trajectories;

import java.awt.geom.Point2D;

public class OrientedAcceleration
{
	
	private Point2D direction = null;
	private Double orientation = null;
	
	public OrientedAcceleration(Point2D direction, Double orientation)
	{
		this.direction = direction;
		this.orientation = orientation;
	}
	
	public boolean isZero()
	{
		return direction.getX() == 0d && direction.getY() == 0d;
	}
	
	public Double getValue()
	{
		return Math.sqrt(Math.pow(direction.getX(), 2) + Math.pow(direction.getY(), 2));
	}
	
	public Double getOrientation()
	{
		return orientation;
	}
	
}
