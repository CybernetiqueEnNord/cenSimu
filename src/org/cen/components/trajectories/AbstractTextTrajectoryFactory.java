package org.cen.components.trajectories;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;

import org.cen.components.io.trajectory.ITrajectoryParser;
import org.cen.components.order.IOrderList;
import org.cen.components.order.OrderList;

public abstract class AbstractTextTrajectoryFactory extends AbstractTrajectoryFactory
{
	@Override
	public IOrderList getTrajectoryPath(String name, BufferedReader stream)
	{
		
		ITrajectoryParser parser = createParser();
		try
		{
			return parser.parseStream(stream);
		}
		catch (IOException | ParseException e)
		{
			e.printStackTrace();
		}
		return new OrderList();
	}
	
	protected abstract ITrajectoryParser createParser();
	
}
