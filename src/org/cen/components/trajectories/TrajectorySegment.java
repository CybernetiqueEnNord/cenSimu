package org.cen.components.trajectories;

import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.util.Map;

import org.cen.components.gameboard.generic.StraightLine;
import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.distance.IDistance2D;
import org.cen.components.math.orientedPosition.IOrientedPosition2D;
import org.cen.components.math.orientedPosition.OrientedPosition2D;
import org.cen.components.math.position.IPosition2D;
import org.cen.components.math.position.Position2DMillimeter;

public class TrajectorySegment extends AChainedTrajectory implements ITrajectoryMovement
{
	
	private IOrientedPosition2D endPosition = null;
	
	public TrajectorySegment(ITrajectoryPart previous)
	{
		super(previous);
		if(previous != null)
			this.endPosition = previous.getEndPosition();
	}
	
	@Override
	public String getPartName()
	{
		return "Line";
	}
	
	@Override
	public IOrientedPosition2D getEndPosition()
	{
		return endPosition;
	}
	
	@Override
	public OrientedSpeed getEndSpeed()
	{
		return new OrientedSpeed(new Point2D.Double(), 0d);
	}
	
	@Override
	public OrientedAcceleration getEndAcceleration()
	{
		return new OrientedAcceleration(new Point2D.Double(), 0d);
	}
	
	@Override
	public boolean setEndPosition(IPosition2D p2d)
	{
		if(!endPosition.getPosition().getValue().equals(p2d))
		{
			endPosition = new OrientedPosition2D(p2d, new AngleRadian(Math.atan2(
				p2d.getValue().getY() - previous.getEndPosition().getPosition().getValue().getY(),
				p2d.getValue().getX() - previous.getEndPosition().getPosition().getValue().getX()
			)));
			setStartAngle(endPosition.getOrientation());
		}
		return true;
	}
	
	@Override
	public boolean setEndXPosition(Double x)
	{
		return setEndPosition(new Position2DMillimeter(x, endPosition.getPosition().getValue().getY()));
	}
	
	@Override
	public boolean setEndYPosition(Double y)
	{
		return setEndPosition(new Position2DMillimeter(endPosition.getPosition().getValue().getX(), y));
	}
	
	@Override
	public boolean setStartAngle(IAngle th)
	{
		if(super.getStartPosition().getOrientation() != th && super.setStartAngle(th))
		{
			// recalcul de la trajectoire
			// on avait une direction, elle a été changée pour un angle donné
			Double d = length();
			setEndPosition(new Position2DMillimeter(
				getStartPosition().getPosition().getValue().getX() + d * th.toCosinus().getValue(), 
				getStartPosition().getPosition().getValue().getY() + d * th.toSinus().getValue()
			));
			return true;
		}
		return false;
	}
	
	@Override
	public boolean setEndAngle(IAngle th)
	{
		if(setStartAngle(th))
		{
			endPosition = new OrientedPosition2D(endPosition.getPosition(), th);
			return true;
		}
		return false;
	}
	
	@Override
	public boolean setEndLinearSpeed(Double v)
	{
		return false;
	}
	
	@Override
	public boolean setEndAngularSpeed(Double thp)
	{
		return false;
	}
	
	@Override
	public boolean setEndLinearAcceleration(Double a)
	{
		return false;
	}
	
	@Override
	public boolean setEndAngularAcceleration(Double ths)
	{
		return false;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder("{TrajectorySegment");
		sb.append(" start=").append(previous.getEndPosition());
		sb.append(" end=").append(getEndPosition());
		return sb.append("}").toString();
	}
	
	public IDistance2D distance()
	{
		return previous.getEndPosition().distance(endPosition);
	}
	
	@Override
	public Double length()
	{
		return previous.getEndPosition().distance(endPosition).getValue();
	}
	
	@Override
	public boolean setDirection(IPosition2D p2d)
	{
		// sets the segment to point into given direction, keeping its length
		IAngle angle = new AngleRadian(Math.atan2(
			p2d.getValue().getY() - getStartPosition().getPosition().getValue().getY(), 
			p2d.getValue().getX() - getStartPosition().getPosition().getValue().getX()
		));
		return setEndAngle(angle);
	}
	
	// --------- ITrajectoryMovement functions --------- \\
	
	@Override
	public void move(StraightLine straightLine, boolean forward, KeyFrame frame)
	{
		Path2D path = ((Path2D) straightLine.getPath());
		if (forward == frame.getMovementSpeed() < 0) {
			Point2D p = path.getCurrentPoint();
			path.moveTo(p.getX(), p.getY());
			forward = !forward;
		}
		Point2D p = frame.getPosition().getValue();
		path.lineTo(p.getX(), p.getY());
	}
	
	@Override
	public void describeTo(StraightLine straightLine, 
					Map<String, String> params, StringBuilder sb, 
					KeyFrame frame, ElementDescription el)
	{
		Point2D p = frame.getPosition().getValue();
		double distance = p.distance(el.last);
		double speed = frame.getMovementSpeed();
		String direction = speed > 0 ? "forward" : "backward";
		straightLine.addComments(params, String.format("// move %s of %.0f mm (%.0f)", direction, distance, 9.557 * distance * Math.signum(speed)), true);
		straightLine.addValue(params, StraightLine.KEY_DISTANCE, 9.557 * distance * Math.signum(speed));
		el.last = p;
	}
	
	@Override
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection)
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public KeyFrame beInterpolatedBy(IKeyFrameInterpolator interpolator,
			KeyFrame start, KeyFrame end, double timestamp)
	{
		return interpolator.interpolateSegment(this, start, end, timestamp);
	}
	
	@Override
	public String exportTo(IOrderExporter exporter)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
}
