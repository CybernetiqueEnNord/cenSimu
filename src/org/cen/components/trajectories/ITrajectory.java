package org.cen.components.trajectories;

import org.cen.components.math.position.IPosition2D;

/**
 * ITrajectory interface file.
 * 
 * This interface specifies the needs of a trajectory composed of independant
 * parts. Such trajectory object should guarantee the feasability to physically
 * follow the path it describes.
 * 
 * @author Anastaszor <anastaszor@gmail.com>
 */
public interface ITrajectory extends Iterable<ITrajectoryPart>
{
	
	/**
	 * Gets the number of parts in this trajectory.
	 * @return
	 */
	public int size();
	
	/**
	 * Sets the start of this trajectory at the given position.
	 * @param position
	 * @return boolean success
	 */
	public boolean startsAt(IPosition2D position);
	/**
	 * Adds a new Segment to the trajectory given by its end point. If the 
	 * orientation of last part with the new Segment is not aligned, a rotation
	 * part will be added.
	 * @param p2d
	 * @return boolean success
	 */
	public boolean addSegment(IPosition2D destination);
	/**
	 * Adds a new Rotation to the trajectory given by a point that is the 
	 * direction in which the rotation should point at the end of the movement.
	 * @param direction
	 * @return boolean success
	 */
	public boolean addRotation(IPosition2D direction);
	/**
	 * Adds a new Arc to the trajectory given by its end point. The arc will
	 * be tangeant to current last element's orientation.
	 * @param destination
	 * @return
	 */
	public boolean addArc(IPosition2D destination);
	/**
	 * Adds a new clothoid arc to the trajectory given its end point. The arc
	 * will be tangeant to current last element's orientation.
	 * @param p2d
	 * @return
	 */
	public boolean addClothoid(IPosition2D destination);
	/**
	 * Adds a new quadratic bezier curve with given control and last points.
	 * @param control
	 * @param destination
	 */
	public boolean addBezierQuad(IPosition2D control, IPosition2D destination);
	/**
	 * Adds a new cubic bezier curve with given first and second control points
	 * and last point to exit the curve.
	 * @param first
	 * @param second
	 * @param destination
	 */
	public boolean addBezierCubic(IPosition2D first, IPosition2D second, IPosition2D destination);
	
	/**
	 * Adds a delay corresponding to the given seconds arg.
	 * @param seconds
	 * @return boolean success
	 */
	public boolean addDelay(Double seconds);
	
	/**
	 * Removes the nth element, then reorganize the trajectory parts to fill 
	 * the void let by this missing part.
	 * @param n
	 */
	public void remove(int n);
	
	
	
	
	
	public void addPart(ITrajectoryPart order);

	public ITrajectoryPart get(int row);
	
	public ITrajectoryPath toTrajectoryPath();
	
}
