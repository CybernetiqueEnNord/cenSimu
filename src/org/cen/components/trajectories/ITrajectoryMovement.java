package org.cen.components.trajectories;

import java.util.Map;

import org.cen.components.gameboard.generic.StraightLine;

public interface ITrajectoryMovement {
	
	void move(StraightLine straightLine, boolean forward, KeyFrame frame);
	
	void describeTo(StraightLine straightLine, 
					Map<String, String> params, StringBuilder sb, 
					KeyFrame frame, ElementDescription el);
	
	public KeyFrame beInterpolatedBy(IKeyFrameInterpolator interpolator, KeyFrame start, KeyFrame end, double timestamp);
	
}
