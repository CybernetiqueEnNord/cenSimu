package org.cen.components.trajectories;

import java.util.Iterator;
import java.util.LinkedList;

import org.cen.components.math.position.IPosition2D;
import org.cen.components.math.time.DelayMillisecond;

public class Trajectory implements ITrajectory
{
	
	private LinkedList<ITrajectoryPart> parts = null;
	
	public Trajectory()
	{
		parts = new LinkedList<>();
		parts.add(new TrajectoryStart());
	}
	
	public void addPart(ITrajectoryPart part)
	{
		if(part instanceof TrajectoryStart)
			parts = new LinkedList<>();
		parts.add(part);
	}
	
	@Override
	public int size()
	{
		return parts.size();
	}
	
	@Override
	public boolean startsAt(IPosition2D position)
	{
//		parts.get(0).setStartPosition(position);
		return true;
	}
	
	@Override
	public boolean addSegment(IPosition2D destination)
	{
		// intercaler une rotation pour corriger les angles
		// sauf si départ => corriger le départ...
		ITrajectoryPart last = parts.getLast();
//		ITrajectoryPart newer;
		if(!(last instanceof TrajectoryStart))
		{
			addRotation(destination);
			last = parts.getLast();
//			newer = new TrajectorySegment(last);
		}
		else
		{
//			last.setDirection(destination);
//			newer = new TrajectorySegment(last);
		}
//		newer.setEndPosition(destination);
//		if(newer.length() != 0)
//		{
//			// do not add part if there is no reason to
//			parts.add(newer);
//			return true;
//		}
		return false;
	}
	
	@Override
	public boolean addRotation(IPosition2D destination)
	{
		ITrajectoryPart prev = parts.getLast();
		if(prev instanceof TrajectoryRotation)
		{
//			prev.setDirection(destination);
		}
		else
		{
//			TrajectoryRotation rotnr = new TrajectoryRotation(prev);
//			rotnr.setDirection(destination);
//			parts.add(rotnr);
		}
		return true;
	}
	
	@Override
	public boolean addArc(IPosition2D destination)
	{
//		ITrajectoryPart last = parts.getLast();
//		ITrajectoryPart newer = new TrajectoryArc(last);
//		newer.setEndPosition(destination);
//		parts.add(newer);
		return true;
	}
	
	@Override
	public boolean addClothoid(IPosition2D destination)
	{
//		ITrajectoryPart last = parts.getLast();
//		ITrajectoryPart newer = new TrajectoryClothoid(last);
//		newer.setEndPosition(destination);
//		parts.add(newer);
		return true;
	}
	
	@Override
	public boolean addBezierQuad(IPosition2D control, IPosition2D destination)
	{
//		ITrajectoryPart last = parts.getLast();
//		TrajectoryBezierQuad newer = new TrajectoryBezierQuad(last);
//		newer.setControlPosition(control);
//		newer.setEndPosition(destination);
//		parts.add(newer);
		return true;
	}
	
	@Override
	public boolean addBezierCubic(IPosition2D first, IPosition2D second, IPosition2D destination)
	{
//		ITrajectoryPart last = parts.getLast();
//		TrajectoryBezierCubic newer = new TrajectoryBezierCubic(last);
//		newer.setFirstControlPosition(first);
//		newer.setSecondControlPosition(second);
//		newer.setEndPosition(destination);
//		parts.add(newer);
		return true;
	}
	
	@Override
	public boolean addDelay(Double seconds)
	{
		ITrajectoryPart prev = parts.getLast();
		if(prev instanceof TrajectoryDelay)
		{
			Double secs = ((TrajectoryDelay) prev).getDuration().getValue() + seconds;
			if(secs > 0)
				((TrajectoryDelay) prev).setDuration(new DelayMillisecond(secs));
			else
				parts.remove(prev);
		}
		else if(seconds > 0)
		{
			TrajectoryDelay newer = new TrajectoryDelay(prev, new DelayMillisecond(seconds));
			parts.add(newer);
		}
		// TODO fusionner avec la frame précédente si delay
		return true;
	}
	
	@Override
	public void remove(int n)
	{
		if(n > 1 && n < parts.size())
		{
			parts.remove(n);
			if(n < parts.size() && parts.get(n) instanceof AChainedTrajectory)
			{
//				((AChainedTrajectory) parts.get(n)).setPrevious(parts.get(n-1));
			}
		}
	}
	
	@Override
	public Iterator<ITrajectoryPart> iterator()
	{
		return parts.iterator();
	}
	
	@Override
	public ITrajectoryPart get(int row)
	{
		return parts.get(row);
	}

	@Override
	public ITrajectoryPath toTrajectoryPath()
	{
		// TODO Auto-generated method stub
		return null;
	}
	
}
