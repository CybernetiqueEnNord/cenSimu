package org.cen.components.trajectories;

import org.cen.components.io.export.order.ExportException;
import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.orientedPosition.IOrientedPosition2D;
import org.cen.components.math.position.IPosition2D;
import org.cen.components.order.IOrder;

/**
 * ITrajectoryPart class file.
 * 
 * This interface specifies the characteristics of a portion of the trajectory.
 * The management of the parts and he junctions between them is made by the 
 * trajectory itself.
 * 
 * All measures of length/positions are in mm, all angles in radians, all times
 * in seconds.
 * 
 * @author Anastaszor <anastaszor@gmail.com>
 */
public interface ITrajectoryPart extends IOrder
{
	
	// ----- others ----- \\
	
	/**
	 * Gets the length of this part.
	 * @return
	 */
//	public Double length();
	
	/**
	 * Stringify this object according to the export rules given by the exporter.
	 * This method has no meaning in this class, its here for the double dispatch.
	 * @param exporter
	 * @return
	 */
	public String exportTo(IOrderExporter exporter) throws ExportException;

	IOrientedPosition2D getStartPosition();

	OrientedSpeed getStartSpeed();

	OrientedAcceleration getStartAcceleration();

	boolean setBackward(boolean direction);

	boolean setStartPosition(IPosition2D p2d);

	boolean setStartYPosition(Double y);

	boolean setStartAngle(IAngle th);

	boolean setStartLinearSpeed(Double v);

	boolean setStartAngularSpeed(Double thp);

	boolean setStartLinearAcceleration(Double a);

	boolean setStartAngularAcceleration(Double ths);

	public IOrientedPosition2D getEndPosition();

	public OrientedSpeed getEndSpeed();

	public OrientedAcceleration getEndAcceleration();

	public boolean setEndPosition(IPosition2D p2d);

	boolean setStartXPosition(Double x);

	public boolean setEndXPosition(Double x);

	public boolean setEndYPosition(Double y);

	public boolean setEndAngle(IAngle th);

	public boolean setEndLinearSpeed(Double v);

	public boolean setEndAngularSpeed(Double thp);

	public boolean setEndLinearAcceleration(Double a);

	public boolean setEndAngularAcceleration(Double ths);
	
	public Double length();

	public boolean setDirection(IPosition2D destination);

	String getPartName();

	boolean getBackward();
	
}
