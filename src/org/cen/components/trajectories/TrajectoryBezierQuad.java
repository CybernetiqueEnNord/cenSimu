package org.cen.components.trajectories;

import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.QuadCurve2D;
import java.util.Map;

import org.cen.components.gameboard.generic.StraightLine;
import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.math.Bezier;
import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.orientedPosition.IOrientedPosition2D;
import org.cen.components.math.orientedPosition.OrientedPosition2D;
import org.cen.components.math.position.IPosition2D;
import org.cen.components.math.position.Position2DMillimeter;

public class TrajectoryBezierQuad extends AChainedTrajectory implements ITrajectoryMovement
{
	
	private IPosition2D controlPosition = null;
	
	private IOrientedPosition2D endPosition = null;
	
	/**
	 * The TrajectoryBezierQuadInformation is to carry relevant information
	 * about this bezier spline to be printed successfully. As the parameters
	 * of this spline may change by users actions, a new spline information is
	 * generated and its calculations done again.
	 * 
	 * @author Anastaszor <anastaszor@yahoo.fr>
	 */
	public class TrajectoryBezierQuadInformation
	{
		
		private Double length;
		private IAngle angle;
		private QuadCurve2D curve;
		
		/**
		 * 
		 * @param bquad
		 */
		public TrajectoryBezierQuadInformation(TrajectoryBezierQuad bquad)
		{
			angle = bquad.getEndPosition().getOrientation().getRotationAngle(bquad.getStartPosition().getOrientation());
			// args order for quadcurve :
			// The X coordinate of the start point of the quadratic curve segment.
			// The Y coordinate of the start point of the quadratic curve segment.
			// The X coordinate of the control point of the quadratic curve segment.
			// The Y coordinate of the control point of the quadratic curve segment.
			// The X coordinate of the end point of the quadratic curve segment.
			// The Y coordinate of the end point of the quadratic curve segment.
			curve = new QuadCurve2D.Double(
				bquad.getStartPosition().getPosition().getValue().getX(), 
				bquad.getStartPosition().getPosition().getValue().getY(), 
				bquad.getControlPosition().getValue().getX(), 
				bquad.getControlPosition().getValue().getY(), 
				bquad.getEndPosition().getPosition().getValue().getX(), 
				bquad.getEndPosition().getPosition().getValue().getY()
			);
			
			/* calculus of the length by integration
			 * @see https://algorithmist.wordpress.com/2009/01/05/quadratic-bezier-arc-length/
			 * @see http://www.algorithmist.net/bezierarclength/srcview/index.html
			 */
			Double ax = bquad.getStartPosition().getPosition().getValue().getX()
					- 2 * bquad.getControlPosition().getValue().getX()
					+ bquad.getEndPosition().getPosition().getValue().getX();
			Double ay = bquad.getStartPosition().getPosition().getValue().getY()
					- 2 * bquad.getControlPosition().getValue().getY()
					+ bquad.getEndPosition().getPosition().getValue().getY();
			Double bx = 2 * (bquad.getControlPosition().getValue().getX()
					- bquad.getEndPosition().getPosition().getValue().getX());
			Double by = 2 * (bquad.getControlPosition().getValue().getY()
					- bquad.getEndPosition().getPosition().getValue().getY());
			
			Double a = 4 * (ax * ax + ay * ay);
			Double b = 4 * (ax * bx + ay * by);
			Double c = bx * bx + by * by;
			
			Double abc = 2 * Math.sqrt(a + b + c);
			Double a2 = Math.sqrt(a);
			Double a32 = 2 * a * a2;
			Double c2 = 2 * Math.sqrt(c);
			Double ba = b/a2;
			
			length = (a32 * abc + a2 * b * (abc - c2) + (4 * c * a - b * b) * Math.log((2 * a2 + ba + abc)/(ba + c2)))/(4 * a32);
		}
		
		public Double getLength()
		{
			return length;
		}
		
		public IAngle getAngle()
		{
			return angle;
		}
		
		public QuadCurve2D getCurve()
		{
			return curve;
		}
		
	}
	
	public TrajectoryBezierQuad(ITrajectoryPart previous)
	{
		super(previous);
		if(previous != null)
			endPosition = previous.getEndPosition();
	}
	
	public IPosition2D getControlPosition()
	{
		return controlPosition;
	}
	
	public void setControlPosition(IPosition2D control)
	{
		controlPosition = control;
	}
	
	@Override
	public String getPartName()
	{
		return "BezierQuad";
	}
	
	@Override
	public IOrientedPosition2D getEndPosition()
	{
		return endPosition;
	}
	
	@Override
	public OrientedSpeed getEndSpeed()
	{
		return new OrientedSpeed(new Point2D.Double(), 0d);
	}
	
	@Override
	public OrientedAcceleration getEndAcceleration()
	{
		return new OrientedAcceleration(new Point2D.Double(), 0d);
	}
	
	@Override
	public boolean setEndPosition(IPosition2D p2d)
	{
		endPosition = new OrientedPosition2D(p2d, endPosition.getOrientation());
		return true;
	}
	
	@Override
	public boolean setEndXPosition(Double x)
	{
		return setEndPosition(new Position2DMillimeter(x, endPosition.getPosition().getValue().getY()));
	}
	
	@Override
	public boolean setEndYPosition(Double y)
	{
		return setEndPosition(new Position2DMillimeter(endPosition.getPosition().getValue().getX(), y));
	}
	
	@Override
	public boolean setEndAngle(IAngle th)
	{
		endPosition = new OrientedPosition2D(endPosition.getPosition(), th);
		return true;
	}
	
	@Override
	public boolean setEndLinearSpeed(Double v)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean setEndAngularSpeed(Double thp)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean setEndLinearAcceleration(Double a)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean setEndAngularAcceleration(Double ths)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public Double length()
	{
		TrajectoryBezierQuadInformation info = new TrajectoryBezierQuadInformation(this);
		return info.getLength();
	}
	
	public TrajectoryBezierQuadInformation getInformations()
	{
		return new TrajectoryBezierQuadInformation(this);
	}
	
	@Override
	public boolean setDirection(IPosition2D p2d)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	// --------- ITrajectoryMovement functions --------- \\
	
	@Override
	public void move(StraightLine straightLine, boolean forward, KeyFrame frame)
	{
		Point2D p = frame.getPosition().getValue();
		Point2D[] cp = frame.getControlPoints();
		((Path2D) straightLine.getPath()).curveTo(cp[0].getX(), cp[0].getY(), cp[1].getX(), cp[1].getY(), p.getX(), p.getY());
	}
	
	@Override
	public void describeTo(StraightLine straightLine, 
					Map<String, String> params, StringBuilder sb, 
					KeyFrame frame, ElementDescription el)
	{
		// does nothing
	}

	public KeyFrame interpolate(KeyFrame start, KeyFrame end, double timestamp)
	{
		double ds = start.getTimestamp();
		double duration = end.getTimestamp() - ds;
		double d = (timestamp - ds) / duration;
		Point2D[] points = end.getControlPoints();
		Point2D s = start.getPosition().getValue();
		Point2D e = end.getPosition().getValue();
		Point2D p = Bezier.getPoint(d, s, points[0], points[1], e);
		IAngle angle = new AngleRadian(Bezier.getAngle(d, s, points[0], points[1], e));
		
		KeyFrame frame = new KeyFrame(this, start.getMovementSpeed(), angle, 0, 0, new Position2DMillimeter(p), timestamp, start.getSourceLine(), points);
		return frame;
	}

	@Override
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public KeyFrame beInterpolatedBy(IKeyFrameInterpolator interpolator,
			KeyFrame start, KeyFrame end, double timestamp)
	{
		return interpolator.interpolateBezierQuad(this, start, end, timestamp);
	}

	@Override
	public String exportTo(IOrderExporter exporter)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
}
