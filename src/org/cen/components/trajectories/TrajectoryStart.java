
package org.cen.components.trajectories;

import java.awt.geom.Point2D;
import java.util.Map;

import org.cen.components.gameboard.generic.StraightLine;
import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.orientedPosition.OrientedPosition2D;
import org.cen.components.math.position.IPosition2D;
import org.cen.components.math.position.Position2DMillimeter;

/**
 * TrajectoryStart class file.
 * 
 * This part of a trajectory represents the start of the trajectory. This is
 * the only part of a trajectory that does not need an ancestor part. This way
 * it is the only element able to begin the chained list of trajectory parts
 * until the trajectory is complete.
 * 
 * The only settable option of this part is the start position and the start
 * angle of the robot. All others options are fixed to 0, as the robot should
 * not be in movement before starting the match begins.
 * 
 * @author Arnaud PETIT <anastaszor@gmail.com>
 */
public class TrajectoryStart implements ITrajectoryPart, ITrajectoryMovement
{
	
	/**
	 * The start position of the trajectory. As the trajectory counts only
	 * one frame, the start position is also the end position of the robot.
	 */
	private OrientedPosition2D startPosition = null;
	
	public TrajectoryStart()
	{
		startPosition = new OrientedPosition2D(new Position2DMillimeter(0d, 0d), new AngleRadian(0.0));
	}	
	
	@Override
	public String getPartName()
	{
		return "Start";
	}
	
	@Override
	public boolean getBackward()
	{
		return false;
	}
	
	@Override
	public OrientedPosition2D getStartPosition()
	{
		return startPosition;
	}
	
	@Override
	public OrientedPosition2D getEndPosition()
	{
		return startPosition;
	}
	
	@Override
	public OrientedSpeed getStartSpeed()
	{
		return new OrientedSpeed(new Point2D.Double(0, 0), 0d);
	}
	
	@Override
	public OrientedSpeed getEndSpeed()
	{
		return new OrientedSpeed(new Point2D.Double(0, 0), 0d);
	}
	
	@Override
	public OrientedAcceleration getStartAcceleration()
	{
		return new OrientedAcceleration(new Point2D.Double(0, 0), 0d);
	}
	
	@Override
	public OrientedAcceleration getEndAcceleration()
	{
		return new OrientedAcceleration(new Point2D.Double(0, 0), 0d);
	}
	
	@Override
	public boolean setBackward(boolean direction)
	{
		return true;
	}
	
	@Override
	public boolean setStartPosition(IPosition2D p2d)
	{
		startPosition = new OrientedPosition2D(p2d, startPosition.getOrientation());
		return true;
	}
	
	@Override
	public boolean setEndPosition(IPosition2D p2d)
	{
		return setStartPosition(p2d);
	}
	
	@Override
	public boolean setStartXPosition(Double x)
	{
		startPosition = new OrientedPosition2D(
			new Position2DMillimeter(x, startPosition.getPosition().getValue().getY()), 
			startPosition.getOrientation()
		);
		return true;
	}
	
	@Override
	public boolean setEndXPosition(Double x)
	{
		return setStartXPosition(x);
	}
	
	@Override
	public boolean setStartYPosition(Double y)
	{
		startPosition = new OrientedPosition2D(
			new Position2DMillimeter(startPosition.getPosition().getValue().getX(), y), 
			startPosition.getOrientation()
		);
		return true;
	}
	
	@Override
	public boolean setEndYPosition(Double y)
	{
		return setStartYPosition(y);
	}
	
	@Override
	public boolean setStartAngle(IAngle th)
	{
		startPosition = new OrientedPosition2D(startPosition.getPosition(), th);
		return true;
	}
	
	@Override
	public boolean setEndAngle(IAngle th)
	{
		return setStartAngle(th);
	}
	
	@Override
	public boolean setStartLinearSpeed(Double v)
	{
		return false;
	}
	
	@Override
	public boolean setEndLinearSpeed(Double v)
	{
		return false;
	}
	
	@Override
	public boolean setStartAngularSpeed(Double thp)
	{
		return false;
	}
	
	@Override
	public boolean setEndAngularSpeed(Double thp)
	{
		return false;
	}
	
	@Override
	public boolean setStartLinearAcceleration(Double a)
	{
		return false;
	}
	
	@Override
	public boolean setEndLinearAcceleration(Double a)
	{
		return false;
	}
	
	@Override
	public boolean setStartAngularAcceleration(Double ths)
	{
		return false;
	}
	
	@Override
	public boolean setEndAngularAcceleration(Double ths)
	{
		return false;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder("{TrajectoryStart");
		sb.append(" position=").append(getEndPosition());
		return sb.append("}").toString();
	}
	
	@Override
	public Double length()
	{
		return 0d;
	}
	
	@Override
	public boolean setDirection(IPosition2D p2d)
	{
		startPosition = new OrientedPosition2D(startPosition.getPosition(), new AngleRadian(Math.atan2(
			p2d.getValue().getY() - startPosition.getOrientation().getValue(),
			p2d.getValue().getX() - startPosition.getOrientation().getValue()
		)));
		return true;
	}
	
	@Override
	public void move(StraightLine straightLine, boolean forward, KeyFrame frame)
	{
		// TODO Auto-generated method stub
		
	}
	
	public void describeTo(StraightLine straightLine, 
					Map<String, String> params, StringBuilder sb, 
					KeyFrame frame, ElementDescription el)
	{
		el.last = frame.getPosition().getValue();
		el.lastOrientation = frame.getOrientation().getValue();
		sb.append(String.format("// start at %s\r\n", el.last.toString()));
	}
	
	@Override
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection)
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public KeyFrame beInterpolatedBy(IKeyFrameInterpolator interpolator,
			KeyFrame start, KeyFrame end, double timestamp)
	{
		return interpolator.interpolateStart(this, start, end, timestamp);
	}
	
	@Override
	public String exportTo(IOrderExporter exporter)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
}
