package org.cen.components.trajectories;

import java.awt.geom.Point2D;
import java.awt.geom.QuadCurve2D;
import java.util.Map;

import org.cen.components.gameboard.generic.StraightLine;
import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.orientedPosition.IOrientedPosition2D;
import org.cen.components.math.orientedPosition.OrientedPosition2D;
import org.cen.components.math.position.IPosition2D;
import org.cen.components.math.position.Position2DMillimeter;

public class TrajectoryClothoid extends AChainedTrajectory implements ITrajectoryMovement
{
	
	private IOrientedPosition2D endPosition = null;
	
	/**
	 * The TrajectoryClothoidInformation is to carry relevant information about
	 * this arc of clothoid to be printed successfully. As the parameters of
	 * this arc may change by users actions, a new arc information is generated
	 * and its calculations done again.
	 * 
	 * @author Anastaszor <anastaszor@gmail.com>
	 */
	public class TrajectoryClothoidInformation
	{
		private Point2D center;
		private Point2D control;
		private Double innerRadius;
		private Double outerRadius;
		private Double length;
		private IAngle angle;
		private QuadCurve2D.Double curve;
		
		/**
		 * Builds the information. Geometry involved !
		 * The center of the arc is at the intersection of :
		 * 1 - The perpendicular at the last direction passing at the 
		 * 		previous' end point
		 * 2 - The perpendicular at the new direction passing at the 
		 * 		new end point
		 * Assuming the old point is noted A(xa, ya) and the new point is noted
		 * B(xb, yb), the angle of the previous orientation noted θ (theta),
		 * the angle of the new orientation is noted γ (gamma), and the center
		 * of the concentric circles noted C(X, Y),
		 * Those straight lines have as equations :
		 * Straight line passing by A and vector director (cosθ, sinθ) :
		 * ( Δ1 ) Y = sinθ/cosθ * X + ya - sinθ/cosθ * xa
		 * Its perpendicular passing by A is :
		 * ( Π1 ) Y = - cosθ/sinθ * X + ya + cosθ/sinθ * xa
		 * Straight line passing by B and vector director (cosγ, sinγ) :
		 * ( Δ1 ) Y = sinγ/cosγ * X + yb - sinγ/cosγ * xb
		 * Its perpendicular passing by B is :
		 * ( Π2 ) Y = - cosγ/sinγ * X + yb + cosγ/sinγ * xb
		 * 
		 * The resolution of the intersection of ( Π1 ) and ( Π2 ) : // maple
		 * 		X = ( ya sinθ sinγ + xa sinθ cosγ - yb sinθ sinγ - xb cosθ sinγ) / (cosγ sinθ - cosθ sinγ)
		 * 		Y = ( yb sinθ cosγ + xb cosθ cosγ - ya cosθ sinγ - xa cosθ cosγ) / (cosγ sinθ - cosθ sinγ)
		 * @param clothoid
		 */
		public TrajectoryClothoidInformation(TrajectoryClothoid clothoid)
		{
			// TODO if the angle of source and destination are the same
			// turn the angle 90° left or right, depending on the side of the
			// initial vector and point. use atan2
			Double xa = clothoid.getStartPosition().getPosition().getValue().getX();
			Double ya = clothoid.getStartPosition().getPosition().getValue().getY();
			IAngle θ = clothoid.getStartPosition().getOrientation();
			Double xb = clothoid.getEndPosition().getPosition().getValue().getX();
			Double yb = clothoid.getEndPosition().getPosition().getValue().getY();
			IAngle γ = clothoid.getEndPosition().getOrientation();
			Double cθ = θ.toCosinus().getValue();
			Double sθ = θ.toSinus().getValue();
			Double cγ = γ.toCosinus().getValue();
			Double sγ = γ.toSinus().getValue();
			Double reducteur = (cγ * sθ - cθ * sγ);
			Double reducteur2 = (cγ * cθ + sθ * sγ);
			Double X = (ya * sθ * sγ + xa * sθ * cγ - yb * sθ * sγ - xb * cθ * sγ) / reducteur;
			Double Y = (yb * sθ * cγ + xb * cθ * cγ - ya * cθ * sγ - xa * cθ * cγ) / reducteur;
			Double CX = (yb * sγ * cθ + xb * cγ * cθ - ya * sγ * cθ + xa * sγ * sθ) / reducteur2;
			Double CY = (ya * cγ * cθ - xa * cγ * sθ + yb * sγ * sθ + xb * cγ * sθ) / reducteur2;
			center = new Point2D.Double(X, Y);
			control = new Point2D.Double(CX, CY);
			innerRadius = center.distance(clothoid.getStartPosition().getPosition().getValue());
			outerRadius = center.distance(clothoid.getEndPosition().getPosition().getValue());
			angle = γ.getRotationAngle(θ);
			length = 2 * angle.getValue() * outerRadius;
			// args order for quadcurve :
			// The X coordinate of the start point of the quadratic curve segment.
			// The Y coordinate of the start point of the quadratic curve segment.
			// The X coordinate of the control point of the quadratic curve segment.
			// The Y coordinate of the control point of the quadratic curve segment.
			// The X coordinate of the end point of the quadratic curve segment.
			// The Y coordinate of the end point of the quadratic curve segment.
			curve = new QuadCurve2D.Double(xa, ya, CX, CY, xb, yb);
		}
		
		public Point2D getCenter()
		{
			return center;
		}
		
		public Point2D getControl()
		{
			return control;
		}
		
		public Double getInnerRadius()
		{
			return innerRadius;
		}
		
		public Double getOuterRadius()
		{
			return outerRadius;
		}
		
		public Double getLength()
		{
			return length;
		}
		
		public IAngle getAngle()
		{
			return angle;
		}
		
		public QuadCurve2D getCurve()
		{
			return curve;
		}
		
	}
	
	public TrajectoryClothoid(ITrajectoryPart previous)
	{
		super(previous);
		endPosition = previous.getEndPosition();
	}
	
	@Override
	public String getPartName()
	{
		return "Clothoid";
	}
	
	@Override
	public IOrientedPosition2D getEndPosition()
	{
		return endPosition;
	}
	
	@Override
	public OrientedSpeed getEndSpeed()
	{
		return new OrientedSpeed(new Point2D.Double(), 0d);
	}
	
	@Override
	public OrientedAcceleration getEndAcceleration()
	{
		return new OrientedAcceleration(new Point2D.Double(), 0d);
	}
	
	@Override
	public boolean setEndPosition(IPosition2D p2d)
	{
		endPosition = new OrientedPosition2D(p2d, endPosition.getOrientation());
		return true;
	}
	
	@Override
	public boolean setEndXPosition(Double x)
	{
		return setEndPosition(new Position2DMillimeter(x, endPosition.getPosition().getValue().getY()));
	}
	
	@Override
	public boolean setEndYPosition(Double y)
	{
		return setEndPosition(new Position2DMillimeter(endPosition.getPosition().getValue().getX(), y));
	}
	
	@Override
	public boolean setEndAngle(IAngle th)
	{
		endPosition = new OrientedPosition2D(endPosition.getPosition(), th);
		return true;
	}
	
	@Override
	public boolean setEndLinearSpeed(Double v)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean setEndAngularSpeed(Double thp)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean setEndLinearAcceleration(Double a)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean setEndAngularAcceleration(Double ths)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public Double length()
	{
		TrajectoryClothoidInformation tci = new TrajectoryClothoidInformation(this);
		return tci.getLength();
	}
	
	public TrajectoryClothoidInformation getInformations()
	{
		return new TrajectoryClothoidInformation(this);
	}
	
	@Override
	public boolean setDirection(IPosition2D p2d)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	// --------- ITrajectoryMovement functions --------- \\
	
	@Override
	public void move(StraightLine straightLine, boolean forward, KeyFrame frame)
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void describeTo(StraightLine straightLine, 
					Map<String, String> params, StringBuilder sb, 
					KeyFrame frame, ElementDescription el)
	{
		// does nothing
	}
	
	@Override
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection)
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public KeyFrame beInterpolatedBy(IKeyFrameInterpolator interpolator,
			KeyFrame start, KeyFrame end, double timestamp)
	{
		return interpolator.interpolateClothoid(this, start, end, timestamp);
	}
	
	@Override
	public String exportTo(IOrderExporter exporter)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
}
