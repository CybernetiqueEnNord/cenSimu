package org.cen.components.trajectories;

import java.util.Map;

import org.cen.components.gameboard.generic.StraightLine;
import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.orientedPosition.IOrientedPosition2D;
import org.cen.components.math.position.IPosition2D;
import org.cen.components.math.time.DelayMillisecond;
import org.cen.components.math.time.IDelay;

public class TrajectoryDelay extends AChainedTrajectory implements ITrajectoryMovement
{
	
	private IDelay duration = new DelayMillisecond(0d);
	
	public TrajectoryDelay(ITrajectoryPart previous, IDelay duration)
	{
		super(previous);
		setDuration(duration);
	}
	
	public IDelay getDuration()
	{
		if(duration == null) return new DelayMillisecond(0d);
		return duration;
	}
	
	public void setDuration(IDelay duration)
	{
		this.duration = duration;
	}
	
	@Override
	public String getPartName()
	{
		return "Delay";
	}
	
	@Override
	public IOrientedPosition2D getEndPosition()
	{
		return getStartPosition();
	}
	
	@Override
	public OrientedSpeed getEndSpeed()
	{
		return getStartSpeed();
	}
	
	@Override
	public OrientedAcceleration getEndAcceleration()
	{
		return getStartAcceleration();
	}
	
	@Override
	public boolean setEndPosition(IPosition2D p2d)
	{
		return setStartPosition(p2d);
	}
	
	@Override
	public boolean setEndXPosition(Double x)
	{
		return setStartXPosition(x);
	}
	
	@Override
	public boolean setEndYPosition(Double y)
	{
		return setStartYPosition(y);
	}
	
	@Override
	public boolean setEndAngle(IAngle th)
	{
		return setStartAngle(th);
	}
	
	@Override
	public boolean setEndLinearSpeed(Double v)
	{
		return setStartLinearSpeed(v);
	}
	
	@Override
	public boolean setEndAngularSpeed(Double thp)
	{
		return setStartAngularSpeed(thp);
	}
	
	@Override
	public boolean setEndLinearAcceleration(Double a)
	{
		return setStartLinearAcceleration(a);
	}
	
	@Override
	public boolean setEndAngularAcceleration(Double ths)
	{
		return setStartAngularAcceleration(ths);
	}
	
	@Override
	public Double length()
	{
		return 0d;
	}
	
	@Override
	public boolean setDirection(IPosition2D p2d)
	{
		return previous.setDirection(p2d);
	}
	
	// --------- ITrajectoryMovement functions --------- \\
	
	@Override
	public void move(StraightLine straightLine, boolean forward, KeyFrame frame)
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void describeTo(StraightLine straightLine, 
					Map<String, String> params, StringBuilder sb, 
					KeyFrame frame, ElementDescription el)
	{
//		 double delay = frame.getTimestamp() - lastTimestamp;
//		 addComments(params, String.format("delay_ms(%.0f);", delay * 1000), true);
	}
	
	@Override
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection)
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public KeyFrame beInterpolatedBy(IKeyFrameInterpolator interpolator,
			KeyFrame start, KeyFrame end, double timestamp)
	{
		return interpolator.interpolateDelay(this, start, end, timestamp);
	}
	
	@Override
	public String exportTo(IOrderExporter exporter)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
}
