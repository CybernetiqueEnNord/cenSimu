package org.cen.components.trajectories;

import java.awt.geom.CubicCurve2D;
import java.awt.geom.Point2D;

import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.orientedPosition.IOrientedPosition2D;
import org.cen.components.math.orientedPosition.OrientedPosition2D;
import org.cen.components.math.position.IPosition2D;
import org.cen.components.math.position.Position2DMillimeter;

public class TrajectoryBezierCubic extends AChainedTrajectory
{
	
	private IPosition2D firstControlPosition = null;
	private IPosition2D secondControlPosition = null;
	private IOrientedPosition2D endPosition = null;
	
	public class TrajectoryBezierCubicInformation
	{
		
		private Double length;
		private IAngle angle;
		private CubicCurve2D curve;
		
		/**
		 * 
		 * @param bcub
		 */
		public TrajectoryBezierCubicInformation(TrajectoryBezierCubic bcub)
		{
			angle = bcub.getEndPosition().getOrientation().getRotationAngle(bcub.getStartPosition().getOrientation());
			
			// args order for cubiccurve :
			// startX - the X coordinate of the start point
			// startY - the Y coordinate of the start point
			// controlX1 - the X coordinate of the first control point
			// controlY1 - the Y coordinate of the first control point
			// controlX2 - the X coordinate of the second control point
			// controlY2 - the Y coordinate of the second control point
			// endX - the X coordinate of the end point
			// endY - the Y coordinate of the end point
			curve = new CubicCurve2D.Double(
				bcub.getStartPosition().getPosition().getValue().getX(), 
				bcub.getStartPosition().getPosition().getValue().getY(), 
				bcub.getFirstControlPosition().getValue().getX(), 
				bcub.getFirstControlPosition().getValue().getY(), 
				bcub.getSecondControlPosition().getValue().getX(), 
				bcub.getSecondControlPosition().getValue().getY(), 
				bcub.getEndPosition().getPosition().getValue().getX(), 
				bcub.getEndPosition().getPosition().getValue().getY()
			);
			
			/*
			 * calculus of the length... maybe ? :D
			 */
			length = bcub.getStartPosition().distance(bcub.getEndPosition()).getValue();
		}
		
		public Double getLength()
		{
			return length;
		}
		
		public IAngle getAngle()
		{
			return angle;
		}
		
		public CubicCurve2D getCurve()
		{
			return curve;
		}
		
	}
	
	public TrajectoryBezierCubic(ITrajectoryPart previous)
	{
		super(previous);
		endPosition = previous.getEndPosition();
	}
	
	@Override
	public String getPartName()
	{
		return "BezierCubic";
	}
	
	public IPosition2D getFirstControlPosition()
	{
		return firstControlPosition;
	}
	
	public void setFirstControlPosition(IPosition2D first)
	{
		firstControlPosition = first;
	}
	
	public IPosition2D getSecondControlPosition()
	{
		return secondControlPosition;
	}
	
	public void setSecondControlPosition(IPosition2D second)
	{
		secondControlPosition = second;
	}
	
	@Override
	public IOrientedPosition2D getEndPosition()
	{
		return endPosition;
	}
	
	@Override
	public OrientedSpeed getEndSpeed()
	{
		return new OrientedSpeed(new Point2D.Double(), 0d);
	}
	
	@Override
	public OrientedAcceleration getEndAcceleration()
	{
		return new OrientedAcceleration(new Point2D.Double(), 0d);
	}
	
	@Override
	public boolean setEndPosition(IPosition2D p2d)
	{
		endPosition = new OrientedPosition2D(p2d, endPosition.getOrientation());
		return true;
	}
	
	@Override
	public boolean setEndXPosition(Double x)
	{
		return setEndPosition(new Position2DMillimeter(x, endPosition.getPosition().getValue().getY()));
	}
	
	@Override
	public boolean setEndYPosition(Double y)
	{
		return setEndPosition(new Position2DMillimeter(endPosition.getPosition().getValue().getX(), y));
	}
	
	@Override
	public boolean setEndAngle(IAngle th)
	{
		endPosition = new OrientedPosition2D(endPosition.getPosition(), th);
		return false;
	}
	
	@Override
	public boolean setEndLinearSpeed(Double v)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean setEndAngularSpeed(Double thp)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean setEndLinearAcceleration(Double a)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean setEndAngularAcceleration(Double ths)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public Double length()
	{
		TrajectoryBezierCubicInformation info = new TrajectoryBezierCubicInformation(this);
		return info.getLength();
	}
	
	public TrajectoryBezierCubicInformation getInformations()
	{
		return new TrajectoryBezierCubicInformation(this);
	}
	
	@Override
	public boolean setDirection(IPosition2D p2d)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection)
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public String exportTo(IOrderExporter exporter)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
}
