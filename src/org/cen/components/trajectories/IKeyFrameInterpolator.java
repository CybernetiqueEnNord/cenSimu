package org.cen.components.trajectories;


public interface IKeyFrameInterpolator
{
	
	public KeyFrame interpolate(KeyFrame start, KeyFrame end, ITrajectoryMovement movement, double timestamp);
	
	public KeyFrame interpolateBezierQuad(TrajectoryBezierQuad bquad, KeyFrame start, KeyFrame end, double timestamp);
	
	public KeyFrame interpolateClothoid(TrajectoryClothoid clotho, KeyFrame start, KeyFrame end, double timestamp);

	public KeyFrame interpolateDelay(TrajectoryDelay delay, KeyFrame start, KeyFrame end, double timestamp);

	public KeyFrame interpolateRotation(TrajectoryForcedRotation rotation, KeyFrame start, KeyFrame end, double timestamp);

	public KeyFrame interpolateSegment(TrajectorySegment segment, KeyFrame start, KeyFrame end, double timestamp);

	public KeyFrame interpolateStart(TrajectoryStart tstart, KeyFrame start, KeyFrame end, double timestamp);
	
}
