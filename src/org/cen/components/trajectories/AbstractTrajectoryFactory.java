package org.cen.components.trajectories;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import org.cen.components.order.IOrderList;

public abstract class AbstractTrajectoryFactory {
	public abstract IOrderList getTrajectoryPath(String name, BufferedReader stream);

	public IOrderList loadFromFile(String filePath) throws FileNotFoundException {
		File file = new File(filePath);
		String name = file.getName();
		BufferedReader fis = new BufferedReader(new FileReader(file));
		return getTrajectoryPath(name, fis);
	}
}
