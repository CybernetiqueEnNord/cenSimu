package org.cen.components.trajectories;

import java.awt.geom.Point2D;

import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.position.Position2DMillimeter;

public class KeyFrameLinearInterpolator implements IKeyFrameInterpolator
{
	
	public KeyFrame interpolate(KeyFrame start, KeyFrame end,
		ITrajectoryMovement movement, double timestamp)
	{
		return movement.beInterpolatedBy(this, start, end, timestamp);
	}
	
	@Override
	public KeyFrame interpolateBezierQuad(TrajectoryBezierQuad bquad,
			KeyFrame start, KeyFrame end, double timestamp)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public KeyFrame interpolateClothoid(TrajectoryClothoid clotho,
			KeyFrame start, KeyFrame end, double timestamp)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public KeyFrame interpolateDelay(TrajectoryDelay delay, KeyFrame start,
			KeyFrame end, double timestamp)
	{
		return new KeyFrame(delay, 0, end.getOrientation(), 0, 0, end.getPosition(), timestamp, end.getSourceLine());
	}
	
	@Override
	public KeyFrame interpolateRotation(TrajectoryForcedRotation rotation,
			KeyFrame start, KeyFrame end, double timestamp)
	{
		double startAngle = start.getOrientation().toRadians().getValue();
		double endAngle = end.getOrientation().toRadians().getValue();
		double theta = start.getOrientation().getRotationAngle(end.getOrientation()).getValue();
//		double theta = Angle.getRotationAngle(startAngle, endAngle);
		if (end.useRelativeAngle()) {
			theta = endAngle - startAngle;
		}
		
		double ds = start.getTimestamp();
		double duration = end.getTimestamp() - ds;
		double d = (timestamp - ds) / duration;
		double angle = startAngle + d * theta;
		
		KeyFrame frame = new KeyFrame(rotation, 0, new AngleRadian(angle), 0, start.getRotationSpeed(), start.getPosition(), timestamp, start.getSourceLine());
		return frame;
	}
	
	@Override
	public KeyFrame interpolateSegment(TrajectorySegment segment,
			KeyFrame start, KeyFrame end, double timestamp)
	{
		Point2D startPoint = start.getPosition().getValue();
		Point2D endPoint = end.getPosition().getValue();
		double ds = start.getTimestamp();
		double duration = end.getTimestamp() - ds;
		double d = (timestamp - ds) / duration;
		double x1 = startPoint.getX();
		double y1 = startPoint.getY();
		double x2 = endPoint.getX();
		double y2 = endPoint.getY();
		double x = x1 + d * (x2 - x1);
		double y = y1 + d * (y2 - y1);
		Point2D p = new Point2D.Double(x, y);
		
		KeyFrame frame = new KeyFrame(segment, start.getMovementSpeed(), end.getOrientation(), 0, 0, new Position2DMillimeter(p), timestamp, start.getSourceLine());
		return frame;
	}
	
	@Override
	public KeyFrame interpolateStart(TrajectoryStart tstart, KeyFrame start,
			KeyFrame end, double timestamp)
	{
		return end;
	}
	
}
