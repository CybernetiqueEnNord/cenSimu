package org.cen.components.trajectories;

import java.util.Map;

import org.cen.components.gameboard.generic.StraightLine;
import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.orientedPosition.IOrientedPosition2D;
import org.cen.components.math.position.IPosition2D;

public class TrajectoryForcedRotation extends AChainedTrajectory implements ITrajectoryMovement
{
	
	private IOrientedPosition2D endPosition = null;
	/**
	 * True if trigonometric.
	 * False if clockwise.
	 * Null if default : shortest.
	 */
//	private Boolean way = null;
	
	public TrajectoryForcedRotation(ITrajectoryPart previous)
	{
		super(previous);
		if(previous != null)
			endPosition = previous.getEndPosition();
	}
	
	@Override
	public String getPartName()
	{
		return "ForcedRot";
	}
	
	@Override
	public IOrientedPosition2D getEndPosition()
	{
		return endPosition;
	}
	
	@Override
	public OrientedSpeed getEndSpeed()
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public OrientedAcceleration getEndAcceleration()
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean setEndPosition(IPosition2D p2d)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean setEndXPosition(Double x)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean setEndYPosition(Double y)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean setEndAngle(IAngle th)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean setEndLinearSpeed(Double v)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean setEndAngularSpeed(Double thp)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean setEndLinearAcceleration(Double a)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean setEndAngularAcceleration(Double ths)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public Double length()
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean setDirection(IPosition2D p2d)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void move(StraightLine straightLine, boolean forward, KeyFrame frame)
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void describeTo(StraightLine straightLine, 
					Map<String, String> params, StringBuilder sb, 
					KeyFrame frame, ElementDescription el)
	{
		double o = frame.getOrientation().getValue();
		double angle = new AngleRadian(el.lastOrientation).getRotationAngle(new AngleRadian(0.0)).getValue();
//		double angle = Angle.getRotationAngle(el.lastOrientation, o);
		if (frame.useRelativeAngle()) {
			// the angle is a relative angle that can be > 180°
			angle = frame.getRelativeAngle();
		}
		angle = Math.toDegrees(angle);
		straightLine.addComments(params, String.format("// rotation of %.0f° (%.0f)", angle, 22527.5d / 360d * angle), true);
		straightLine.addValue(params, StraightLine.KEY_ANGLE, 22527.5d / 360d * angle);
		if (params.containsKey(StraightLine.KEY_ORIENTATION)) {
			double opposite = straightLine.getDoubleValue(params, StraightLine.KEY_ORIENTATION);
			opposite = Math.toDegrees(opposite);
			straightLine.addValue(params, StraightLine.KEY_ORIENTATION, 22527.5d / 360d * opposite);
		}
		el.lastOrientation = o;
	}
	
	@Override
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public KeyFrame beInterpolatedBy(IKeyFrameInterpolator interpolator,
			KeyFrame start, KeyFrame end, double timestamp)
	{
		return interpolator.interpolateRotation(this, start, end, timestamp);
	}
	
	@Override
	public String exportTo(IOrderExporter exporter)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
}
