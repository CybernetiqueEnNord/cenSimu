package org.cen.components.trajectories;

import org.cen.components.io.trajectory.ITrajectoryParser;

public class TextTrajectoryFactory extends AbstractTextTrajectoryFactory {
	private ITrajectoryParser parser;

	public TextTrajectoryFactory(ITrajectoryParser parser) {
		super();
		this.parser = parser;
	}

	@Override
	protected ITrajectoryParser createParser() {
		return parser;
	}
}
