package org.cen.components.trajectories;

import java.awt.geom.Point2D;

import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.orientedPosition.IOrientedPosition2D;
import org.cen.components.math.orientedPosition.OrientedPosition2D;
import org.cen.components.math.position.IPosition2D;
import org.cen.components.math.position.Position2DMillimeter;

/**
 * TrajectoryArc and TrajectoryArcInformation class file.
 * 
 * A TrajectoryArc is a part of a trajectory representing a circle arc between
 * two points. The direction of the arc is given by the previous part of the
 * trajectory.
 * 
 * @author Anastaszor <anastaszor@yahoo.fr>
 */
public class TrajectoryArc extends AChainedTrajectory
{
	
	private IOrientedPosition2D endPosition = null;
	
	/**
	 * The TrajectoryArcInformation is to carry relevant information about
	 * this arc to be printed successfully. As the parameters of this arc
	 * may change by user actions, a new arc information is generated and
	 * its calculation done again.
	 * 
	 * @author Anastaszor <anastaszor@gmail.com>
	 */
	public class TrajectoryArcInformation 
	{
		private Point2D center;
		private Double radius;
		private IAngle startAngle;
		private IAngle arcAngle;
		private Double length;
		
		/**
		 * Builds the information. Geometry involved !
		 * The center of the arc is at the intersection of :
		 * 1 - the perpendicular at the last direction passing at the previous' end point
		 * 2 - the bissector of the straight passing by the previous' end point
		 * 		and the new end point of this arc.
		 * Assuming the old point is noted A(xa, ya) and the new point is noted
		 * B(xb, yb) and the angle of the previous orientation noted θ (theta),
		 * and assuming the center of the arc is C(X, Y)
		 * those straight lines have as equations :
		 * Straight line passing by A and vector director (cosθ, sinθ) :
		 * ( Δ1 ) Y = sinθ/cosθ * X + ya - sinθ/cosθ * xa
		 * Its perpendicular passing by A is : 
		 * ( Π1 ) Y = - cosθ/sinθ * X + ya + cosθ/sinθ * xa
		 * Straight line passing by A and B:
		 * ( Δ2 ) Y = (yb - ya)/(xb - xa) X +  ( ya - xa (yb - ya)/(xb - xa))
		 * Its perpendicular passing by the middle of [A,B] is :
		 * ( Π2 ) Y = (xb - xa)/(yb - ya) X + (yb - ya)/2 - 1/2 * (xb - xa)²/(yb - ya)
		 * The resolution is the intersection of ( Π1 ) and ( Π2 ) : // maple
		 * 		X = 1/2 * (sinθ * ((xb - xa)² - (2 * ya - yb)² + ya²) + cosθ * 2 * xa (ya - yb)) / ((xb - xa)sinθ + (ya - yb)cosθ)
		 * 		Y = 1/2 * (cosθ * ((xb - 2 * xa)² - xa² - (ya - yb)²) + sinθ * 2 * ya (xb - xa)) / ((xb - xa)sinθ + (ya - yb)cosθ)
		 * @param trajectoryArc
		 */
		public TrajectoryArcInformation(TrajectoryArc trajectoryArc)
		{
			Double xa = trajectoryArc.getStartPosition().getPosition().getValue().getX();
			Double ya = trajectoryArc.getStartPosition().getPosition().getValue().getY();
			Double xb = trajectoryArc.getEndPosition().getPosition().getValue().getX();
			Double yb = trajectoryArc.getEndPosition().getPosition().getValue().getY();
			IAngle θ = trajectoryArc.getStartPosition().getOrientation();
			Double cθ = θ.toCosinus().getValue();
			Double sθ = θ.toSinus().getValue();
			Double reducteur = 2 *((xa - xb) * sθ + (yb - ya) * cθ);
			Double X = (sθ * (2 * ya * yb - sq(ya) + sq(xa) - sq(yb) - sq(xb)) + cθ * 2 * xa * (yb - ya)) / reducteur;
			Double Y = (cθ * (sq(yb) - sq(ya) + sq(xa) + sq(xb) - 2 * xb * xa) + sθ * 2 * ya * (xa - xb)) / reducteur;
			center = new Point2D.Double(X, Y);
			radius = (center.distance(trajectoryArc.getStartPosition().getPosition().getValue()) 
						+ center.distance(trajectoryArc.getEndPosition().getPosition().getValue())
					)/2;	// on moyenne pour minimiser l'erreur < 1e-12
			startAngle = new AngleRadian(Math.atan2(ya - Y, xa - X));
			arcAngle = new AngleRadian(Math.atan2(yb - Y, xb - X));
			arcAngle = arcAngle.getRotationAngle(startAngle);
//			if(Math.signum(θ) == Math.signum(arcAngle))	// TODO CA MARCHE PAS C'EST PAS LA BONNE CONDITION
//			{
//				arcAngle += 2 * Math.PI;
//				arcAngle %= 2 * Math.PI;
//			}
			length = Math.abs((arcAngle.getValue()) * radius);
			trajectoryArc.endPosition = new OrientedPosition2D(trajectoryArc.endPosition.getPosition(), new AngleRadian(Math.atan2(X - xb, yb - Y)));
//			System.out.println(trajectoryArc.getStartPosition().getLocation());
//			System.out.println(trajectoryArc.getEndPosition().getLocation());
//			System.out.println(θ);
//			System.out.println(this);
		}
		
		private Double sq(Double i)
		{
			return Math.pow(i, 2);
		}
		
		public Point2D getCenter()
		{
			return center;
		}
		
		public Double getRadius()
		{
			return radius;
		}
		
		public IAngle getStartAngle()
		{
			return startAngle;
		}
		
		public IAngle getArcAngle()
		{
			return arcAngle;
		}

		public Double getLength()
		{
			return length;
		}
		
		public String toString()
		{
			StringBuilder sb = new StringBuilder("{TrajectoryArcInformation");
			sb.append(" center=").append(center);
			sb.append(" radius=").append(radius);
			sb.append(" startAngle=").append(startAngle);
			sb.append(" arcAngle=").append(arcAngle);
			sb.append(" length=").append(length);
			return sb.append("}").toString();
		}
	}
	
	public TrajectoryArc(ITrajectoryPart previous)
	{
		super(previous);
		endPosition = previous.getEndPosition();
	}
	
	@Override
	public String getPartName()
	{
		return "Arc";
	}
	
	@Override
	public IOrientedPosition2D getEndPosition()
	{
		return endPosition;
	}
	
	@Override
	public OrientedSpeed getEndSpeed()
	{
		return new OrientedSpeed(new Point2D.Double(), 0d);
	}
	
	@Override
	public OrientedAcceleration getEndAcceleration()
	{
		return new OrientedAcceleration(new Point2D.Double(), 0d);
	}
	
	@Override
	public boolean setBackward(boolean direction)
	{
		if(super.setBackward(direction))
		{
			
			return true;
		}
		return false;
	}
	
	@Override
	public boolean setEndPosition(IPosition2D p2d)
	{
		endPosition = new OrientedPosition2D(p2d, endPosition.getOrientation());
		new TrajectoryArcInformation(this);	// updates the end orientation /!?\ program architecture
		return false;
	}
	
	@Override
	public boolean setEndXPosition(Double x)
	{
		return setEndPosition(new Position2DMillimeter(x, endPosition.getPosition().getValue().getY()));
	}
	
	@Override
	public boolean setEndYPosition(Double y)
	{
		return setEndPosition(new Position2DMillimeter(endPosition.getPosition().getValue().getX(), y));
	}
	
	@Override
	public boolean setEndAngle(IAngle th)
	{
		// get the tangeant of this arc's full circle that lies is 
		// this arc, then crop/prolong the arc at/to the tangeant point
		IAngle rotation = getStartPosition().getOrientation().getRotationAngle(th);
		TrajectoryArcInformation info = new TrajectoryArcInformation(this);
		Point2D.Double endPoint = new Point2D.Double(
			info.getCenter().getX() + rotation.toCosinus().getValue(), 
			info.getCenter().getY() + rotation.toSinus().getValue()
		);
		this.endPosition = new OrientedPosition2D(new Position2DMillimeter(endPoint), th);
		new TrajectoryArcInformation(this);
		return true;
	}
	
	@Override
	public boolean setEndLinearSpeed(Double v)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean setEndAngularSpeed(Double thp)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean setEndLinearAcceleration(Double a)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean setEndAngularAcceleration(Double ths)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public Double length()
	{
		return new TrajectoryArcInformation(this).getLength();
	}
	
	public TrajectoryArcInformation getInformations()
	{
		return new TrajectoryArcInformation(this);
	}
	
	@Override
	public boolean setDirection(IPosition2D p2d)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection)
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public String exportTo(IOrderExporter exporter)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
}
