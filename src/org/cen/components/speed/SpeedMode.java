package org.cen.components.speed;

public enum SpeedMode
{
	SLOW(1),
	MEDIUM(2),
	FAST(3),
	;
	
	private Integer value = null;
	
	private SpeedMode(Integer mode)
	{
		this.value = mode;
	}
	
	public Integer getValue() {
		return value;
	}
	
}
