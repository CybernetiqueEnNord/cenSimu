package org.cen.components.math.angle;

import org.cen.components.math.Signum;

/**
 * Class representing an angle, measured in radians. 
 * This angle is NOT in range ]-PI; PI].
 * 
 * @author Anastaszor
 */
public class AngleRadian implements IAngle
{
	
	public static final double PI = Math.PI;
	public static final double _2PI = 2 * Math.PI;
	public static final double PI_2 = Math.PI / 2;
	
	/**
	 * The given value of this angle. (in radians)
	 */
	private Double value = null;
	
	/**
	 * Generic constructor that wraps its value.
	 * 
	 * @param value
	 */
	public AngleRadian(Double value)
	{
		while(value > Math.PI)
			value -= _2PI;
		while(value <= -Math.PI)
			value += _2PI;
		this.value = value;
	}
	
	@Override
	public Double getValue()
	{
		return value;
	}
	
	@Override
	public AngleRadian toRadians()
	{
		return this;
	}
	
	@Override
	public AngleDegree toDegrees()
	{
		return new AngleDegree(Math.toDegrees(this.getValue()));
	}
	
	@Override
	public AngleDecidegree toDecidegrees()
	{
		return new AngleDecidegree(Math.toDegrees(this.getValue()) * 10);
	}
	
	@Override
	public AngleCosinus toCosinus()
	{
		return new AngleCosinus(Math.cos(this.toRadians().getValue()), Signum.eval(Math.sin(this.getValue())));
	}
	
	@Override
	public AngleSinus toSinus()
	{
		return new AngleSinus(Math.sin(this.toRadians().getValue()), Signum.eval(Math.cos(this.getValue())));
	}
	
	@Override
	public IAngle getRotationAngle(IAngle angle)
	{
		return new AngleRadian(this.getValue() - angle.toRadians().getValue());
	}
	
	@Override
	public IAngle add(IAngle angle)
	{
		return new AngleRadian(this.getValue() + angle.toRadians().getValue());
	}
	
	@Override
	public Signum getSignum()
	{
		return Signum.eval(this.getValue());
	}
	
	@Override
	public String toString()
	{
		return String.format("%.2f rad", getValue());
	}
	
	@Override
	public IAngle opposite()
	{
		return new AngleRadian(this.getValue() + PI);
	}
	
}
