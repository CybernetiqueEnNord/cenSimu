package org.cen.components.math.angle;

import org.cen.components.math.Signum;

/**
 * Class representing an angle, measured by its cosinus and the signum of its
 * sinus.
 * 
 * @author Anastaszor
 */
public class AngleCosinus implements IAngle
{
	
	/**
	 * The given value of the cosinus of this angle.
	 */
	private Double value = null;
	
	/**
	 * The signum of the sinus of this angle.
	 */
	private Signum sinusSignum = null;
	
	/**
	 * Generic constructor that wraps its value
	 * @param value
	 * @param sinusSignum
	 */
	public AngleCosinus(Double value, Signum sinusSignum)
	{
		this.value = value;
		this.sinusSignum = sinusSignum;
	}
	
	@Override
	public Double getValue()
	{
		return this.value;
	}
	
	@Override
	public AngleRadian toRadians()
	{
		return new AngleRadian(Math.acos(this.getValue()) * sinusSignum.getValue());
	}
	
	@Override
	public AngleDegree toDegrees()
	{
		return this.toRadians().toDegrees();
	}
	
	@Override
	public AngleDecidegree toDecidegrees()
	{
		return this.toDegrees().toDecidegrees();
	}
	
	@Override
	public AngleCosinus toCosinus()
	{
		return this;
	}
	
	@Override
	public AngleSinus toSinus()
	{
		return new AngleSinus(Math.sqrt(1 - Math.pow(this.getValue(), 2)), this.getSignum());
	}
	
	@Override
	public IAngle getRotationAngle(IAngle angle)
	{
		return this.toRadians().getRotationAngle(angle);
	}
	
	@Override
	public IAngle add(IAngle angle)
	{
		return this.toRadians().add(angle);
	}
	
	@Override
	public Signum getSignum()
	{
		return Signum.eval(this.value);
	}
	
	/**
	 * Gets the signum of the sinus associated to this angle
	 * 
	 * @return the signum of the sinus
	 */
	public Signum getSinusSignum()
	{
		return sinusSignum;
	}
	
	@Override
	public IAngle opposite()
	{
		return new AngleCosinus(-this.getValue(), sinusSignum.opposite());
	}
	
	@Override
	public String toString()
	{
		return String.format("cos(%.2f)", getValue());
	}
	
}
