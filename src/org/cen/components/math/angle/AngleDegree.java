package org.cen.components.math.angle;

import org.cen.components.math.Signum;

/**
 * Class representing an angle, measured in degrees.
 * This angle is NOT in range ]-180;180]
 * 
 * @author Anastaszor
 */
public class AngleDegree implements IAngle
{
	
	/**
	 * The given value of this angle. (in degrees)
	 */
	private Double value = null;
	
	/**
	 * Generic constructor that wraps its value.
	 * 
	 * @param value
	 */
	public AngleDegree(Double value)
	{
//		while(value > 180)
//			value -= 360;
//		while(value <= -180)
//			value += 360;
		this.value = value;
	}
	
	@Override
	public Double getValue()
	{
		return value;
	}
	
	@Override
	public AngleRadian toRadians()
	{
		return new AngleRadian(Math.toRadians(this.getValue()));
	}
	
	@Override
	public AngleDegree toDegrees()
	{
		return this;
	}
	
	@Override
	public AngleDecidegree toDecidegrees()
	{
		return new AngleDecidegree(this.getValue() * 10);
	}
	
	@Override
	public AngleCosinus toCosinus()
	{
		return this.toRadians().toCosinus();
	}
	
	@Override
	public AngleSinus toSinus()
	{
		return this.toRadians().toSinus();
	}
	
	@Override
	public IAngle getRotationAngle(IAngle angle)
	{
		return new AngleDegree(this.getValue() - angle.toDegrees().getValue());
	}
	
	@Override
	public IAngle add(IAngle angle)
	{
		return new AngleDegree(this.getValue() + angle.toDegrees().getValue());
	}
	
	@Override
	public Signum getSignum()
	{
		return Signum.eval(this.getValue());
	}
	
	@Override
	public String toString()
	{
		Double simplified = getValue();
		while(simplified > 180)
			simplified -= 360;
		while(simplified <= -180)
			simplified += 360;
		return String.format("%.2f °", simplified);
	}
	
	@Override
	public IAngle opposite()
	{
		return new AngleDegree(this.getValue() + 180d);
	}
	
}
