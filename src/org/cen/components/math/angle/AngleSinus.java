package org.cen.components.math.angle;

import org.cen.components.math.Signum;


public class AngleSinus implements IAngle
{
	
	private Double value = null;
	
	private Signum cosinusSignum = null;
	
	public AngleSinus(Double value, Signum cosinusSignum)
	{
		this.value = value;
		this.cosinusSignum = cosinusSignum;
	}
	
	@Override
	public Double getValue()
	{
		return value;
	}
	
	@Override
	public AngleRadian toRadians()
	{
		double angle = Math.asin(this.getValue());
		if(this.cosinusSignum == Signum.NEGATVE)
			angle += Math.PI;
		return new AngleRadian(angle);
	}
	
	@Override
	public AngleDegree toDegrees()
	{
		return this.toRadians().toDegrees();
	}
	
	@Override
	public AngleDecidegree toDecidegrees()
	{
		return this.toDegrees().toDecidegrees();
	}
	
	@Override
	public AngleCosinus toCosinus()
	{
		return new AngleCosinus(Math.sqrt(1 - Math.pow(this.getValue(), 2)), this.getSignum());
	}
	
	@Override
	public AngleSinus toSinus()
	{
		return this;
	}
	
	@Override
	public IAngle getRotationAngle(IAngle angle)
	{
		return this.toRadians().getRotationAngle(angle);
	}
	
	@Override
	public IAngle add(IAngle angle)
	{
		return this.toRadians().add(angle);
	}
	
	@Override
	public Signum getSignum()
	{
		return Signum.eval(this.getValue());
	}
	
	/**
	 * Gets the signum of the cosinus associated to this angle.
	 * 
	 * @return the signum of the cosinus
	 */
	public Signum getCosinusSignum()
	{
		return cosinusSignum;
	}
	
	@Override
	public IAngle opposite()
	{
		return new AngleSinus(-this.getValue(), cosinusSignum.opposite());
	}
	
	@Override
	public String toString()
	{
		return String.format("sin(%.2f)", getValue());
	}
	
}
