package org.cen.components.math.angle;

import org.cen.components.math.Signum;

/**
 * IAngle generic interface. This interface is for all mesurable angle units.
 * 
 * @author Anastaszor
 */
public interface IAngle
{
	
	/**
	 * Gets the value of the angle, in the given unit.
	 * 
	 * @return Double the value of the angle in its unit.
	 */
	public Double getValue();
	
	/**
	 * Converts this angle to radians.
	 * 
	 * @return the converted angle
	 */
	public AngleRadian toRadians();
	
	/**
	 * Converts this angle to degrees.
	 * 
	 * @return the converted angle
	 */
	public AngleDegree toDegrees();
	
	/**
	 * Converts this angle to decidegrees.
	 * 
	 * @return the converted angle
	 */
	public AngleDecidegree toDecidegrees();
	
	/**
	 * Gets the cosinus of this angle.
	 * 
	 * @return the converted angle
	 */
	public AngleCosinus toCosinus();
	
	/**
	 * Gets the sinus of this angle.
	 * 
	 * @return the converted angle
	 */
	public AngleSinus toSinus();
	
	/**
	 * Gets the angle formed by the rotation from this angle to argument angle.
	 * 
	 * @param angle
	 * @return the result of substracting argument angle from this one
	 */
	public IAngle getRotationAngle(IAngle angle);
	
	/**
	 * Adds given angle to this angle, and returns the added angle.
	 * 
	 * @param rad
	 * @return the result of the adding angle operation
	 */
	public IAngle add(IAngle angle);
	
	/**
	 * Gets the angle that is at the opposite of the circle from this angle.
	 * (i.e. this + PI radians)
	 * 
	 * @return
	 */
	public IAngle opposite();
	
	/**
	 * Gets the signum of the value of this angle. This may not have the same
	 * result whether the angle is in an angle measure, or in an indirect
	 * measure like cos/sin.
	 * 
	 * @return
	 */
	public Signum getSignum();
	
}
