package org.cen.components.math.angle;

import org.cen.components.math.Signum;

/**
 * Class representing an angle, measured in deci-degrees.
 * This angle is NOT  in range ]-1800;1800]
 * 
 * @author Anastaszor
 */
public class AngleDecidegree implements IAngle 
{
	
	/**
	 * The given value of this angle. (in decidegrees)
	 */
	private Double value = null;
	
	/**
	 * Generic constructor that wraps its value
	 * 
	 * @param value
	 */
	public AngleDecidegree(Double value)
	{
		this.value = value;
	}
	
	@Override
	public Double getValue()
	{
		return value;
	}
	
	@Override
	public AngleRadian toRadians()
	{
		return this.toDegrees().toRadians();
	}
	
	@Override
	public AngleDegree toDegrees()
	{
		return new AngleDegree(this.getValue() / 10);
	}
	
	@Override
	public AngleDecidegree toDecidegrees()
	{
		return this;
	}
	
	@Override
	public AngleCosinus toCosinus()
	{
		return this.toDegrees().toCosinus();
	}
	
	@Override
	public AngleSinus toSinus()
	{
		return this.toDegrees().toSinus();
	}
	
	@Override
	public IAngle getRotationAngle(IAngle angle)
	{
		return new AngleDecidegree(this.getValue() - angle.toDecidegrees().getValue());
	}
	
	@Override
	public IAngle add(IAngle angle)
	{
		return new AngleDecidegree(this.getValue() + angle.toDecidegrees().getValue());
	}
	
	@Override
	public IAngle opposite()
	{
		return new AngleDecidegree(this.getValue() + 1800d);
	}
	
	@Override
	public Signum getSignum()
	{
		return Signum.eval(this.getValue());
	}
	
	@Override
	public String toString()
	{
		return String.format("%d d°", getValue());
	}
	
}
