package org.cen.components.math.position;

import java.awt.geom.Point2D;

import org.cen.components.math.angle.IAngle;
import org.cen.components.math.distance.Distance2DCentimeter;
import org.cen.components.math.distance.IDistance2D;

/**
 * Class representing a position, measured in centimeters from its axes.
 * 
 * @author Anastaszor
 */
public class Position2DCentimeter implements IPosition2D
{
	
	/**
	 * The given value of the position.
	 */
	private Point2D value = null;
	
	/**
	 * Generic constructor that wraps its values.
	 * 
	 * @param x the distance to the X axis.
	 * @param y the distance to the Y axis.
	 */
	public Position2DCentimeter(Double x, Double y)
	{
		this(new Point2D.Double(x, y));
	}
	
	/**
	 * Generic constructor that wraps its values.
	 * 
	 * @param position the distance from the axes' origin.
	 */
	public Position2DCentimeter(Point2D position)
	{
		value = position;
	}

	@Override
	public Point2D getValue()
	{
		return value;
	}
	
	@Override
	public Position2DMillimeter toMillimeters()
	{
		return new Position2DMillimeter(
			new Point2D.Double(this.getValue().getX() * 10, this.getValue().getY() * 10)
		);
	}
	
	@Override
	public Position2DCentimeter toCentimeters()
	{
		return this;
	}
	
	@Override
	public IDistance2D distance(IPosition2D position)
	{
		return new Distance2DCentimeter(this.value.distance(position.toCentimeters().getValue()));
	}
	
	@Override
	public IPosition2D move(IDistance2D distance, IAngle angle)
	{
		Distance2DCentimeter d2c = distance.toCentimeters();
		return new Position2DCentimeter(new Point2D.Double(
			this.getValue().getX() + d2c.getValue() * angle.toCosinus().getValue(),
			this.getValue().getY() + d2c.getValue() * angle.toSinus().getValue()
		));
	}
	
	@Override
	public String toString()
	{
		return String.format("%.0f@%.0f cm", this.getValue().getX(), this.getValue().getY());
	}
	
}
