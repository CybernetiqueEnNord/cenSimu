package org.cen.components.math.position;

import java.awt.geom.Point2D;

import org.cen.components.math.angle.IAngle;
import org.cen.components.math.distance.IDistance2D;

/**
 * IPosition2D generic interface. This interface is for all mesurable 2D positions.
 * 
 * @author Anastaszor
 */
public interface IPosition2D
{
	
	/**
	 * Gets the value of this position, in absolute coordinates, in current axes,
	 * in its own units.
	 * 
	 * @return the absolute position.
	 */
	public Point2D getValue();
	
	/**
	 * Converts this position to millimeters.
	 * 
	 * @return the converted position.
	 */
	public Position2DMillimeter toMillimeters();
	
	/**
	 * Converts this position to centimeters.
	 * 
	 * @return the converted position.
	 */
	public Position2DCentimeter toCentimeters();
	
	/**
	 * Gets the distance from other given position.
	 * 
	 * @param position the other position
	 * @return the distance between both positions.
	 */
	public IDistance2D distance(IPosition2D position);
	
	/**
	 * Gets the new position once this position has moved from where it is to
	 * its new position which is given distance far at given angle.
	 * 
	 * @param distance the distance to move to
	 * @param angle the angle to move to
	 * @return the new position once moved
	 */
	public IPosition2D move(IDistance2D distance, IAngle angle);
	
}
