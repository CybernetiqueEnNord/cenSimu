package org.cen.components.math.orientedPosition;

import org.cen.components.math.angle.IAngle;
import org.cen.components.math.distance.IDistance2D;
import org.cen.components.math.position.IPosition2D;

public class OrientedPosition2D implements IOrientedPosition2D
{
	private IPosition2D position;
	private IAngle orientation;
	
	public OrientedPosition2D(IPosition2D location, IAngle orientation) {
		super();
		this.position = location;
		this.orientation = orientation;
	}
	
	@Override
	public IPosition2D getPosition()
	{
		return position;
	}
	
	@Override
	public IAngle getOrientation()
	{
		return orientation;
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName()+"["+position+" "+orientation+"]";
	}
	
	@Override
	public IDistance2D distance(IOrientedPosition2D destination)
	{
		return this.getPosition().distance(destination.getPosition());
	}
	
}
