package org.cen.components.math.orientedPosition;

import org.cen.components.math.angle.IAngle;
import org.cen.components.math.distance.IDistance2D;
import org.cen.components.math.position.IPosition2D;


public interface IOrientedPosition2D
{
	
	public IPosition2D getPosition();
	
	public IAngle getOrientation();
	
	/**
	 * @deprecated inventer le concept de distance orientée
	 * @param destination
	 * @return
	 */
	public IDistance2D distance(IOrientedPosition2D destination);
	
	
}
