package org.cen.components.math;

import java.awt.geom.Point2D;

/**
 * Classe représentant une droite dans un espace 2D.
 * 
 * Une droite est représentée par l'équation caractéristique aY + bX + c = 0
 * dans le plan.
 * 
 * @author Anastaszor
 */
public class Droite2D
{
	// quand a = -1, alors Y = bX + c <=> version normalisée.
	private double a = 0;
	private double b = 0;
	private double c = 0;
	
	/**
	 * Crée une nouvelle droite d'équation Y = aX + b
	 * 
	 * @param a
	 * @param b
	 */
	public Droite2D(double a, double b)
	{
		this.a = -1;
		this.b = a;
		this.c = b;
	}
	
	/**
	 * Crée une nouvelle droite d'équation aY + bX + c = 0
	 * 
	 * @param a
	 * @param b
	 * @param c
	 */
	public Droite2D(double a, double b, double c)
	{
		if(a == 0 && b == 0)
			throw new IllegalArgumentException("Au moins un parmi a et b doit être non nul.");
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	/**
	 * Crée une nouvelle droite qui passe par point1 et de coefficient 
	 * directeur a.
	 * 
	 * @param point1
	 * @param a
	 */
	public Droite2D(Point2D point1, double a)
	{
		this.a = -1;
		this.b = a;
		this.c = point1.getY() - this.b * point1.getX();
	}
	
	/**
	 * Crée une nouvelle droite qui passe par point1 et point2
	 * 
	 * @param point1
	 * @param point2
	 */
	public Droite2D(Point2D point1, Point2D point2)
	{
		if(point1.equals(point2))
			throw new IllegalArgumentException("Point1 et Point2 doivent être différents.");
		double xa, ya, xb, yb;
		xa = point1.getX();
		ya = point1.getY();
		xb = point2.getX();
		yb = point2.getY();
		if(xa == xb)
		{
			// droite verticale
			a = 0;
			b = -1;
			c = xa;
		}
		else
		{
			// droite normale
			a = -1;
			b = (yb - ya) / (xb - xa);
			c = ya - xa * b;
		}
	}
	
	/**
	 * Renvoie l'ordonnée du point d'abscisse x, si disponible, null sinon.
	 * 
	 * @param x
	 * @return
	 */
	public Double getOrdonnee(double x)
	{
		if(this.a == 0)
			return null;
		return - (this.b * x + this.c) / this.a;
	}
	
	/**
	 * Renvoie l'abscisse du point d'ordonnée y, si disponible, null sinon.
	 * 
	 * @param y
	 * @return
	 */
	public Double getAbscisse(double y)
	{
		if(this.b == 0)
			return null;
		return - (this.a * y + this.c) / this.b;
	}
	
	/**
	 * Change le a de (aY + bX + c = 0) à -1 ou 0, selon approprié. Si a = 0,
	 * alors b est normalisé à 1.
	 */
	public void normalize()
	{
		if(a == 0)
		{
			if(b == 0)
			{
				// pas une droite... dont know what happened
			}
			else
			{
				c = - c / b;
				b = - 1;
			}
		}
		else
		{
			b = - b / a;
			c = - c / a;
			a = -1;
		}
	}
	
	/**
	 * Renvoie true si le point argument fait partie de la droite
	 * 
	 * @param point
	 * @return
	 */
	public boolean isOn(Point2D point)
	{
		return this.a * point.getY() + this.b * point.getX() + this.c == 0;
	}
	
	/**
	 * Renvoie true si la droite argument est parallèle à cette droite.
	 * 
	 * @param droite
	 * @return
	 */
	public boolean isParallel(Droite2D droite)
	{
		return this.b * droite.a == droite.b * this.a;
	}
	
	/**
	 * Renvoie true si la droite argument est perpendiculaire à cette droite.
	 * 
	 * @param droite
	 * @return
	 */
	public boolean isPerpendicular(Droite2D droite)
	{
		return droite.a * this.a + droite.b * this.b == 0;
	}
	
	/**
	 * Renvoie la droite parallèle à celle là passant par le point argument.
	 * 
	 * @param passing
	 * @return
	 */
	public Droite2D getParallel(Point2D passing)
	{
		if(this.a == 0)
		{
			return new Droite2D(0, -1, passing.getX());
		}
		else
		{
			this.normalize();
			return new Droite2D(-1, this.b, this.c + passing.getY() - this.getOrdonnee(passing.getX()));
		}
	}
	
	/**
	 * Renvoie la droite perpendiculaire à celle là passant par le point argument.
	 * 
	 * @param passing
	 * @return
	 */
	public Droite2D getPerpendicular(Point2D passing)
	{
		if(this.b == 0)
		{
			return new Droite2D(0, -1, passing.getX());
		}
		else
		{
			this.normalize();
			if(this.a == 0)
			{
				// droite verticale, pas besoin d'ajustage pour trouver la bonne horizontale
				return new Droite2D( - this.b, this.a, - passing.getY());
			}
			// droite d'ajustage, perpendiculaire passant par le même point à l'origine
			Droite2D ajust = new Droite2D(-this.b, this.a, this.c);
			double ordajust = ajust.getOrdonnee(passing.getX());
			return new Droite2D( -this.b, this.a, this.c + passing.getY() - ordajust);
		}
	}
	
	/**
	 * Renvoie le point d'intersection entre cette droite et la droit argument.
	 * Attention, renvoie null si les droites sont parallèles.
	 * 
	 * @param secante
	 * @return
	 */
	public Point2D intersect(Droite2D secante)
	{
		if(this.isParallel(secante))
			return null;
		
		this.normalize();
		secante.normalize();
		double a1, b1, a2, b2;
		a1 = this.b;
		b1 = this.c;
		a2 = secante.b;
		b2 = secante.c;
		
		if(this.a == 0)
		{
			// cette droite est verticale, l'intersection est à son abscisse
			return new Point2D.Double(
				this.c,
				secante.getOrdonnee(this.c)
			);
		}
		else if(secante.a == 0)
		{
			// l'autre droite est verticale, l'intersection est à son abscisse
			return new Point2D.Double(
				secante.c,
				this.getOrdonnee(secante.c)
			);
		}
		
		return new Point2D.Double(
			(b1 - b2) / (a2 - a1),
			a2 * (b1 - b2) / (a2 - a1) + b2
		);
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if(obj instanceof Droite2D)
		{
			Droite2D other = (Droite2D) obj;
			this.normalize();
			other.normalize();
			return this.a == other.a && this.b == other.b && this.c == other.c;
		}
		return false;
	}
	
	@Override
	public String toString()
	{
		this.normalize();
		if(a == 0)
		{
			return (-this.b)+"X = "+this.c;
		}
		else
		{
			return (-this.a)+"Y = "+this.b+"X + "+this.c;
		}
	}
	
}
