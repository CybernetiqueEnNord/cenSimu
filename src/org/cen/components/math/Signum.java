package org.cen.components.math;

/**
 * Class representing the signum of a number. This class is a wrapper of
 * the Math.signum() function.
 * 
 * @author Anastaszor
 */
public enum Signum
{
	/**
	 * A positive signum.
	 */
	POSITIVE(1.0),
	/**
	 * A negative signum.
	 */
	NEGATVE(-1.0);
	
	/**
	 * Gets the signum of argument value
	 * 
	 * @param value any number to gets the signum
	 * @return the signum of the argument
	 */
	public static Signum eval(Double value)
	{
		if(Math.signum(value) >= 0)
			return POSITIVE;
		return NEGATVE;
	}
	
	/**
	 * The inner value for the multiplying operation.
	 */
	private Double value = null;
	
	/**
	 * Generic constructor for this enum.
	 * 
	 * @param value
	 */
	private Signum(Double value)
	{
		this.value = value;
	}
	
	/**
	 * Gets the value for any multiplication operations.
	 * @return
	 */
	public Double getValue()
	{
		return this.value;
	}
	
	/**
	 * Gets the opposite signum.
	 * 
	 * @return
	 */
	public Signum opposite()
	{
		return Signum.eval(-this.getValue());
	}
	
}
