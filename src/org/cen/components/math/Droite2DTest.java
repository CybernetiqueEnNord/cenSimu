package org.cen.components.math;

import java.awt.geom.Point2D;

import junit.framework.TestCase;

public class Droite2DTest extends TestCase
{
	
	private Droite2D d2d;
	
	protected void setUp() throws Exception
	{
		// Y = X + 1
		d2d = new Droite2D(1, 1);
	}
	
	public void testPositionnement1()
	{
		assertTrue(d2d.isOn(new Point2D.Double(0, 1)));
	}
	
	public void testPositionnement2()
	{
		assertTrue(d2d.isOn(new Point2D.Double(1, 2)));
	}
	
	public void testParallel1()
	{
		assertTrue(d2d.isParallel(new Droite2D(1,1)));
	}
	
	public void testParallel2()
	{
		// Y = 2X + 2;
		assertFalse(d2d.isParallel(new Droite2D(2, 2)));
	}
	
	public void testParallel3()
	{
		// 2Y = 2X + 2;
		assertTrue(d2d.isParallel(new Droite2D(-2,2,2)));
	}
	
	public void testPerpendicular1()
	{
		assertTrue(d2d.isPerpendicular(new Droite2D(-1, 1)));
	}
	
	public void testPerpendicular2()
	{
		assertFalse(d2d.isPerpendicular(new Droite2D(2, 2)));
	}
	
	public void testGetParallel1()
	{
		// parallèle à Y = X + 1 passant par (2, 2) => Y = X + 0
		assertEquals(new Droite2D(1, 0), d2d.getParallel(new Point2D.Double(2, 2)));
	}
	
	public void testGetParallel2()
	{
		// parallèle à Y = X + 1 passant par (5, 4) => Y = X - 1
		assertEquals(new Droite2D(1, -1), d2d.getParallel(new Point2D.Double(5, 4)));
	}
	
	public void testGetIsParallel1()
	{
		assertTrue(d2d.isParallel(d2d.getParallel(new Point2D.Double(2, 2))));
	}
	
	public void testGetIsParallel2()
	{
		assertTrue(d2d.isParallel(d2d.getParallel(new Point2D.Double(5, 4))));
	}
	
	public void testGetPerpendicular1()
	{
		// perpendiculaire à Y = X + 1 passant par (2, 2) => Y = -X + 4
		assertEquals(new Droite2D(-1, 4), d2d.getPerpendicular(new Point2D.Double(2, 2)));
	}
	
	public void testGetPerpendicular2()
	{
		// perpendiculaire à Y = X + 1 passant par (5, 4) => Y = -X + 9
		assertEquals(new Droite2D(-1, 9), d2d.getPerpendicular(new Point2D.Double(5, 4)));
	}
	
	public void testGetPerpendicular3()
	{
		d2d = new Droite2D(-1, 0, 6);	// Y + 6 = 0  _|_ (5, 4) => X = 5
		assertEquals(new Droite2D(0, 1, -5), d2d.getPerpendicular(new Point2D.Double(5, 4)));
	}
	
	public void testGetIsPerpendicular1()
	{
		assertTrue(d2d.isPerpendicular(d2d.getPerpendicular(new Point2D.Double(2, 2))));
	}
	
	public void testGetIsPerpendicular2()
	{
		assertTrue(d2d.isPerpendicular(d2d.getPerpendicular(new Point2D.Double(5, 4))));
	}
	
}
