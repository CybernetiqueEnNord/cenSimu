package org.cen.components.math.distance;

import org.cen.components.math.angle.IAngle;
import org.cen.components.math.position.IPosition2D;


/**
 * IDistance2D generic interface. This interface is for all mesurable 2D distances.
 * 
 * @author Anastaszor
 */
public interface IDistance2D
{
	
	/**
	 * Gets the value of the distance, in the given unit.
	 * 
	 * @return Double the value of the distance in its unit.
	 */
	public Double getValue();
	
	/**
	 * Converts this distance to millimeters.
	 * 
	 * @return the converted distance.
	 */
	public Distance2DMillimeter toMillimeters();
	
	/**
	 * Converts this distance to centimeters.
	 * 
	 * @return the converted distance.
	 */
	public Distance2DCentimeter toCentimeters();
	
	/**
	 * Gets the position where which you go when you travel this distance from
	 * given source position and given direction.
	 * 
	 * @param source the starting position
	 * @param direction the direction where to travel
	 * @return the new position
	 */
	public IPosition2D getDestination(IPosition2D source, IAngle direction);
	
}
