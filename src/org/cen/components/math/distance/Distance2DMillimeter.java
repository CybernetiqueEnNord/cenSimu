package org.cen.components.math.distance;

import java.awt.geom.Point2D;

import org.cen.components.math.angle.IAngle;
import org.cen.components.math.position.IPosition2D;
import org.cen.components.math.position.Position2DMillimeter;

/**
 * Class representing a distance 2D, measured in millimeters.
 * 
 * @author Anastaszor
 */
public class Distance2DMillimeter implements IDistance2D
{
	
	/**
	 * The current value of the distance, in millimeters.
	 */
	private Double value = null;
	
	/**
	 * Generic constructor that wraps its value.
	 * 
	 * @param distance the distance, in millimeters.
	 */
	public Distance2DMillimeter(Double distance)
	{
		value = distance;
	}
	
	@Override
	public Double getValue()
	{
		return value;
	}
	
	@Override
	public Distance2DMillimeter toMillimeters()
	{
		return this;
	}
	
	@Override
	public Distance2DCentimeter toCentimeters()
	{
		return new Distance2DCentimeter(this.value / 10);
	}
	
	@Override
	public IPosition2D getDestination(IPosition2D source, IAngle direction)
	{
		Position2DMillimeter smm = source.toMillimeters();
		double x = smm.getValue().getX() + direction.toCosinus().getValue() * this.value;
		double y = smm.getValue().getY() + direction.toSinus().getValue() * this.value;
		return new Position2DMillimeter(new Point2D.Double(x, y));
	}
	
	@Override
	public String toString()
	{
		return String.format("%.2f mm", value);
	}
	
}
