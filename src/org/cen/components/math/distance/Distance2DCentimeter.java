package org.cen.components.math.distance;

import java.awt.geom.Point2D;

import org.cen.components.math.angle.IAngle;
import org.cen.components.math.position.IPosition2D;
import org.cen.components.math.position.Position2DCentimeter;

/**
 * Class representing a distance 2D, measured in centimeters.
 * 
 * @author Anastaszor
 */
public class Distance2DCentimeter implements IDistance2D
{
	/**
	 * The current value of the distance, in centimeters.
	 */
	private Double value = null;
	
	/**
	 * Generic constructor that wraps its value.
	 * 
	 * @param value the distance, in centimeters.
	 */
	public Distance2DCentimeter(double value)
	{
		this.value = value;
	}
	
	@Override
	public Double getValue()
	{
		return value;
	}
	
	@Override
	public Distance2DMillimeter toMillimeters()
	{
		return new Distance2DMillimeter(this.value * 10);
	}
	
	@Override
	public Distance2DCentimeter toCentimeters()
	{
		return this;
	}
	
	@Override
	public IPosition2D getDestination(IPosition2D source, IAngle direction)
	{
		Position2DCentimeter cmm = source.toCentimeters();
		double x = cmm.getValue().getX() + direction.toCosinus().getValue() * this.value;
		double y = cmm.getValue().getY() + direction.toSinus().getValue() * this.value;
		return new Position2DCentimeter(new Point2D.Double(x, y));
	}
	
	@Override
	public String toString()
	{
		return String.format("%.2f cm", getValue());
	}
	
}
