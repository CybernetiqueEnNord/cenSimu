package org.cen.components.math.time;

/**
 * IDelay generic interface. This interface is for all mesurable time intervals.
 * 
 * @author Anastaszor
 */
public interface IDelay
{
	
	/**
	 * Gets the value of the delay, in given unit.
	 * 
	 * @return Double the value of the delay, in its unit.
	 */
	public Double getValue();
	
	/**
	 * Converts the delay to seconds.
	 * 
	 * @return the converted delay.
	 */
	public DelaySecond toSeconds();
	
	/**
	 * Converts the delay to milliseconds.
	 * 
	 * @return the converted delay.
	 */
	public DelayMillisecond toMilliseconds();
	
}
