package org.cen.components.math.time;

/**
 * Class representing a delay, measured in seconds.
 * 
 * @author Anastaszor
 */
public class DelaySecond implements IDelay
{
	/**
	 * The current value of the delay, in seconds.
	 */
	private Double value = null;
	
	/**
	 * Generic constructor that wraps its value.
	 * 
	 * @param delay
	 */
	public DelaySecond(Double delay)
	{
		value = delay;
	}
	
	@Override
	public Double getValue()
	{
		return value;
	}
	
	@Override
	public DelaySecond toSeconds()
	{
		return this;
	}
	
	@Override
	public DelayMillisecond toMilliseconds()
	{
		return new DelayMillisecond(this.getValue() * 1000d);
	}
	
	@Override
	public String toString()
	{
		return String.format("%.2f s", value);
	}
	
}
