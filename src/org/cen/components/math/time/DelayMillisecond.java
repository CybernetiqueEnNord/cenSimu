package org.cen.components.math.time;

/**
 * Class representing a delay, measured in milliseconds.
 * 
 * @author Anastaszor
 */
public class DelayMillisecond implements IDelay
{
	
	/**
	 * The current value of the delay, in milliseconds.
	 */
	private Double value = null;
	
	/**
	 * Generic constructor that wraps its value.
	 * 
	 * @param delay
	 */
	public DelayMillisecond(Double delay)
	{
		value = delay;
	}
	
	@Override
	public Double getValue()
	{
		return value;
	}
	
	@Override
	public DelaySecond toSeconds()
	{
		return new DelaySecond(this.getValue() / 1000d);
	}
	
	@Override
	public DelayMillisecond toMilliseconds()
	{
		return this;
	}
	
	@Override
	public String toString()
	{
		return String.format("%.0f ms", getValue());
	}
	
}
