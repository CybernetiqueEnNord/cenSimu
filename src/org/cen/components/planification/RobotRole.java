package org.cen.components.planification;

public enum RobotRole
{
	GRAND_ROBOT("Grand Robot"),
	PETIT_ROBOT("Petit Robot"),
	;
	
	private String label = null;
	
	private RobotRole(String label)
	{
		this.label = label;
	}
	
	public String getLabel()
	{
		return label;
	}
	
}
