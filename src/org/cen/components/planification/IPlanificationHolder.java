package org.cen.components.planification;

import org.cen.components.gauge.IGauge;
import org.cen.components.order.IOrderList;

public interface IPlanificationHolder
{
	
	public void setGauge(RobotRole role, IGauge gauge);
	
	public void setTrajectory(RobotRole role, IOrderList trajectory);
	
	public IPlanification getValidPlanification(RobotRole role);
	
}
