package org.cen.components.planification;

import java.util.HashMap;
import java.util.Map;

import org.cen.components.gauge.IGauge;
import org.cen.components.order.IOrderList;

public class PlanificationHolder implements IPlanificationHolder
{
	
	private Map<RobotRole, IPlanification> planifications = new HashMap<>();
	
	@Override
	public IPlanification getValidPlanification(RobotRole role)
	{
		IPlanification planification = planifications.get(role);
		if(null == planification)
			return null;
		if(!planification.isValid())
			return null;
		
		return planification;
	}
	
	@Override
	public void setGauge(RobotRole role, IGauge gauge)
	{
		IPlanification planification = planifications.get(role);
		if(null == planification)
		{
			planification = new Planification();
			planifications.put(role, planification);
		}
		planification.setGauge(gauge);
	}
	
	@Override
	public void setTrajectory(RobotRole role, IOrderList trajectory)
	{
		IPlanification planification = planifications.get(role);
		if(null == planification)
		{
			planification = new Planification();
			planifications.put(role, planification);
		}
		planification.setTrajectory(trajectory);
	}
	
}
