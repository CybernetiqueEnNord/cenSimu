package org.cen.components.planification;

import org.cen.components.gauge.IGauge;
import org.cen.components.order.IOrderList;

public interface IPlanification
{
	
	public Boolean isValid();
	
	public void setGauge(IGauge gauge);
	
	public void setTrajectory(IOrderList trajectory);
	
	public IGauge getGauge();
	
	public IOrderList getTrajectory();
	
}
