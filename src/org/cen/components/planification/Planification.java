package org.cen.components.planification;

import org.cen.components.gauge.IGauge;
import org.cen.components.order.IOrderList;

public class Planification implements IPlanification
{
	
	private IOrderList trajectory = null;
	
	private IGauge gauge = null;
	
	@Override
	public Boolean isValid()
	{
		return null != trajectory && null != gauge;
	}
	
	@Override
	public void setGauge(IGauge gauge)
	{
		this.gauge = gauge;
	}
	
	@Override
	public void setTrajectory(IOrderList trajectory)
	{
		this.trajectory = trajectory;
	}
	
	@Override
	public IGauge getGauge()
	{
		return gauge;
	}
	
	@Override
	public IOrderList getTrajectory()
	{
		return trajectory;
	}
	
}
