package org.cen.components.order;

import org.cen.components.io.export.order.ExportException;
import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.math.distance.IDistance2D;
import org.cen.components.trajectories.KeyFrameCollection;

public class RecalageOrder implements IOrder {

	private IDistance2D distance = null;
	
	public RecalageOrder(IDistance2D distance)
	{
		this.distance = distance;
	}
	
	@Override
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection)
	{
		// TODO pas d'interpolation
	}
	
	@Override
	public String exportTo(IOrderExporter exporter) throws ExportException
	{
		return exporter.exportRecalageOrder(this);
	}
	
	public IDistance2D getDistance()
	{
		return distance;
	}
	
}
