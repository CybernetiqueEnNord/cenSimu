package org.cen.components.order;

public class OrderMetadata
{
	
	private Integer line;
	
	private String commandOrder = "";
	
	private String commandValue = "";
	
	public Integer getLine()
	{
		return line;
	}
	
	public void setLine(Integer line)
	{
		this.line = line;
	}
	
	public String getCommandOrder()
	{
		return commandOrder;
	}
	
	public void setCommandOrder(String commandOrder)
	{
		this.commandOrder = commandOrder;
	}
	
	public String getCommandValue()
	{
		return commandValue;
	}
	
	public void setCommandValue(String commandValue)
	{
		this.commandValue = commandValue;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("// ").append("line ").append(line).append("\n");
		sb.append(commandOrder).append(";").append(commandValue).append("\n");
		return sb.toString();
	}
	
}
