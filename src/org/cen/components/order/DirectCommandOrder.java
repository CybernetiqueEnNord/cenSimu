package org.cen.components.order;

import org.cen.components.io.export.order.ExportException;
import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.trajectories.KeyFrameCollection;

public class DirectCommandOrder implements IOrder
{
	private String commandCline = null;
	
	public DirectCommandOrder(String nextLine)
	{
		commandCline = nextLine;
	}
	
	@Override
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection)
	{
		// nothing to do, directs orders needs no instructions
	}
	
	@Override
	public String exportTo(IOrderExporter exporter) throws ExportException
	{
		return exporter.exportDirectCommandOrder(this);
	}
	
	public String getCommandCline()
	{
		return commandCline;
	}
	
}
