package org.cen.components.order;

import org.cen.components.io.export.order.ExportException;
import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.trajectories.KeyFrameCollection;


public class TransformOrder implements IOrder
{
	
	public TransformOrder(double xScale, double xTranslation, double yScale,
			double yTranslation, double angleScale, double angleTranslation)
	{
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection)
	{
		keyFrameCollection.populateTransform(this);
	}
	
	@Override
	public String exportTo(IOrderExporter exporter) throws ExportException
	{
		return exporter.exportTransformOrder(this);
	}
	
}
