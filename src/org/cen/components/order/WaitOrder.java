package org.cen.components.order;

import org.cen.components.io.export.order.ExportException;
import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.math.time.IDelay;
import org.cen.components.trajectories.KeyFrameCollection;


public class WaitOrder implements IOrder
{
	
	private IDelay delay = null;
	/**
	 * 
	 * @deprecated
	 */
	private Integer sourceLine = null;
	
	public WaitOrder(IDelay delay, int sourceLine)
	{
		this.delay = delay;
		this.sourceLine = sourceLine;
	}
	
	@Override
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection)
	{
		keyFrameCollection.populateWait(this);
	}
	
	public IDelay getDelay()
	{
		return delay;
	}
	
	public Integer getSourceLine()
	{
		return sourceLine;
	}
	
	@Override
	public String exportTo(IOrderExporter exporter) throws ExportException
	{
		return exporter.exportWaitOrder(this);
	}
	
}
