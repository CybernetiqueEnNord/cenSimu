package org.cen.components.order;

import org.cen.components.io.export.order.ExportException;
import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.math.distance.IDistance2D;
import org.cen.components.trajectories.KeyFrameCollection;


public class DistanceOrder implements IOrder
{
	
	private IDistance2D distance = null;
	/**
	 * 
	 * @deprecated
	 */
	private Integer sourceLine = null;
	
	public DistanceOrder(IDistance2D distance, int sourceLine)
	{
		this.distance = distance;
		this.sourceLine = sourceLine;
	}
	
	@Override
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection)
	{
		keyFrameCollection.populateDistance(this);
	}
	
	public int getSourceLine()
	{
		return sourceLine;
	}
	
	public IDistance2D getDistance()
	{
		return distance;
	}
	
	@Override
	public String exportTo(IOrderExporter exporter) throws ExportException
	{
		return exporter.exportDistanceOrder(this);
	}
	
}
