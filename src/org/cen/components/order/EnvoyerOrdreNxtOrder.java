package org.cen.components.order;

import org.cen.components.io.export.order.ExportException;
import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.nxt.IOrdreNxt;
import org.cen.components.trajectories.KeyFrameCollection;

public class EnvoyerOrdreNxtOrder implements IOrder
{
	
	private IOrdreNxt ordre = null;
	
	public EnvoyerOrdreNxtOrder(IOrdreNxt ordreNxt)
	{
		this.ordre = ordreNxt;
	}
	
	@Override
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection)
	{
		// nothing to do, it takes no time to send an order to nxt
	}
	
	@Override
	public String exportTo(IOrderExporter exporter) throws ExportException
	{
		return exporter.exportEnvoyerOrdreNxtOrder(this);
	}
	
	public IOrdreNxt getOrdre()
	{
		return ordre;
	}
	
}
