package org.cen.components.order;

import org.cen.components.io.export.order.ExportException;
import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.position.IPosition2D;
import org.cen.components.trajectories.KeyFrameCollection;


public class InitialPositionOrder implements IOrder
{
	
	private IPosition2D initialPosition = null;
	private IAngle initialAngle = null;
	/**
	 * 
	 * @deprecated
	 */
	private Double defaultLinearSpeed = null;
	/**
	 * 
	 * @deprecated
	 */
	private Double defaultAngularSpeed = null;
	/**
	 * 
	 * @deprecated
	 */
	private Double initialTimestamp = null;
	/**
	 * 
	 * @deprecated
	 */
	private Integer sourceLine = null;
	
	public InitialPositionOrder(IPosition2D p, 
			IAngle initialAngle,
			double defaultLinearSpeed, 
			double defaultRotationSpeed, 
			double timestamp, 
			int sourceLine
	) {
		this.initialPosition = p;
		this.initialAngle = initialAngle;
		this.defaultLinearSpeed = defaultLinearSpeed;
		this.defaultAngularSpeed = defaultRotationSpeed;
		this.initialTimestamp = timestamp;
		this.sourceLine = sourceLine;
	}
	
	@Override
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection)
	{
		keyFrameCollection.populateInitial(this);
	}
	
	public IPosition2D getPosition()
	{
		return initialPosition;
	}
	
	public IAngle getInitialAngle()
	{
		return initialAngle;
	}
	
	public Double getDefaultLinearSpeed()
	{
		return defaultLinearSpeed;
	}
	
	public Double getDefaultRotationSpeed()
	{
		return defaultAngularSpeed;
	}
	
	public Double getTimestamp()
	{
		return initialTimestamp;
	}
	
	public Integer getSourceLine()
	{
		return sourceLine;
	}
	
	@Override
	public String toString()
	{
		return super.toString();
	}
	
	@Override
	public String exportTo(IOrderExporter exporter) throws ExportException
	{
		return exporter.exportInitialPositionOrder(this);
	}
	
}
