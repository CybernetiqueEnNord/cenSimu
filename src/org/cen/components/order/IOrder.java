package org.cen.components.order;

import org.cen.components.io.export.order.ExportException;
import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.trajectories.KeyFrameCollection;

/**
 * An individual order that came out of a trajectory file.
 * 
 * @author Anastaszor
 */
public interface IOrder
{
	
	/**
	 * Adds all the relevant keyframes to the collection.
	 * 
	 * @param keyFrameCollection the collection to fulfill
	 */
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection);
	
	/**
	 * Exports the order to a specific exporter.
	 * 
	 * @param exporter the exporter to exports to
	 * @return the string exported
	 */
	public String exportTo(IOrderExporter exporter) throws ExportException;
	
}
