package org.cen.components.order;

import java.awt.geom.Point2D;

import org.cen.components.io.export.order.ExportException;
import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.angle.IAngle;
import org.cen.components.math.position.Position2DMillimeter;
import org.cen.components.trajectories.KeyFrameCollection;


public class BezierQuadOrder implements IOrder
{
	
	public BezierQuadOrder(AngleRadian angleRadian,
			Position2DMillimeter position2dMillimeter,
			Position2DMillimeter position2dMillimeter2)
	{
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection)
	{
		keyFrameCollection.populateBezierQuad(this);
	}

	public IAngle getEndAngle()
	{
		// TODO Auto-generated method stub
		return null;
	}

	public Point2D getFinalPoint()
	{
		// TODO Auto-generated method stub
		return null;
	}

	public Point2D getFirstControlPoint()
	{
		// TODO Auto-generated method stub
		return null;
	}

	public Point2D getSecondControlPoint()
	{
		// TODO Auto-generated method stub
		return null;
	}

	public int getSourceLine()
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public String exportTo(IOrderExporter exporter) throws ExportException
	{
		return exporter.exportBezierQuadOrder(this);
	}
	
}
