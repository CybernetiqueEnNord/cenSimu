package org.cen.components.order;

import org.cen.components.io.export.order.ExportException;
import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.speed.SpeedMode;
import org.cen.components.trajectories.KeyFrameCollection;

public class ChangeSpeedModeOrder implements IOrder
{
	
	private SpeedMode mode = null;
	
	public ChangeSpeedModeOrder(SpeedMode sp)
	{
		this.mode = sp;
	}
	
	public SpeedMode getMode()
	{
		return mode;
	}
	
	@Override
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection)
	{
		// nothing to do, it takes no time to change speed
	}
	
	@Override
	public String exportTo(IOrderExporter exporter) throws ExportException
	{
		return exporter.exportChangeSpeedModeOrder(this);
	}
	
}
