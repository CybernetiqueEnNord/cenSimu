package org.cen.components.order;

import org.cen.components.trajectories.ITrajectoryPath;

/**
 * The list of orders that comes out of a trajectory file.
 * 
 * @author Anastaszor
 */
public interface IOrderList extends Iterable<IOrder>
{
	
	public void add(IOrder part);
	
	public void append(IOrderList list);
	
	public ITrajectoryPath getTrajectoryPath();
	
}
