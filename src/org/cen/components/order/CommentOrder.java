package org.cen.components.order;

import org.cen.components.io.export.order.ExportException;
import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.trajectories.KeyFrameCollection;


public class CommentOrder implements IOrder
{
	
	private String line = null;
	
	public CommentOrder(String line)
	{
		this.line = line;
	}
	
	@Override
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection)
	{
		keyFrameCollection.populateComment(this);
	}
	
	public String getLine()
	{
		return line;
	}
	
	@Override
	public String toString()
	{
		return getClass()+"["+line+"]";
	}
	
	@Override
	public String exportTo(IOrderExporter exporter) throws ExportException
	{
		return exporter.exportCommentOrder(this);
	}
	
}
