package org.cen.components.order;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.cen.components.gameboard.generic.StraightLine;
import org.cen.components.math.angle.AngleRadian;
import org.cen.components.trajectories.ITrajectoryPath;
import org.cen.components.trajectories.KeyFrameCollection;

public class OrderList implements IOrderList
{
	
	private List<IOrder> orderlist = null;
	
	
	public OrderList()
	{
		orderlist = new ArrayList<IOrder>();
	}
	
	@Override
	public Iterator<IOrder> iterator()
	{
		return orderlist.iterator();
	}
	
	@Override
	public void add(IOrder part)
	{
		orderlist.add(part);
	}
	
	@Override
	public void append(IOrderList list)
	{
		for(IOrder order:list)
		{
			orderlist.add(order);
		}
	}
	
	@Override
	public ITrajectoryPath getTrajectoryPath()
	{
		KeyFrameCollection frames = new KeyFrameCollection();
		for(IOrder order:orderlist)
		{
			frames.populateWith(order);
		}
		return new StraightLine("path", new AngleRadian(0d), new AngleRadian(0d), frames);
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder(getClass().getName());
		sb.append('[');
		sb.append(orderlist);
		sb.append(']');
		return sb.toString();
	}
	
}
