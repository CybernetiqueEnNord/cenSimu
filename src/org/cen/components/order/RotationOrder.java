package org.cen.components.order;

import org.cen.components.io.export.order.ExportException;
import org.cen.components.io.export.order.IOrderExporter;
import org.cen.components.math.angle.IAngle;
import org.cen.components.trajectories.KeyFrameCollection;


public class RotationOrder implements IOrder
{
	
	private IAngle rotationAngle = null;
	/**
	 * 
	 * @deprecated
	 */
	private Integer sourceLine = null;
	
	public RotationOrder(IAngle angle, Integer sourceLine)
	{
		this.rotationAngle = angle;
		this.sourceLine = sourceLine;
	}
	
	@Override
	public void populateKeyFrameCollection(KeyFrameCollection keyFrameCollection)
	{
		keyFrameCollection.populateRotation(this);
	}
	
	public IAngle getRotationAngle()
	{
		return rotationAngle;
	}
	
	public int getSourceLine()
	{
		return sourceLine;
	}
	
	@Override
	public String exportTo(IOrderExporter exporter) throws ExportException
	{
		return exporter.exportRotationOrder(this);
	}
	
}
