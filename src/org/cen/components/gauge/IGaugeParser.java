package org.cen.components.gauge;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;

public interface IGaugeParser
{
	
	public IGauge parseStream(BufferedReader reader) throws IOException, ParseException;
	
}
