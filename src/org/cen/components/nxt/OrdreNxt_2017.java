package org.cen.components.nxt;

public enum OrdreNxt_2017 implements IOrdreNxt
{
	START(1),
	ARRET_URGENCE(15),
	;
	
	private Integer number = null;
	
	private OrdreNxt_2017(Integer number)
	{
		this.number = number;
	}
	
	@Override
	public Integer getOrdreNumber()
	{
		return number;
	}
	
	@Override
	public String getOrdreName()
	{
		return this.name();
	}
	
}
