package org.cen.components.nxt;

public enum OrdreNxt2018 implements IOrdreNxt
{
	START(1),
	TOURNER_BARILLET_SENS_TRIGO(2),
	TOURNER_BARILLET_SENS_TEMPO(3),
	TOURNER_TRANCHE_BASSE_SENS_TRIGO(4),
	TOURNER_TRANCHE_BASSE_SENS_TEMPO(5),
	ARRET_URGENCE(15),
	;
	
	private Integer number = null;
	
	private OrdreNxt2018(Integer number)
	{
		this.number = number;
	}
	
	@Override
	public Integer getOrdreNumber()
	{
		return number;
	}
	
	@Override
	public String getOrdreName()
	{
		return this.name();
	}
	
}
