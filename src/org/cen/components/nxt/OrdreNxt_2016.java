package org.cen.components.nxt;

public enum OrdreNxt_2016 implements IOrdreNxt
{
	START(1),
	ABAISSER_BRAS_DOUBLE(2),
	RELEVER_BRAS_DOUBLE(3),
	ABAISSER_BRAS_POISSON(6),
	PRENDRE_BRAS_POISSON(7),
	LACHER_BRAS_POISSON(8),
	ALERT_GREEN(10),
	ALERT_RED(11),
	ARRET_URGENCE(15),
	;
	
	private Integer number = null;
	
	private OrdreNxt_2016(Integer number)
	{
		this.number = number;
	}
	
	@Override
	public Integer getOrdreNumber()
	{
		return number;
	}
	
	@Override
	public String getOrdreName()
	{
		return this.name();
	}
	
}
