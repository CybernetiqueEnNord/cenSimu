package org.cen.components.nxt;

public interface IOrdreNxt
{
	public String getOrdreName();
	
	public Integer getOrdreNumber();
}
