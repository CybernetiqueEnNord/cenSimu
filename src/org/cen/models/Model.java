package org.cen.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Model class file.
 * 
 * Base class for all models that should be validated and filled by a form.
 * 
 * @author Anastaszor <anastaszor@yahoo.fr>
 */
public abstract class Model
{
	
	/**
	 * All the errors that occured when the model is validated.
	 */
	private Map<String, List<String>> errors = null;
	
	/**
	 * Gets all the errors that are currently on this model.
	 * @return Map<String, List<String>>
	 */
	private Map<String, List<String>> getAllErrors()
	{
		if(errors == null)
			errors = new HashMap<>();
		return errors;
	}
	
	/**
	 * Gets all errors relative to given attribute. If given attribute is 
	 * null, errors from all attributes are given.
	 * @param attribute
	 * @return
	 */
	public List<String> getErrors(String attribute)
	{
		if(attribute == null)
		{
			List<String> list = new ArrayList<String>();
			for(List<String> lstr:getAllErrors().values())
			{
				list.addAll(lstr);
			}
			return list;
		}
		List<String> list = getAllErrors().get(attribute);
		if(list == null)
		{
			list = new ArrayList<>();
			getAllErrors().put(attribute, list);
		}
		return list;
	}
	
	/**
	 * Adds an error for given attribute.
	 * @param attribute
	 * @param message
	 */
	public void addError(String attribute, String message)
	{
		getErrors(attribute).add(message);
	}
	
	/**
	 * True if this has an error for given attribute. If given attribute is 
	 * null, true if this has an error for any attribute.
	 * @param attribute
	 * @return
	 */
	public boolean hasErrors(String attribute)
	{
		if(attribute == null)
		{
			int count = 0;
			for(List<String> errlist:getAllErrors().values())
			{
				count += errlist.size();
			}
			return count > 0;
		}
		List<String> errors = getErrors(attribute);
		return errors.size() > 0;
	}
	
	/**
	 * Removes all errors from this model.
	 * @param attribute
	 */
	public void clearErrors(String attribute)
	{
		if(attribute == null)
			getAllErrors().clear();
		else
			getErrors(attribute).clear();
	}
	
	/**
	 * Passthru all validators for this model according to the rules.
	 * @return
	 */
	public boolean validate()
	{
		return !hasErrors(null);
	}
	
	/**
	 * Gets the label of given attribute. If the label does not exists, then
	 * the attribute is used as a label.
	 * @param attribute
	 * @return
	 */
	public String getAttributeLabel(String attribute)
	{
		String label = getAttributeLabels().get(attribute);
		if(label == null)
			return attribute;
		return label;
	}
	
	/**
	 * Gets the dictionnary that maps the string attribute of this model to
	 * the internationalized label of the field.
	 * Such a method should be overridden by children classes.
	 * @return
	 */
	public Map<String, String> getAttributeLabels()
	{
		return new HashMap<String, String>();
	}
	
	/**
	 * Gets the string that should be displayed before the field for given 
	 * attribute. If the field does not exists, then an empty string is
	 * returned.
	 * @param attribute
	 * @return
	 */
	public String getBeforeField(String attribute)
	{
		String before = getBeforeFields().get(attribute);
		if(before == null)
			return "";
		return before;
	}
	
	/**
	 * Gets the dictionnary that maps the string attribute of this model to
	 * the internationalized pre-field indicator.
	 * Such a method should be overridden by children classes.
	 * @return
	 */
	public Map<String, String> getBeforeFields()
	{
		return new HashMap<String, String>();
	}
	
	/**
	 * Gets the string that should be displayed after the field for given
	 * attribute. If the field does not exists, then an empty string is
	 * returned.
	 * @param attribute
	 * @return
	 */
	public String getAfterField(String attribute)
	{
		String after = getAfterFields().get(attribute);
		if(after == null)
			return "";
		return after;
	}
	
	/**
	 * Gets the dictionnary that maps the string attribute of this model to
	 * the internationalized post-field indicator.
	 * Such a method should be overridden by children classes.
	 * @return
	 */
	public Map<String, String> getAfterFields()
	{
		return new HashMap<String, String>();
	}
	
}
