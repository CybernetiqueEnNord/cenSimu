package org.cen.models.actuators;

import java.awt.geom.Point2D;

import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.orientedPosition.OrientedPosition2D;
import org.cen.components.math.position.Position2DMillimeter;
import org.cen.models.IModel;

public class ActuatorStateModel implements IModel {
	@Override
	public String toString() {
		return name;
	}

	private String name;
	private OrientedPosition2D position;

	public ActuatorStateModel() {
		this("undefined", new OrientedPosition2D(new Position2DMillimeter(new Point2D.Double(0, 0)), new AngleRadian(0.0)));
	}

	public ActuatorStateModel(String name, OrientedPosition2D position) {
		super();
		this.name = name;
		this.position = position;
	}

	public String getName() {
		return name;
	}

	public OrientedPosition2D getPosition() {
		return position;
	}
}
