package org.cen.models;

import java.awt.geom.Point2D;

import org.cen.components.math.angle.AngleRadian;
import org.cen.components.math.orientedPosition.OrientedPosition2D;
import org.cen.components.math.position.Position2DMillimeter;
import org.cen.models.actuators.ActuatorModel;
import org.cen.models.actuators.ActuatorStateModel;
import org.cen.persistance.OrientedPositionMixIn;

import com.fasterxml.jackson.databind.ObjectMapper;

public class RobotModelTest {
	public static void main(String[] args) {
		ActuatorStateModel state = new ActuatorStateModel("default", new OrientedPosition2D(new Position2DMillimeter(new Point2D.Double(0, 0)), new AngleRadian(0.0)));
		ActuatorModel actuator = new ActuatorModel("engine");
		actuator.addState(state);
		RobotModel model = new RobotModel();
		model.addActuator(actuator);

		ObjectMapper mapper = new ObjectMapper();
		mapper.addMixIn(OrientedPosition2D.class, OrientedPositionMixIn.class);
		try {
			String json = mapper.writeValueAsString(model);
			System.out.println(json);
			model = mapper.readValue(json, RobotModel.class);
			json = mapper.writeValueAsString(model);
			System.out.println(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
