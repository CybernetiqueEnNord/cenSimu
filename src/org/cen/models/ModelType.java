package org.cen.models;

public enum ModelType {
	MATCH, ROBOT, TRAJECTORY, ACTUATORS, ACTUATOR_STATE, TRAJECTORY_ELEMENT
}
