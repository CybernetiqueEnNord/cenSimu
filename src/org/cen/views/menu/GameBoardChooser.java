package org.cen.views.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;

import org.cen.components.gameboard.GameBoardConfigurationMetadata;
import org.cen.components.gameboard.GameBoardMetadata;
import org.cen.views.frames.SimulatorMainFrame;

public class GameBoardChooser extends JMenu implements ActionListener
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<AbstractButton> items = null;
	private SimulatorMainFrame smframe = null;
	
	public GameBoardChooser(SimulatorMainFrame smframe)
	{
		super("GameBoard");
		
		items = new ArrayList<>();
		this.smframe = smframe;
		
		for(GameBoardMetadata gbm:GameBoardMetadata.values())
		{
			if(gbm.getConfigurations().size() == 1)
			{
				JCheckBoxMenuItem menuitem = new JCheckBoxMenuItem(gbm.getName());
				menuitem.addActionListener(this);
				this.add(menuitem);
				items.add(menuitem);
			}
			else
			{
				JMenu menu = new JMenu(gbm.getName());
				menu.addActionListener(this);
				this.add(menu);
				items.add(menu);
				for(GameBoardConfigurationMetadata gbcm:gbm.getConfigurations())
				{
					JCheckBoxMenuItem menuitem = new JCheckBoxMenuItem(gbcm.getName());
					menuitem.addActionListener(this);
					menu.add(menuitem);
					items.add(menuitem);
				}
			}
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent event)
	{
		for(GameBoardMetadata gbm:GameBoardMetadata.values())
		{
			if(gbm.getName().equals(event.getActionCommand()))
			{
				resetTicks();
				((AbstractButton) event.getSource()).setSelected(true);
				smframe.createGameBoard(gbm.getConfigurations().get(0).getGameBoard());
			}
			else
			{
				for(GameBoardConfigurationMetadata gbcm:gbm.getConfigurations())
				{
					if(gbcm.getName().equals(event.getActionCommand()))
					{
						resetTicks();
						((AbstractButton) event.getSource()).setSelected(true);
						smframe.createGameBoard(gbcm.getGameBoard());
					}
				}
			}
		}
		smframe.updateGameBoard(true);
	}
	
	private void resetTicks()
	{
		for(AbstractButton button:items)
		{
			button.setSelected(false);
		}
	}
	
}
