package org.cen.views.menu;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import org.cen.components.planification.RobotRole;
import org.cen.controllers.main.ActionChooseGauge;
import org.cen.controllers.main.ActionChooseTrajectory;
import org.cen.controllers.main.ActionExit;
import org.cen.controllers.main.ActionExport;
import org.cen.controllers.main.ActionShowConsole;
import org.cen.controllers.main.ActionShowElementsName;
import org.cen.views.frames.SimulatorMainFrame;

public class SimulateurMenuBar extends JMenuBar
{
	
	private static final long serialVersionUID = 1L;
	
	public SimulateurMenuBar(SimulatorMainFrame smframe)
	{
		JMenu file = new JMenu("File");
		this.add(file);
		file.add(new JMenuItem(new ActionExport("Export")));
		file.add(new JMenuItem(new ActionExit("Exit")));
		
		this.add(new GameBoardChooser(smframe));
		
		for(RobotRole role:RobotRole.values())
		{
			JMenu robotMenu = new JMenu(role.getLabel());
			this.add(robotMenu);
			robotMenu.add(new JMenuItem(new ActionChooseGauge(role)));
			robotMenu.add(new JMenuItem(new ActionChooseTrajectory(role)));
		}
		
		JMenu display = new JMenu("Display");
		this.add(display);
		display.add(new JCheckBoxMenuItem(new ActionShowElementsName("Show elements Name")));
		display.add(new JCheckBoxMenuItem(new ActionShowConsole("Console")));
	}
	
}
