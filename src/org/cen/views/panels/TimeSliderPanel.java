package org.cen.views.panels;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cen.Main;

public class TimeSliderPanel extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final int MATCH_DURATION = 900;
	private static final String TXT_PLAY = "play";
	private static final String TXT_STOP = "stop";
	
	private double timestamp = 0d;
	
	public TimeSliderPanel()
	{
		this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		
		final JSlider slider = new JSlider(JSlider.HORIZONTAL, -100, MATCH_DURATION, 0);
		final JLabel timeLabel = new JLabel("-10 s");
		final JButton playButton = new JButton(TXT_PLAY);
		
		this.add(slider);
		this.add(Box.createRigidArea(new Dimension(10, 0)));
		this.add(timeLabel);
		this.add(Box.createRigidArea(new Dimension(10, 0)));
		this.add(playButton);
		Dimension size = new Dimension(616, 30);
		this.setPreferredSize(size);
		this.setSize(size);
		
		slider.addChangeListener(new ChangeListener()
		{
			
			@Override
			public void stateChanged(ChangeEvent e)
			{
				int value = slider.getValue();
				timestamp = 0.1d * value;
				timeLabel.setText(String.format("%.1f s", timestamp));
				Main.app().getSimulatorMainFrame().updateGameBoard(false);
			}
		});
		
		playButton.addActionListener(new ActionListener()
		{
			final ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);
			private volatile ScheduledFuture<?> task = null;
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				class Player implements Runnable
				{
					private int position = slider.getValue();
					private long start = System.currentTimeMillis();
					
					@Override
					public void run()
					{
						long now = System.currentTimeMillis();
						long timestamp = (now - start) / 100 + position;
						if(timestamp > MATCH_DURATION)
						{
							cancel();
						}
						slider.setValue((int) timestamp);
					}
					
					public void scheduleExecution()
					{
						task = executor.scheduleAtFixedRate(this, 0, 100, TimeUnit.MILLISECONDS);
					}
				}
				
				if(task != null)
				{
					playButton.setText(TXT_PLAY);
					cancel();
				}
				else
				{
					playButton.setText(TXT_STOP);
					new Player().scheduleExecution();
				}
			}
			
			private void cancel()
			{
				if(task == null)
				{
					return;
				}
				task.cancel(false);
				task = null;
			}
		});
	}
	
	public Double getTimestamp()
	{
		return timestamp;
	}
	
}
