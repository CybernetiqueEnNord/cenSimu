package org.cen.views.panels;

import java.awt.LayoutManager;
import java.awt.geom.Point2D;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cen.components.gameboard.generic.GameBoardMouseMoveEvent;
import org.cen.components.gameboard.generic.IGameBoardEvent;
import org.cen.components.gameboard.generic.IGameBoardEventListener;

public class CoordinatePanel extends JPanel implements IGameBoardEventListener
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JLabel statusBarLabel;
	
	public CoordinatePanel()
	{
		super();
		init();
	}
	
	public CoordinatePanel(LayoutManager layout)
	{
		super(layout);
		init();
	}
	
	public CoordinatePanel(boolean isDoubleBuffered)
	{
		super(isDoubleBuffered);
		init();
	}
	
	public CoordinatePanel(LayoutManager layout, boolean isDoubleBuffered)
	{
		super(layout, isDoubleBuffered);
		init();
	}
	
	public void init()
	{
		statusBarLabel = new JLabel();
		statusBarLabel.setHorizontalAlignment(SwingConstants.LEFT);
		this.add(statusBarLabel);
	}
	
	@Override
	public void onGameBoardEvent(IGameBoardEvent event)
	{
		if (event instanceof GameBoardMouseMoveEvent) {
			GameBoardMouseMoveEvent e = (GameBoardMouseMoveEvent) event;
			handleMouseMove(e);
		}
	}
	
	private void handleMouseMove(GameBoardMouseMoveEvent e) {
		Point2D p = e.getPosition();
		double x = p.getX();
		double y = p.getY();
		String s = String.format("x=%.0f mm ; y=%.0f mm", x, y);
		statusBarLabel.setText(s);
	}
	
}
