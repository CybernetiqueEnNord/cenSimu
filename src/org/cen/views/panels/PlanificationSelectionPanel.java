package org.cen.views.panels;

import java.awt.Dimension;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.cen.components.planification.RobotRole;
import org.cen.controllers.main.ActionChooseGauge;
import org.cen.controllers.main.ActionChooseTrajectory;
import javax.swing.JLabel;

public class PlanificationSelectionPanel extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JButton btn_grand_robot_gauge;
	private JButton btn_grand_robot_trajectory;
	private JButton btn_petit_robot_gauge;
	private JButton btn_petit_robot_trajectory;
	private JLabel lblGrandRobot;
	private JLabel lblPetitRobot;
	
	/**
	 * Create the panel.
	 */
	public PlanificationSelectionPanel()
	{
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup().addContainerGap()
										.addComponent(getBtn_grand_robot_gauge())
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(getBtn_grand_robot_trajectory()))
								.addGroup(groupLayout.createSequentialGroup().addGap(104)
										.addComponent(getLblGrandRobot())))
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup().addGap(50)
										.addComponent(getBtn_petit_robot_gauge())
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(getBtn_petit_robot_trajectory()))
								.addGroup(groupLayout.createSequentialGroup().addGap(150)
										.addComponent(getLblPetitRobot())))
						.addGap(24)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblGrandRobot()).addComponent(getLblPetitRobot()))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getBtn_grand_robot_gauge())
								.addComponent(getBtn_grand_robot_trajectory())
								.addComponent(getBtn_petit_robot_gauge())
								.addComponent(getBtn_petit_robot_trajectory()))
						.addContainerGap()));
		setLayout(groupLayout);
		Dimension size = new Dimension(616, 50);
		this.setPreferredSize(size);
		this.setSize(size);
	}
	
	private JButton getBtn_grand_robot_gauge()
	{
		if(btn_grand_robot_gauge == null)
		{
			btn_grand_robot_gauge = new JButton(RobotRole.GRAND_ROBOT.getLabel());
			btn_grand_robot_gauge.setAction(new ActionChooseGauge(RobotRole.GRAND_ROBOT));
		}
		return btn_grand_robot_gauge;
	}
	
	private JButton getBtn_grand_robot_trajectory()
	{
		if(btn_grand_robot_trajectory == null)
		{
			btn_grand_robot_trajectory = new JButton("Trajectoire");
			btn_grand_robot_trajectory.setAction(new ActionChooseTrajectory(RobotRole.GRAND_ROBOT));
		}
		return btn_grand_robot_trajectory;
	}
	
	private JButton getBtn_petit_robot_gauge()
	{
		if(btn_petit_robot_gauge == null)
		{
			btn_petit_robot_gauge = new JButton(RobotRole.PETIT_ROBOT.getLabel());
			btn_petit_robot_gauge.setAction(new ActionChooseGauge(RobotRole.PETIT_ROBOT));
		}
		return btn_petit_robot_gauge;
	}
	
	private JButton getBtn_petit_robot_trajectory()
	{
		if(btn_petit_robot_trajectory == null)
		{
			btn_petit_robot_trajectory = new JButton("Trajectoire");
			btn_petit_robot_trajectory.setAction(new ActionChooseTrajectory(RobotRole.PETIT_ROBOT));
		}
		return btn_petit_robot_trajectory;
	}
	
	private JLabel getLblGrandRobot()
	{
		if(lblGrandRobot == null)
		{
			lblGrandRobot = new JLabel(RobotRole.GRAND_ROBOT.getLabel());
		}
		return lblGrandRobot;
	}
	
	private JLabel getLblPetitRobot()
	{
		if(lblPetitRobot == null)
		{
			lblPetitRobot = new JLabel(RobotRole.PETIT_ROBOT.getLabel());
		}
		return lblPetitRobot;
	}
	
	public JButton getGaugeButton(RobotRole role)
	{
		switch (role)
		{
			case PETIT_ROBOT:
				return this.getBtn_petit_robot_gauge();
			default:
			case GRAND_ROBOT:
				return this.getBtn_grand_robot_gauge();
		}
	}
	
	public JButton getTrajectoryButton(RobotRole role)
	{
		switch (role)
		{
			case PETIT_ROBOT:
				return this.getBtn_petit_robot_trajectory();
			default:
			case GRAND_ROBOT:
				return this.getBtn_grand_robot_trajectory();
		}
	}
	
}
