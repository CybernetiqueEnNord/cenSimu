package org.cen.views.frames;

import java.awt.BorderLayout;
import java.awt.Container;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ConsoleFrame extends JFrame
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4566481860330598596L;
	
	private JTextArea consoleTextArea = null;
	
	public ConsoleFrame()
	{
		setTitle("Console");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Container c = getContentPane();
		c.setLayout(new BorderLayout(10, 10));
		
		consoleTextArea = new JTextArea(30, 80);
		JScrollPane scrollPane = new JScrollPane(consoleTextArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		c.add(scrollPane, BorderLayout.CENTER);
		
		setType(Type.UTILITY);
		setAlwaysOnTop(true);
		pack();
	}
	
	public void appendText(String text)
	{
		consoleTextArea.setText(consoleTextArea.getText()+text);
	}
	
	public void resetText()
	{
		consoleTextArea.setText("");
	}
	
	public String getText()
	{
		return consoleTextArea.getText();
	}
	
}
