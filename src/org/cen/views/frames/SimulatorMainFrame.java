package org.cen.views.frames;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;

import org.cen.Main;
import org.cen.components.gameboard.generic.GameBoardMouseMoveEvent;
import org.cen.components.gameboard.generic.GameBoardView;
import org.cen.components.gameboard.generic.IGameBoardElement;
import org.cen.components.gameboard.generic.IGameBoardEvent;
import org.cen.components.gameboard.generic.IGameBoardEventListener;
import org.cen.components.gameboard.generic.IGameBoardService;
import org.cen.components.planification.IPlanification;
import org.cen.components.planification.RobotRole;
import org.cen.components.trajectories.ITrajectoryPath;
import org.cen.views.menu.SimulateurMenuBar;
import org.cen.views.panels.PlanificationSelectionPanel;
import org.cen.views.panels.TimeSliderPanel;

public class SimulatorMainFrame extends JFrame implements IGameBoardEventListener
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3136303980110310304L;
	
	private IGameBoardService gameBoard;
	private GameBoardView gameBoardView;
	private PlanificationSelectionPanel selectionPanel;
	private JLabel statusBarLabel;
	private JPanel statusBar;
	private JPanel gameBoardPanel;
	private TimeSliderPanel timesliderPanel;
	private boolean repaintPending;
	
	// le text area où vont être affichés en temps réel les coordonnées et
	// autres mesures des robots présents.
//	private JTextArea statusTextArea;
	
	private Map<RobotRole, ITrajectoryPath> currentTrajectories = new HashMap<>();
	
	public SimulatorMainFrame()
	{
		setTitle("Simulateur de Cybernétique en Nord");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent e)
			{
				super.windowClosing(e);
				try
				{
					Main.app().getMemorySettings().save();
				}
				catch (IOException e1)
				{
					// nothing to do, comfort feature
					e1.printStackTrace();
				}
			}
		});
		
		setJMenuBar(new SimulateurMenuBar(this));
		
		createMiddlePanel(Main.app().getDefaultGameBoardConfiguration());
	}
	
	public void createMiddlePanel(IGameBoardService gameBoard)
	{
		Container c = getContentPane();
		c.setLayout(new BorderLayout(10, 10));
		c.removeAll();
		
		JPanel generalPanel = new JPanel();
		generalPanel.setLayout(new BoxLayout(generalPanel, BoxLayout.LINE_AXIS));
		
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.PAGE_AXIS));
		
		centerPanel.add(selectionPanel = new PlanificationSelectionPanel(), BorderLayout.NORTH);
		
		gameBoardPanel = new JPanel();
		gameBoardPanel.setLayout(new BoxLayout(gameBoardPanel, BoxLayout.LINE_AXIS));
		centerPanel.add(gameBoardPanel, BorderLayout.CENTER);
		centerPanel.setPreferredSize(new Dimension(800, 600));
		
		createGameBoard(gameBoard);
		
		centerPanel.add(timesliderPanel = new TimeSliderPanel(), BorderLayout.SOUTH);
		
//		addStatusBar(c);
		
		generalPanel.add(centerPanel, BorderLayout.CENTER);
		c.add(generalPanel, BorderLayout.CENTER);
		
//		JPanel statusPanel = new JPanel();
//		statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.PAGE_AXIS));
//		statusPanel.setPreferredSize(new Dimension(480, 480));
//		statusTextArea = new JTextArea("Here will be displayed all information about the robots\n"
//				+ "moving. There must be selected into the checkbox list\n"
//				+ "(hoverlined, not just checkboxed).");
//		statusPanel.add(statusTextArea);
//		generalPanel.add(statusPanel, BorderLayout.WEST);
		
		pack();
		setVisible(true);
	}
	
	public void createGameBoard(IGameBoardService gameBoard)
	{
		Main.app().setDefaultGameBoardConfiguration(gameBoard);
		gameBoardPanel.removeAll();
		this.gameBoard = gameBoard;
		gameBoardView = new GameBoardView(gameBoard);
		// gameBoardView.setPreferredSize(new Dimension(800, 700));
		gameBoardView.addGameBoardEventListener(this);
		gameBoardPanel.add(gameBoardView, BorderLayout.CENTER);
	}
	
	public IGameBoardService getGameBoard()
	{
		return gameBoard;
	}
	
	public GameBoardView getGameBoardView()
	{
		return gameBoardView;
	}
	
	public PlanificationSelectionPanel getSelectionPanel()
	{
		return selectionPanel;
	}
	
	public TimeSliderPanel getTimeSliderPanel()
	{
		return timesliderPanel;
	}
	
	@Override
	public void onGameBoardEvent(IGameBoardEvent event)
	{
		if(event instanceof GameBoardMouseMoveEvent)
		{
			GameBoardMouseMoveEvent e = (GameBoardMouseMoveEvent) event;
			handleMouseMove(e);
		}
	}
	
	private void handleMouseMove(GameBoardMouseMoveEvent e)
	{
		Point2D p = e.getPosition();
		double x = p.getX();
		double y = p.getY();
		String s = String.format("x=%.0f mm ; y=%.0f mm", x, y);
		if(statusBarLabel != null)
			statusBarLabel.setText(s);
	}
	
//	private void addStatusBar(Container c)
//	{
//		statusBar = new JPanel();
//		statusBar.setBorder(new BevelBorder(BevelBorder.LOWERED));
//		statusBar.setPreferredSize(new Dimension(0, 16));
//		statusBar.setLayout(new BoxLayout(statusBar, BoxLayout.X_AXIS));
//		c.add(statusBar, BorderLayout.SOUTH);
//		
//		statusBarLabel = new JLabel();
//		statusBarLabel.setHorizontalAlignment(SwingConstants.LEFT);
//		statusBar.add(statusBarLabel);
//	}
	
	public void updateGameBoard(final boolean trajectoriesNeedRecalculation)
	{
		if(repaintPending)
		{
			return;
		}
		repaintPending = true;
		
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				StringBuilder sb = new StringBuilder();
				RobotInfoFormatter rif = new RobotInfoFormatter();
				for(RobotRole role:RobotRole.values())
				{
					IPlanification planification = Main.app().getPlanificationHolder()
							.getValidPlanification(role);
					if(null != planification)
					{
						ITrajectoryPath trajpath = null;
						if((trajpath = currentTrajectories.get(role)) == null
								|| trajectoriesNeedRecalculation)
						{
							trajpath = planification.getTrajectory().getTrajectoryPath();
							
							ITrajectoryPath oldpath = currentTrajectories.put(role, trajpath);
							if(null != oldpath)
							{
								gameBoard.getElements().remove(oldpath);
							}
							
							trajpath.setGauge(planification.getGauge());
							gameBoard.getElements().add((IGameBoardElement) trajpath);
						}
						
						sb.append(rif.format(trajpath, planification.getGauge(),
								getTimeSliderPanel().getTimestamp()));
						sb.append("\n\n");
					}
				}
				
				gameBoardView.setTimestamp(getTimeSliderPanel().getTimestamp());
//				statusTextArea.setText(sb.toString());
				gameBoardView.repaint();
				repaintPending = false;
			}
		});
	}
	
}
