package org.cen.views.frames;

import org.cen.components.gameboard.generic.StraightLine;
import org.cen.components.gauge.IGauge;
import org.cen.components.trajectories.ITrajectoryPath;
import org.cen.components.trajectories.KeyFrame;

public class RobotInfoFormatter
{
	
	public String format(ITrajectoryPath trajectory, IGauge gauge, Double timestamp)
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append("Path Name : ").append(trajectory).append("\n");
		sb.append("Gauge Name : ").append(gauge).append("\n");
		
		StraightLine sl = (StraightLine) trajectory;
		sb.append("Frame actuelle : \n");
		KeyFrame frame = sl.getKeyFrame(timestamp);
		sb.append("\tPosition : ").append(frame.getPosition().toMillimeters()).append("\n");
		sb.append("\tAngle : ").append(frame.getOrientation().toDegrees()).append("\n");
		sb.append("Cible : \n");
		KeyFrame target = sl.getNextKeyFrame(timestamp);
		sb.append("\tPosition : ").append(target.getPosition().toMillimeters()).append("\n");
		sb.append("\tAngle : ").append(target.getOrientation().toDegrees()).append("\n");
		sb.append("\tDistance : ").append(frame.getPosition().distance(target.getPosition()).toMillimeters()).append("\n");
		sb.append("\tDistance angulaire : ").append(frame.getOrientation().getRotationAngle(target.getOrientation()).toDegrees()).append("\n");
		
		return sb.toString();
	}

}
