package org.cen;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.cen.components.gameboard.cup2020.GameBoard2020;
import org.cen.components.gameboard.generic.AbstractGameBoard;
import org.cen.components.gameboard.generic.IGameBoardService;
import org.cen.components.gauge.GaugeParser;
import org.cen.components.gauge.IGauge;
import org.cen.components.gauge.IGaugeParser;
import org.cen.components.io.XYParser;
import org.cen.components.io.file.IInputFileWatcher;
import org.cen.components.io.file.InputFileWatcher;
import org.cen.components.io.filepath.FilePathManager;
import org.cen.components.io.filepath.IFilePathManager;
import org.cen.components.io.trajectory.ITrajectoryParser;
import org.cen.components.order.IOrderList;
import org.cen.components.planification.IPlanificationHolder;
import org.cen.components.planification.PlanificationHolder;
import org.cen.components.planification.RobotRole;
import org.cen.persistance.user.IMemorySetting;
import org.cen.persistance.user.MemoryKey;
import org.cen.persistance.user.MemorySetting;
import org.cen.views.frames.ConsoleFrame;
import org.cen.views.frames.SimulatorMainFrame;

public class CenApplication
{
	
	private SimulatorMainFrame simulatorMainFrame = null;
	private ConsoleFrame consoleFrame = null;
	
	private IPlanificationHolder planificationHolder = null;
	private IMemorySetting memorySetting = null;
	private IFilePathManager filepathManager = null;
	private IInputFileWatcher inputFileWatcher = null;
	
	public void init()
	{
		try
		{
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				// step 1 : load preferences
				try
				{
					Main.app().getMemorySettings().load();
				}
				catch (IOException e)
				{
					// ignore, nothing to do
					e.printStackTrace();
					return;
				}
				
				// step 2 : load gauges
				MemoryKey key = MemoryKey.GRAND_ROBOT_GAUGE_FILEPATH;
				RobotRole role = RobotRole.GRAND_ROBOT;
				
				loadGaugeFromMemory(key, role, false);
				
				key = MemoryKey.PETIT_ROBOT_GAUGE_FILEPATH;
				role = RobotRole.PETIT_ROBOT;
				
				loadGaugeFromMemory(key, role, false);
				
				// step 3 : load trajectories
				
				key = MemoryKey.GRAND_ROBOT_TRAJECTORY_FILEPATH;
				role = RobotRole.GRAND_ROBOT;
				
				loadTrajectoryFromMemory(key, role, false);
				
				key = MemoryKey.PETIT_ROBOT_TRAJECTORY_FILEPATH;
				role = RobotRole.PETIT_ROBOT;
				
				loadTrajectoryFromMemory(key, role, false);
			}
			
			public void loadGaugeFromMemory(MemoryKey key, RobotRole role, Boolean warnOnFail)
			{
				String filepath = Main.app().getMemorySettings().getMemorySetting(key);
				if(null == filepath) return;
				File file = new File(filepath);
				if(!file.exists()) return;
				
				Main.app().loadGaugeFromFile(file, role, warnOnFail);
			}
			
			public void loadTrajectoryFromMemory(MemoryKey key, RobotRole role, Boolean warnOnFail)
			{
				String filepath = Main.app().getMemorySettings().getMemorySetting(key);
				if(null == filepath) return;
				File file = new File(filepath);
				if(!file.exists()) return;
				
				Main.app().loadTrajectoryFromFile(file, role, warnOnFail);
			}
			
		});
	}
	
	public void loadGaugeFromFile(File file, RobotRole role, Boolean warnOnFail)
	{
		try(
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
		) {
			IGaugeParser gp = new GaugeParser();
			IGauge gauge = gp.parseStream(br);
			getSimulatorMainFrame().getSelectionPanel().getGaugeButton(role).setText(file.getName());
			getPlanificationHolder().setGauge(role, gauge);
			getInputFileWatcher().watchGauge(role, file);
			SwingUtilities.invokeLater(new Runnable()
			{
				@Override
				public void run()
				{
					Main.app().getSimulatorMainFrame().updateGameBoard(true);
				}
			});
			switch(role)
			{
				case GRAND_ROBOT:
					getMemorySettings().setSetting(MemoryKey.GRAND_ROBOT_GAUGE_FILEPATH, file.getAbsolutePath());
					break;
				case PETIT_ROBOT:
					getMemorySettings().setSetting(MemoryKey.PETIT_ROBOT_GAUGE_FILEPATH, file.getAbsolutePath());
					break;
				default:
					break;
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			if(warnOnFail)
			{
				JOptionPane.showMessageDialog(null, e, "Une erreur de lecture a eu lieu", JOptionPane.ERROR_MESSAGE);
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
			if(warnOnFail)
			{
				JOptionPane.showMessageDialog(null, e, "Une erreur de lecture a eu lieu", JOptionPane.ERROR_MESSAGE);
			}
		}
		catch (ParseException e)
		{
			e.printStackTrace();
			if(warnOnFail)
			{
				JOptionPane.showMessageDialog(null, e, "Le fichier de jauge n'est pas bien formatté", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	public void loadTrajectoryFromFile(File file, RobotRole role, Boolean warnOnFail)
	{
		try(
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
		) {
			ITrajectoryParser parser = new XYParser(";");
//			ITrajectoryParser parser = new TrajectoryParser();
			IOrderList ol = parser.parseStream(br);
			Main.app().getSimulatorMainFrame().getSelectionPanel().getTrajectoryButton(role).setText(file.getName());
			Main.app().getPlanificationHolder().setTrajectory(role, ol);
			getInputFileWatcher().watchTrajectory(role, file);
			SwingUtilities.invokeLater(new Runnable()
			{
				@Override
				public void run()
				{
					Main.app().getSimulatorMainFrame().updateGameBoard(true);
				}
			});
			switch(role)
			{
				case GRAND_ROBOT:
					getMemorySettings().setSetting(MemoryKey.GRAND_ROBOT_TRAJECTORY_FILEPATH, file.getAbsolutePath());
					break;
				case PETIT_ROBOT:
					getMemorySettings().setSetting(MemoryKey.PETIT_ROBOT_TRAJECTORY_FILEPATH, file.getAbsolutePath());
					break;
				default:
					break;
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			if(warnOnFail)
			{
				JOptionPane.showMessageDialog(null, e, "Une erreur de lecture a eu lieu", JOptionPane.ERROR_MESSAGE);
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
			if(warnOnFail)
			{
				JOptionPane.showMessageDialog(null, e, "Une erreur de lecture a eu lieu", JOptionPane.ERROR_MESSAGE);
			}
		}
		catch (ParseException e)
		{
			e.printStackTrace();
			if(warnOnFail)
			{
				JOptionPane.showMessageDialog(null, e, "Le fichier de trajectoire n'est pas bien formatté", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	public IGameBoardService getDefaultGameBoardConfiguration()
	{
		String gameboardclass = getMemorySettings().getMemorySetting(MemoryKey.DEFAULT_GAMEBOARD_CONFIGURATION);
		if(null != gameboardclass)
		{
			try
			{
				Class<?> clazz = Class.forName(gameboardclass);
				Object instance = clazz.newInstance();
				AbstractGameBoard agb = (AbstractGameBoard) instance;
				return agb;
			}
			catch(Exception e)
			{
				// doesnt matter, comfort feature
				e.printStackTrace();
			}
		}
		return new GameBoard2020();
	}
	
	public void setDefaultGameBoardConfiguration(IGameBoardService gameBoard)
	{
		getMemorySettings().setSetting(MemoryKey.DEFAULT_GAMEBOARD_CONFIGURATION, gameBoard.getClass().getCanonicalName());
	}
	
	public synchronized SimulatorMainFrame getSimulatorMainFrame()
	{
		if(null == simulatorMainFrame)
			simulatorMainFrame = new SimulatorMainFrame();
		return simulatorMainFrame;
	}
	
	public synchronized ConsoleFrame getConsoleFrame()
	{
		if(null == consoleFrame)
			consoleFrame = new ConsoleFrame();
		return consoleFrame;
	}
	
	public synchronized IPlanificationHolder getPlanificationHolder()
	{
		if(null == planificationHolder)
			planificationHolder = new PlanificationHolder();
		return planificationHolder;
	}
	
	public synchronized IMemorySetting getMemorySettings()
	{
		if(null == memorySetting)
			memorySetting = new MemorySetting();
		return memorySetting;
	}
	
	public synchronized IFilePathManager getFilePathManager()
	{
		if(null == filepathManager)
			filepathManager = new FilePathManager();
		return filepathManager;
	}
	
	public synchronized IInputFileWatcher getInputFileWatcher()
	{
		if(null == inputFileWatcher)
			inputFileWatcher = new InputFileWatcher();
		return inputFileWatcher;
	}
	
}
