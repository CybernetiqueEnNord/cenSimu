// Chaque ligne représente un mouvement
// Paramètres :
//   - t : type de trajectoire
//   - x;y : coordonnées x;y du point de destination en mm
//   - o : angle de l'orientation en degrés
//   - vl : vitesse linéaire en mm/s
//   - va : vitesse angulaire en tour/s
//   - cx1;cy1, ..., cxn;cyn: coordonnées x;y du n-ième point de contrôle en mm
//   - ta : temps additionnel en fin de mouvement en s (correspond à la durée d'une action, d'un temps d'attente, etc.) 
// Unités : distances en mm, temps en s, angles en degrés
// Axes : x = largeur, y = longueur, repère direct (angle 0° correspond au sens des x croissants, 90° au sens des y croissants)
// Types de trajectoires :
//   - t : transformation affine : sx;tx;sy;ty;sa;ta
//   - i : positionnement et angle initiaux, vitesses par défaut : t;x;y;o;vl;va;ta
//   - s : segment : t;x;y;vl;va;ta
//   - b : courbe de Bézier quadratique : t;x;y;cx1;cy1;cx2;cy2;vl;va;ta
//   - d : distance droite : distance
//   - r : rotation sur place : angle
//   - o : rotation orientée sur place : orientation finale;sens (+/- 1)

// plan 1 : démarrage avec gabarit : au bout de la zone
//i;750;170;90;300;0,25;0

// plan 2 : démarrage sans gabarit appuyé contre la bordure
i;750;113;0;300;0,25;0
d;1000
d;-1000

